<?php

use App\Http\Controllers\FrontEnd\BoothController;
use App\Http\Controllers\FrontEnd\ClassroomController;
use App\Http\Controllers\FrontEnd\InvoiceController;
use App\Http\Controllers\FrontEnd\LandingController;
use App\Http\Controllers\FrontEnd\LobbyController;
use App\Http\Controllers\FrontEnd\MainStageController;
use App\Http\Controllers\FrontEnd\NotificationController;
use App\Http\Controllers\FrontEnd\ProfileController;
use App\Http\Controllers\FrontEnd\ScheduleController;
use App\Http\Middleware\CheckSession;
use App\Http\Middleware\CheckUserStatus;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => [CheckSession::class, CheckUserStatus::class],
], function () {
    Route::get('/', [LandingController::class, 'index'])->withoutMiddleware([CheckSession::class, CheckUserStatus::class]);

    Route::post('/profile', [ProfileController::class, 'update']);

    Route::get('/notifications', [NotificationController::class, 'index']);
    Route::post('/notifications', [NotificationController::class, 'update']);

    Route::get('/home', [LobbyController::class, 'home']);

    Route::get('/download-qr/{qr}', [LobbyController::class, 'downloadQr']);

    Route::get('/mainstage', [MainStageController::class, 'index']);

    Route::get('/classroom', [ClassroomController::class, 'index']);

    Route::get('/schedule-list', [ScheduleController::class, 'index']);
    Route::post('/schedule-find', [ScheduleController::class, 'show']);
    Route::post('/schedule-send-message', [ScheduleController::class, 'sendMessage']);

    Route::get('/booth', [BoothController::class, 'index']);
    Route::get('/booth-list', [BoothController::class, 'boothList']);
    Route::post('/booth-find', [BoothController::class, 'boothFind']);
    Route::get('/booth/detail', [BoothController::class, 'show']);

    Route::post('/count-visitor', function () {
        return 0;
    });

    //Invoice
    Route::get('/invoice', [InvoiceController::class, 'index']);
});

Route::get('/scannerapp', function () {
    return redirect(config('eventy.scanner_link'));
});


Route::get('/email', function () {
    $participant = \App\Models\Participant::find(64);

    $mail['event_logo'] = preferGet('logo_events');
    $mail['name'] = $participant->name;
    $mail['email'] = $participant->email;

//    $participant->sendEmailRegistration($mail);

    return view('web.Email.reminder', $mail);
});
