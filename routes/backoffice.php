<?php

use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\middlewares\CBBackend;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => [
        CBBackend::class
    ],
    'namespace' => 'App\Http\Controllers',
], function () {
    CRUDBooster::routeController('backoffice', 'Backoffice\BackofficeController');
});
