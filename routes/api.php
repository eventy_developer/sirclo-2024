<?php

use App\Http\Controllers\API\ApiHomeController;
use App\Http\Controllers\API\ApiLoginController;
use App\Http\Controllers\API\ApiScanController;
use App\Http\Controllers\API\ApiScanHistoryController;
use App\Http\Controllers\API\ApiScanTypeController;
use App\Http\Controllers\Payment\EventyPaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Eventy Xendit Callback
Route::group([
    'middleware' => [],
    'prefix' => 'payment',
], function () {
    Route::post('save', [EventyPaymentController::class, 'postSave']);
});

// API QR
Route::group([
    'prefix' => 'v1'
], function () {
    Route::post('/admin/login', ApiLoginController::class);

    Route::middleware(['auth:api'])->group(function () {
        Route::post('/scan', [ApiScanController::class, 'store']);
        Route::get('/scan-types', [ApiScanTypeController::class, 'index']);
        Route::get('/scan-histories', [ApiScanHistoryController::class, 'index']);
        Route::get('/homes', ApiHomeController::class);
    });
});
