<?php

use App\Http\Controllers\Registration\TicketController;
use App\Http\Controllers\Registration\TransactionConfirmationController;
use App\Http\Controllers\Registration\TransactionController;
use App\Http\Middleware\CheckSession;
use App\Http\Middleware\CheckUserStatus;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => [CheckSession::class, CheckUserStatus::class],
], function () {
    Route::get('/ticket', [TicketController::class, 'index']);

    Route::get('/classroom-addition', [TicketController::class, 'classroomAddition']);
    Route::post('/find-email', [TicketController::class, 'findEmailExist']);

    Route::post('/transactions', [TransactionController::class, 'store']);
    Route::post('/ticket-free', [TransactionController::class, 'ticketFree']);

    Route::post('/change-payment-method/{id}', [TransactionController::class, 'update']);
    Route::get('/finish', [TransactionController::class, 'show']);

    Route::get('/confirmation', [TransactionConfirmationController::class, 'index']);
    Route::post('/confirmation', [TransactionConfirmationController::class, 'store']);
    Route::post('/find-invoice', [TransactionConfirmationController::class, 'show']);
});
