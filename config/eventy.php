<?php

return [
    'event_id' => env('EVENT_ID', 16), // for xendit
    'event_name' => env('EVENT_NAME', 'virtualeventv3'), // for socket channel name, S3 bucket folder
    'version' => 'v2.0',
    'event_channel' => env('EVENT_CHANNEL', 'virtualeventv3'),
    'zoom' => [
        'debug' => env('ZOOM_DEBUG', 'YES'),
        'api_key' => env('ZOOM_API_KEY', 'xiVv2ZbKScaRXbW4dibC-Q'),
        'api_secret' => env('ZOOM_API_SECRET', '2VlV8Y62Yu2Edv8wzKMGaLlvE2GGXgGm4H8I'),
    ],
    'payment_debug' => env('PAYMENT_DEBUG', 'true'),
    'file_types' => 'pdf,docx,odt,xls,xlsx,pptx',
    'image_types' => 'jpg,jpeg,png',
    'max_attachment' => 10,

    // S3 Custom Endpoint for getting image
    'eventy_s3_url' => env('EVENTY_S3_URL', 'https://d1bzsjp5fzkb7z.cloudfront.net'),

    '3d_version' => env('EVENTY_3D_VERSION', 'v1'),

    'scanner_link' => env('SCANNER_LINK', 'https://registersircloexpo.com/scanner/ScannerEventy-v.0.0.2.apk'),

    'top_grandprize' => (int)env('TOP_GRANDPRIZE', 10),
    'top_general' => (int)env('TOP_GENERAL', 5),
];
