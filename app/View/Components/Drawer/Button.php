<?php

namespace App\View\Components\Drawer;

use Illuminate\View\Component;

class Button extends Component
{
    public $drawerButtons;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($drawerButtons)
    {
        $this->drawerButtons = $drawerButtons;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.drawer.button');
    }
}
