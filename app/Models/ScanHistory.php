<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ScanHistory extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function participant() {
        return $this->belongsTo(Participant::class);
    }

    public function scanType()
    {
        return $this->belongsTo(ScanType::class);
    }

    public function allWithPagination()
    {
        $date = request('date');
        $scan_type_id = request('scan_type_id');

        // prepare pagination
        $page = request('page') ?: 1;
        $take = 50;
        $skip = ($page * $take) - $take;

        return ScanHistory::select('id', 'created_at', 'date', 'participant_id')
            ->with(['participant' => function ($participant) {
                $participant->select('id', 'name', 'type_code', 'registration_number');
            }])
            ->when($scan_type_id, function ($q) use ($scan_type_id) {
                return $q->whereScanTypeId($scan_type_id);
            })
            ->when($date, function ($q) use ($date) {
                return $q->where('date', $date);
            })
            ->take($take)
            ->skip($skip)
            ->latest('created_at')
            ->get();
    }
}
