<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booth extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
        'present_image',
        'present_icon',
    ];

    public function contacts(): HasMany
    {
        return $this->hasMany(BoothContact::class);
    }

    public function productCategories(): HasMany
    {
        return $this->hasMany(BoothProductCategory::class);
    }

    public function boothProducts(): HasManyThrough
    {
        return $this->hasManyThrough(BoothProduct::class, BoothProductCategory::class);
    }

    public function attachments(): HasMany
    {
        return $this->hasMany(BoothAttachment::class);
    }

    public function boothAdmin(): HasOne
    {
        return $this->hasOne(BoothAdmin::class);
    }

    public function getPresentImageAttribute()
    {
        return $this->image ? getS3EndpointFile($this->image) : getS3EndpointFile('virtual-event/virtualeventv3/example/placeholder/ph_booth@2x.png');
    }

    public function getPresentIconAttribute()
    {
        return $this->icon ? getS3EndpointFile($this->icon) : asset('images/example/logo.png');
    }
}
