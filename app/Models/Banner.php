<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function getPresentImageAttribute()
    {
        return $this->type === 'Image' ? getS3EndpointFile($this->image) : $this->image;
    }
}
