<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScanGroup extends Model
{
    use HasFactory;

    public function scanTypes()
    {
        return $this->hasMany(ScanType::class);
    }

    public function cmsUsers()
    {
        return $this->hasMany(CmsUser::class);
    }
}
