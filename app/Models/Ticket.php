<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;


class Ticket extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function ticketSchedules(): HasMany
    {
        return $this->hasMany(TicketSchedule::class);
    }

    public function couponTicket(): HasMany
    {
        return $this->hasMany(CouponTicket::class);
    }

    public function scopeCouponFilter(Builder $query, $coupon = null): Builder
    {
        if ($coupon) {
            $query->whereHas('couponTicket', function ($q) use ($coupon) {
                $q->where('code', $coupon);
            });
        }

        return $query;
    }

    public static function countTicketSales(): int
    {
        return self::query()
            ->join('transaction_ticket', 'transaction_ticket.ticket_id', '=', 'tickets.id')
            ->join('transactions', 'transactions.id', '=', 'transaction_ticket.transaction_id')
            ->join('participants', 'participants.id', '=', 'transactions.participant_id')
            ->where('transactions.status', Transaction::STATUS_SUCCESS)
            ->where('participants.is_activated_manually', 0)
            ->whereNull('transactions.deleted_at')
            ->whereNull('tickets.deleted_at')
            ->whereNull('participants.deleted_at')
            ->count();
    }

    public static function getDataWithClassroom($pageType = 'newTicket', $ticket_id = '', $coupon = '')
    {
        if ($pageType === 'newTicket') {
            $now = date('Y-m-d'); // Outputs the current date in 'YYYY-MM-DD' format
            $tickets = Ticket::with(['ticketSchedules.schedule.sessions'])
                ->where('type', 'Paid')
                ->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now)
                ->get();
        } else if ($pageType === 'newTicketWithCoupon') {
            if ($ticket_id){
                $tickets = Ticket::with(['ticketSchedules.schedule.sessions', 'couponTicket'])
                    ->where('type', 'Free')
                    ->where('id', $ticket_id)
                    ->get();
            }else{
                $tickets = Ticket::with(['ticketSchedules.schedule.sessions', 'couponTicket'])
                    ->where('type', 'Free')
                    ->whereHas('couponTicket', function ($q) use ($coupon) {
                        $q->whereIn('code', [$coupon]);
                    })
                    ->first();
            }

        } else {
//            $except_participant_schedule_ids = ParticipantSchedule::select('schedule_id')
//                ->whereParticipantId(gSession('id'))
//                ->pluck('schedule_id')
//                ->toArray();
//
//            $tickets = Ticket::with(['ticketSchedules' => function ($ticketSchedule) use ($except_participant_schedule_ids) {
//                    if (count($except_participant_schedule_ids) > 0) {
//                        $ticketSchedule->whereNotIn('schedule_id', $except_participant_schedule_ids);
//                    }
//                    $ticketSchedule->with(['schedule.sessions']);
//                }])
//                ->whereId($ticket_id)
//                ->get();
            $now = date('Y-m-d'); // Outputs the current date in 'YYYY-MM-DD' format
            $tickets = Ticket::with(['ticketSchedules.schedule.sessions'])
                ->where('type', 'Paid')
                ->where('start_date', '<=', $now)
                ->where('end_date', '>=', $now)
                ->get();
        }



//        foreach ($tickets as $ticket) {
//            if ($pageType === 'additionalTicket') {
//                $ticket->price = 0;
//            }
//        }

        return $tickets;
    }
}
