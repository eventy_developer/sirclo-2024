<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function location(): BelongsTo
    {
        return $this->belongsTo(ScheduleLocation::class, 'schedule_location_id');
    }

    public function sessions(): HasMany
    {
        return $this->hasMany(ScheduleSession::class);
    }

    public function participantSchedules(): HasMany
    {
        return $this->hasMany(ParticipantSchedule::class);
    }
}
