<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doorprize extends Model
{
    use HasFactory, SoftDeletes;

    public function getParticipantsAttribute()
    {
        if ($this->doorprize_type === 'Doorprize 1') {
            return $this->hasMany(Participant::class, 'doorprize_id', 'id')->latest('is_pickup_doorprize')->get();
        }

        if ($this->doorprize_type === 'Doorprize 2') {
            return $this->hasMany(Participant::class, 'doorprize2_id', 'id')->latest('is_pickup_doorprize2')->get();
        }

        return $this->hasMany(Participant::class, 'doorprize3_id', 'id')->latest('is_pickup_doorprize3')->get();
    }

    public function getParticipantsLeftAttribute()
    {
        if ($this->doorprize_type === 'Doorprize 1') {
            return $this->hasMany(Participant::class, 'doorprize_id', 'id')->where('is_pickup_doorprize', 0)->get();
        }

        if ($this->doorprize_type === 'Doorprize 2') {
            return $this->hasMany(Participant::class, 'doorprize2_id', 'id')->where('is_pickup_doorprize2', 0)->get();
        }

        return $this->hasMany(Participant::class, 'doorprize3_id', 'id')->where('is_pickup_doorprize3', 0)->get();
    }

    public function getPresentImageAttribute()
    {
        if ($this->image) {
            return getS3EndpointFile($this->image);
        }

        return '';
    }

    public function getPresentNameAttribute()
    {
        $day = date('d', strtotime($this->available_at));
        $name = 'Day Test: ';
        if ($day == '24') {
            $name = 'Day 1: ';
        } else if ($day == '25') {
            $name = 'Day 2: ';
        }

        return $name . $this->name;
    }

    public function getPresentCountdownAttribute()
    {
        return strtotime(date('Y-m-d H:i:s', strtotime("+{$this->minutes} minutes", strtotime($this->draw_at)))) - time();
    }

    public function getIsPickupDoorprizeAttribute()
    {
        if ($this->doorprize_type === 'Doorprize 1') {
            return $this->hasMany(Participant::class, 'doorprize_id', 'id')->where('is_pickup_doorprize', 1)->count();
        }

        if ($this->doorprize_type === 'Doorprize 2') {
            return $this->hasMany(Participant::class, 'doorprize2_id', 'id')->where('is_pickup_doorprize2', 1)->count();
        }

        return $this->hasMany(Participant::class, 'doorprize3_id', 'id')->where('is_pickup_doorprize3', 1)->count();
    }

    public function getIsLeftAttribute()
    {
        return $this->quantity - $this->is_pickup_doorprize;
    }
}
