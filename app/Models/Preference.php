<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    use HasFactory;

    public function getPresentContentAttribute()
    {
        if ($this->type === 'Image' && $this->content) {
            return getS3EndpointFile($this->content);
        }

        return $this->content;
    }
}
