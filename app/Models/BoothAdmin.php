<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BoothAdmin extends Model
{
    use HasFactory;

    protected $table = 'booth_admin';

    public function cmsUser(): BelongsTo
    {
        return $this->belongsTo(CmsUser::class)->withDefault();
    }

    public function booth(): BelongsTo
    {
        return $this->belongsTo(Booth::class)->withDefault();
    }
}
