<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleSession extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
        'present_date_text',
        'abbreviation_timezone',
        'present_speaker',
        'present_image',
        'present_time_start',
        'present_time_end',
    ];

    public function schedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class);
    }

    public function attachments(): HasMany
    {
        return $this->hasMany(ScheduleAttachment::class);
    }

    public function speakers(): BelongsToMany
    {
        return $this->belongsToMany(Speaker::class);
    }

    public function scheduleSessionSpeaker()
    {
        return $this->hasMany(ScheduleSessionSpeaker::class);
    }

    public function getPresentTimeStartAttribute()
    {
        return date('h:i a', strtotime($this->time_start));
    }

    public function getPresentTimeEndAttribute()
    {
        return date('h:i a', strtotime($this->time_end));
    }

    public function getPresentDateTextAttribute()
    {
        return carbon($this->date)->isoFormat('dddd, D MMMM YYYY');
    }

    public function getPresentImageAttribute()
    {
        return $this->image ? getS3EndpointFile($this->image) : '/images/icon/landingpage/icon_schedule.svg';
    }

    public function getAbbreviationTimezoneAttribute(): string
    {
        return abbreviationTimezone();
    }

    public function getPresentSpeakerAttribute()
    {
        return $this->relationLoaded('speakers') ?
            implode(', ', $this->speakers->pluck('name')->toArray()) : null;
    }
}
