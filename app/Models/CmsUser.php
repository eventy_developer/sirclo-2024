<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CmsUser extends Model
{
    use HasFactory;

    public const SUPERADMIN_PRIVILEGE_ID = 1;
    public const ADMIN_PRIVILEGE_ID = 2;
    public const BOOTH_PRIVILEGE_ID = 3;

    public function scanGroup()
    {
        return $this->belongsTo(ScanGroup::class);
    }

    public function getPhotoAttribute($value)
    {
        return $value ? getS3EndpointFile($value) : preferGet('icon_events');
    }

    public function boothAdmin(): HasOne
    {
        return $this->hasOne(BoothAdmin::class)->withDefault();
    }
}
