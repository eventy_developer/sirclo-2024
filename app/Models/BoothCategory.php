<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoothCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
        'present_thumbnail',
        'present_icon',
    ];

    public function booths(): HasMany
    {
        return $this->hasMany(Booth::class);
    }

    public function getPresentThumbnailAttribute()
    {
        return $this->image ? getS3EndpointFile($this->image) : getS3EndpointFile('virtual-event/virtualeventv3/example/placeholder/ph_categorybooth.png');
    }

    public function getPresentIconAttribute()
    {
        return $this->icon ? getS3EndpointFile($this->icon) : asset('images/example/icon.jpg');
    }
}
