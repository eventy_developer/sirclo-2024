<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoothProduct extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = ['present_price'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(BoothProductCategory::class, 'booth_product_category_id')->withDefault();
    }

    public function getPresentPriceAttribute(): string
    {
        return 'Rp ' . number_format($this->price, 0, '.', '.');
    }
}
