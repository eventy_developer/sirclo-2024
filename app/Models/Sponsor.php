<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
        'present_image',
    ];

    public function getPresentImageAttribute()
    {
        return getS3EndpointFile($this->image);
    }
}
