<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionTicket extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'transaction_ticket';
    protected $guarded = [];

    public function transactions(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    public function ticket(): BelongsTo
    {
        return $this->belongsTo(Ticket::class)->withDefault();
    }
}
