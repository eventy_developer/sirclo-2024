<?php

namespace App\Models;

use App\Helpers\Eventy;
use App\Services\TransactionService;
use App\Traits\Participant\CheckStatus;
use App\Traits\Participant\SendEmail;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Participant extends Model implements AuthenticatableContract
{
    use HasFactory, SoftDeletes, Authenticatable, CheckStatus, SendEmail;

    public const STATUS_WAITING_TICKET = 'Waiting Ticket';
    public const STATUS_WAITING_PAYMENT = 'Waiting Payment';
    public const STATUS_WAITING_CONFIRMATION = 'Waiting Confirmation';
    public const STATUS_ACTIVE = 'Active';
    public const STATUS_PENDING = 'Pending';
    public const STATUS_PAYMENT_CHANGED = 'Payment Changed';
    public const STATUS_BLOCKED = 'Blocked';

    protected $guarded = [];
    protected $attributes = [
        'status' => self::STATUS_WAITING_TICKET,
        'is_invitation' => 0,
        'is_admin' => 0,
        'is_feedback_filled' => 0,
        'is_activated_manually' => 0,
        'total_point' => 0,
        'is_pickup_doorprize' => 0,
        'is_pickup_doorprize2' => 0,
        'is_pickup_doorprize3' => 0,
        'exclude_doorprize' => 0,
        'exclude_doorprize2' => 0,
        'exclude_doorprize3' => 0,
        'is_skip_doorprize' => 0,
    ];
    protected $appends = [
        'full_registration_number',
    ];

    public function ticket(): BelongsTo
    {
        return $this->belongsTo(Ticket::class)->withDefault();
    }

    public function schedules(): HasMany
    {
        return $this->hasMany(ParticipantSchedule::class);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    public function history(): HasOne
    {
        return $this->hasOne(ScanHistory::class);
    }

    public function histories()
    {
        return $this->hasMany(ScanHistory::class)->latest('created_at');
    }

    public function doorprize(): BelongsTo
    {
        return $this->belongsTo(Doorprize::class);
    }

    public function getFullRegistrationNumberAttribute()
    {
        return 'SRL-' . $this->type_code . '-' . $this->registration_number;
    }

    public function getPresentImageAttribute()
    {
        return $this->image ? getS3EndpointFile($this->image) : preferGet('icon_events');
    }

    public function getPresentQrCodeAttribute()
    {
        return Eventy::QRCode($this->qr_code);
    }

    public function getSendWaitingAtAttribute()
    {
        if ($this->latest_email_reminder_at) {
            return date('Y-m-d H:i:s', strtotime("+1 day", strtotime($this->latest_email_reminder_at)));
        }

        return date('Y-m-d H:i:s', strtotime($this->created_at));
    }

    public static function deleteRelationById($ids)
    {
        $participants = Participant::where(function ($q) use ($ids) {
            if (is_array($ids)) {
                $q->whereIn('id', $ids);
            } else {
                $q->where('id', $ids);
            }
        })
            ->get();

        foreach ($participants as $participant) {

            // delete all participant schedule (classroom)
            ParticipantSchedule::where('participant_id', $participant->id)->delete();

            // delete history login
            ParticipantActivity::where('participant_id', $participant->id)
                ->where('action', 'Login')
                ->delete();

            // delete history payment
            TransactionService::deleteRelationByParticipantId($participant->id);
        }
    }

    public static function getRangeDataByDates($dates, $type_code): array
    {
        return static::query()
            ->select(
                DB::raw('COUNT(DATE(created_at)) as total'),
                DB::raw('DATE(created_at) as date')
            )
            ->whereIn(DB::raw('DATE(created_at)'), $dates)
            ->whereNull('deleted_at')
            ->whereIn('type_code', $type_code)
            ->where('status', self::STATUS_ACTIVE)
            ->groupBy(['date'])
            ->get()->toArray();
    }

    public static function getChartByDateRange($dates = [], $start_date = null, $end_date = null, $type_code = null)
    {
        $start_date = $start_date ?: carbon()->now()->firstOfMonth()->toDateString();
        $end_date = $end_date ?: date('Y-m-d');

        $dates = Eventy::getArrayIntervalDate($start_date, $end_date);
        $intervalDates = [];
        foreach ($dates as $date) {
            $intervalDates[$date] = (object)[
                'total' => 0,
                'date' => $date
            ];
        }

        $dataGroup = [];
        $data = self::getRangeDataByDates($dates, $type_code);
        foreach ($data as $x) {
            $dataGroup[$x['date']] = $x;
        }

        foreach ($intervalDates as $k => $date) {
            if (array_key_exists($k, $dataGroup)) {
                $intervalDates[$k] = (object)$dataGroup[$k];
            }
        }

        return array_values($intervalDates);
    }

    public function nametag()
    {
        return $this->hasOne(PrintNametagHistory::class)->latest('created_at');
    }
}
