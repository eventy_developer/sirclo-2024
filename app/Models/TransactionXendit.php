<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionXendit extends Model
{
    use HasFactory, SoftDeletes;

    public static function savePayment($transaction_id, $external_id, $status)
    {
        $transactionXendit = new TransactionXendit();
        $transactionXendit->transaction_id = $transaction_id;
        $transactionXendit->platform_code = $external_id;
        $transactionXendit->status = $status;
        $transactionXendit->save();

        return $transactionXendit->id;
    }
}
