<?php

namespace App\Models;

use App\Traits\Payment\CheckStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    use HasFactory, SoftDeletes, CheckStatus;

    public const STATUS_WAITING_PAYMENT = 'WAITING_PAYMENT';
    public const STATUS_WAITING_CONFIRMATION = 'WAITING_CONFIRMATION';
    public const STATUS_PAYMENT_CHANGED = 'PAYMENT_CHANGED';
    public const STATUS_PENDING = 'PENDING';
    public const STATUS_SUCCESS = 'SUCCESS';
    public const STATUS_FAILED = 'FAILED';
    public const STATUS_EXPIRED = 'EXPIRED';

    protected $guarded = [];
    protected $attributes = [
        'status' => self::STATUS_WAITING_PAYMENT,
    ];

    public function getTotalFeeAttribute()
    {
        return $this->vat_fee + $this->var_fee + $this->fixed_fee;
    }

    public function participant(): BelongsTo
    {
        return $this->belongsTo(Participant::class);
    }

    public function transactionTicket(): HasOne
    {
        return $this->hasOne(TransactionTicket::class);
    }

    public function transactionDetails(): HasMany
    {
        return $this->hasMany(TransactionDetail::class);
    }

    public static function sumTotalIncome($type = null)
    {
        return self::query()
            ->join('participants', 'participants.id', '=', 'transactions.participant_id')
            ->when($type, function ($q) use ($type) {
                return $q->where('transactions.type', $type);
            })
            ->where('transactions.status', Transaction::STATUS_SUCCESS)
            ->where('participants.is_activated_manually', 0)
            ->where('participants.email', '!=', 'demo@eventy.id')
            ->whereNull('transactions.deleted_at')
            ->whereNull('participants.deleted_at')
            ->sum('amount');
    }

    public static function sumTotalFee($type = null)
    {
        return self::query()
            ->join('participants', 'participants.id', '=', 'transactions.participant_id')
            ->when($type, function ($q) use ($type) {
                return $q->where('transactions.type', $type);
            })
            ->where('transactions.status', Transaction::STATUS_SUCCESS)
            ->where('participants.is_activated_manually', 0)
            ->whereNull('transactions.deleted_at')
            ->whereNull('participants.deleted_at')
            ->sum(DB::raw('vat_fee + var_fee + fixed_fee'));
    }
}
