<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrintNametagHistory extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function participant() {
        return $this->belongsTo(Participant::class);
    }
}
