<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $appends = [
        'time_elapsed',
    ];

    public function detail()
    {
        return $this->hasOne(NotificationDetail::class);
    }

    public function details()
    {
        return $this->hasMany(NotificationDetail::class);
    }

    public function getTimeElapsedAttribute()
    {
        dateTime();
        return carbon($this->send_at)->diffForHumans();
    }
}
