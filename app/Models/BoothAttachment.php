<?php

namespace App\Models;

use App\Helpers\Eventy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoothAttachment extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function getFileAttribute($value)
    {
        return $value ? getS3EndpointFile($value) : '';
    }

    public function getFileSizeAttribute($value)
    {
        return Eventy::formatSizeUnits($value);
    }
}
