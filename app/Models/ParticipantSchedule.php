<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParticipantSchedule extends Model
{
    use HasFactory, SoftDeletes;

    public function schedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class)->withDefault();
    }

    public function participant(): BelongsTo
    {
        return $this->belongsTo(Participant::class);
    }

    public static function saveRegistration($transaction_id, $participant_id)
    {
        // check if user selected schedule (classroom)
        $transactionDetail = TransactionDetail::with(['ticketSchedule'])->whereTransactionId($transaction_id)->get();

        // create schedule(classroom) access
        if (count($transactionDetail) > 0) {
            foreach ($transactionDetail as $detail) {
                $schedule_id = $detail->ticketSchedule->schedule_id;

                $participantSchedule = ParticipantSchedule::whereParticipantId($participant_id)->whereScheduleId($schedule_id)->firstOrNew();
                $participantSchedule->participant_id = $participant_id;
                $participantSchedule->schedule_id = $schedule_id;
                $participantSchedule->save();
            }
        }
    }
}
