<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleSessionSpeaker extends Model
{
    use HasFactory;

    protected $table = 'schedule_session_speaker';
}
