<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionConfirmation extends Model
{
    use HasFactory, SoftDeletes;

    public const STATUS_APPROVED = 'APPROVED';
    public const STATUS_WAITING = 'WAITING';
    public const STATUS_REJECTED = 'REJECTED';

    protected $guarded = [];

    public static function findByInvoiceExist($invoice_code, $status = null)
    {
        return self::query()
            ->where('invoice_code', $invoice_code)
            ->when($status, function ($q) use ($status) {
                return $q->where('status', $status);
            })
            ->whereNull('deleted_at')
            ->first();
    }
}
