<?php

namespace App\Jobs;

use App\Helpers\Eventy;
use App\Helpers\SendEmail;
use App\Models\EmailHistory;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $participant;
    protected $emailTemplate;
    protected $fieldUsers;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($participant, $emailTemplate, $fieldUsers)
    {
        $this->participant = $participant;
        $this->emailTemplate = $emailTemplate;
        $this->fieldUsers = $fieldUsers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = 'web.Email.blast_email';
        $contentView = $this->emailTemplate->content;

        if ($this->emailTemplate->type === 'Invitation') {
            $template = 'web.Email.invitation';
        }

        if ($this->emailTemplate->type === 'Reminder') {
            $template = 'web.Email.reminder';
        }

        $data = [
            'template' => $contentView,
            'logo' => getS3EndpointFile(preferGet('logo_events')),
            'name' => $this->participant->name,
        ];
        // send email
        $sendEmail = new SendEmail();
        $sendEmail->setFrom('no-reply@eventy.id');
        $sendEmail->setTo($this->participant->email);
        $sendEmail->setSubject($this->emailTemplate->subject);
        $sendEmail->setEvent(preferGet('name_events'));
        $sendEmail->setData($data);
        $sendEmail->setTemplate($template);
        // Handle attachments
        if ($this->emailTemplate->attachment) {
            // Convert attachment path to URL
            $attachment = getS3EndpointFile($this->emailTemplate->attachment);
            $attachments = [$attachment];

            $sendEmail->setAttachments($attachments);
        }
        $send = $sendEmail->sendEmailSMTP();

        $params['name'] = $this->participant->name;
        $params['email'] = $this->participant->email;
        $params['subject'] = $this->emailTemplate->subject;
        $params['response'] = $send;

        if ($send['status'] == 1) {
            $emailHistory = new EmailHistory();
            $emailHistory->name = $this->participant->name;
            $emailHistory->to = $this->participant->email;
            $emailHistory->subject = $this->emailTemplate->subject;
            $emailHistory->content = $this->emailTemplate->content;
            $emailHistory->status = 'Delivered';
            $emailHistory->save();

            Eventy::LogFile('success', 'email', 'success', $params);
        } else {
            Eventy::LogFile($send['message'], 'email', 'error', $params, 'error');

            return $this->fail(new Exception($send['message']));
        }
    }
}
