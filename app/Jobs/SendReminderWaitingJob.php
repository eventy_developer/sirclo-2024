<?php

namespace App\Jobs;

use App\Helpers\Eventy;
use App\Helpers\SendEmail;
use App\Models\EmailHistory;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendReminderWaitingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $participant;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($participant)
    {
        $this->participant = $participant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'event_logo' => preferGet('logo_events'),
            'name' => $this->participant->name,
            'email' => $this->participant->email,
        ];

        // send email
        $sendEmail = new SendEmail();
        $sendEmail->setFrom('no-reply@eventy.id');
        $sendEmail->setTo($this->participant->email);
        $sendEmail->setSubject('Reminder to Continue Registration for E-Commerce Expo 2023: powered by SIRCLO');
        $sendEmail->setEvent(preferGet('name_events'));
        $sendEmail->setData($data);
        $sendEmail->setTemplate('web.Email.waiting-ticket');
        $send = $sendEmail->sendEmailSMTP();

        $params['name'] = $this->participant->name;
        $params['email'] = $this->participant->email;
        $params['response'] = $send;

        if ($send['status'] == 1) {

            Eventy::LogFile('success', 'email-waiting-ticket', 'success', $params);
        } else {
            Eventy::LogFile($send['message'], 'email-waiting-ticket', 'error', $params, 'error');

            return $this->fail(new Exception($send['message']));
        }
    }
}
