<?php

namespace App\Services;

use App\Helpers\Eventy;
use App\Http\Controllers\Auth\LoginController;
use App\Models\Participant;
use App\Models\ParticipantInterest;
use App\Models\Ticket;
use App\Models\TicketSchedule;
use App\Models\Transaction;
use App\Models\TransactionConfirmation;
use App\Models\TransactionDetail;
use App\Models\TransactionTicket;
use App\Models\TransactionXendit;
use App\Traits\EventyPayment;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function PHPUnit\Framework\isEmpty;

class TransactionService
{
    use EventyPayment;

    private $transaction;
    private $transactionTicket;
    private $ticket;
    private $participant;

    public function __construct(
        Transaction       $transaction,
        TransactionTicket $transactionTicket,
        Ticket            $ticket,
        Participant       $participant
    )
    {
        $this->transaction = $transaction;
        $this->transactionTicket = $transactionTicket;
        $this->ticket = $ticket;
        $this->participant = $participant;
    }

    /**
     * @throws Exception
     */
    public function create($request)
    {

        DB::beginTransaction();
        try {
            $ticket_schedule_ids = request('ticket_schedule_ids');
            $participant_ids_group_exist = request('participant_ids_group');

            $this->participant = $this->participant->whereEmail($request['email'])->first();
            $this->ticket = $this->ticket->find($request['ticket_id']);

            $participants_group = request('participants_group');
            $participant_group_ids = [];

            if (empty($participant_ids_group_exist)) {
                if (!empty($participants_group)) {
                    // create new participants if ticket group
                    foreach ($participants_group as $newParticipant) {
                        $phone = $newParticipant['phone_prefix'] .  $newParticipant['phone'];
                        $fullName = $newParticipant['name'];
                        $names = explode(' ', $fullName);
                        if (count($names) > 1) {
                            $lastName = array_pop($names);
                            $firstName = implode(' ', $names);
                        } else {
                            $firstName = $fullName;
                            $lastName = '-';
                        }
                        $participantNew = new Participant();
                        $participantNew->name = $newParticipant['name'];
                        $participantNew->first_name = $firstName;
                        $participantNew->last_name = $lastName;
                        $participantNew->email = $newParticipant['email'];
                        $participantNew->password = Hash::make($newParticipant['password']);
                        $participantNew->company = htmlspecialchars($newParticipant['company']);
                        $participantNew->job_title = htmlspecialchars($newParticipant['job_title']);
                        $participantNew->business_type = $newParticipant['business_type'];
                        $participantNew->nationality = $newParticipant['nationality'];
                        $participantNew->phone = $phone;
                        $participantNew->ticket_id = $this->ticket->id;
                        $participantNew->type_code = $this->ticket->type_index_ticket;
                        $participantNew->type = $this->ticket->label_categories;
                        $participantNew->save();

                        $participant_group_ids[] = $participantNew->id;

                        $participantNew->qr_code = Eventy::hashString($participantNew->id);
                        $participantNew->status = Participant::STATUS_PENDING;
                        $participantNew->save();

                        // save area of interests
                        foreach ($newParticipant['area_of_interests'] as $area) {
                            $checkDetail = ParticipantInterest::whereParticipantId($participantNew->id)
                                ->whereArea($area)
                                ->first();
                            if (!$checkDetail) {
                                $participantInterest = new ParticipantInterest();
                                $participantInterest->participant_id = $participantNew->id;
                                $participantInterest->area = $area;
                                $participantInterest->save();
                            }
                        }
                    }
                }
            }

            $this->transaction->participant_id = $this->participant->id;
            $this->transaction->invoice_code = invoiceCode();
            $this->transaction->type = $request['type'];
            $this->transaction->payment_method = $request['payment_method'];
            $this->transaction->amount = $request['amount'];
            $this->transaction->initial_amount = $request['initial_amount'];
            $this->transaction->fixed_fee = $request['fixed_fee'];
            $this->transaction->var_fee = $request['var_fee'];
            $this->transaction->vat_fee = $request['vat_fee'];
            if (!empty($participant_group_ids)){
                $this->transaction->participant_ids_group = json_encode($participant_group_ids);
            }
            if (!empty($participant_ids_group_exist)){
                $this->transaction->participant_ids_group = json_encode($participant_ids_group_exist);
            }
            $this->transaction->save();

            // Database Table : transactions
            Eventy::LogFile('Database Save : `transactions`', 'logging', 'logging', [
                'id' => $this->transaction->id,
                'invoice_code' => $this->transaction->invoice_code,
                'payment_method' => $this->transaction->payment_method,
                'amount' => $this->transaction->amount,
                'email' => $this->participant->email,
            ]);

            // save details
            if (!empty($participant_group_ids)){
                $mergeWithBuyer = json_encode(array_merge($participant_group_ids, [$this->participant->id]));
                $this->saveDetailGroup(json_decode($mergeWithBuyer));
            } else if (!empty($participant_ids_group_exist)){
                $this->saveDetailGroup($participant_ids_group_exist);
            } else{
                $this->saveDetail($ticket_schedule_ids);
            }

            if (in_array($this->transaction->type, ['Virtual Accounts', 'Cards', 'QR Codes'])) {
                // submit payment to payment platform
                $this->postSendEventyPayment($this->transaction);

                $this->participant->status = Participant::STATUS_PENDING;
            }

            // update participant ticket & status
            if ($this->ticket && !$this->participant->isActiveStatus()) {
                $this->participant->ticket_id = $this->ticket->id;
                $this->participant->status = Participant::STATUS_WAITING_PAYMENT;
                $this->participant->qr_code = Eventy::hashString($this->participant->id);
                $this->participant->type_code = $this->ticket->type_index_ticket;
//                $this->participant->type = $this->ticket->label_categories;
                $this->participant->save();

                if (!empty($participant_group_ids)) {
                    $mergeWithBuyer = json_encode(array_merge($participant_group_ids, [$this->participant->id]));
                    $this->ticket->quota_achieve = $this->ticket->quota_achieve + count(json_decode($mergeWithBuyer));
                    $this->ticket->save();
                } else if (!empty($participant_ids_group_exist)){
                    $this->ticket->quota_achieve = $this->ticket->quota_achieve  + count($participant_ids_group_exist);
                    $this->ticket->save();
                } else {
                    $this->ticket->quota_achieve = $this->ticket->quota_achieve + 1;
                    $this->ticket->save();
                }

                // Database Table : participants
                Eventy::LogFile('Database Update : `participants`', 'logging', 'logging', [
                    'id' => $this->participant->id,
                    'email' => $this->participant->email,
                    'status' => $this->participant->status,
                    'transaction_id' => $this->transaction->id,
                ]);
            }

            // load model
            if ($this->transaction) {
                $this->transaction->load(['transactionDetails.participant', 'transactionTicket.ticket']);
            }

            DB::commit();

            // send email invoice
            $send = $this->participant->sendEmailInvoice([
                'event_logo' => preferGet('logo_events'),
                'event_name' => preferGet('name_events'),
                'event_email' => preferGet('contact_email_events'),
                'contact_name_1' => preferGet('contact_name_1'),
                'contact_phone_1' => preferGet('contact_phone_1'),
                'contact_name_2' => preferGet('contact_name_2'),
                'contact_phone_2' => preferGet('contact_phone_2'),
                'participant' => $this->participant,
                'payment' => $this->transaction,
                'order_id' => $this->transaction->invoice_code,
                'url' => action([LoginController::class, 'index'], [
                    'token' => encrypt($this->participant->email),
                ]),
            ]);

            // Send Email Invoice
            Eventy::LogFile('Email Send : `invoice`', 'logging', 'logging', [
                'email' => $this->participant->email,
                'status' => $send['status'],
                'message' => $send['message'],
            ]);

            return $this->transaction->id;
        } catch (Exception $e) {
            DB::rollBack();

            // Failed Transaction
            Eventy::LogFile('Transaction Failed : ' . $this->participant->email, 'logging', 'logging', [
                'email' => $this->participant->email,
                'exception_message' => $e->getMessage(),
            ]);

            throw new Exception($e->getMessage(), 400);
        }
    }

    public function saveDetailGroup($participant_ids_group)
    {
        // create ticket
        if ($this->ticket) {
            $this->transactionTicket->transaction_id = $this->transaction->id;
            $this->transactionTicket->ticket_id = $this->ticket->id;
            $this->transactionTicket->price = $this->ticket->price;
            $this->transactionTicket->save();
        }

        // create group
        if (!empty($participant_ids_group)) {
            $method_changed = request('method_changed');
            $participant_id = $this->participant->id;

            foreach ($participant_ids_group as $id) {
                $checkDetail = TransactionDetail::where('transaction_id', $this->transaction->id)
                    ->where('participant_id', $id)
                    ->first();

                if (!$checkDetail) {
                    $transactionDetail = new TransactionDetail();
                    $transactionDetail->transaction_id = $this->transaction->id;
                    $transactionDetail->participant_id = $id;
                    $transactionDetail->ticket_schedule_id = 0;
                    $transactionDetail->schedule_id = 0;
                    $transactionDetail->price = 0;
                    $transactionDetail->save();
                }
            }
        }
    }

    public function saveDetail($ticket_schedule_ids)
    {
        // create ticket
        if ($this->ticket) {
            $this->transactionTicket->transaction_id = $this->transaction->id;
            $this->transactionTicket->ticket_id = $this->ticket->id;
            $this->transactionTicket->price = $this->ticket->price;
            $this->transactionTicket->save();
        }

        // create schedules
        if (!empty($ticket_schedule_ids)) {
            $method_changed = request('method_changed');
            $participant_id = $this->participant->id;

            foreach ($ticket_schedule_ids as $id) {
                $checkDetail = TransactionDetail::where('transaction_id', $this->transaction->id)
                    ->where('ticket_schedule_id', $id)
                    ->first();

                if (!$checkDetail) {
                    $transactionDetail = new TransactionDetail();
                    $ticketSchedule = TicketSchedule::with(['schedule.location'])->find($id);

                    $countSelectedClassroom = TransactionDetail::where('schedule_id', $ticketSchedule->schedule->id)
                        ->where(function ($query) use ($method_changed, $participant_id) {
                            if ($method_changed) {
                                $query->where('participant_id', '!=', $participant_id);
                            }
                        })
                        ->distinct(['participant_id', 'schedule_id'])
                        ->lockForUpdate()
                        ->count();

                    if ($countSelectedClassroom >= $ticketSchedule->quota) {
                        throw new Exception("We're sorry, Entrepreneur Pass has been fully booked.", 404);
                    }

                    $transactionDetail->transaction_id = $this->transaction->id;
                    $transactionDetail->participant_id = $participant_id;
                    $transactionDetail->ticket_schedule_id = $ticketSchedule->id;
                    $transactionDetail->schedule_id = $ticketSchedule->schedule->id;
                    $transactionDetail->price = $ticketSchedule->price;
                    $transactionDetail->save();

                    // update type participant
                    $this->participant->type = $ticketSchedule->schedule->location->name;
                    $this->participant->save();
                }
            }
        }
    }

    /**
     * post send payment to eventy xendit platform
     *
     * @param Transaction $transaction
     *
     * @return array|mixed
     * @throws Exception
     */
    public function postSendEventyPayment(Transaction $transaction)
    {
        try {
            switch ($transaction->type) {
                case 'Virtual Accounts':
                    $response = $this->Invoice($transaction, [
                        'type' => 'Virtual Accounts',
                        'payment_method' => $transaction->payment_method,
                    ]);
                    break;

                case 'Cards':
                    $response = $this->Invoice($transaction, [
                        'type' => 'Cards',
                        'payment_method' => 'CREDIT_CARD',
                    ]);
                    break;

                case 'QR Codes':
                    $response = $this->Invoice($transaction, [
                        'type' => 'QR Codes',
                        'payment_method' => 'QRIS',
                    ]);
                    break;

                case 'Manual':
                    $data['url'] = url('confirmation?key=' . encrypt($transaction->id));
                    $response['data'] = $data;
                    break;

                default:
                    throw new Exception('Payment Method is invalid', 400);
            }

            debug([request()->fullUrl(), $response]);

            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * create new payment from bank transfer
     *
     * $data[field] : transaction_id, participant_id, invoice_code, account_name, paid_date, paid_amount, file
     *
     * @return void
     * @throws Exception
     */
    public function createNewBankTransferInvoice($data = [])
    {
        $manual = new TransactionConfirmation();
        $manual->transaction_id = $data['transaction_id'];
        $manual->participant_id = $data['participant_id'];
        $manual->invoice_code = $data['invoice_code'];
        $manual->account_name = $data['account_name'];
        $manual->transfer_date = $data['paid_date'];
        $manual->nominal = $data['paid_amount'];
        $manual->picture = $data['file'];
        $manual->status = 'WAITING';
        $manual->save();
    }

    public static function deleteRelationByParticipantId($participant_id)
    {
        $transactions = Transaction::where('participant_id', $participant_id)->get();

        if (count($transactions) > 0) {
            foreach ($transactions as $xrow) {

                $transaction = Transaction::find($xrow->id);

                // delete payment confirmation
                TransactionConfirmation::where('transaction_id', $transaction->id)->delete();

                // delete payment ticket
                TransactionTicket::where('transaction_id', $transaction->id)->delete();

                // delete payment detail
                TransactionDetail::where('transaction_id', $transaction->id)->delete();

                // delete payment xendit
                TransactionXendit::where('transaction_id', $transaction->id)->delete();

                // delete master payment
                $transaction->delete();
            }
        }
    }

    public static function findAllTotalSumByMethod(): array
    {
        $types = [];

        if (preferGet('payment_method_manual_status') === 'active') {
            array_push($types, (object)['type' => 'Manual', 'name' => 'Bank Transfer']);
        }

        if (preferGet('payment_method_ovo_status') === 'active') {
            array_push($types, (object)['type' => 'eWallets', 'name' => 'OVO']);
        }

        if (preferGet('payment_method_cc_status') === 'active') {
            array_push($types, (object)['type' => 'Cards', 'name' => 'Credit Card']);
        }

        if (preferGet('payment_method_va_status') === 'active') {
            array_push($types, (object)['type' => 'Virtual Accounts', 'name' => 'Virtual Accounts']);
        }

        if (preferGet('payment_method_qris_status') === 'active') {
            array_push($types, (object)['type' => 'QR Codes', 'name' => 'QRIS']);
        }


        foreach ($types as $type) {
            $type->amount = (int)Transaction::sumTotalIncome($type->type);
            $type->fee = (int)Transaction::sumTotalFee($type->type);
        }

        return $types;
    }
}
