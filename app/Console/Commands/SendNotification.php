<?php

namespace App\Console\Commands;

use App\Events\Notification\SendNotification as SendNotif;
use App\Models\Participant;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eventy:send-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification to Active Participant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set(preferGet('timezone_events'));

        $data = DB::table('notifications')
            ->where('is_send', 0)
            ->where('is_active', 1)
            ->whereNotNull('send_at')
            ->get();

        foreach ($data as $x => $xrow) {
            $time = date('Y-m-d H:i:s', strtotime($xrow->send_at));

            if (new Datetime($time) < new DateTime(dateTime())) {
                $id = $xrow->id;

                DB::table('notifications')->where('id', $id)->update(['is_send' => 1]);

                // create notifications detail
                $participantIds = Participant::whereStatus('Active')->pluck('id')->toArray();
                $array = [];
                foreach ($participantIds as $pId) {
                    $array[] = [
                        'notification_id' => $id,
                        'participant_id' => $pId,
                        'is_read' => 0,
                        'is_deleted' => 0
                    ];
                }

                DB::table('notification_details')->insert($array);

                // send broadcast notif
                $message = [
                    'id' => $id,
                    'title' => $xrow->title,
                    'message' => $xrow->message,
                    'send_at' => date('H:i'),
                    'time_elapsed' => carbon($xrow->send_at)->diffForHumans(),
                    'is_read' => 0,
                ];

                $broadcast = (object)$message;

                broadcast(new SendNotif($broadcast))->toOthers();
            }
        }
    }
}
