<?php

namespace App\Console\Commands;

use App\Jobs\SendReminderWaitingJob;
use App\Models\Participant;
use DateTime;
use Illuminate\Console\Command;
use Exception;

class SendEmailWaiting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eventy:waiting-ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email waiting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set(preferGet('timezone_events'));

        $now = dateTime();
        try {
            $listWaiting = Participant::whereStatus(Participant::STATUS_WAITING_TICKET)->get();

            foreach ($listWaiting as $participant) {
                if ($now >= $participant->send_waiting_at) {

                    SendReminderWaitingJob::dispatch($participant);

                    $participant->latest_email_reminder_at = $now;
                    $participant->save();
                }
            }

            $this->info('Send Email Reminder Ticket at: ' . $now);
            $this->info('Next Email Reminder at: ' . date('Y-m-d H:i:s', strtotime("+1 day", strtotime($now))));
        } catch (Exception $e) {
            $this->info('Failed Email Reminder Ticket at: ' . $now);
        }

        return 0;
    }
}
