<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmail
{
    private $subject;
    private $cc;
    private $sender;
    private $template;
    private $to;
    private $data;
    private $event;
    private $from;
    private $attachments = [];

    function setSubject($subject)
    {
        $this->subject = $subject;
    }
    function getSubject()
    {
        return $this->subject;
    }
    function setCc($cc)
    {
        $this->cc = $cc;
    }
    function getCc()
    {
        return $this->cc;
    }
    function setSender($sender)
    {
        $this->sender = $sender;
    }
    function getSender()
    {
        return $this->sender;
    }
    function setTemplate($template)
    {
        $this->template = $template;
    }
    function getTemplate()
    {
        return $this->template;
    }
    function setTo($to)
    {
        $this->to = $to;
    }
    function getTo()
    {
        return $this->to;
    }
    function setData($data = [])
    {
        $this->data = $data;
    }
    function getData()
    {
        return $this->data;
    }
    function setEvent($event)
    {
        $this->event = $event;
    }
    function getEvent()
    {
        return $this->event;
    }
    function setFrom($from)
    {
        $this->from = $from;
    }
    function getFrom()
    {
        return $this->from;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments): void
    {
        $this->attachments = $attachments;
    }


    /**
     * return send Email by : SMTP
     *
     * @return array
     */
    function sendEmailSMTP()
    {
        $template = $this->getTemplate();
        $data = $this->getData();
        $from = $this->getFrom();
        $to = $this->getTo();
        $subject = $this->getSubject();
        $event = $this->getEvent();
        $attachments = $this->getAttachments();

        try {
            Mail::send($template, $data, function ($email) use ($from, $to, $subject, $event, $attachments) {
                $email->priority(1);
                $email->to($to, $event)->subject($subject);
                $email->from($from, $event);

                if (count($attachments) > 0) {
                    foreach ($attachments as $row) {
                        $fileContents = file_get_contents($row);
                        if ($fileContents === false) {
                            Log::error('Failed to download attachment:', ['attachment' => $row]);
                            continue;
                        }
                        $fileName = basename($row);
//                        $email->attach($row);
                        $email->attachData($fileContents, $fileName);
                    }
                }
            });

            $res['status'] = 1;
            $res['message'] = 'send';
        } catch (\Throwable $th) {
            $res['status'] = 2;
            $res['message'] = $th->getMessage();
        }

        return $res;
    }
}
