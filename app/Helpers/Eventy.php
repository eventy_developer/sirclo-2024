<?php

namespace App\Helpers;

use App\Models\Participant;
use App\Models\Transaction;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ReflectionClass;
use ReflectionMethod;

class Eventy
{
    public static function randomString(): string
    {
        return Str::random();
    }

    public static function getAvailablePaymentMethod(): array
    {
        $data = [];

        if (preferGet('payment_method_va_status') === 'active') {
            array_push($data, (object)[
                'type' => 'Virtual Accounts',
                'name' => 'VIRTUAL_ACCOUNT'
            ]);
        }

        if (preferGet('payment_method_qris_status') === 'active') {
            array_push($data, (object)[
                'type' => 'QR Codes',
                'name' => 'QRIS'
            ]);
        }

        if (preferGet('payment_method_manual_status') === 'active') {
            array_push($data, (object)[
                'type' => 'Manual',
                'name' => 'BANK_TRANSFER'
            ]);
        }

        if (preferGet('payment_method_cc_status') === 'active') {
            array_push($data, (object)[
                'type' => 'Cards',
                'name' => 'CREDIT_CARD'
            ]);
        }

        return $data;
    }

    /**
     * --------------------------------------------------------------------------------------------------------------
     * Order ID (thn+bln+tgl+ order) ORDER_2102060001
     * --------------------------------------------------------------------------------------------------------------
     *
     * @return string
     */
    public static function invoiceCode(): string
    {
        $date = date('ymd', strtotime(dateTime()));
        $check = Transaction::whereDate('created_at', d())
            ->latest('id')
            ->withTrashed()
            ->lockForUpdate()
            ->first();

        if (env('APP_DEBUG') == true) {
            if (env('APP_URL') == "http://localhost"){
                $char = 'DSRL';
            }else{
                $char = 'SSRL';
            }
        }else{
            $char = 'PSRL';
        }

        $invoice = 'ORDER'. $char . '_' . $date . str_pad(1, 4, "0", STR_PAD_LEFT);

        if ($check) {
            $val = $check->invoice_code;
            $val = substr($val, -4) + 1;
            $invoice = 'ORDER'. $char . '_' . $date . str_pad($val, 4, "0", STR_PAD_LEFT);
        }

        $exist = Transaction::with(['participant'])->whereInvoiceCode($invoice)->withTrashed()->first();
        if ($exist) {
            Eventy::LogFile('Duplicate Exist Invoice : ' . $exist->invoice_code, 'logging', 'logging', [
                'email' => $exist->participant->email,
            ]);
            $invoice = self::invoiceCode();
        }

        return $invoice;
    }

    public static function registrationNumber(): string
    {
        $check = Participant::where('is_admin', 0)
            ->whereNotNull('registration_number')
            ->orderBy('registration_number', 'desc')
            ->lockForUpdate()
            ->first();

        $registrationNumber = str_pad(1, 4, "0", STR_PAD_LEFT);

        if ($check) {
            if ($check->registration_number) {
                $val = $check->registration_number;
                $val = substr($val, -4) + 1;
                $registrationNumber = str_pad($val, 4, "0", STR_PAD_LEFT);
            }
        }

        $exist = Participant::whereRegistrationNumber($registrationNumber)->first();
        if ($exist) {
            $registrationNumber = self::registrationNumber();
        }

        return $registrationNumber;
    }

    /**
     * Daily log to file
     *
     * @param string $log_message
     * @param string $path
     * @param string $filename
     * @param array $data
     * @param string $type [info, error, warning]
     *
     * @return bool
     */
    public static function LogFile(string $log_message, string $path, string $filename, $data = [], $type = 'info'): bool
    {
        try {
            $log_message = $log_message ?: request()->fullUrl();
            $log_data = $data;
            $log_file = 'logs/' . $path . '/' . date('Y-m/d/') . $filename . '.log';
            $log_path = storage_path($log_file);

            // save log file
            $log = new Logger($filename);
            $log->pushHandler(new StreamHandler($log_path, Logger::INFO));

            switch ($type) {
                case 'error':
                    $log->error($log_message, $log_data);
                    break;
                case 'warning':
                    $log->warning($log_message, $log_data);
                    break;
                default:
                    $log->info($log_message, $log_data);
                    break;
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Root folder on S3
     *
     * @return string
     */
    private static function getRootDirS3(): string
    {
        $rootDir = 'virtual-event/' . config('eventy.event_name');
        if (in_array(request()->ip(), ['localhost', '127.0.0.1', '::1'])) {
            $rootDir .= '/local';
        } else if (in_array(request()->server('SERVER_ADDR'), ['178.128.127.246'])) {
            $rootDir .= '/dev';
        } else {
            $rootDir .= '/prod';
        }

        return $rootDir;
    }

    /**
     * Upload file to S3 bucket
     *
     * @param string $name
     * @param string $customDir
     * @param string $customFilename
     * @param null $resize_width
     *
     * @return null|string
     */
    public static function uploadFileS3(string $name, $customDir = '', $customFilename = '', $resize_width = null): ?string
    {
        $rootDir = self::getRootDirS3();

        if (request()->hasFile($name)) {
            $file = request()->file($name);
            $images_ext = config('crudbooster.IMAGE_EXTENSIONS', 'jpg,png,gif,bmp');
            $images_ext = explode(',', $images_ext);
            $ext = $file->getClientOriginalExtension() ?: $file->clientExtension();
            // default
            $filename = md5(str_random(5) . dateTime() . time());
            $dir = 'uploads/' . date('Y-m');

            if (!empty($customDir)) {
                $dir = $customDir;
            }
            if (!empty($customFilename)) {
                $filename = $customFilename;
            }

            $filename = $filename . '.' . $ext;
            $file_path = $rootDir . DIRECTORY_SEPARATOR . $dir;

            if (in_array(strtolower($ext), $images_ext)) {
                return self::resizeImage($file, $dir . DIRECTORY_SEPARATOR . $filename, $resize_width);
            } else {
                $options = [];
                if (config('filesystems.default') !== 's3') {
                    $options = ['visibility' => 'public'];
                }

                $upload = Storage::disk(config('filesystems.default'))->putFileAs($file_path, $file, $filename, $options);
            }

            if ($upload) {
                return $upload;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function resizeImage($file, $file_path, $resize_width = null): ?string
    {
        ini_set('memory_limit', '256M'); // Increase memory limit if needed

        $images_ext = config('crudbooster.IMAGE_EXTENSIONS', 'jpg,png,gif,bmp');
        $images_ext = explode(',', $images_ext);

        $ext = $file->getClientOriginalExtension() ?: $file->clientExtension();

        if (in_array(strtolower($ext), $images_ext)) {
            $img = Image::make($file);

            if ($resize_width) {
                $img->resize($resize_width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                if ($img->width() > 1000) {
                    $img->resize(1000, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else if ($img->width() > 800) {
                    $img->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else if ($img->width() > 500) {
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }

            if (!in_array(strtolower($ext), ['png'])) {
                return self::uploadRawFileS3($file_path, $img->encode($ext, 75));
            }

            return self::uploadRawFileS3($file_path, $img->encode());
        }

        return null;
    }

    /**
     * Upload base64 file to S3 bucket
     *
     * @param string $base64
     * @param int|null $id
     *
     * @return null|string
     */
    public static function uploadBase64FileS3(string $base64, $id = null): ?string
    {
        $file = preg_replace('#^data:image/\w+;base64,#i', '', $base64);
        $filedata = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $filedata, FILEINFO_MIME_TYPE);
        @$mime_type = explode('/', $mime_type);
        @$mime_type = $mime_type[1];

        // default
        $filename = md5(str_random(5) . dateTime() . time()) . '.' . $mime_type;
        $dir = 'uploads/' . date('Y-m');

        if (CRUDBooster::myId()) {
            $id = CRUDBooster::myId();
        }

        if ($id) {
            $dir = 'uploads/' . $id . DIRECTORY_SEPARATOR . date('Y-m');
        }

        // set path upload
        $file_path = $dir . DIRECTORY_SEPARATOR . $filename;

        if ($mime_type) {
            return self::uploadRawFileS3($file_path, $filedata);
        } else {
            return null;
        }
    }

    /**
     * Upload raw file to S3 bucket
     *
     * @param string $path
     * @param string $content
     *
     * @return null|string
     */
    public static function uploadRawFileS3(string $path, string $content): ?string
    {
        $rootDir = self::getRootDirS3();

        if ($path != '' && $content != '') {
            $path = $rootDir . DIRECTORY_SEPARATOR . $path;

            $options = [];
            if (config('filesystems.default') !== 's3') {
                $options = ['visibility' => 'public'];
            }

            $upload = Storage::disk(config('filesystems.default'))->put($path, $content, $options);

            if ($upload) {
                return $path;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * remove file from S3 bucket
     *
     * @param string|null $path
     *
     * @return null|void
     */
    public static function removeFileS3(?string $path)
    {
        if ($path != '') {
            if (!(strpos($path, 'virtual-event/virtualeventv3/example/') !== false)) {
                if (Storage::disk(config('filesystems.default'))->exists($path)) {
                    Storage::disk(config('filesystems.default'))->delete($path);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    /**
     * get file from S3 bucket
     *
     * @param string|null $path
     *
     * @return null|string
     */
    public static function getFileS3(?string $path): ?string
    {
        if ($path != '') {
            if (Storage::disk(config('filesystems.default'))->exists($path)) {
                return Storage::disk(config('filesystems.default'))->url($path);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * check file from S3 bucket
     *
     * @param string|null $path
     *
     * @return null|boolean
     */
    public static function isFileS3Exists(?string $path): ?bool
    {
        if ($path != '') {
            if (Storage::disk(config('filesystems.default'))->exists($path)) {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    public static function formatSizeUnits($bytes): string
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } else if ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } else if ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } else if ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } else if ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function hashString($string = '', $withRandomCode = false): string
    {
        if (!$withRandomCode) return md5($string);

        return md5($string . microtime() . str_random(5));
    }

    public static function QRCode($kode)
    {
//        $base = 'http://chart.googleapis.com/chart?chs=300x300&chld=L|0&cht=qr&chl=' . $kode;
        $base = 'https://qrcode.tec-it.com/API/QRCode?data=' . $kode;
        $path = 'qrcode/' . $kode . '.png';
        $file = Eventy::file($path);

        if ($file == '') {
            $qr = file_get_contents($base);
            file_put_contents('qrcode/' . $kode . '.png', $qr);
            $file = url($path);
        }

        return $file;
    }

    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } else if (file_exists($check_public)) {
            return url($path);
        } else if (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    /**
     * @throws \Exception
     */
    public static function getArrayIntervalDate($start_date, $end_date): array
    {
        // retrieve all dates even if they are empty.
        $dates = [];
        $period = new DatePeriod(new DateTime($start_date), new DateInterval('P1D'), new DateTime("$end_date +1 day"));
        foreach ($period as $date) {
            $dates[] = $date->format("Y-m-d");
        }

        return $dates;
    }
}
