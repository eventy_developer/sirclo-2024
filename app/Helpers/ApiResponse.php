<?php

namespace App\Helpers;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ApiResponse
{
    private static $response = [
        'message' => null,
        'data' => [],
        'errors' => null
    ];

    private static function send($data, $http_status = 200)
    {
        return response()->json($data, $http_status);
    }

    public static function success($data = [], $message = 'Success')
    {
        self::$response['message'] = $message;
        self::$response['data'] = $data;

        return self::send(self::$response);
    }

    public static function failed($error = null)
    {
        if (config('app.debug')) {
            Log::debug($error);
        }

        $http_status = 500;

        if ($error instanceof ValidationException) {
            self::$response['message'] = 'Request Failed';
            self::$response['errors'] = $error->errors();
            $http_status = $error->getCode() ?: 400;

        } elseif ($error instanceof QueryException) {
            $errors = [];

            if (config('app.debug')) {
                $errors['code'][] = $error->getCode();
                $errors['sql'][] = $error->getSql();
                $errors['bindings'][] = $error->getBindings();
            }

            self::$response['message'] = $error->getMessage();
            self::$response['errors'] = $errors;

        } elseif ($error instanceof Exception) {
            self::$response['message'] = $error->getMessage();
            $http_status = $error->getCode() ?: 500;

            if (config('app.debug')) {
                self::$response['errors'] = json_decode(json_encode($error->getTrace()));
            }

        } else {
            if (is_object($error) || is_array($error)) {
                self::$response['message'] = json_encode($error);
            } else {
                self::$response['message'] = $error;
            }

        }

        return self::send(self::$response, $http_status);
    }

    public static function unauthorized($message = null)
    {
        self::$response['message'] = $message ?: 'Unauthorized.';

        return self::send(self::$response, 401);
    }

    public static function forbidden($message = null)
    {
        self::$response['message'] = $message ?: 'You don\'t have permission to access.';

        return self::send(self::$response, 403);
    }

    public static function serviceUnavailable($message = null)
    {
        self::$response['message'] = $message ?: 'Service Unavailable';

        return self::send(self::$response, 503);
    }
}
