<?php

use App\Helpers\Eventy;
use App\Models\Preference;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

if (!function_exists('randomString')) {
    function randomString(): string
    {
        return Eventy::randomString();
    }
}

if (!function_exists('preferGet')) {
    /**
     * return value preference
     *
     * @param string $key
     *
     * @return null
     */
    function preferGet(string $key): ?string
    {
        return Cache::remember(prefixCacheName($key), (new Carbon())->addMinutes(60), function () use ($key) {
            return Preference::where('key', $key)->first()->present_content ?? false;
        });
    }
}

if (!function_exists('S3Endpoint')) {
    function S3Endpoint(): string
    {
        return config('eventy.eventy_s3_url') . '/';
    }
}

if (!function_exists('getS3EndpointFile')) {
    /**
     * get endpoint file
     *
     * @param string $path
     *
     * @return string
     */
    function getS3EndpointFile($path): string
    {
        return S3Endpoint() . $path;
    }
}

if (!function_exists('adjustColorBrightness')) {
    /**
     * Increases or decreases the brightness of a color by a percentage of the current brightness.
     *
     * @param string $hexCode Supported formats: `#FFF`, `#FFFFFF`, `FFF`, `FFFFFF`
     * @param float $adjustPercent A number between -1 and 1. E.g. 0.3 = 30% lighter; -0.4 = 40% darker.
     *
     * @return  string
     */
    function adjustColorBrightness($hexCode, $adjustPercent)
    {
        $hexCode = ltrim($hexCode, '#');

        if (strlen($hexCode) == 3) {
            $hexCode = $hexCode[0] . $hexCode[0] . $hexCode[1] . $hexCode[1] . $hexCode[2] . $hexCode[2];
        }

        $hexCode = array_map('hexdec', str_split($hexCode, 2));

        foreach ($hexCode as &$color) {
            $adjustableLimit = $adjustPercent < 0 ? $color : 255 - $color;
            $adjustAmount = ceil($adjustableLimit * $adjustPercent);

            $color = str_pad(dechex($color + $adjustAmount), 2, '0', STR_PAD_LEFT);
        }

        return '#' . implode('', $hexCode);
    }
}

if (!function_exists('dateTime')) {
    /**
     * return datetime now
     */
    function dateTime()
    {
        date_default_timezone_set(preferGet('timezone_events'));
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('d')) {
    /**
     * return date now
     */
    function d()
    {
        date_default_timezone_set(preferGet('timezone_events'));
        return date('Y-m-d');
    }
}

if (!function_exists('countActivePaymentMethod')) {
    /**
     * get total payment yg aktif
     *
     * @return int
     */
    function countActivePaymentMethod(): int
    {
        $count = 0;

        if (preferGet('payment_method_va_status') === 'active') {
            $count += 1;
        }
        if (preferGet('payment_method_qris_status') === 'active') {
            $count += 1;
        }
        if (preferGet('payment_method_cc_status') === 'active') {
            $count += 1;
        }
        if (preferGet('payment_method_manual_status') === 'active') {
            $count += 1;
        }

        return $count;
    }
}

if (!function_exists('gSession')) {
    /**
     * return session login
     *
     * @param string $name
     *
     * @return mixed|null
     */
    function gSession(string $name)
    {
        $session = auth()->user();
        return ($session ? $session->$name : null);
    }
}

if (!function_exists('dateStartEvent')) {
    /**
     * @return string datetime event start
     */
    function dateStartEvent(): string
    {
        date_default_timezone_set(preferGet('timezone_events'));
        return preferGet('date_start_events');
    }
}

if (!function_exists('isEventLocked')) {
    /**
     * Check if event is locked
     *
     */
    function isEventLocked(): bool
    {
        return (dateTime() < dateStartEvent()) && (gSession('is_admin') == 0);
    }
}

if (!function_exists('isLoginLocked')) {
    /**
     * Check if login is locked
     *
     */
    function isLoginLocked(): bool
    {
        return trim(preferGet('lock_login_wording_events')) != '';
    }
}

if (!function_exists('invoiceCode')) {
    /**
     * return invoice code
     *
     */
    function invoiceCode(): string
    {
        return Eventy::invoiceCode();
    }
}

if (!function_exists('getFirstName')) {
    /**
     * @param $name
     *
     * @return string
     */
    function getFirstName($name): string
    {
        $results = array();
        $data = preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#', $name, $results);
        if ($data === 1) {
            if ($results[1] == '') return trim($results[2]);

            return trim($results[1] . ' ' . $results[2]);
        }

        $split = explode(' ', $name);
        return trim($split[0] . ' ' . $split[1]);
    }
}

if (!function_exists('prefixCacheName')) {
    function prefixCacheName($name): string
    {
        return config('eventy.event_name') . '_' . $name;
    }
}

if (!function_exists('abbreviationTimezone')) {
    /**
     * @return string
     */
    function abbreviationTimezone(): string
    {
        switch (preferGet('timezone_events')) {
            case 'Asia/Makassar':
                $abbreviation = 'WITA';
                break;
            case 'Asia/Jayapura':
                $abbreviation = 'WIT';
                break;
            default:
                $abbreviation = 'WIB';
                break;
        }

        return $abbreviation;
    }
}

if (!function_exists('carbon')) {
    /**
     * get $dateTime or current datetime
     *
     * @param string $dateTime
     *
     * @return Carbon
     */
    function carbon($dateTime = null)
    {
        return new Carbon($dateTime);
    }
}

if (!function_exists('compareDate')) {
    /**
     * compare between 2 date
     *
     * @param $date1
     * @param $date2
     * @param $compareTo
     * @param $operator
     * @return bool
     */
    function compareDate($date1, $date2, $compareTo = 'date', $operator = 'eq'): bool
    {
        $date1 = carbon($date1);
        $date2 = carbon($date2);

        $newDate1 = Carbon::create($date1->year, $date1->month, $compareTo === 'date' ? $date1->day : null);
        $newDate2 = Carbon::create($date2->year, $date2->month, $compareTo === 'date' ? $date2->day : null);

        return $newDate1->$operator($newDate2);
    }
}

if (!function_exists('plusOneHour')) {
    /**
     * return datetime now
     *
     * @param $date
     * @param integer $hours
     *
     * @return false|string
     */
    function plusOneHour($date, int $hours)
    {
        date_default_timezone_set(preferGet('timezone_events'));
        return date('Y-m-d H:i:s', strtotime("+" . $hours . " hours", strtotime($date)));
    }
}
