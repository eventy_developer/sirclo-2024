<?php

namespace App\Http\Middleware;

use App\Models\Participant;
use Closure;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $url = $request->fullUrl();
        if (strpos($url, 'www')) {
            return redirect(str_replace('www.', '', $url));
        }

        if (auth()->check()) {
            $url_prev = url()->previous();
            $url_prev = str_replace(url('/'), '', $url_prev);
            if ($request->segment(1) !== 'logout') {
                $exception = ['/login', '/register', '/confirmation'];

                if (in_array($url_prev, $exception)) {
                    return redirect('/');
                } else {
                    if (isEventLocked() && isLoginLocked()) {
                        return redirect('/logout');
                    }

                    if (
                        preg_match('/^[^?]*\.(JPG|JPEG|jpg|jpeg|gif|png|mp3)/i', $url_prev) == 1 ||
                        strpos($url_prev, 'zoom-meet') ||
                        strpos($url_prev, 'empty-classroom') ||
                        strpos($url_prev, 'empty-stage') ||
                        strpos($url_prev, 'admin') ||
                        strpos($url_prev, 'landingpage')
                    ) {
                        return redirect('/');
                    }

                    return redirect()->back();
                }
            }
        }

        $token = request('token');
        if ($token) {
            try {
                // get email user from url
                $email = decrypt($token);

                // create new session if user exists
                $participant = Participant::whereEmail($email)->first();
                if ($participant) {

                    if ((dateTime() < dateStartEvent()) && isLoginLocked() && $participant->isActiveStatus()) {
                        return redirect('/logout');
                    }

                    // create login session
                    auth()->login($participant);

                    return redirect('/home');
                }
            } catch (DecryptException $e) {
            }

            return redirect('/login');
        }

        return $next($request);
    }
}
