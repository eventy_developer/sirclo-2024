<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Registration\TicketController;
use App\Http\Controllers\Registration\TransactionController;
use App\Models\Transaction;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $participant = auth()->user();

        $url_current = url()->current();
        $url_current = str_replace(url('/'), '', $url_current) ?: '/';

        if (!$request->ajax()) {
            if (!$participant || (isEventLocked() && isLoginLocked() && $participant->isActiveStatus())) {
                return redirect('/logout');
            }
        }

        // merge $participant into $request input array
        $request->merge(['participant' => $participant]);

        if ($request->method() === 'GET') {
            if ($participant->isActiveStatus()) {
                $latestTransaction = Transaction::whereParticipantId(gSession('id'))->latest('id')->first();

                // merge $latestTransaction into $request input array
                $request->merge(['latestTransaction' => $latestTransaction]);

                if ($latestTransaction) {
                    if ($latestTransaction->isSuccess()) {
                        $blockPath = ['/ticket', '/finish', '/confirmation'];
                        if (in_array($url_current, $blockPath)) {
                            return redirect('/home');
                        }
                    }
                } else {
                    $blockPath = ['/ticket', '/finish', '/confirmation'];
                    if (in_array($url_current, $blockPath)) {
                        return redirect('/home');
                    }
                }
            } else if ($participant->isPaymentChanged()) {
                $exception = ['/ticket'];
                if (!in_array($url_current, $exception)) {
                    return redirect()->action([TicketController::class, 'index'], [
                        'method_changed' => encrypt([
                            'id' => $participant->id,
                            'is_payment_changed' => $participant->isPaymentChanged()
                        ])
                    ]);
                }

                if (!g('method_changed')) {
                    return redirect('/home');
                }
            } else if ($participant->isWaitingTicket()) {
                $exception = ['/ticket'];
                if (!in_array($url_current, $exception)) {
                    return redirect()->action([TicketController::class, 'index']);
                }

                if (g('method_changed')) {
                    return redirect('/home');
                }
            } else if ($participant->isWaitingPayment() || $participant->isWaitingConfirmation() || $participant->isPending()) {
                $exception = ['/finish', '/confirmation'];
                if (!in_array($url_current, $exception)) {
                    // find latest payment
                    $payment = Transaction::whereParticipantId($participant->id)->latest('id')->first();
                    return redirect()->action([TransactionController::class, 'show'], [
                        'key' => encrypt($payment->id)
                    ]);
                }
            }
        }

        return $next($request);
    }
}
