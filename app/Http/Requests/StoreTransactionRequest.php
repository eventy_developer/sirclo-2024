<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'amount' => 'required',
            'initial_amount' => 'required',
            'fixed_fee' => 'required',
            'var_fee' => 'required',
            'vat_fee' => 'required',
            'ticket_id' => '',
            'payment_method' => 'required|in:BRI,MANDIRI,BNI,PERMATA,BCA,SAHABAT_SAMPOERNA,ALFAMART,INDOMARET,OVO,DANA,LINKAJA,CREDIT_CARD,QR_CODE,BANK_TRANSFER,VIRTUAL_ACCOUNT,QRIS,SHOPEEPAY',
            'type' => 'required|in:Virtual Accounts,Retail Outlets,eWallets,Cards,QR Codes,Manual',
        ];
    }
}
