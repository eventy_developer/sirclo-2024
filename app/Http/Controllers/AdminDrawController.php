<?php

namespace App\Http\Controllers;

use App\Models\Doorprize;
use App\Models\Participant;
use App\Traits\ApiResponser;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Exception;
use Illuminate\Support\Facades\DB;

class AdminDrawController extends CBController
{
    use ApiResponser;

    public function postDrawWinner()
    {
        DB::beginTransaction();
        try {
            $doorprize_id = request('doorprize_id');
            $doorprize = Doorprize::find($doorprize_id);
            $limit = config('eventy.top_general');
            if ($doorprize->draw_type === 'Grand Prize') {
                $limit = config('eventy.top_grandprize');
            }

            $quantity = (int)$doorprize->quantity;
            $randomNumber = $quantity - (int)$doorprize->is_pickup_doorprize;

            // get user by total quantity doorprize
            $randomUserIds = Participant::where('is_admin', 0)
                ->where(function ($query) use ($doorprize) {
                    if ($doorprize->doorprize_type === 'Doorprize 1') {
                        $query->whereNull('doorprize_id')
                            ->where('exclude_doorprize', 0);
                    } else if ($doorprize->doorprize_type === 'Doorprize 2'){
                        $query->whereNull('doorprize2_id')
                            ->where('exclude_doorprize2', 0);
                    } else {
                        $query->whereNull('doorprize3_id')
                            ->where('exclude_doorprize3', 0);
                    }
                })
                ->where('status', Participant::STATUS_ACTIVE)
                ->where('is_skip_doorprize', 0)
                ->latest('total_point')
                ->limit($limit)
                ->get()
                ->random($randomNumber)
                ->pluck('id')
                ->toArray();
//            $totalUserDoorprize = count($randomUserIds);

            // insert doorprize into participant
            if ($doorprize->doorprize_type === 'Doorprize 1') {
                $update = [
                    'doorprize_id' => $doorprize->id
                ];
            } else if ($doorprize->doorprize_type === 'Doorprize 2') {
                $update = [
                    'doorprize2_id' => $doorprize->id
                ];
            } else {
                $update = [
                    'doorprize3_id' => $doorprize->id
                ];
            }
            Participant::whereIn('id', $randomUserIds)->update($update);

            // reduce doorprize quantity
//            $doorprize->quantity = $quantity - $totalUserDoorprize;
            $doorprize->is_drawn = 1;
            $doorprize->draw_at = dateTime();
            $doorprize->save();

            DB::commit();

            $res['countdown'] = $doorprize->present_countdown;

            return $this->successResponse($res);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }

    public function getButton()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $doorprize = Doorprize::find(request('id'));

        // Scanner Staff
        if (CRUDBooster::myPrivilegeId() == 3 && $doorprize->is_drawn == 0) {
            CRUDBooster::redirect(CRUDBooster::mainpath(),'You do not have permission to perform this action');
        }

        if (!request('id')) {
            CRUDBooster::redirect(CRUDBooster::mainpath(),'Parameter ID is required');
        }

        if (dateTime() < $doorprize->available_at) {
            CRUDBooster::redirect(CRUDBooster::mainpath(),'Available at ' . date('d M Y, H:i', strtotime($doorprize->available_at)));
        }

        if (g('reset') == 1) {
            // reset
            $doorprize->is_drawn = 0;
            $doorprize->save();

            $participantReset = $doorprize->participants_left->pluck('id')->toArray();
            if ($doorprize->doorprize_type === 'Doorprize 1') {
                $update = [
                    'exclude_doorprize' => 1,
                    'doorprize_id' => null,
                ];
            }
            if ($doorprize->doorprize_type === 'Doorprize 2') {
                $update = [
                    'exclude_doorprize2' => 1,
                    'doorprize2_id' => null,
                ];
            }
            if ($doorprize->doorprize_type === 'Doorprize 3') {
                $update = [
                    'exclude_doorprize3' => 1,
                    'doorprize3_id' => null,
                ];
            }

            Participant::whereIn('id', $participantReset)->update($update);

            CRUDBooster::redirect(CRUDBooster::mainpath(),'Doorprize successfully reset');
        }
        $chunk_size = 3;
        if ($doorprize->quantity <= 5) {
            $chunk_size = 1;
        } else if ($doorprize->quantity <= 10) {
            $chunk_size = 2;
        }

        $data['page_title'] = '';
        $data['doorprize'] = $doorprize;
        $data['chunk_size'] = $chunk_size;

        return $this->view('admin.Doorprize.winner', $data);
    }

    public function getIndex()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = '';
        $data['doorprizes'] = Doorprize::orderBy('sort')->get();

        return $this->view('admin.Doorprize.draw', $data);
    }

    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "doorprizes";
        $this->title_field = "name";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = TRUE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Name", "name" => "name");
        $this->col[] = array("label" => "Quantity", "name" => "quantity");

        # END COLUMNS DO NOT REMOVE THIS LINE
        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Name", "name" => "name", "type" => "text", "required" => TRUE, "validation" => "required|string|min:3|max:70", "placeholder" => "You can only enter the letter only"];
        $this->form[] = ["label" => "Quantity", "name" => "quantity", "type" => "number", "required" => TRUE, "validation" => "required|integer|min:0"];

        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
