<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ApiScanTypeRequest;
use App\Models\CmsUser;
use App\Models\ScanGroup;
use App\Models\ScanType;
use Exception;

class ApiScanTypeController extends Controller
{
    public function index(ApiScanTypeRequest $apiScanTypeRequest)
    {
        try {
            // get only validated request
            $requestValidated = $apiScanTypeRequest->validated();
            $cmsUser = CmsUser::find($requestValidated['id_admin']);

            $data = ScanGroup::with(['scanTypes:id,scan_group_id,name,type,point'])->select('id', 'name')
                ->when($cmsUser->id_cms_privileges == CmsUser::BOOTH_PRIVILEGE_ID, function ($query) use ($cmsUser) {
                    return $query->where('id', $cmsUser->scan_group_id);
                })
                ->get();

            return ApiResponse::success($data);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }
}
