<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ApiScanHistoryRequest;
use App\Models\ScanHistory;
use Exception;

class ApiScanHistoryController extends Controller
{
    public function index(
        ApiScanHistoryRequest $apiScanHistoryRequest,
        ScanHistory           $scanHistory
    )
    {
        try {
            $data = $scanHistory->allWithPagination();

            return ApiResponse::success($data);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }
}
