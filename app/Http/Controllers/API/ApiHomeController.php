<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ApiHomeRequest;
use App\Models\Participant;
use App\Models\ScanHistory;
use App\Models\ScanType;
use Exception;

class ApiHomeController extends Controller
{
    public function __invoke(ApiHomeRequest $apiHomeRequest)
    {
        try {
            $request = request()->all();
            $date_start = carbon(preferGet('date_start_events'));
            $date_end = carbon(preferGet('date_end_events'));

//            $event_date = $date_start->format('d F') . ' - ' . $date_end->format('d F Y');
//            if (compareDate($date_start, $date_end, 'month')) {
//                $event_date = date('d', strtotime($date_start)) . ' - ' . date('d F Y', strtotime($date_end));
//            }
//
//            if (compareDate($date_start, $date_end)) {
//                $event_date = date('d F Y', strtotime($date_start));
//            }

            $scanType = ScanType::with(['scanGroup'])->find($request['scan_type_id']);
            if (!$scanType) {
                throw new Exception('Scan Type not found', 404);
            }

            $data['type_location'] = $scanType->scanGroup->name . ' - ' . $scanType->name;
            $data['total_register'] = Participant::whereIsAdmin(0)->count();
            $data['checkin_count_today'] = ScanHistory::whereHas('participant')->whereScanTypeId($request['scan_type_id'])->where('date', d())->count();
            $data['today'] = date('l, d F Y', strtotime(dateTime()));

            return ApiResponse::success($data);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }

}
