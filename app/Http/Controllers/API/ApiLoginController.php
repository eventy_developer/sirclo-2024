<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Requests\API\ApiLoginRequest;
use App\Models\ApiClient;
use App\Models\CmsUser;
use Exception;
use Illuminate\Support\Facades\Hash;

class ApiLoginController
{
    public function __invoke(ApiLoginRequest $apiLoginRequest)
    {
        try {
            $requestValidated = $apiLoginRequest->validated();

            $cmsUsers = CmsUser::select('id', 'name', 'email', 'password')
                ->whereEmail($requestValidated['email'])->first();
            if (!$cmsUsers) {
                throw new Exception('User not found', 404);
            }

            if (!Hash::check($requestValidated['password'], $cmsUsers->password)) {
                throw new Exception('Password wrong!', 404);
            }

            $cmsUsers->api_token = ApiClient::value('api_token');

            return ApiResponse::success($cmsUsers);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }
}
