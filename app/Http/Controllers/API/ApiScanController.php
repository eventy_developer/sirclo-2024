<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ApiScanRequest;
use App\Models\CmsUser;
use App\Models\Doorprize;
use App\Models\Participant;
use App\Models\ScanHistory;
use App\Models\ScanType;
use Exception;
use Illuminate\Support\Facades\DB;

class ApiScanController extends Controller
{
    public function store(ApiScanRequest $apiScanRequest)
    {
        DB::beginTransaction();
        try {
            // get only validated request
            $requestValidated = $apiScanRequest->validated();

            // check whether scan type is exists
            $scanType = ScanType::with(['scanGroup'])->find($requestValidated['scan_type_id']);
            if (!$scanType) {
                throw new Exception('Scan Type not found', 404);
            }
            $point = (int) $scanType->point;

            // check whether participant is exists by QR Code
            $participant = Participant::select('id', 'ticket_id', 'qr_code', 'name', 'email', 'phone', 'job_title', 'company', 'type', 'type_code', 'registration_number', 'total_point', 'doorprize_id', 'doorprize2_id', 'doorprize3_id')
                ->whereQrCode($requestValidated['qr_code'])
                ->first();

            if (!$participant) {
                throw new Exception('Participant not found', 404);
            }

            $participant->type = $participant->ticket->name;

            $cmsUser = CmsUser::with(['scanGroup'])->find(request('id_admin'));
            if ($scanType->scan_group_id != $cmsUser->scan_group_id) {
                throw new Exception('This admin doesn\'t have permission to scan!', 404);
            }

            $scanHistory = ScanHistory::whereParticipantId($participant->id)
                ->where('scan_type_id', $requestValidated['scan_type_id'])
                ->first();
            if ($scanHistory && $participant->email !== 'demo@eventy.id') {
                throw new Exception('You have been scanned.', 422);
            }

            $doorprize_name = '';
            if ($scanType->type === 'Pickup') {
                if ($scanType->doorprize_type === 'Doorprize 1') {
                    $doorprize = Doorprize::find($participant->doorprize_id);
                } else if ($scanType->doorprize_type === 'Doorprize 2') {
                    $doorprize = Doorprize::find($participant->doorprize2_id);
                } else {
                    $doorprize = Doorprize::find($participant->doorprize3_id);
                }

                if (!$doorprize) {
                    throw new Exception('You don\'t have a doorprize', 403);
                }

                $doorprize_name = $doorprize->name ?: '';
            }

            if ($requestValidated['action'] === 'Save') {
                // check whether participant have been scanned
                if ($scanType->type != 'Point') {
                    $scanHistory = ScanHistory::whereParticipantId($participant->id)
                        ->where('scan_type_id', $requestValidated['scan_type_id'])
                        ->where(function ($query) use ($scanType) {
                            if ($scanType->type != 'Pickup') {
                                $query->where('date', d());
                            }
                        })
                        ->first();
                }

                // update last login & point
                $participant->last_login = dateTime();
                $participant->total_point = $participant->total_point += $point;

                if ($scanType->type === 'Pickup') {
                    if ($scanType->doorprize_type === 'Doorprize 1') {
                        $participant->is_pickup_doorprize = 1;
                    } else if ($scanType->doorprize_type === 'Doorprize 2') {
                        $participant->is_pickup_doorprize2 = 1;
                    } else {
                        $participant->is_pickup_doorprize3 = 1;
                    }
                }

                $participant->save();

                // create scan history
                $scanHistory = new ScanHistory();
                $scanHistory->scan_type_id = $requestValidated['scan_type_id'];
                $scanHistory->participant_id = $participant->id;
                $scanHistory->cms_user_id = $cmsUser->id;
                $scanHistory->date = d();
                $scanHistory->point_added = $point;
                if ($scanType->type === 'Pickup') {
                    $scanHistory->doorprize_id = $doorprize->id;
                }
                $scanHistory->save();
            }

            DB::commit();

            $participant->load(['history' => function ($query) use ($scanType) {
                $query->whereScanTypeId($scanType->id)->whereDate('created_at', d());
            }]);
            $participant->doorprize_name = $doorprize_name;
            $participant->day1_visit = ScanHistory::whereParticipantId($participant->id)->whereDate('created_at', '2023-08-25')->value('created_at');

            return ApiResponse::success($participant, 'Success Scan');
        } catch (Exception $e) {
            DB::rollBack();
            return ApiResponse::failed($e);
        }
    }

}
