<?php

namespace App\Http\Controllers;

use App\Helpers\Eventy;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Registration\TransactionConfirmationController;
use App\Models\Participant;
use App\Models\ParticipantSchedule;
use App\Models\Transaction;
use App\Models\TransactionConfirmation;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class AdminTransactionConfirmationController extends CBController
{
    public function getChangeStatus()
    {
        $transaction = Transaction::with(['participant', 'transactionTicket'])->whereInvoiceCode(g('invoice'))->first();

        $statusCheck = [Transaction::STATUS_SUCCESS, Transaction::STATUS_FAILED];
        if (in_array($transaction->status, $statusCheck)) {
            CRUDBooster::redirectBack('Transaction has been ' . str_replace('_', ' ', $transaction->status) . ' !. Please make new transaction.');
        }

        DB::transaction(function () use ($transaction) {
            $participant = $transaction->participant;
            $transactionTicket = $transaction->transactionTicket;

            $manual = TransactionConfirmation::find(g('id'));
            $manual->status = g('status');
            $manual->save();

            // if payment manual is approved
            if ($manual->status === TransactionConfirmation::STATUS_APPROVED) {

                // change payment status to Success
                $transaction->status = Transaction::STATUS_SUCCESS;
                $transaction->save();

                // save participant
                if (!$participant->isActiveStatus()) {
                    $participant->ticket_id = $transactionTicket->ticket_id;
                    $participant->status = Participant::STATUS_ACTIVE;
                    $participant->save();
                }

                // save schedule (classroom)
                ParticipantSchedule::saveRegistration($transaction->id, $participant->id);

                // data
                $qr = Eventy::QRCode($participant->qr_code);
                $mail['event_name'] = preferGet('name_events');
                $mail['event_start'] = date('F jS, Y', strtotime(preferGet('date_start_events')));
                $mail['event_email'] = preferGet('contact_email_events');
                $mail['event_logo'] = preferGet('logo_events');
                $mail['contact_name_1'] = preferGet('contact_name_1');
                $mail['contact_phone_1'] = preferGet('contact_phone_1');
                $mail['contact_name_2'] = preferGet('contact_name_2');
                $mail['contact_phone_2'] = preferGet('contact_phone_2');
                $mail['participant'] = $participant;
                $mail['qr_code'] = $qr;
                $mail['email'] = $participant->email;
                $mail['type'] = $participant->type;
                $mail['type_code'] = $participant->type_code;
                $mail['registration_number'] = $participant->registration_number;
                $mail['payment'] = $transaction;
                $mail['order_id'] = $transaction->invoice_code;
                $mail['url'] = action([LoginController::class, 'index'], ['token' => encrypt($participant->email)]);

                // Postmark SMTP
                $participant->sendEmailRegistration($mail);
            }

            if ($manual->status === TransactionConfirmation::STATUS_REJECTED) {
                /**
                 * SEND EMAIL payment rejected
                 */
                // $redirect_url = action('FrontEnd\FrontPaymentController@getIndexFinishRegister', [
                // 	'key' => encrypt($this->payment->getId())
                // ]);

                $ticket = $transactionTicket->ticket;
                $ticket->quota_achieve = $ticket->quota_achieve - 1;
                $ticket->save();

                $redirect_url = action([TransactionConfirmationController::class, 'index'], [
                    'key' => encrypt($transaction->id),
                ]);

                // data
                $mail['event_name'] = preferGet('name_events');
                $mail['event_start'] = date('F d, Y', strtotime(preferGet('date_start_events')));
                $mail['event_email'] = preferGet('contact_email_events');
                $mail['event_logo'] = preferGet('logo_events');
                $mail['contact_name_1'] = preferGet('contact_name_1');
                $mail['contact_phone_1'] = preferGet('contact_phone_1');
                $mail['contact_name_2'] = preferGet('contact_name_2');
                $mail['contact_phone_2'] = preferGet('contact_phone_2');
                $mail['name'] = $participant->name;
                $mail['redirect_url'] = $redirect_url;

                // Postmark SMTP
                $participant->sendEmailRejectPayment($mail);
            }
        });

        CRUDBooster::redirectBack('Transaction has been ' . g('status') . ' !', 'success');
    }

    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "account_name";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = false;
        $this->button_bulk_action = false;
        $this->button_action_style = "button_icon";
        $this->button_add = false;
        $this->button_edit = false;
        $this->button_delete = false;
        $this->button_detail = false;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = false;
        $this->button_export = false;
        $this->table = "transaction_confirmations";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Invoice Code", "name" => "invoice_code"];
        $this->col[] = ["label" => "Image Payment", "name" => "picture", "image" => true];
        $this->col[] = ["label" => "Account Name", "name" => "account_name"];
        $this->col[] = ["label" => "Amount", "name" => "nominal", "callback" => function ($row) {
            return 'Rp' . number_format($row->nominal);
        }];
        $this->col[] = ["label" => "Transfer Date", "name" => "transfer_date"];
        $this->col[] = ["label" => "Status", "name" => "status", "callback" => function ($row) {
            if ($row->status === TransactionConfirmation::STATUS_APPROVED) {
                $btn = '<span style="cursor: default;" class="btn btn-sm btn-success">APPROVED</span>';
            }
            if ($row->status === TransactionConfirmation::STATUS_REJECTED) {
                $btn = '<span style="cursor: default;" class="btn btn-sm btn-danger">REJECTED</span>';
            }
            if ($row->status === TransactionConfirmation::STATUS_WAITING) {

                $btn = '
				<div class="dropdown">
					<button class="btn btn-sm btn-warning dropdown-toggle" type="button" data-toggle="dropdown">WAITING
				</button>
                    <div class="dropdown-menu">';

                $link = 'change-status?invoice=' . $row->invoice_code . '&id=' . $row->id . '&status=';
                $btn .= '
						<a class="dropdown-item" href="#" onclick="' . $this->DialogConfirm(CRUDBooster::mainpath($link . TransactionConfirmation::STATUS_APPROVED)) . '">Approved</a>
						<a class="dropdown-item" href="#" onclick="' . $this->DialogConfirm(CRUDBooster::mainpath($link . TransactionConfirmation::STATUS_REJECTED)) . '">Reject</a>';
                $btn .= '</div>
				</div>';
            }

            return $btn;
        }];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Transaction Id', 'name' => 'transaction_id', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'datatable' => 'transaction,id'];
        $this->form[] = ['label' => 'Participant Id', 'name' => 'participant_id', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'datatable' => 'participant,id'];
        $this->form[] = ['label' => 'Invoice Code', 'name' => 'invoice_code', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Account Name', 'name' => 'account_name', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Transfer Date', 'name' => 'transfer_date', 'type' => 'date', 'validation' => 'required|date', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Nominal', 'name' => 'nominal', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Picture', 'name' => 'picture', 'type' => 'upload', 'validation' => 'required|image|max:3000', 'width' => 'col-sm-10', 'help' => 'File types support : JPG, JPEG, PNG, GIF, BMP'];
        $this->form[] = ['label' => 'Status', 'name' => 'status', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Transaction Id","name"=>"transaction_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"transaction,id"];
        //$this->form[] = ["label"=>"Participant Id","name"=>"participant_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"participant,id"];
        //$this->form[] = ["label"=>"Invoice Code","name"=>"invoice_code","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Account Name","name"=>"account_name","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Transfer Date","name"=>"transfer_date","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
        //$this->form[] = ["label"=>"Nominal","name"=>"nominal","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Picture","name"=>"picture","type"=>"upload","required"=>TRUE,"validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
        //$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
