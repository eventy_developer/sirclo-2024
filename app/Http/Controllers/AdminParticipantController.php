<?php

namespace App\Http\Controllers;

use App\Helpers\Eventy;
use App\Http\Controllers\Auth\LoginController;
use App\Models\Participant;
use App\Models\ParticipantSchedule;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\TransactionConfirmation;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\imports\DefaultImportExcel;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class AdminParticipantController extends CBController
{
    public function getBypass()
    {
        $id = g('id');
        $participant = Participant::find($id);

        auth()->login($participant);

        return redirect('/home');
    }

    public function getChangeStatus()
    {
        $id = g('id');
        $status = g('status');

        // save participant
        $participant = Participant::find($id);
        $participant->status = $status;
        $participant->is_activated_manually = 1;
        $participant->save();

        // update participant relation data
        $this->updateParticipantData($participant);

        CRUDBooster::redirectBack('Successfully activated ' . $participant->name, 'success');
    }

    private function updateParticipantData(Participant $participant)
    {
        // change transaction status to Success
        $transaction = Transaction::whereParticipantId($participant->id)->latest('id')->first();
        $ticket = Ticket::find($transaction->transactionTicket->ticket_id);
        if ($transaction) {
            $transaction->status = Transaction::STATUS_SUCCESS;
            $transaction->save();

            $manual = TransactionConfirmation::findByInvoiceExist($transaction->invoice_code, TransactionConfirmation::STATUS_WAITING);
            if ($manual) {
                $manual->status = TransactionConfirmation::STATUS_APPROVED;
                $manual->save();
            }
        }

        // save schedule (classroom)
        ParticipantSchedule::saveRegistration($transaction->id, $participant->id);

        // data
        $qr = Eventy::QRCode($participant->qr_code);
        $mail['event_name'] = preferGet('name_events');
        $mail['event_start'] = date('F jS, Y', strtotime(preferGet('date_start_events')));
        $mail['event_email'] = preferGet('contact_email_events');
        $mail['event_logo'] = preferGet('logo_events');
        $mail['contact_name_1'] = preferGet('contact_name_1');
        $mail['contact_phone_1'] = preferGet('contact_phone_1');
        $mail['contact_name_2'] = preferGet('contact_name_2');
        $mail['contact_phone_2'] = preferGet('contact_phone_2');
        $mail['participant'] = $participant;
        $mail['qr_code'] = $qr;
        $mail['email'] = $participant->email;
        $mail['type'] = $participant->type;
        $mail['type_code'] = $participant->type_code;
        $mail['registration_number'] = $participant->registration_number;
        $mail['payment'] = $transaction;
        $mail['order_id'] = $transaction->invoice_code;
        $mail['url'] = action([LoginController::class, 'index'], ['token' => encrypt($participant->email)]);
//        // Postmark SMTP
        $participant->sendEmailRegistration($mail);
    }

    public function getResetDoorprize()
    {
        $id = g('id');

        // save participant
        $participant = Participant::find($id);
        $participant->doorprize_id = null;
        $participant->doorprize2_id = null;
        $participant->doorprize3_id = null;
        $participant->is_pickup_doorprize = 0;
        $participant->is_pickup_doorprize2 = 0;
        $participant->is_pickup_doorprize3 = 0;
        $participant->exclude_doorprize = 0;
        $participant->exclude_doorprize2 = 0;
        $participant->exclude_doorprize3 = 0;
        $participant->save();

        CRUDBooster::redirectBack('Successfully reset doorprize ' . $participant->name, 'success');
    }

    public function postDoImportChunk()
    {
        $this->cbLoader();
        $file_md5 = md5(request('file'));

        if (request('file') && request('resume') == 1) {
            $total = Session::get('total_data_import');
            $prog = intval(Cache::get('success_' . $file_md5)) / $total * 100;
            $prog = round($prog, 2);
            if ($prog >= 100) {
                Cache::forget('success_' . $file_md5);
            }

            return response()->json(['progress' => $prog, 'last_error' => Cache::get('error_' . $file_md5)]);
        }

        $select_column = Session::get('select_column');
        $select_column = array_filter($select_column);
        $table_columns = DB::getSchemaBuilder()->getColumnListing($this->table);

        $file = base64_decode(request('file'));
        $file = storage_path('app/' . $file);

        $import = new DefaultImportExcel();
        Excel::import($import, $file);

        $has_created_at = false;
        if (CRUDBooster::isColumnExists($this->table, 'created_at')) {
            $has_created_at = true;
        }

        $has_password = false;
        if (CRUDBooster::isColumnExists($this->table, 'password')) {
            $has_password = true;
        }

        foreach ($import->sheetData as $rows) {
            foreach ($rows as $value) {
                $a = [];
                foreach ($select_column as $sk => $s) {
                    $colname = $table_columns[$sk];

                    if (CRUDBooster::isForeignKey($colname)) {

                        //Skip if value is empty
                        if ($value[$s] == '') {
                            continue;
                        }

                        if (intval($value[$s])) {
                            $a[$colname] = $value[$s];
                        } else {
                            $relation_table = CRUDBooster::getTableForeignKey($colname);
                            $relation_moduls = DB::table('cms_moduls')->where('table_name', $relation_table)->first();

                            $relation_class = __NAMESPACE__ . '\\' . $relation_moduls->controller;
                            if (!class_exists($relation_class)) {
                                $relation_class = '\App\Http\Controllers\\' . $relation_moduls->controller;
                            }
                            $relation_class = new $relation_class;
                            $relation_class->cbLoader();

                            $title_field = $relation_class->title_field;

                            $relation_insert_data = [];
                            $relation_insert_data[$title_field] = $value[$s];

                            if (CRUDBooster::isColumnExists($relation_table, 'created_at')) {
                                $relation_insert_data['created_at'] = date('Y-m-d H:i:s');
                            }

                            try {
                                $relation_exists = DB::table($relation_table)->where($title_field, $value[$s])->first();
                                if ($relation_exists) {
                                    $relation_primary_key = $relation_class->primary_key;
                                    $relation_id = $relation_exists->$relation_primary_key;
                                } else {
                                    $relation_id = DB::table($relation_table)->insertGetId($relation_insert_data);
                                }

                                $a[$colname] = $relation_id;
                            } catch (Exception $e) {
                                exit($e);
                            }
                        } //END IS INT

                    } else {
                        $a[$colname] = $value[$s];
                    }
                }

                $has_title_field = true;
                foreach ($a as $k => $v) {
                    if ($k == $this->title_field && $v == '') {
                        $has_title_field = false;
                        break;
                    }
                }

                if ($has_title_field == false) {
                    continue;
                }

                try {
                    // remove spaces
                    $a = array_map('trim', $a);

                    if ($has_created_at) {
                        $a['created_at'] = date('Y-m-d H:i:s');
                    }

                    if ($has_password && $a['password'] != '') {
                        $a['password'] = bcrypt($a['password']);
                    }else{
                        $a['password'] = Hash::make(123456);
                    }

                    $exist = DB::table($this->table)->where('email', $a['email'])->whereNull('deleted_at')->first();
                    if ($exist) {
                        continue;
                    }

                    // custom data
                    $a['email'] = strtolower($a['email']);
                    $a['ticket_id'] = 14;
                    $a['is_invitation'] = 1;
                    $a['is_admin'] = 0;
//                    $a['is_ceo'] = 0;
                    $a['is_feedback_filled'] = 0;
                    $a['is_activated_manually'] = 0;
                    $a['status'] = 'Active';

                    $id = DB::table($this->table)->insertGetId($a);

                    // update data
                    $qr_code = Eventy::hashString($id);
                    DB::table($this->table)->where('id', $id)->update([
                        'qr_code' => $qr_code
                    ]);

//                    Eventy::QRCode($qr_code);

                    Cache::increment('success_' . $file_md5);
                } catch (Exception $e) {
                    $e = (string)$e;
                    Cache::put('error_' . $file_md5, $e, 500);
                }
            }
        }

        return response()->json(['status' => true]);
    }

    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "name";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->button_add = true;
        $this->button_edit = true;
        $this->button_delete = true;
        $this->button_detail = true;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = true;
        $this->button_export = false;
        $this->table = "participants";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Status", "name" => "status", "callback" => function ($row) {
            switch ($row->status) {
                case 'Waiting Ticket':
                    $color = 'dark';
                    break;
                case 'Waiting Confirmation':
                    $color = 'primary';
                    break;
                case 'Waiting Payment':
                    $color = 'warning';
                    break;
                case 'Active':
                    $color = 'success';
                    break;
                case 'Blocked':
                    $color = 'danger';
                    break;
                default:
                    $color = 'info';
                    break;
            }

            $text = str_replace('_', ' ', $row->status);

            if ($text === 'Active' || $text === 'Waiting Ticket') return "<span class='btn btn-sm btn-$color' style='cursor: default;'>$text</span>";

            $link = 'change-status?id=' . $row->id . '&status=Active';
            $message = 'Be Careful. Payment will be automatically successful!';

            return '
				<div class="dropdown">
					<button class="btn btn-sm btn-' . $color . ' dropdown-toggle" type="button" data-toggle="dropdown">' . $text . '
				</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" onclick="' . $this->DialogConfirm(CRUDBooster::mainpath($link), $message) . '">Active</a>
                    </div>
				</div>';
        }];
        $this->col[] = ["label" => "Ticket Categories", "name" => "ticket_id", "join" => "tickets,label_categories"];
        $this->col[] = ["label" => "Ticket", "name" => "ticket_id", "join" => "tickets,name"];
        $this->col[] = ["label" => "Image", "name" => "image", "image" => true];
        $this->col[] = ["label" => "QR", "name" => "qr_code", "callback" => function ($row) {
            if ($row->qr_code) {
                $image = Eventy::QRCode($row->qr_code);
                return "<a data-lightbox='QRCode' title='QRCode' href='" . $image . "'>
                        <img src='" . $image . "' style='max-width: 50px;' alt='QR Code'>
                    </a>";
            }

            return "<a data-lightbox='Icon' title='Icon' href='" . preferGet('icon_events') . "'>
                        <img src='" . preferGet('icon_events') . "' style='max-width: 50px;' alt='Icon'>
                    </a>";
        }];
        $this->col[] = ["label" => "Code", "name" => "qr_code"];
        $this->col[] = ["label" => "Registration Number", "name" => "(CONCAT('SRL-', type_code, '-', registration_number)) as reg_number"];
        $this->col[] = ["label" => "Type", "name" => "type"];
        $this->col[] = ["label" => "Full Name", "name" => "name"];
        $this->col[] = ["label" => "Job title", "name" => "job_title"];
        $this->col[] = ["label" => "Company", "name" => "company"];
        $this->col[] = ["label" => "Business Type", "name" => "business_type"];
        $this->col[] = ["label" => "Email", "name" => "email"];
        $this->col[] = ["label" => "Phone", "name" => "phone"];
        $this->col[] = ["label" => "Nationality", "name" => "nationality"];
        $this->col[] = ["label" => "Area of Interest", "name" => "(SELECT GROUP_CONCAT(area) FROM participant_interests WHERE participant_id=participants.id) as area_of_interest"];
        $this->col[] = ["label" => "Total Point", "name" => "total_point"];
        if (CRUDBooster::isSuperadmin()) {
            $this->col[] = ["label" => "Reset Doorprize", "name" => "id", "callback" => function ($row) {
                $link = 'reset-doorprize?id=' . $row->id;
                $message = 'Be Careful. Doorprize will be reset!';

                return '<a class="btn btn-sm btn-danger" href="#" onclick="' . $this->DialogConfirm(CRUDBooster::mainpath($link), $message) . '">Reset Doorprize</a>';
            }];
            $this->col[] = ["label" => "Login", "name" => "id", "callback" => function ($row) {
                $link = 'bypass?id=' . $row->id;

                return '
                    <a class="btn btn-sm btn-warning" href="#" onclick="' . $this->DialogConfirm(CRUDBooster::mainpath($link), 'Bypass Login?') . '">Login</a>
                ';
            }];
        }
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Ticket', 'name' => 'ticket_id', 'type' => 'select2', 'validation' => '', 'width' => 'col-sm-10', 'datatable' => 'tickets,name'];
        $this->form[] = ['label' => 'Image', 'name' => 'image', 'type' => 'upload', 'validation' => 'max:500', 'width' => 'col-sm-10', 'help' => 'File types support : JPG, JPEG, PNG'];
        $this->form[] = ['label' => 'Name', 'name' => 'name', 'type' => 'text', 'validation' => 'required|string|min:3|max:150', 'width' => 'col-sm-10', 'placeholder' => 'You can only enter the letter only'];
        $this->form[] = ['label' => 'Email', 'name' => 'email', 'type' => 'email', 'validation' => 'required|min:3|max:255|email|unique:participants', 'width' => 'col-sm-10', 'placeholder' => 'Please enter a valid email address'];
        $this->form[] = ['label' => 'Phone', 'name' => 'phone', 'type' => 'number', 'validation' => 'numeric', 'width' => 'col-sm-10', 'placeholder' => 'You can only enter the number only'];
        $this->form[] = ['label' => 'Job Title', 'name' => 'job_title', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Company', 'name' => 'company', 'type' => 'text', 'validation' => 'min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Type of Business', 'name' => 'business_type', 'type' => 'select2', 'validation' => 'required', 'width' => 'col-sm-10', 'dataenum' => 'Enterprise Brands / Principal;Local Brands / UMKM;E-Commerce;Government / BUMN;Information Technology;Transportation / Logistics;Media & Advertising;Finance;Others'];
        if (in_array(request()->segment(3), ['edit', 'edit-save'])) {
            $helpText = 'Please leave empty if you did not change the password.';
        } else {
            $helpText = 'If you leave the password. The default password is 123456';
        }
        $this->form[] = ['label' => 'Password', 'name' => 'password', 'type' => 'password', 'validation' => 'min:3|max:32', 'width' => 'col-sm-10', 'help' => $helpText];
        if (in_array(request()->segment(3), ['edit', 'edit-save'])) {
            $this->form[] = ['label' => 'Status', 'name' => 'status', 'type' => 'select2', 'validation' => 'required', 'width' => 'col-sm-10', 'dataenum' => 'Waiting Ticket;Waiting Payment;Waiting Confirmation;Active'];
        }
        $this->form[] = ['label' => 'Set as Admin?', 'name' => 'is_admin', 'type' => 'radio', 'validation' => 'required|integer', 'width' => 'col-sm-10', 'dataenum' => '0|No;1|Yes'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Ticket Id","name"=>"ticket_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"ticket,id"];
        //$this->form[] = ["label"=>"Image","name"=>"image","type"=>"upload","required"=>TRUE,"validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
        //$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
        //$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:participants","placeholder"=>"Please enter a valid email address"];
        //$this->form[] = ["label"=>"Password","name"=>"password","type"=>"password","required"=>TRUE,"validation"=>"min:3|max:32","help"=>"Minimum 5 characters. Please leave empty if you did not change the password."];
        //$this->form[] = ["label"=>"Phone","name"=>"phone","type"=>"number","required"=>TRUE,"validation"=>"required|numeric","placeholder"=>"You can only enter the number only"];
        //$this->form[] = ["label"=>"Company","name"=>"company","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Is Invitation","name"=>"is_invitation","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
        //$this->form[] = ["label"=>"Is Admin","name"=>"is_admin","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
        //$this->form[] = ["label"=>"Is Feedback Filled","name"=>"is_feedback_filled","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
        //$this->form[] = ["label"=>"Is Activated Manually","name"=>"is_activated_manually","type"=>"radio","required"=>TRUE,"validation"=>"required|integer","dataenum"=>"Array"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();
        $this->sub_module[] = ['label' => 'Area of Interests', 'path' => 'participant-interest', 'parent_columns' => 'name,email', 'foreign_key' => 'participant_id', 'button_color' => 'warning'];


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */

        $this->script_js = NULL;
        $this->script_js .= "
			$('.table th[data-column-name]').hide();
            $('.table td[data-column-name]').hide();

            $('.table th[data-column-name=name]').show();
            $('.table td[data-column-name=name]').show();

            $('.table th[data-column-name=email]').show();
            $('.table td[data-column-name=email]').show();

            $('.table th[data-column-name=company]').show();
            $('.table td[data-column-name=company]').show();

            $('.table th[data-column-name=job_title]').show();
            $('.table td[data-column-name=job_title]').show();

            $('.table th[data-column-name=password]').show();
            $('.table td[data-column-name=password]').show();
		";

        $this->script_js .= "
			$(function () {
				$(document).find('#export-data select[name=fileformat] option[value=pdf]').remove();
				$(document).find('#export-data .toggle_advanced_report').parent().remove();
			});
		";


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */

        $this->style_css = NULL;
        $this->style_css .= "
            #table_dashboard.table-bordered tbody tr td:nth-child(6) {
                white-space: nowrap;
            }
        ";


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        if (g('type') === 'web') {
            $query->where('is_invitation', 0);
        } else if (g('type') === 'invitation') {
            $query->where('is_invitation', 1);
        } else if (g('type') === 'admin') {
            $query->where('is_invitation', 1)
                ->where('is_admin', 1);
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        $postdata['status'] = Participant::STATUS_ACTIVE;
        $postdata['is_invitation'] = 1;
        $postdata['is_feedback_filled'] = 0;
        $postdata['is_activated_manually'] = 0;
        $postdata['email'] = trim(strtolower($postdata['email']));
        $postdata['password'] = trim($postdata['password']) ?: Hash::make(123456);
        $fullName = $postdata['name'];
        $names = explode(' ', $fullName);
        if (count($names) > 1) {
            $postdata['first_name'] = $names[0];
            $postdata['last_name'] = $names[1];
        } else {
            $postdata['first_name'] = $fullName;
            $postdata['last_name'] = '-';
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        $participant = Participant::find($id);
        $participant->qr_code = Eventy::hashString($participant->id);
        $participant->save();
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        $email = trim(strtolower($postdata['email']));
        $postdata['email'] = $email;

        $participant = Participant::find($id);
        $participant->qr_code = Eventy::hashString($participant->id);
        $participant->save();

        $fullName = $postdata['name'];
        $names = explode(' ', $fullName);
        if (count($names) > 1) {
            $postdata['first_name'] = $names[0];
            $postdata['last_name'] = $names[1];
        } else {
            $postdata['first_name'] = $fullName;
            $postdata['last_name'] = '-';
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        Participant::deleteRelationById($id);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
