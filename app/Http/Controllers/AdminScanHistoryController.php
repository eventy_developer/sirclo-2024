<?php

namespace App\Http\Controllers;

use App\Models\CmsUser;
use App\Models\ScanGroup;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class AdminScanHistoryController extends CBController
{
    public $cmsUser;

    public function cbInit()
    {
        $this->cmsUser = CmsUser::find(CRUDBooster::myId());

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "id";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = CRUDBooster::isSuperadmin();
        $this->button_bulk_action = CRUDBooster::isSuperadmin();
        $this->button_action_style = "button_icon";
        $this->button_add = false;
        $this->button_edit = false;
        $this->button_delete = CRUDBooster::isSuperadmin();
        $this->button_detail = false;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = false;
        $this->button_export = true;
        $this->table = "scan_histories";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Scan Type", "name" => "(SELECT CONCAT(scan_groups.name, ' - ', scan_types.name) FROM scan_types INNER JOIN scan_groups ON scan_groups.id=scan_types.scan_group_id WHERE scan_types.id = scan_histories.scan_type_id) as scan_type_name"];
        $this->col[] = ["label" => "Staff", "name" => "cms_user_id", "join" => "cms_users,name"];
        if (CRUDBooster::myPrivilegeId() != CmsUser::BOOTH_PRIVILEGE_ID) {
            $this->col[] = ["label" => "Doorprize", "name" => "doorprize_id", "join" => "doorprizes,name"];
        }
        $this->col[] = ["label" => "Registration Number", "name" => "(CONCAT('SRL-', participants.type_code, '-', participants.registration_number)) as reg_number"];
        if (CRUDBooster::myPrivilegeId() != CmsUser::BOOTH_PRIVILEGE_ID) {
            $this->col[] = ["label" => "Type", "name" => "participant_id", "join" => "participants,type"];
        }
        $this->col[] = ["label" => "Name", "name" => "participant_id", "join" => "participants,name"];
        $this->col[] = ["label" => "Email", "name" => "participant_id", "join" => "participants,email"];
        $this->col[] = ["label" => "Job title", "name" => "participant_id", "join" => "participants,job_title"];
        $this->col[] = ["label" => "Company", "name" => "participant_id", "join" => "participants,company"];
        $this->col[] = ["label" => "Nationality", "name" => "participant_id", "join" => "participants,nationality"];
        $this->col[] = ["label" => "Phone", "name" => "participant_id", "join" => "participants,phone"];
        $this->col[] = ["label" => "Point Added", "name" => "point_added"];
        $this->col[] = ["label" => "Date", "name" => "created_at"];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Participant Id', 'name' => 'participant_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'participant,id'];
        $this->form[] = ['label' => 'Cms User Id', 'name' => 'cms_user_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'cms_user,id'];
        $this->form[] = ['label' => 'Scan Type Id', 'name' => 'scan_type_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'scan_type,id'];
        $this->form[] = ['label' => 'Date', 'name' => 'date', 'type' => 'date', 'validation' => 'required|date', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Point Added', 'name' => 'point_added', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Participant Id","name"=>"participant_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"participant,id"];
        //$this->form[] = ["label"=>"Cms User Id","name"=>"cms_user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_user,id"];
        //$this->form[] = ["label"=>"Scan Type Id","name"=>"scan_type_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"scan_type,id"];
        //$this->form[] = ["label"=>"Date","name"=>"date","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
        //$this->form[] = ["label"=>"Point Added","name"=>"point_added","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;
        $this->script_js .= "
			$(function () {
				$(document).find('#export-data select[name=fileformat] option[value=pdf]').remove();
				$(document).find('#export-data .toggle_advanced_report').parent().remove();
			});
		";


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;
        $text = g('type') ? 'FILTER TYPE: ' . g('type') : 'FILTER TYPE';
        $text2 = g('scan_group_id') ? 'FILTER GROUP: ' . g('name') : 'FILTER GROUP';
        $this->pre_index_html .= '<div class="d-flex">';
        $this->pre_index_html .= '
            <div class="dropdown mb-3 mr-3">
                <button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'. $text2 .'
            </button>
                <div class="dropdown-menu" style="width: 300px;transform: translate3d(0px, 0px, 0px)!important;">';
                $el = '';
                $groups = ScanGroup::orderBy('sort')->get();
                foreach ($groups as $group) {
                    $el .= '<a class="dropdown-item" href="' . CRUDBooster::mainpath('?type=' . g('type') . '&scan_group_id=' . $group->id . '&name=' . $group->name) . '">'. $group->name .'</a>';
                }
                $this->pre_index_html .= $el;
                $this->pre_index_html .= '</div>
            </div>
            <div class="dropdown mb-3">
					<button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'. $text .'
				</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?type=Checkin&scan_group_id=' . g('scan_group_id') . '&name=' . g('name')) . '">Check-in Visit</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?type=Point&scan_group_id=' . g('scan_group_id') . '&name=' . g('name')) . '">Point Activity</a>
                        ';
                if (CRUDBooster::myPrivilegeId() != CmsUser::BOOTH_PRIVILEGE_ID) {
                    $this->pre_index_html .= '<a class="dropdown-item" href="' . CRUDBooster::mainpath('?type=Pickup&scan_group_id=' . g('scan_group_id') . '&name=' . g('name')) . '">Pengambilan Doorprize</a>';
                }
        $this->pre_index_html .='
                    </div>
				</div>
        ';

        $this->pre_index_html .= '</div>';


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;
        $this->style_css .= "
            .dropdown .dropdown-menu {
                transform: translate3d(0px, 0px, 0px)!important;
            }
        ";


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        $query->join('scan_types', 'scan_types.id', '=', 'scan_histories.scan_type_id');

        if (CRUDBooster::myPrivilegeId() == CmsUser::BOOTH_PRIVILEGE_ID) {
            $query->where('scan_types.scan_group_id', $this->cmsUser->scan_group_id);
        }

        if (g('type')) {
            $query->where('scan_types.type', g('type'));
        }

        if (g('scan_group_id')) {
            $query->where('scan_types.scan_group_id', g('scan_group_id'));
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
