<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\Booth;
use App\Models\BoothCategory;
use Exception;
use Illuminate\Http\JsonResponse;

class BoothController extends Controller
{
    public function index()
    {
        return view('web.Booth.booth');
    }

    public function show()
    {
        $data['drawerButtons'] = [
            [
                'icon' => asset('images/icon/icon_about.svg'),
                'text' => 'About',
            ],
            [
                'icon' => asset('images/icon/icon_product.svg'),
                'text' => 'Product',
            ],
            [
                'icon' => asset('images/icon/icon_attachment.svg'),
                'text' => 'Attachment',
            ],
        ];

        return view('web.Booth.detail', $data);
    }

    public function boothList(): JsonResponse
    {
        try {
            $item = BoothCategory::with(['booths'])->get();

            return $this->successResponse($item);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }

    public function boothFind(): JsonResponse
    {
        try {
            $item = Booth::with(['attachments', 'productCategories.products'])->find(request('booth_id'));

            return $this->successResponse($item);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }
}
