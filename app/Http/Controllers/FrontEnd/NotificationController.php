<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationDetail;
use Exception;
use Illuminate\Http\JsonResponse;

class NotificationController extends Controller
{
    /**
     * notification list
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $data = Notification::with('detail')
                ->whereHas('detail', function ($query) {
                    $query->whereParticipantId(gSession('id'));
                })
                ->latest('send_at')
                ->get();

            return ApiResponse::success($data);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }

    /**
     * read notification
     *
     * @return JsonResponse
     */
    public function update(): JsonResponse
    {
        try {
            $data = NotificationDetail::whereParticipantId(gSession('id'))
                ->update([
                    'updated_at' => dateTime(),
                    'is_read' => 1
                ]);

            return ApiResponse::success($data);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }
}
