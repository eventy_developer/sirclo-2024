<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Models\Participant;
use App\Models\ParticipantSchedule;
use App\Models\SponsorCategory;
use App\Models\TicketSchedule;
use App\Models\Transaction;
use App\Models\TransactionDetail;

class LobbyController extends Controller
{
    public function index()
    {
        $data['drawerButtons'] = [['icon' => asset('images/icon/icon_schedule.svg'), 'text' => 'Schedule',], ['icon' => asset('images/icon/icon_boothlist.svg'), 'text' => 'Booth List',], ['icon' => asset('images/icon/icon_sponsor.svg'), 'text' => 'Sponsor',],];
        $data['sponsors'] = SponsorCategory::with(['sponsors'])->get();

        return view('web.Lobby.lobby', $data);
    }

    public function home()
    {
        // generate image qr code
        Eventy::QRCode(gSession('qr_code'));

        $participant_schedule_ids = ParticipantSchedule::select('schedule_id')->whereParticipantId(request('participant')->id)->pluck('schedule_id')->toArray();
        $ticket_id = request('participant')->ticket_id;
        $is_activated_manually = (request('participant')->is_activated_manually == 1);
        $has_payment = (request('latestTransaction')->id != '');
        $can_add_classroom = !$is_activated_manually && $has_payment && (TicketSchedule::whereTicketId($ticket_id)->whereNotIn('schedule_id', $participant_schedule_ids)->count() > 0) && (gSession('is_admin') == 0);

        $data = [];

        $participantCheck = Participant::find(auth()->id());

        if ($participantCheck->type == 'Group') {

            $participant = Participant::with(['ticket', 'schedules.schedule.location', 'transactions', 'histories.scanType.scanGroup'])->find($participantCheck->id);

            $lastTransaction = TransactionDetail::query()->where('participant_id', $participantCheck->id)->latest('id')->first();

            $participant->transactions = Transaction::query()->where('id', $lastTransaction->transaction_id)->get();


            $data['participant'] = $participant;

        } else {
            $data['participant'] = Participant::with(['ticket', 'schedules.schedule.location', 'transactions' => function ($transactions) {
                $transactions->latest('id');
            }, 'histories.scanType.scanGroup'])->find(auth()->id());
        }

        $data['can_add_classroom'] = $can_add_classroom;

        return view('web.Home.home', $data);
    }

    public function downloadQr($qr)
    {
        return response()->download('qrcode/' . $qr . '.png');
    }
}
