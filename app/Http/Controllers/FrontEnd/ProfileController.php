<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\ApiResponse;
use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Participant;
use Exception;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function update(ProfileUpdateRequest $profileUpdateRequest)
    {
        try {
            $request = $profileUpdateRequest->validated();

            $participant = Participant::find(request('participant_id'));
            $message = 'Success';
            $data = [];

            if ($request['image']) {
                Eventy::removeFileS3($participant->image);
                $upload = Eventy::uploadFileS3('image', 'profile/' . request('participant_id'));
                if ($upload) {
                    $participant->image = $upload;
                    $message = __('eventy/website_config.res_profile_photo_success_updated');
                    $data['image'] = getS3EndpointFile($upload);
                }
            }

            if ($request['current_password'] && $request['new_password']) {
                if (!Hash::check($request['current_password'], $participant->password)) {
                    throw new Exception(__('eventy/website_config.res_current_password_wrong'), 422);
                }

                $participant->password = Hash::make($request['new_password']);
                $message = __('eventy/website_config.res_password_success_updated');
            }

            $participant->save();


            return ApiResponse::success($data, $message);
        } catch (Exception $e) {
            return ApiResponse::failed($e);
        }
    }
}
