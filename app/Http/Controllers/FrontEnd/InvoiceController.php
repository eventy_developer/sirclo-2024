<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class InvoiceController extends Controller
{
    public function index()
    {
        $key = request('key');
        if ($key) {
            try {
                $transaction_id = decrypt($key);
                $transaction = Transaction::find($transaction_id['id']);
                $transaction->load(['transactionDetails.participant', 'transactionTicket.ticket']);

                if ($transaction->isEmpty()) {
                    abort(404);
                }

                $file_name = "Invoice " . $transaction->invoice_code . ' - ' . preferGet('name_events');
                $data = [];
                $data['transaction'] = $transaction;
                $data['file_name'] = $file_name;
                $data['color_primary'] = preferGet('color_primary');

                $pdf = App::make('dompdf.wrapper');
                $pdf->loadHTML(view('web.PDF.invoice', $data)->render());
                $pdf->setPaper('A4', 'portrait');

                return $pdf->stream("$file_name.pdf");
            } catch (Exception $e) {
                abort(404);
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }
    }
}
