<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\ScheduleSession;
use Exception;
use Illuminate\Http\JsonResponse;

class ScheduleController extends Controller
{
    /**
     * get schedule list
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $type = request('type');

            $dates = ScheduleSession::select('date')->groupBy(['date'])->oldest('date')->get();
            $item = ScheduleSession::with(['schedule.location', 'speakers', 'attachments'])
                ->oldest('time_start')
                ->get()
                ->filter(function ($item) use ($type) {
                    if ($type) {
                        return $item->schedule->location->name === $type;
                    }

                    return $item;
                })
                ->values();

            $data['dates'] = $dates;
            $data['item'] = $item;

            return $this->successResponse($data);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * post find Schedule
     *
     * @return JsonResponse
     */
    public function show(): JsonResponse
    {
        try {
            $item = ScheduleSession::with(['attachments', 'speakers'])->find(request('id'));

            return $this->successResponse($item);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * post send message
     *
     * @return JsonResponse
     */
    public function sendMessage(): JsonResponse
    {

        try {
            // * send message

            return $this->successResponse();
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace());
        }
    }
}
