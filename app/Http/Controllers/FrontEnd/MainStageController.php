<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainStageController extends Controller
{
    public function index()
    {
        $data['drawerButtons'] = [
            [
                'icon' => asset('images/icon/icon_schedule.svg'),
                'text' => 'Schedule',
            ],
            [
                'icon' => asset('images/icon/icon_attachment.svg'),
                'text' => 'Attachment',
            ],
        ];

        return view('web.MainStage.mainstage', $data);
    }
}
