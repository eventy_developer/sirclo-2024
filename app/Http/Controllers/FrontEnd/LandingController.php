<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\ScheduleLocation;
use App\Models\ScheduleSession;
use App\Models\Speaker;
use App\Models\SponsorCategory;

class LandingController extends Controller
{
    public function index()
    {
        if (auth()->guest()) {
            return redirect('/register');
        }

        return redirect('/home');

        date_default_timezone_set(preferGet('timezone_events'));
        $diffDateSeconds = strtotime(preferGet('date_start_events')) - time();

        $data['banners'] = Banner::whereType('Image')->orderBy('sort')->get();
        $data['youtube'] = Banner::whereType('Youtube')->first();
        $data['speakers'] = Speaker::orderBy('sort')->get();
        $data['sponsor_categories'] = SponsorCategory::with(['sponsors'])->orderBy('sort')->get();
        $data['numberOfCountdown'] = $diffDateSeconds;
        $data['schedule_sessions'] = ScheduleSession::with(['schedule.location', 'speakers'])->get();
        $data['locations'] = ScheduleLocation::pluck('name');

        return view('web.LandingPage.landing-page', $data);
    }
}
