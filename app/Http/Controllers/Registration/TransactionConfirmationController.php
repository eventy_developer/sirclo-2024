<?php

namespace App\Http\Controllers\Registration;

use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Models\Participant;
use App\Models\Transaction;
use App\Models\TransactionConfirmation;
use App\Services\TransactionService;
use Exception;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\DB;

class TransactionConfirmationController extends Controller
{
    public function index()
    {
        $key = request('key');

        $data['now'] = dateTime();
        if ($key) {
            try {
                $id = decrypt($key);
                $transaction = Transaction::with(['participant'])->find($id);

                $data['transaction'] = $transaction;
                $data['invoice'] = $transaction->invoice_code;
                $data['name'] = $transaction->participant->name;
                $data['amount'] = $transaction->amount;
                $data['created_at'] = $transaction->created_at;
                $data['participant'] = $transaction->participant;
            } catch (DecryptException $e) {
                abort(404);
            }
        } else {
            return redirect()->back();
        }

        return view('web.Registration.payment-confirmation', $data);
    }

    public function store(TransactionService $transactionService)
    {
        DB::beginTransaction();
        try {
            $payment = Transaction::with(['participant'])->whereInvoiceCode(request('no_invoice'))->first();
            $manual = TransactionConfirmation::whereInvoiceCode(request('no_invoice'))->whereStatus('WAITING')->first();

            if (!$payment) {
                throw new Exception(__('eventy/payment_confirmation.res_invoice_not_found'), 404);
            } else if ($manual) {
                throw new Exception(__('eventy/payment_confirmation.res_payment_is_waiting'), 409);
            } else if ($payment->isSuccess()) {
                throw new Exception(__('eventy/payment_confirmation.res_payment_is_approved'), 409);
            } else {
                $paid_amount = str_replace(['Rp ', '$ ', '.', 'IDR '], '', request('paid_amount'));

                // create a new payment from bank transfer
                $transactionService->createNewBankTransferInvoice([
                    'transaction_id' => $payment->id,
                    'participant_id' => $payment->participant->id,
                    'invoice_code' => $payment->invoice_code,
                    'account_name' => request('account_name'),
                    'paid_date' => request('paid_date'),
                    'paid_amount' => $paid_amount,
                    'file' => Eventy::uploadFileS3('file', 'payment/' . date('Y-m')),
                ]);

                // change payment status to Waiting Confirmation
                $payment->status = Transaction::STATUS_WAITING_CONFIRMATION;
                $payment->save();

                // change participant status to Waiting Confirmation
                if (!$payment->participant->isActiveStatus()) {
                    $payment->participant->status = Participant::STATUS_WAITING_CONFIRMATION;
                    $payment->participant->save();
                }

                /**
                 * SEND EMAIL Waiting Confirmation
                 */
                $send = $payment->participant->sendEmailWaitingPayment([
                    'event_name' => preferGet('name_events'),
                    'event_logo' => preferGet('logo_events'),
                    'event_email' => preferGet('contact_email_events'),
                    'name' => $payment->participant->name,
                    'contact_name_1' => preferGet('contact_name_1'),
                    'contact_phone_1' => preferGet('contact_phone_1'),
                    'contact_name_2' => preferGet('contact_name_2'),
                    'contact_phone_2' => preferGet('contact_phone_2'),
                ]);

                if ($send['status'] == 1) {
                    DB::commit();

                    $response['message'] = __('eventy/payment_confirmation.res_confirmation_is_submitted');
                    $response['redirect_url'] = action([TransactionController::class, 'show'], [
                        'key' => encrypt($payment->id),
                    ]);
                } else {
                    throw new Exception('Failed to send email waiting', 500);
                }

                return $this->successResponse($response, $response['message'], 202);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    public function show()
    {
        try {
            $payment = Transaction::with(['participant'])->whereInvoiceCode(request('invoice'))->first();
            debug($payment);

            if (!$payment) {
                throw new Exception(__('eventy/payment_confirmation.res_invoice_not_found'), 404);
            } else if ($payment->isPaymentChanged()) {
                throw new Exception(__('eventy/payment_confirmation.res_invoice_expired'), 410);
            }

            $response['message'] = 'success';
            $response['payment'] = [
                'amount' => $payment->amount,
                'name' => $payment->participant->name,
                'created_at' => date('Y-m-d', strtotime($payment->created_at)),
            ];

            return $this->successResponse($response, $response['message']);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }
}
