<?php

namespace App\Http\Controllers\Registration;

use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Participant;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Exception;
use Illuminate\Http\Request;


class TicketController extends Controller
{
    public function index(Request $request)
    {
        $coupon = $request->input('coupon');

        if ($coupon != null) {
            // Here you can add the logic to validate the coupon and fetch the ticket accordingly
            $now = date('Y-m-d H:i:s');
            $ticketFree = Ticket::getDataWithClassroom('newTicketWithCoupon', '' , $coupon);
            $ticketDefault = Ticket::getDataWithClassroom();

            if ($ticketFree) {
                $couponFound = false;
                    if (isset($ticketFree->couponTicket)) {
                        // Assuming $thisCoupon is an array of coupon data
                            foreach ($ticketFree->couponTicket as $couponData) {
                                if ($couponData->code === $coupon) {
                                    $couponFound = true;
                                    if ($couponData->quota <= $couponData->quota_achieve) {
                                        return response()->json([
                                            'message' => 'Quota coupon full.',
                                            'ticket' => $ticketDefault
                                        ], 400);
                                    }

                                    if ($couponData->start_date > $now) {
                                        return response()->json([
                                            'message' => 'Coupon not available yet.',
                                            'ticket' => $ticketDefault
                                        ], 400);
                                    }

                                    if ($now > $couponData->end_date) {
                                        return response()->json([
                                            'message' => 'Coupon expired.',
                                            'ticket' => $ticketDefault
                                        ], 400);
                                    }
                                }
                            }
                    } else {
                        return response()->json([
                            'message' => 'Invalid coupon code.',
                            'ticket' => $ticketDefault
                        ], 400);
                    }

                if (!$couponFound) {
                    return response()->json([
                        'message' => 'Coupon not found.',
                        'ticket' => $ticketDefault
                    ], 400);
                }
            } else {
                return response()->json([
                    'message' => 'No tickets found.',
                    'ticket' => $ticketDefault
                ], 400);
            }

            $ticketFreeFinal = Ticket::getDataWithClassroom('newTicketWithCoupon', $ticketFree->id);

            return response()->json(['ticket' => $ticketFreeFinal]);

        }

        $method_changed = 'no';
        $payment_id = '';
        $amount = '';
        $initial_amount = '';
        $ticket_id = '';
        $schedule_ids = [];
        $ticket_schedule_ids = [];
        $participant_ids_group = [];

        if (g('method_changed')) {
            $key = g('method_changed');
            try {
                $param = decrypt($key);

                if ($param['id'] == gSession('id') && $param['is_payment_changed']) {
                    $payment = Transaction::with(['participant', 'transactionTicket'])
                        ->whereParticipantId($param['id'])->whereIn('status', ['PAYMENT_CHANGED', 'EXPIRED'])
                        ->latest('id')
                        ->first();

                    $ticket = Ticket::query()->firstWhere('id', $payment->transactionTicket->ticket_id);

                    if (!$payment || $ticket->quota <= $ticket->quota_achieve && $payment->status == 'EXPIRED') {
                        // reset participant if payment is null
                        $payment->participant->status = Participant::STATUS_WAITING_TICKET;
                        $payment->participant->save();

                        return redirect('/');
                    }

                    $method_changed = 'yes';
                    $payment_id = $payment->id;
                    $amount = $payment->amount;
                    $initial_amount = $payment->initial_amount;
                    $ticket_id = $payment->transactionTicket->ticket_id;
                    $schedule_ids = [];
                    $ticket_schedule_ids = TransactionDetail::whereTransactionId($payment_id)->pluck('ticket_schedule_id')->toArray();
                    $participant_ids_group = TransactionDetail::whereTransactionId($payment_id)->pluck('participant_id')->toArray();
                } else {
                    abort(400, 'Forbidden');
                }
            } catch (Exception $e) {
                abort(404);
            }
        }

        $data['method_changed'] = $method_changed;
        $data['payment_id'] = $payment_id;
        $data['amount'] = $amount;
        $data['initial_amount'] = $initial_amount;
        $data['ticket_id'] = $ticket_id;
        $data['schedule_ids'] = $schedule_ids;
        $data['ticket_schedule_ids'] = $ticket_schedule_ids;
        $data['participant_ids_group'] = $participant_ids_group;

        // Fetch tickets dynamically based on whether a coupon is provided or not
        $data['tickets'] = Ticket::getDataWithClassroom(); // Original logic to fetch tickets without coupon
        $data['countries'] = Country::all();

        $data['payment_methods'] = Eventy::getAvailablePaymentMethod();

        return view('web.Registration.ticket', $data);
    }

    public function findEmailExist(Request $request): \Illuminate\Http\JsonResponse
    {
        $email = $request->input('email');
        $check = Participant::where('email', $email)->whereNull('deleted_at')->first();
        if ($check) {
            return response()->json([
                'message' => 'Email already exist.',
                'data' => 0
            ]);
        }
        return response()->json([
            'message' => 'Email not found.',
            'data' => 1
        ]);
    }

    public function classroomAddition()
    {
        $latestTransaction = Transaction::whereParticipantId(gSession('id'))->latest('id')->first();

        if (!$latestTransaction->isSuccess() && !g('method_changed')) {
            return redirect()->back()->with([
                'message_title' => __('eventy/ticket.res_other_payment_exists_title'),
                'message_body' => __('eventy/ticket.res_other_payment_exists_body')
            ]);
        }

        $method_changed = 'no';
        $payment_id = '';
        $amount = '';
        $initial_amount = '';
        $ticket_id = gSession('ticket_id');
        $schedule_ids = [];
        $ticket_schedule_ids = [];

        if (g('method_changed')) {
            $key = g('method_changed');
            try {
                $param = decrypt($key);

                if ($param['id'] == gSession('id') && $param['is_payment_changed']) {
                    $payment = Transaction::with(['participant', 'transactionTicket'])
                        ->whereParticipantId($param['id'])->whereIn('status', ['PAYMENT_CHANGED', 'EXPIRED'])
                        ->latest('id')
                        ->first();

                    if (!$payment) {
                        // reset participant if payment is null
                        $payment->participant->status = Participant::STATUS_WAITING_TICKET;
                        $payment->participant->save();

                        return redirect('/');
                    }

                    $method_changed = 'yes';
                    $payment_id = $payment->id;
                    $amount = $payment->amount;
                    $initial_amount = $payment->initial_amount;
                    $schedule_ids = [];
                    $ticket_schedule_ids = TransactionDetail::whereTransactionId($payment_id)->pluck('ticket_schedule_id')->toArray();
                } else {
                    abort(400, 'Forbidden');
                }
            } catch (Exception $e) {
                abort(404);
            }
        }

        $data['method_changed'] = $method_changed;
        $data['payment_id'] = $payment_id;
        $data['amount'] = $amount;
        $data['initial_amount'] = $initial_amount;
        $data['ticket_id'] = $ticket_id;
        $data['schedule_ids'] = $schedule_ids;
        $data['ticket_schedule_ids'] = $ticket_schedule_ids;
        $data['tickets'] = Ticket::getDataWithClassroom('additionalTicket', $ticket_id);
        $data['payment_methods'] = Eventy::getAvailablePaymentMethod();

        return view('web.Registration.classroom-addition', $data);
    }
}
