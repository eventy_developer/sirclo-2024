<?php

namespace App\Http\Controllers\Registration;

use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontEnd\LobbyController;
use App\Http\Requests\StoreTransactionRequest;
use App\Models\CouponTicket;
use App\Models\Participant;
use App\Models\ParticipantSchedule;
use App\Models\Schedule;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\TransactionConfirmation;
use App\Services\TransactionService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends Controller
{
    public function store(StoreTransactionRequest $storeTransactionRequest, TransactionService $transactionService): JsonResponse
    {
        try {
            if (dateTime() >= preferGet('date_end_events')) {
                throw new Exception('Registration has closed!', 400);
            }

            $participant = Participant::whereEmail(request('email'))->first();
            $participant_ids_group_exist = request('participant_ids_group');

            $ticketId =  request('ticket_id');
            $ticket = Ticket::query()->firstWhere('id', $ticketId);

            $now = date('Y-m-d');

            if ($ticket->quota <= $ticket->quota_achieve){
                $participant->status = Participant::STATUS_WAITING_TICKET;
                $participant->ticket_id = null;
                $participant->type_code = null;
                $participant->save();
                $response['message'] = 'Quota Ticket has been full booked, your order will be reset!';
                $response['redirect_url'] = action([TicketController::class, 'index']);
                return $this->successResponse($response, $response['message'], Response::HTTP_BAD_REQUEST);
            }

            if ($now > $ticket->end_date){
                $participant->status = Participant::STATUS_WAITING_TICKET;
                $participant->ticket_id = null;
                $participant->type_code = null;
                $participant->save();
                $response['message'] = 'Ticket has been expired, your order will be reset!';
                $response['redirect_url'] = action([TicketController::class, 'index']);
                return $this->successResponse($response, $response['message'], Response::HTTP_BAD_REQUEST);
            }

            $transactionId = $transactionService->create($storeTransactionRequest->validated());

            $response['message'] = 'Thanks for confirmation';
            $response['redirect_url'] = action([TransactionController::class, 'show'], [
                'key' => encrypt($transactionId),
            ]);

            return $this->successResponse($response, $response['message'], Response::HTTP_CREATED);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    public function ticketFree(): JsonResponse
    {
        DB::beginTransaction();
        try {
            $schedule_ids = request('schedule_ids');
            $participant = Participant::whereEmail(request('email'))->first();

            // create schedules
            if (!empty($schedule_ids)) {
                foreach ($schedule_ids as $id) {
                    $schedule = Schedule::with(['location'])->find($id);

                    $participantSchedule = ParticipantSchedule::whereParticipantId($participant->id)->whereScheduleId($schedule->id)->firstOrNew();
                    $participantSchedule->participant_id = $participant->id;
                    $participantSchedule->schedule_id = $schedule->id;
                    $participantSchedule->save();

                    // update type participant
                    $participant->type = $schedule->location->name;
                    $participant->save();
                }
            }

            $participant->ticket_id = request('ticket_id');
            $ticket = Ticket::find($participant->ticket_id);
            $ticket->quota_achieve = $ticket->quota_achieve + 1;
            $ticket->save();

            $coupon = CouponTicket::query()->where('code', request('coupon'))->first();
            $participant->coupon_id = $coupon->id;
            $coupon->quota_achieve = $coupon->quota_achieve + 1;
            $coupon->save();

            $participant->status = Participant::STATUS_ACTIVE;
            $participant->qr_code = Eventy::hashString($participant->id);
            if (!$participant->registration_number) {
                $participant->registration_number = Eventy::registrationNumber();
                    $participant->type_code = $ticket->type_index_ticket;

            }
            $participant->save();

            // Database Table : participants
            Eventy::LogFile('Database Update : `participants`', 'logging', 'logging', [
                'id' => $participant->id,
                'email' => $participant->email,
                'status' => $participant->status,
            ]);

            // data
            $mail['event_name'] = preferGet('name_events');
            $mail['event_start'] = date('F d, Y', strtotime(preferGet('date_start_events')));
            $mail['event_email'] = preferGet('contact_email_events');
            $mail['contact_name_1'] = preferGet('contact_name_1');
            $mail['contact_phone_1'] = preferGet('contact_phone_1');
            $mail['contact_name_2'] = preferGet('contact_name_2');
            $mail['contact_phone_2'] = preferGet('contact_phone_2');
            $mail['name'] = $participant->name;
            $mail['qr_code'] = $participant->present_qr_code;
            $mail['email'] = $participant->email;
            $mail['type'] = $participant->type;
            $mail['type_code'] = $participant->type_code;
            $mail['registration_number'] = $participant->registration_number;

            $send = $participant->sendEmailRegistration($mail);

            DB::commit();

            $response['message'] = 'Thanks for registering';
            $response['redirect_url'] = action([LobbyController::class, 'home']);

            return $this->successResponse($response, $response['message'], Response::HTTP_CREATED);
        } catch (Exception $e) {
            DB::rollBack();

            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    public function update($id)
    {
        try {
            $payment = Transaction::with(['participant'])->find($id);

            if ($payment) {

                if ($payment->isWaitingPayment() || $payment->isPending() || $payment->isExpired()) {
                    // Log Payment
                    // TODO: Send platform code expired to eventy xendit

                    // set expired payment
                    $payment->status = Transaction::STATUS_PAYMENT_CHANGED;
                    $payment->save();

                    $url = '';
                    if (!$payment->participant->isActiveStatus()) {
                        // change participant status
                        $payment->participant->status = Participant::STATUS_PAYMENT_CHANGED;
                        $payment->participant->save();

                        $url = action([TicketController::class, 'index'], [
                            'method_changed' => encrypt([
                                'id' => $payment->participant->id,
                                'is_payment_changed' => $payment->isPaymentChanged(),
                            ]),
                        ]);
                    } else {
                        $url = action([TicketController::class, 'classroomAddition'], [
                            'method_changed' => encrypt([
                                'id' => $payment->participant->id,
                                'is_payment_changed' => $payment->isPaymentChanged(),
                            ]),
                        ]);
                    }

                    $data['url'] = $url;

                    $response['message'] = 'You will be redirected to the form payment method';
                    $response['data'] = $data;

                    return $this->successResponse($response, $response['message']);
                }

            } else {
                throw new Exception('Payment not found', 404);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    public function show()
    {
        try {
            if (!g('key')) {
                abort(404);
            }

            $key = decrypt(g('key'));

            $payment = Transaction::with(['participant'])->find($key);
            $payment->load(['transactionDetails.participant', 'transactionTicket.ticket']);

            if (!$payment) {
                return redirect('logout');
            }


            $url = '';
            if ($payment->type === 'Manual') {
                $url = url('confirmation?key=' . encrypt($payment->id));
                $method_readable = 'Bank Transfer';
            } else if ($payment->type === 'Cards') {
                $url = $payment->checkout_url;
                $method_readable = 'Credit Card';
            } else if ($payment->type === 'Virtual Accounts') {
                $url = $payment->checkout_url;
                $method_readable = 'Virtual Account';
            } else if ($payment->type === 'QR Codes') {
                $url = $payment->checkout_url;
                $method_readable = 'QRIS';
            } else {
                $method_readable = 'OVO';
            }

            $is_pending = false;
            $is_manual_have_confirm = false;
            if ($payment->type === 'Manual') {
                $is_manual_have_confirm = TransactionConfirmation::where('transaction_id', $payment->id)->count();

                $is_pending = $payment->isWaitingConfirmation();
                $latest = TransactionConfirmation::where('transaction_id', $payment->id)->latest()->first();
                if (in_array($latest->status, ['REJECTED'])) {
                    $is_pending = false;
                }
            }

            $data['payment'] = $payment;
            $data['payment_method'] = $payment->payment_method;
            $data['payment_type'] = $payment->type;
            $data['amount'] = ($payment->id ? number_format($payment->amount, 0, '.', ',') : '');
            $data['no_invoice'] = ($payment->id ? $payment->invoice_code : '');
            $data['virtual_account_number'] = $payment->virtual_account_number;
            $data['url'] = $url;
            $data['method_readable'] = $method_readable;
            $data['is_pending'] = $is_pending;
            $data['is_manual_have_confirm'] = $is_manual_have_confirm;
            $data['contact_name_1'] = preferGet('contact_name_1');
            $data['contact_phone_1'] = preferGet('contact_phone_1');
            $data['contact_name_2'] = preferGet('contact_name_2');
            $data['contact_phone_2'] = preferGet('contact_phone_2');
            $data['event_email'] = preferGet('contact_email_events');

            return view('web.Registration.finish', $data);
        } catch (Exception $e) {
            abort(404);
        }
    }
}
