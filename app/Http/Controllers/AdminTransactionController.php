<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;

class AdminTransactionController extends CBController
{

    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "id";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->button_add = false;
        $this->button_edit = false;
        $this->button_delete = false;
        $this->button_detail = true;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = false;
        $this->button_export = false;
        $this->table = "transactions";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Invoice Code", "name" => "invoice_code"];
        $this->col[] = ["label" => "Ticket", "name" => "(SELECT name FROM tickets JOIN transaction_ticket ON tickets.id = transaction_ticket.ticket_id WHERE transaction_id = transactions.id) as ticket"];
        $this->col[] = ["label" => "Name", "name" => "participant_id", "join" => "participants,name"];
        $this->col[] = ["label" => "Email", "name" => "participant_id", "join" => "participants,email"];
        $this->col[] = ["label" => "Phone", "name" => "participant_id", "join" => "participants,phone"];
        $this->col[] = ["label" => "Amount", "name" => "amount", "callback" => function ($row) {
            return 'Rp' . number_format($row->amount);
        }];
        $this->col[] = [
            "label" => "Method",
            "name" => "payment_method",
            "callback" => function ($row) {
                switch ($row->payment_method) {
                    case 'CREDIT_CARD':
                        $color = 'primary';
                        break;
                    case 'BANK_TRANSFER':
                        $color = 'info';
                        break;
                    case 'OVO':
                        $color = 'purple';
                        break;
                    default:
                        $color = 'warning';
                        break;
                }

                $text = str_replace('_', ' ', $row->payment_method);
                return "<span class='badge badge-$color' style='font-size: 10px;'>$text</span>";
            },
        ];
        $this->col[] = ["label" => "Status", "name" => "status", "callback" => function ($row) {
            switch ($row->status) {
                case 'WAITING_PAYMENT':
                    $color = 'default';
                    break;
                case 'WAITING_CONFIRMATION':
                    $color = 'primary';
                    break;
                case 'PENDING':
                    $color = 'warning';
                    break;
                case 'SUCCESS':
                    $color = 'success';
                    break;
                case 'FAILED':
                    $color = 'danger';
                    break;
                case 'PAYMENT_CHANGED':
                    $color = 'info';
                    break;
                default:
                    $color = 'expired';
                    break;
            }

            $text = str_replace('_', ' ', $row->status);
            return "<span class='badge badge-$color' style='font-size: 10px;'>$text</span>";
        }];
        $this->col[] = ["label" => "Date Submit", "name" => "created_at"];
        $this->col[] = ["label" => "Activated Manually?", "name" => "(CASE participants.is_activated_manually WHEN 1 THEN 'Yes' ELSE 'No' END) as is_activated_manually"];
        if (CRUDBooster::isSuperadmin()) {
            $this->col[] = ["label" => "Transaction Code", "name" => "transaction_code"];
            $this->col[] = ["label" => "Xendit Url", "name" => "checkout_url", "callback" => function($row) {
                return "<a href='$row->checkout_url' target='_blank'>$row->checkout_url</a>";
            }];
        }
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Name', 'name' => 'participant_id', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'datatable' => 'participant,id'];
        $this->form[] = ['label' => 'Invoice Code', 'name' => 'invoice_code', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Amount', 'name' => 'amount', 'type' => 'number', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Type', 'name' => 'type', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Payment Method', 'name' => 'payment_method', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Status', 'name' => 'status', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Transaction Code', 'name' => 'transaction_code', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Virtual Account Number', 'name' => 'virtual_account_number', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Checkout Url', 'name' => 'checkout_url', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Transaction Xendit Id', 'name' => 'transaction_xendit_id', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'transaction_xendit,id'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Participant Id","name"=>"participant_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"participant,id"];
        //$this->form[] = ["label"=>"Invoice Code","name"=>"invoice_code","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Amount","name"=>"amount","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
        //$this->form[] = ["label"=>"Type","name"=>"type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Payment Method","name"=>"payment_method","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Transaction Code","name"=>"transaction_code","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Virtual Account Number","name"=>"virtual_account_number","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Checkout Url","name"=>"checkout_url","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
        //$this->form[] = ["label"=>"Transaction Xendit Id","name"=>"transaction_xendit_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"transaction_xendit,id"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();
        $this->sub_module[] = ['label' => 'Confirmation', 'path' => 'transaction-confirmation', 'parent_columns' => 'invoice_code,amount,status', 'foreign_key' => 'transaction_id', 'button_color' => 'warning', 'button_icon' => 'fa fa-check', 'showIf' => '[payment_method] === "BANK_TRANSFER" AND [status] !== "PAYMENT_CHANGED"'];
        $this->sub_module[] = ['label' => 'Detail', 'path' => 'transaction-detail', 'parent_columns' => 'invoice_code,amount,status', 'foreign_key' => 'transaction_id', 'button_color' => 'primary', 'button_icon' => 'fa fa-th'];


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;
        $text = g('payment_method') ? 'PAYMENT METHOD: ' . str_replace('_', ' ', g('payment_method')) : 'FILTER PAYMENT METHOD';
        $this->pre_index_html .= '
            <div class="dropdown mb-3">
					<button class="btn btn-sm btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'. $text .'
				</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=BANK_TRANSFER') . '">BANK TRANSFER</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=CREDIT_CARD') . '">CREDIT CARD</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=QRIS') . '">QRIS</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=BCA') . '">BCA</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=BNI') . '">BNI</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=MANDIRI') . '">MANDIRI</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=BRI') . '">BRI</a>
                        <a class="dropdown-item" href="' . CRUDBooster::mainpath('?payment_method=PERMATA') . '">PERMATA</a>
                    </div>
				</div>
        ';


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = "
            .badge-purple,
			.badge-purple:hover {
				background-color: #4A328F;
				color: #fff;
			}
			.badge-expired,
			.badge-expired:hover {
				background-color: #8f3232;
				color: #fff;
			}
        ";


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        $query->whereNull('participants.deleted_at');

        if (g('payment_method')) {
            $query->where('payment_method', g('payment_method'));
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
