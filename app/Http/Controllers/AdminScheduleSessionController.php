<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminScheduleSessionController extends CBController
{
    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "title";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->button_add = true;
        $this->button_edit = true;
        $this->button_delete = true;
        $this->button_detail = true;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = false;
        $this->button_export = false;
        $this->table = "schedule_sessions";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
//        $this->col[] = ["label" => "Schedule", "name" => "schedule_id", "join" => "schedules,name"];
        $this->col[] = ["label" => "Image", "name" => "image", "image" => true];
        $this->col[] = ["label" => "Title", "name" => "title"];
//        $this->col[] = ["label" => "Description", "name" => "description", "str_limit" => 50];
        $this->col[] = ["label" => "Date", "name" => "date"];
        $this->col[] = ["label" => "Time Start", "name" => "time_start", "callback" => function ($row) {
            return date('H:i A', strtotime($row->time_start));
        }];
        $this->col[] = ["label" => "Time End", "name" => "time_end", "callback" => function ($row) {
            return date('H:i A', strtotime($row->time_end));
        }];
        $this->col[] = ["label" => "Platform Type", "name" => "platform_type"];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Schedule', 'name' => 'schedule_id', 'type' => 'hidden', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'datatable' => 'schedules,name'];
        $this->form[] = ['label' => 'Image', 'name' => 'image', 'type' => 'upload', 'validation' => 'max:100|mimes:' . config('eventy.image_types'), 'width' => 'col-sm-10', 'help' => 'File types support : ' . config('eventy.image_types') . ' with maximum size is 100KB'];
        $this->form[] = ['label' => 'Title', 'name' => 'title', 'type' => 'text', 'validation' => 'required|string|min:3|max:255', 'width' => 'col-sm-10', 'placeholder' => 'You can only enter the letter only'];
        $this->form[] = ['label' => 'Description', 'name' => 'description', 'type' => 'textarea', 'validation' => 'required|string|min:5|max:5000', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Date', 'name' => 'date', 'type' => 'date', 'validation' => 'required|date', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Time Start', 'name' => 'time_start', 'type' => 'time', 'validation' => 'required|date_format:H:i', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Time End', 'name' => 'time_end', 'type' => 'time', 'validation' => 'required|date_format:H:i', 'width' => 'col-sm-10'];
//        $this->form[] = ['label' => 'Type', 'name' => 'type', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'dataenum' => 'Main Stage;Classroom', 'value' => 'Main Stage'];
        $this->form[] = ['label' => 'Platform Type', 'name' => 'platform_type', 'type' => 'select2', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'dataenum' => 'Youtube;Zoom', 'value' => 'Youtube'];
        $this->form[] = ['label' => 'Youtube ID', 'name' => 'youtube_id', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10', 'help' => 'Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk'];
        $this->form[] = ['label' => 'Zoom Conference Id', 'name' => 'zoom_conference_id', 'type' => 'text', 'validation' => 'required|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Zoom Conference Password', 'name' => 'zoom_conference_password', 'type' => 'text', 'validation' => 'required|max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Zoom Api Key', 'name' => 'zoom_api_key', 'type' => 'text', 'validation' => 'max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Zoom Api Secret', 'name' => 'zoom_api_secret', 'type' => 'text', 'validation' => 'max:255', 'width' => 'col-sm-10'];
        $this->form[] = ['label' => 'Zoom Link', 'name' => 'zoom_link', 'type' => 'text', 'validation' => 'max:255', 'width' => 'col-sm-10'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Schedule Id","name"=>"schedule_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"schedule,id"];
        //$this->form[] = ["label"=>"Image","name"=>"image","type"=>"upload","required"=>TRUE,"validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
        //$this->form[] = ["label"=>"Title","name"=>"title","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
        //$this->form[] = ["label"=>"Description","name"=>"description","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
        //$this->form[] = ["label"=>"Date","name"=>"date","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
        //$this->form[] = ["label"=>"Time Start","name"=>"time_start","type"=>"time","required"=>TRUE,"validation"=>"required|date_format:H:i:s"];
        //$this->form[] = ["label"=>"Time End","name"=>"time_end","type"=>"time","required"=>TRUE,"validation"=>"required|date_format:H:i:s"];
        //$this->form[] = ["label"=>"Platform Type","name"=>"platform_type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Youtube Id","name"=>"youtube_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"youtube,id"];
        //$this->form[] = ["label"=>"Zoom Api Key","name"=>"zoom_api_key","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Zoom Api Secret","name"=>"zoom_api_secret","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Zoom Conference Id","name"=>"zoom_conference_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"zoom_conference,id"];
        //$this->form[] = ["label"=>"Zoom Conference Password","name"=>"zoom_conference_password","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        //$this->form[] = ["label"=>"Zoom Link","name"=>"zoom_link","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();
        $this->sub_module[] = [
            'label' => 'Attachments',
            'path' => 'schedule-attachment',
            'foreign_key' => 'schedule_session_id',
            'button_color' => 'primary',
            'button_icon' => 'fas fa-file',
            'parent_columns' => 'title',
        ];


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;
        $this->script_js .= "
			$('.input_date').datepicker({
				format: 'yyyy-mm-dd',
				language: 'lang'
			});
			$('.timepicker').timepicker({
				showInputs: true,
				showSeconds: false,
				showMeridian: false
			});

			let platform_type = $('#platform_type').val();

			if (platform_type === 'Youtube') {
				$('#form-group-youtube_id').show();
				$('#youtube_id').prop('required', true);

				$('#form-group-zoom_conference_id').hide();
				$('#zoom_conference_id').prop('required', false);
				$('#form-group-zoom_conference_password').hide();
				$('#zoom_conference_password').prop('required', false);
				$('#form-group-zoom_api_key').hide();
				$('#form-group-zoom_api_secret').hide();
				$('#form-group-zoom_link').hide();
			} else {
				$('#form-group-youtube_id').hide();
				$('#youtube_id').prop('required', false);

				$('#form-group-zoom_conference_id').show();
				$('#zoom_conference_id').prop('required', true);
				$('#form-group-zoom_conference_password').show();
				$('#zoom_conference_password').prop('required', true);
				$('#form-group-zoom_api_key').show();
				$('#form-group-zoom_api_secret').show();
				$('#form-group-zoom_link').show();
			}

			$('#platform_type').on('change', function(){
				if (this.value === 'Youtube') {
					$('#form-group-youtube_id').show();
					$('#youtube_id').prop('required', true);

					$('#form-group-zoom_conference_id').hide();
					$('#zoom_conference_id').prop('required', false);
					$('#form-group-zoom_conference_password').hide();
					$('#zoom_conference_password').prop('required', false);
					$('#form-group-zoom_api_key').hide();
					$('#form-group-zoom_api_secret').hide();
					$('#form-group-zoom_link').hide();
				} else {
					$('#form-group-youtube_id').hide();
					$('#youtube_id').prop('required', false);

					$('#form-group-zoom_conference_id').show();
					$('#zoom_conference_id').prop('required', true);
					$('#form-group-zoom_conference_password').show();
					$('#zoom_conference_password').prop('required', true);
					$('#form-group-zoom_api_key').show();
					$('#form-group-zoom_api_secret').show();
					$('#form-group-zoom_link').show();
				}
			});
		";



        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
