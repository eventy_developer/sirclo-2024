<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontEnd\LandingController;
use App\Models\Participant;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index()
    {
        return view('web.Auth.login');
    }

    public function store(): JsonResponse
    {
        $email = request('email');
        $password = request('password');

        try {
            $check = Participant::whereEmail($email)->with(['ticket'])->first();
            debug($check);
            $is_login_locked = (dateTime() < dateStartEvent()) && ($check->is_admin == 0) && isLoginLocked();

            if (!$check) {
                throw new Exception(__('eventy/login.res_login_not_found'), 404);
            } else {
                if (!Hash::check($password, $check->password)) {
                    throw new Exception(__('eventy/login.res_login_not_match'), 401);
                } else if ($check->isActiveStatus() && $check->ticket->id == '') {
                    throw new Exception(__('eventy/login.res_ticket_not_found'), 403);
                } else if ($check->isActiveStatus() && $is_login_locked) {
                    throw new Exception(preferGet('lock_login_wording_events'), 403);
                } else {
                    // create login session
                    auth()->login($check);

                    return $this->successResponse([], __('eventy/login.res_login_success'));
                }
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    public function destroy()
    {
        auth()->logout();

        session()->invalidate();
        session()->regenerateToken();

        return redirect()->action([RegisterController::class, 'index']);
    }
}
