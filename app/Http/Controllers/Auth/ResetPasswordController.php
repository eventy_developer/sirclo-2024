<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Participant;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    public function index()
    {
        $key = request('key');

        try {
            if (empty($key)) {
                abort(404);
            } else {
                $key = decrypt($key);

                if (new DateTime(date('Y-m-d H:i:s')) < new DateTime($key['expired_date'])) {
                    return view('web.Auth.reset');
                } else {
                    abort(404);
                }
            }
        } catch (Exception $e) {
            abort(404);
        }
    }

    public function update()
    {
        $key = request('key');
        $password = request('password');

        try {
            if (empty($key)) {
                throw new Exception(__('eventy/forgot.res_change_expired'), 410);
            } else {
                $key = decrypt($key);

                if (new DateTime(date('Y-m-d H:i:s')) < new DateTime($key['expired_date'])) {
                    $participant = Participant::find($key['id']);
                    $participant->password = Hash::make($password);
                    $participant->save();

                    $response['status'] = 1;
                    $response['message'] = __('eventy/forgot.res_change_success');

                    return $this->successResponse($response, $response['message']);
                } else {
                    throw new Exception(__('eventy/forgot.res_change_error'), 400);
                }
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }

    }
}
