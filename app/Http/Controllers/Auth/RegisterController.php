<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Registration\TicketController;
use App\Models\Country;
use App\Models\Participant;
use App\Models\ParticipantInterest;
use App\Models\Ticket;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    public function index()
    {
        $data['countries'] = Country::all();

        return view('web.Auth.register', $data);
    }

    public function store(): JsonResponse
    {
        DB::beginTransaction();
        try {
            if (dateTime() >= preferGet('date_end_events')) {
                throw new Exception('Registration has closed!', 400);
            }

            // check ticket
            $firstTicket = Ticket::first();
            if (!$firstTicket) {
                throw new Exception(__('eventy/register.res_ticket_not_found'), 404);
            }

            $check = Participant::whereEmail(request('email'))->first();
            if ($check) {
                if ($check->isBlocked()) {
                    throw new Exception(__('eventy/register.res_email_is_blocked'), 403);
                }

                throw new Exception(__('eventy/register.res_email_not_available'), 409);
            } else {
                $area_of_interests = request('area_of_interests');
                $phone = request('phone_prefix') . request('phone');

                $fullName = request('name');
                $names = explode(' ', $fullName);
                if (count($names) > 1) {
                    $lastName = array_pop($names);
                    $firstName = implode(' ', $names);
                } else {
                    $firstName = $fullName;
                    $lastName = '-';
                }


                $participant = new Participant();
                $participant->name = $fullName;
                $participant->first_name = $firstName;
                $participant->last_name = $lastName;
                $participant->email = request('email');
                $participant->password = Hash::make(request('password'));
                $participant->company = htmlspecialchars(request('company'));
                $participant->job_title = htmlspecialchars(request('job_title'));
                $participant->business_type = request('business_type');
                $participant->nationality = request('nationality');
                $participant->phone = $phone;
                $participant->save();

                // save area of interests
                foreach ($area_of_interests as $area) {
                    $checkDetail = ParticipantInterest::whereParticipantId($participant->id)
                        ->whereArea($area)
                        ->first();
                    if (!$checkDetail) {
                        $participantInterest = new ParticipantInterest();
                        $participantInterest->participant_id = $participant->id;
                        $participantInterest->area = $area;
                        $participantInterest->save();
                    }
                }

                DB::commit();

                // default variable
                $res_message = __('eventy/register.res_success_register');
                $redirect_url = action([TicketController::class, 'index']);

                auth()->login($participant);

                $response['message'] = $res_message;
                $response['redirect_url'] = $redirect_url;

                return $this->successResponse($response, $response['message'], Response::HTTP_CREATED);
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }
}
