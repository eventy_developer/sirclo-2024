<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Participant;
use Exception;

class ForgotPasswordController extends Controller
{
    public function index()
    {
        return view('web.Auth.forgot');
    }

    public function update()
    {
        $email = request('email');
        $check = Participant::whereEmail($email)->first();

        try {
            if (!$check) {
                throw new Exception(__('eventy/forgot.res_forgot_not_found'), 404);
            } else {
                $name = explode(' ', $check->name)[0];
                $id = $check->id;

                $reset_link = action([ResetPasswordController::class, 'index'], [
                    'key' => encrypt([
                        'id' => $id,
                        'expired_date' => plusOneHour(dateTime(), 1),
                    ])
                ]);

                // send forgot password email
                $sendForgot = $check->sendEmailForgotPassword([
                    'name' => $name,
                    'event_name' => preferGet('name_events'),
                    'event_logo' => preferGet('logo_events'),
                    'reset_link' => $reset_link,
                ]);

                if ($sendForgot['status'] == 1) {
                    $response['status'] = 1;
                    $response['message'] = __('eventy/forgot.res_forgot_success');

                    return $this->successResponse($response, $response['message']);
                } else {
                    throw new Exception($sendForgot['message'], 500);
                }
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }
}
