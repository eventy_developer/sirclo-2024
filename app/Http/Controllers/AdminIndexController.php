<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Backoffice\BackofficeController;
use App\Models\CmsUser;
use App\Models\Participant;
use App\Models\ParticipantActivity;
use App\Models\PrintNametagHistory;
use App\Models\ScanHistory;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Services\TransactionService;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class AdminIndexController extends CBController
{
    public function getEligible()
    {
        $is_skip_doorprize = g('is_skip_doorprize');
        $participant = Participant::find(g('id'));
        $participant->is_skip_doorprize = $is_skip_doorprize;
        $participant->save();

        $msg = !$is_skip_doorprize == 1 ? 'Eligible' : 'Skip';

        CRUDBooster::redirectBack('Successfully set ' . $participant->name . ' to ' . $msg, 'success');
    }

    public function getIndex()
    {
        // redirect if booth admin
        $cmsUser = CmsUser::find(CRUDBooster::myId());
//        if ($cmsUser->id_cms_privileges === CmsUser::BOOTH_PRIVILEGE_ID) {
//            if ($cmsUser->boothAdmin->booth->id) {
//                return redirect()->action([BackofficeController::class, 'getIndex']);
//            } else {
//                return redirect(config('crudbooster.ADMIN_PATH') . '/logout')->with(['message' => 'Admin Booth not found!', 'message_type' => 'danger']);
//            }
//        }

        $dataRegisterIntervalDateGeneral = Participant::getChartByDateRange([], '2024-06-01', '2024-10-01', ['01', '03', '05']);
        $dataRegisterIntervalDateEntrepreneur = Participant::getChartByDateRange([], '2024-06-01', '2024-10-01', ['02', '04', '06']);
        $dataRegisterIntervalInvitation = Participant::getChartByDateRange([], '2024-06-01', '2024-10-01', ['07', '08', '12']);
        $dataRegisterIntervalGroup = Participant::getChartByDateRange([], '2024-06-01', '2024-10-01', ['09','10', '11']);

        $registers = collect([
            (object)[
                'type' => 'General Pass',
                'type_code' => ['01', '03', '05']
            ],
            (object)[
                'type' => 'VIP',
                'type_code' => ['02', '04', '06'],
            ],
            (object)[
                'type' => 'Invitation General',
                'type_code' => ['07', '12'],
            ],
            (object)[
                'type' => 'Invitation VIP',
                'type_code' => ['08'],
            ],
            (object)[
                'type' => 'Group 3',
                'type_code' => ['09'],
            ],
            (object)[
                'type' => 'Group 5',
                'type_code' => ['10'],
            ],
            (object)[
                'type' => 'Group 10',
                'type_code' => ['11'],
            ]
        ]);
        foreach ($registers as $data) {
            $data->count = Participant::whereIn('type_code',$data->type_code)->whereIsAdmin(0)->whereStatus(Participant::STATUS_ACTIVE)->count();
            $data->day1 = PrintNametagHistory::whereHas('participant', function ($query) use ($data) {
                $query->whereIn('type_code', $data->type_code);
            })->whereDate('printed_date', '2024-09-24')->count();
            $data->day2 = PrintNametagHistory::whereHas('participant', function ($query) use ($data) {
                $query->whereIn('type_code', $data->type_code);
            })->whereDate('printed_date', '2024-09-25')->count();
        }

        $top_point = g('top_point') ?: 20;

        $total_income_methods = TransactionService::findAllTotalSumByMethod();
        $total_income = array_sum(array_column($total_income_methods, 'amount')) - array_sum(array_column($total_income_methods, 'fee'));
        $last_withdrawal = DB::table('withdrawal')->orderBy('created_at', 'desc')->first();

        $data = [];
        $data['page_title'] = 'Dashboard';
        $data['total_register'] = Participant::whereIsAdmin(0)->count();
        $data['total_register_success'] = Participant::whereIsAdmin(0)->whereStatus(Participant::STATUS_ACTIVE)->count();
        $data['total_login'] = ParticipantActivity::where('action', 'Login')->count();
        $data['total_income'] = $total_income;
        $data['total_withdrawal'] = $last_withdrawal;
        $data['total_ticket_sales'] = Ticket::countTicketSales(); // TODO : recheck query ticket
        $data['total_income_methods'] = $total_income_methods;
        $data['dataRegisterIntervalDateGeneral'] = $dataRegisterIntervalDateGeneral;
        $data['dataRegisterIntervalDateEntrepreneur'] = $dataRegisterIntervalDateEntrepreneur;
        $data['dataRegisterIntervalInvitation'] = $dataRegisterIntervalInvitation;
        $data['dataRegisterIntervalGroup'] = $dataRegisterIntervalGroup;
        $data['registers'] = $registers;
        $data['top_point'] = $top_point;
        $data['participants'] = Participant::whereStatus(Participant::STATUS_ACTIVE)->whereIsAdmin(0)->latest('total_point')->limit($top_point)->where('is_skip_doorprize', 0)->get();
        $data['participants_skip'] = Participant::whereStatus(Participant::STATUS_ACTIVE)->whereIsAdmin(0)->latest('total_point')->where('is_skip_doorprize', 1)->get();

        if ($cmsUser->id_cms_privileges === CmsUser::BOOTH_PRIVILEGE_ID) {
            return redirect(CRUDBooster::adminPath('scan-history'));
        }

        return $this->view('admin.Dashboard.index', $data);
    }

    public function cbInit()
    {

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field = "name";
        $this->limit = "20";
        $this->orderby = "id,desc";
        $this->global_privilege = false;
        $this->button_table_action = true;
        $this->button_bulk_action = true;
        $this->button_action_style = "button_icon";
        $this->button_add = false;
        $this->button_edit = true;
        $this->button_delete = true;
        $this->button_detail = true;
        $this->button_show = false;
        $this->button_filter = false;
        $this->button_import = false;
        $this->button_export = false;
        $this->table = "cms_users";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = ["label" => "Name", "name" => "name"];
        $this->col[] = ["label" => "Photo", "name" => "photo", "image" => true];
        $this->col[] = ["label" => "Email", "name" => "email"];
        $this->col[] = ["label" => "Privileges", "name" => "id_cms_privileges", "join" => "cms_privileges,name"];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Name', 'name' => 'name', 'type' => 'text', 'validation' => 'required|string|min:3|max:70', 'width' => 'col-sm-10', 'placeholder' => 'You can only enter the letter only'];
        $this->form[] = ['label' => 'Photo', 'name' => 'photo', 'type' => 'upload', 'validation' => 'required|image|max:3000', 'width' => 'col-sm-10', 'help' => 'File types support : JPG, JPEG, PNG, GIF, BMP'];
        $this->form[] = ['label' => 'Email', 'name' => 'email', 'type' => 'email', 'validation' => 'required|min:1|max:255|email|unique:cms_users', 'width' => 'col-sm-10', 'placeholder' => 'Please enter a valid email address'];
        $this->form[] = ['label' => 'Password', 'name' => 'password', 'type' => 'password', 'validation' => 'min:3|max:32', 'width' => 'col-sm-10', 'help' => 'Minimum 5 characters. Please leave empty if you did not change the password.'];
        $this->form[] = ['label' => 'Cms Privileges', 'name' => 'id_cms_privileges', 'type' => 'select2', 'validation' => 'required|integer|min:0', 'width' => 'col-sm-10', 'datatable' => 'cms_privileges,name'];
        $this->form[] = ['label' => 'Status', 'name' => 'status', 'type' => 'text', 'validation' => 'required|min:1|max:255', 'width' => 'col-sm-10'];
        # END FORM DO NOT REMOVE THIS LINE

        # OLD START FORM
        //$this->form = [];
        //$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
        //$this->form[] = ["label"=>"Photo","name"=>"photo","type"=>"upload","required"=>TRUE,"validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
        //$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:cms_users","placeholder"=>"Please enter a valid email address"];
        //$this->form[] = ["label"=>"Password","name"=>"password","type"=>"password","required"=>TRUE,"validation"=>"min:3|max:32","help"=>"Minimum 5 characters. Please leave empty if you did not change the password."];
        //$this->form[] = ["label"=>"Cms Privileges","name"=>"id_cms_privileges","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_privileges,name"];
        //$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
        # OLD END FORM

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();
        $this->load_js[] = 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js';


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();
        $this->load_css[] = 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css';


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}
