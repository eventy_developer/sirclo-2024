<?php

namespace App\Http\Controllers\Backoffice;

use App\Helpers\Eventy;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditPasswordRequest;
use App\Http\Requests\Product\Banner\CreateBannerRequest;
use App\Http\Requests\Product\Banner\EditBannerRequest;
use App\Http\Requests\Product\CreateRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Requests\UploadImageRequest;
use App\Models\Booth;
use App\Models\BoothAttachment;
use App\Models\CmsUser;
use App\Repositories\BoothAdminRepository;
use App\Repositories\BoothAttachmentRepository;
use App\Repositories\BoothChatRepository;
use App\Repositories\BoothProductBannersRepository;
use App\Repositories\BoothProductEcommerceRepository;
use App\Repositories\BoothProductRepository;
use App\Repositories\BoothRepository;
use App\Repositories\BoothVisitorRepository;
use App\Repositories\CmsUsersRepository;
use App\Services\BoothAttachmentService;
use App\Services\BoothChatMessagesService;
use App\Services\BoothChatService;
use App\Services\BoothProductBannersService;
use App\Services\BoothProductEcommerceService;
use App\Traits\ApiResponser;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class BackofficeController extends Controller
{
    use ApiResponser;

    /**
     * return get view BackOffice : index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getIndex()
    {
        $admin = CmsUser::with(['boothAdmin.booth'])->find(session('admin_id'));

        if (!$admin) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        if ($admin->id_cms_privileges != CmsUser::BOOTH_PRIVILEGE_ID)
            CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['admin'] = $admin;
        $data['booth'] = $admin->boothAdmin->booth;

        return view('admin.Backoffice.index', $data);
    }

    /**
     * get list attachment
     *
     * @param $booth_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListItems($booth_id): JsonResponse
    {
        try {
            $booth = Booth::with(['attachments', 'boothProducts.category'])->find($booth_id);
            $items['attachments'] = $booth->attachments;
            $items['products'] = $booth->boothProducts;

            return $this->successResponse($items);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    /**
     * count total visitor
     *
     * @param $booth_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotalVisitor($booth_id): JsonResponse
    {
        try {
            $response['message'] = 'success';
            $response['total'] = 0; // TODO : implement visitor

            return response()->json($response, 200);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    /**
     * post upload attachment
     *
     * @throws \Exception
     */
    public function postUploadAttachment()
    {
        $type = request('type');
        $booth_attachment = new BoothAttachmentRepository();
        $booth_attachment->setBoothId(request('booth_id'));

        $total = BoothAttachmentRepository::countLimitByBoothId(request('booth_id'), $type);

        if ($total >= config('eventy.max_attachment'))
            CRUDBooster::redirectBack(sprintf('Maximum %s attachment is %u.', $type, config('eventy.max_attachment')));

        if ($type === 'video') {
            CRUDBooster::valid([
                'video_url' => 'required|string|min:1|max:255',
            ], 'session');

            $booth_attachment->setFileName(request('video_name'));
            $booth_attachment->setType('Video');
            $booth_attachment->setVideoUrl(request('video_url'));
        } else {
            if ($type === 'document') {
                CRUDBooster::valid([
                    'file' => 'required|max:2000|mimes:pdf,xls,xlsx,csv,docx,pdf,ppt,pptx',
                ], 'session');

                $file = request()->file('file');
                $filesize = $file->getSize();
                $ext = $file->getClientOriginalExtension() ?: $file->clientExtension();
                $upload = Eventy::uploadFileS3('file', '', pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                $booth_attachment->setType('Document');
                $booth_attachment->setFileSize($filesize);
                $booth_attachment->setVideoUrl('');
            } else if ($type === 'image') {
                CRUDBooster::valid([
                    'image' => 'required|max:1000|mimes:jpg,jpeg,png',
                ], 'session');

                $booth_attachment->setType('Image');
                $booth_attachment->setVideoUrl('');
                $file = request()->file('image');
                $upload = Eventy::uploadFileS3('image');
            }

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

            $booth_attachment->setFile($upload);
            $booth_attachment->setFileName($filename);
            $booth_attachment->setExtension($extension);
        }

        $booth_attachment->save();

        CRUDBooster::redirectBack(__('eventy/backoffice.res_upload_success'), 'success');
    }

    /**
     * post delete attachment
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteAttachment($id): JsonResponse
    {
        try {
            $find = BoothAttachmentRepository::findById($id);

            removeFileS3($find->getFile());

            $find->delete();

            $response['message'] = 'success';

            return response()->json($response, 200);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    /**
     * update booth description
     *
     */
    public function postUpdateDescription()
    {
        CRUDBooster::valid([
            'description' => 'required|min:10|max:15000|string',
        ], 'session');

        $booth = BoothRepository::findById(request('booth_id'));
        $booth->setDescription(request('description'));
        $booth->save();

        CRUDBooster::redirectBack(__('eventy/backoffice.res_updated_success'), 'success');
    }

    public function postUploadSummernote()
    {
        $name = 'userfile';
        $dir = 'uploads/' . CRUDBooster::myId() . DIRECTORY_SEPARATOR . date('Y-m');
        if ($file = Eventy::uploadFileS3($name, $dir)) {
            echo Eventy::getFileS3($file);
        }
    }

    /**
     * post BackOffice : edit ecommerce
     *
     */
    public function postEcommerceEdit()
    {
        try {
            $booth_product_id = request('booth_product_id');
            $types = request('types');

            BoothProductEcommerceService::updateAllTypeByBoothProductId($types, $booth_product_id);

            CRUDBooster::redirectBack('The data has been updated !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to update data !', 'danger');
        }
    }

    /**
     * post BackOffice : move banner up
     *
     */
    public function getBannerMoveUp($id)
    {
        try {
            $banner = BoothProductBannersRepository::findById($id);

            if ($banner->getSort() == 1) {
                CRUDBooster::redirectBack('The data is already at the top of the list', 'danger');
            } else if ($banner->getSort() > 1) {

                $above = BoothProductBannersRepository::findBySortAndBoothProductId($banner->getSort() - 1, $banner->getBoothProductId()->getId());
                if ($above->getId() != '') {
                    $above->setSort($banner->getSort());
                    $above->save();
                }

                $banner->setSort($banner->getSort() - 1);
                $banner->save();
            }

            CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to move data !', 'danger');
        }
    }

    /**
     * post BackOffice : move banner down
     *
     */
    public function getBannerMoveDown($id)
    {
        try {
            $banner = BoothProductBannersRepository::findById($id);
            $bottom = BoothProductBannersRepository::findBySortAndBoothProductId($banner->getSort() + 1, $banner->getBoothProductId()->getId());

            if ($bottom->getId() == '') {
                CRUDBooster::redirectBack('The data is already at the bottom of the list', 'danger');
            } else if ($bottom->getId() != '') {
                if ($bottom->getId() != '') {
                    $bottom->setSort($banner->getSort());
                    $bottom->save();
                }

                $banner->setSort($banner->getSort() + 1);
                $banner->save();
            }

            CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to move data !', 'danger');
        }
    }

    /**
     * return get view BackOffice : detail banner
     *
     */
    public function getBannerDetail($booth_product_id = null, $id = null)
    {
        if (!$booth_product_id || !$id)
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Page not found !', 'danger');

        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);
        $row = BoothProductBannersRepository::findById($id);

        if ($row->getImage() != '') {
            $row->setImage(getS3EndpointFile($row->getImage()));
        }

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $data['admin'] = $admin;
        $data['booth'] = $booth;
        $data['row'] = $row;

        return view('admin.Backoffice.product.banner.detail', $data);
    }

    /**
     * post BackOffice : edit banner
     *
     * @param \App\Http\Requests\Product\Banner\EditBannerRequest $editBannerRequest
     */
    public function postBannerEdit(EditBannerRequest $editBannerRequest)
    {
        try {
            $booth_product_id = request('booth_product_id');
            $id = request('id');
            $upload = Eventy::uploadFileS3('image');

            $banner = BoothProductBannersRepository::findById($id);

            if ($upload) {
                removeFileS3($banner->getImage());
                $banner->setImage($upload);
            }
            $banner->setYoutubeUrl(request('youtube_url'));
            $banner->setType(request('type'));
            $banner->save();

            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'The data has been updated !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'Failed to update data !', 'danger');
        }
    }

    /**
     * return get view BackOffice : edit banner
     *
     * @param null $booth_product_id
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getBannerEdit($booth_product_id = null, $id = null)
    {
        if (!$booth_product_id || !$id)
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Page not found !', 'danger');

        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $row = BoothProductBannersRepository::findById($id);
        if ($row->getImage() != '') {
            $row->setImage(getS3EndpointFile($row->getImage()));
        }

        $data['booth'] = $booth;
        $data['admin'] = $admin;
        $data['row'] = $row;

        return view('admin.Backoffice.product.banner.edit', $data);
    }

    /**
     * post BackOffice : add banner
     *
     * @param \App\Http\Requests\Product\Banner\CreateBannerRequest $createBannerRequest
     */
    public function postBannerAdd(CreateBannerRequest $createBannerRequest)
    {
        try {
            $booth_product_id = request('booth_product_id');
            $upload = Eventy::uploadFileS3('image');
            $sort = BoothProductBannersRepository::latestBySortAndBoothProductId($booth_product_id)->getSort() + 1;

            $banner = new BoothProductBannersRepository();
            $banner->setBoothProductId($booth_product_id);
            if ($upload) {
                $banner->setImage($upload);
            }
            $banner->setYoutubeUrl(request('youtube_url'));
            $banner->setType(request('type'));
            $banner->setSort($sort);
            $banner->save();

            if (request('submit') === 'Save & Add More') {
                CRUDBooster::redirectBack('The data has been added !', 'success');
            }

            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'The data has been added !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'Failed to add data !', 'danger');
        }
    }

    /**
     * return get view BackOffice : add banner
     *
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getBannerAdd($id = null)
    {
        if (!$id)
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Page not found !', 'danger');

        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $data['booth'] = $booth;
        $data['admin'] = $admin;

        return view('admin.Backoffice.product.banner.add', $data);
    }

    /**
     * post BackOffice : delete banner
     *
     * @param $id
     */
    public function getBannerDelete($id)
    {
        try {
            $banner = BoothProductBannersRepository::findById($id);
            $booth_product_id = $banner->getBoothProductId()->getId();

            removeFileS3($banner->getImage());

            BoothProductBannersRepository::deleteById($banner->getId());

            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'The data has been deleted !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('banner/' . $booth_product_id), 'Failed to delete data !', 'danger');
        }
    }

    /**
     * return get view BackOffice : index banners
     *
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getBanner($id = null)
    {
        if (!$id)
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Page not found !', 'danger');

        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $items = BoothProductBannersService::findAllByBoothProductId($id);

        $data['page_title'] = 'Banners';
        $data['booth'] = $booth;
        $data['admin'] = $admin;
        $data['items'] = $items;

        return view('admin.Backoffice.product.banner.index', $data);
    }

    /**
     * post BackOffice : delete selected data
     *
     * @param $table
     */
    public function postActionSelected($table)
    {
        $id = request('checkbox');
        if (!$id) CRUDBooster::redirectBack('Please select at least one data !');

        try {

            if ($table === 'booth_product') {

                $products = BoothProductRepository::findAllById($id);

                foreach ($products as $product) {
                    removeFileS3($product->image);

                    BoothProductEcommerceRepository::deleteByProductId($product->id);
                    BoothProductBannersService::deleteByProductId($product->id);

                    BoothProductRepository::deleteById($product->id);
                }
            } else if ($table === 'booth_product_banners') {

                $items = BoothProductBannersRepository::findAllById($id);
                foreach ($items as $item) {
                    removeFileS3($item->image);
                    BoothProductBannersRepository::deleteById($item->id);
                }
            }

            CRUDBooster::redirectBack('Delete selected success !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to delete selected data !', 'danger');
        }
    }

    /**
     * post BackOffice : move product up
     *
     */
    public function getProductMoveUp($id)
    {
        try {
            $booth_product = BoothProductRepository::findById($id);

            if ($booth_product->getSort() == 1) {
                CRUDBooster::redirectBack('The data is already at the top of the list!', 'danger');
            } else if ($booth_product->getSort() > 1) {
                $above = BoothProductRepository::findBySortAndBoothId($booth_product->getSort() - 1, $booth_product->getBoothId()->getId());
                if ($above->getId() != '') {
                    $above->setSort($booth_product->getSort());
                    $above->save();
                }

                $booth_product->setSort($booth_product->getSort() - 1);
                $booth_product->save();
            }

            CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to move data !', 'danger');
        }
    }

    /**
     * post BackOffice : move banner down
     *
     */
    public function getProductMoveDown($id)
    {
        try {
            $booth_product = BoothProductRepository::findById($id);

            $above = BoothProductRepository::findBySortAndBoothId($booth_product->getSort() + 1, $booth_product->getBoothId()->getId());
            if ($above->getId() == '') {
                CRUDBooster::redirectBack('The data is already at the bottom of the list!', 'danger');
            } else if ($above->getId() != '') {
                if ($above->getId() != '') {
                    $above->setSort($booth_product->getSort());
                    $above->save();
                }

                $booth_product->setSort($booth_product->getSort() + 1);
                $booth_product->save();
            }

            CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirectBack('Failed to move data !', 'danger');
        }
    }

    /**
     * post BackOffice : delete product
     *
     * @param $id
     */
    public function getProductDelete($id)
    {
        try {
            $products = BoothProductRepository::findAllById($id);

            foreach ($products as $product) {
                removeFileS3($product->image);

                BoothProductEcommerceRepository::deleteByProductId($product->id);
                BoothProductBannersService::deleteByProductId($product->id);

                BoothProductRepository::deleteById($product->id);
            }

            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'The data has been deleted !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Failed to delete data !', 'danger');
        }
    }

    /**
     * post BackOffice : edit product
     *
     * @param \App\Http\Requests\Product\UpdateRequest $updateRequest
     */
    public function postProductEdit(UpdateRequest $updateRequest)
    {
        try {
            $price = (int)str_replace(',', '', request('price'));
            $upload = Eventy::uploadFileS3('image');

            $product = BoothProductRepository::findById(request('id'));

            if ($upload) {
                removeFileS3($product->getImage());
                $product->setImage($upload);
            }
            $product->setName(request('name'));
            $product->setDescription(request('description'));
            $product->setPrice($price);
            $product->save();

            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'The data has been updated !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Failed to update data !', 'danger');
        }
    }

    /**
     * post BackOffice : add product
     *
     * @param \App\Http\Requests\Product\CreateRequest $createRequest
     */
    public function postProductAdd(CreateRequest $createRequest)
    {
        try {
            $booth_id = request('booth_id');
            $price = (int)str_replace(',', '', request('price'));
            $upload = Eventy::uploadFileS3('image');
            $sort = BoothProductRepository::latestBySortAndBoothId($booth_id)->getSort() + 1;

            $product = new BoothProductRepository();
            $product->setBoothId($booth_id);
            $product->setImage($upload);
            $product->setName(request('name'));
            $product->setDescription(request('description'));
            $product->setPrice($price);
            $product->setSort($sort);
            $id = $product->save();

            // BoothProductEcommerceService::generateDemo($id);

            if (request('submit') === 'Save & Add More') {
                CRUDBooster::redirectBack('The data has been added !', 'success');
            }

            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'The data has been added !', 'success');
        } catch (Exception $e) {
            CRUDBooster::redirect(CRUDBooster::mainpath('product'), 'Failed to add data !', 'danger');
        }
    }

    /**
     * return get view BackOffice : detail product
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getProductDetail($id)
    {
        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);
        $row = BoothProductRepository::findById($id);

        if ($row->getImage() != '') {
            $row->setImage(getS3EndpointFile($row->getImage()));
        }

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $data['admin'] = $admin;
        $data['booth'] = $booth;
        $data['row'] = $row;

        return view('admin.Backoffice.product.detail', $data);
    }

    /**
     * return get view BackOffice : edit product
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getProductEdit($id)
    {
        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);
        $product = BoothProductRepository::findById($id);

        if ($product->getImage() != '') {
            $product->setImage(getS3EndpointFile($product->getImage()));
        }

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $data['admin'] = $admin;
        $data['booth'] = $booth;
        $data['product'] = $product;

        return view('admin.Backoffice.product.edit', $data);
    }

    /**
     * return get view BackOffice : add product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getProductAdd()
    {
        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $data['booth'] = $booth;
        $data['admin'] = $admin;

        return view('admin.Backoffice.product.add', $data);
    }

    /**
     * return get view BackOffice : index product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getProduct()
    {
        $admin = CRUDBooster::me();
        $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

        if (!$admin->photo) {
            $admin->photo = getS3EndpointFile(preferGet('icon_events'));
        } else {
            $admin->photo = getS3EndpointFile($admin->photo);
        }

        $items = BoothProductRepository::findAllByBoothId($booth->getId());

        foreach ($items as $item) {
            $item->tokopedia = BoothProductEcommerceRepository::findByTypeAndBoothProductId('Tokopedia', $item->id);
            $item->shopee = BoothProductEcommerceRepository::findByTypeAndBoothProductId('Shopee', $item->id);
            $item->general = BoothProductEcommerceRepository::findByTypeAndBoothProductId('General', $item->id);
        }

        $data['page_title'] = 'Products';
        $data['booth'] = $booth;
        $data['admin'] = $admin;
        $data['items'] = $items;

        return view('admin.Backoffice.product.index', $data);
    }

    /**
     * get export booth visitor to xlsx
     *
     */
    public function getExportBoothVisitor()
    {
        try {
            ini_set('memory_limit', '1024M');
            set_time_limit(180);

            $admin = CRUDBooster::me();
            $booth = BoothAdminRepository::findBoothByCmsUsersId($admin->id);

            $filename = 'Report Booth ' . $booth->getName() . ' - ' . date('d M Y');
            $booth_visitor = BoothVisitorRepository::getAllParticipantsByBoothId($booth->getId());

            Excel::create($filename, function ($excel) use ($booth_visitor, $booth) {
                $excel->sheet($booth->getName(), function ($sheet) use ($booth_visitor) {
                    $sheet->loadView('admin.Backoffice.exports.visitor', [
                        'participants' => $booth_visitor,
                    ]);
                });
            })->export('xlsx');
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * Upload Image Admin Booth
     *
     * @param \App\Http\Requests\UploadImageRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postProfileUploadImage(UploadImageRequest $request): JsonResponse
    {
        try {
            $base64 = request('image');
            $user_id = request('user_id');

            $admin = CmsUsersRepository::findById($user_id);

            if ($admin->getId() == '') {
                throw new Exception(__('Admin is not found'), 422);
            } else {
                removeFileS3($admin->getPhoto());

                $upload = Eventy::uploadBase64FileS3($base64, $user_id);
                if ($upload) {
                    $save = CmsUsersRepository::findById($user_id);
                    $save->setPhoto($upload);
                    $save->save();
                }

                $response['message'] = __('eventy/website_config.res_profile_photo_success_updated');
                $response['image'] = getS3EndpointFile($upload);

                return $this->successResponse($response, $response['message']);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }

    /**
     * post : Upload Image Admin Booth
     *
     * @param \App\Http\Requests\EditPasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postProfileEditPassword(EditPasswordRequest $request): JsonResponse
    {
        try {
            $current_password = $request->input('current_password');
            $new_password = $request->input('new_password');
            $retype_password = $request->input('retype_password');
            $user_id = $request->input('user_id');

            $data = CmsUsersRepository::findById($user_id);

            $current_database = ($data->getId() != '' ? $data->getPassword() : '');
            if (!Hash::check($current_password, $current_database)) {
                throw new Exception(__('eventy/website_config.res_current_password_wrong'), 422);
            } else {
                $data->setPassword(Hash::make($new_password));
                $data->save();

                $response['message'] = __('eventy/website_config.res_password_success_updated');

                return $this->successResponse([], $response['message']);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e->getTrace(), $e->getCode());
        }
    }
}
