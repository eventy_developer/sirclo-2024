<?php

namespace App\Http\Controllers\Payment;

use App\Helpers\Eventy;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventyPaymentXenditRequest;
use App\Models\Participant;
use App\Models\ParticipantSchedule;
use App\Models\Ticket;
use App\Models\Transaction;
use App\Models\TransactionXendit;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class EventyPaymentController extends Controller
{
    private $transaction;
    private $participant;

    /**
     * save data from xendit callback
     *
     * @param EventyPaymentXenditRequest $eventyPaymentXenditRequest
     *
     * @return JsonResponse
     */
    public function postSave(EventyPaymentXenditRequest $eventyPaymentXenditRequest): JsonResponse
    {
        DB::beginTransaction();
        try {
            $status = request('status');
            $transaction_code = request('transaction_code');

            $transaction = Transaction::with(['participant', 'transactionTicket'])->whereInvoiceCode(request('invoice_code'))->first();
            if (!$transaction) {
                $error['code'] = 1;
                $error['message'] = 'Transaction not found';

                $response['api_status'] = 0;
                $response['api_message'] = 'Failed, Please try again';
                $response['error'] = $error;
                $code = 404;
            } else if ($transaction->isSuccess() || $transaction->isExpired()) {
                $response['api_status'] = 1;
                $response['api_message'] = 'Latest transaction status is ' . $transaction->status;
                $code = 204;
            } else {
                $this->transaction = $transaction;
                $this->participant = $transaction->participant;

                $methods = ['BRI', 'MANDIRI', 'BNI', 'PERMATA', 'BCA', 'CREDIT_CARD', 'OVO', 'QRIS'];
                if (in_array($transaction->payment_method, $methods)) {
                    //success method
                    if ($status === Transaction::STATUS_SUCCESS) {
                        if (!empty($this->transaction->participant_ids_group)){
                            $groupParticipantsIds = json_decode($this->transaction->participant_ids_group);
                            $this->saveRegistrationGroup($groupParticipantsIds);
                            $this->sendEmailSuccessGroup($groupParticipantsIds);
                        }
                        // save registration
                        $this->saveRegistration();
                        // send email success register
                        $this->sendEmailSuccess();
                    }

                    // set to waiting payment if status EXPIRED and participant status != Active
                    if ($status === Transaction::STATUS_EXPIRED) {

                        $ticket = Ticket::query()->firstWhere('id', $transaction->transactionTicket->ticket_id);

                        if (!$this->participant->isActiveStatus()) {

                            if (!empty($this->transaction->participant_ids_group)){
                                $groupParticipantsIds = json_decode($this->transaction->participant_ids_group);
                                foreach ($groupParticipantsIds as $id){
                                    $findParticipantGroup = Participant::query()->firstWhere('id', $id);
                                    Participant::deleteRelationById($findParticipantGroup->id);
                                    $findParticipantGroup->delete();
                                }
                            }

                            $this->participant->status = Participant::STATUS_WAITING_TICKET;
                            $this->participant->ticket_id = null;
                            $this->participant->type_code = null;
                            $this->participant->save();

                            $this->sendEmailExpired();
                        }

                        if (!empty($this->transaction->participant_ids_group)){
                            $groupParticipantsIds = json_decode($this->transaction->participant_ids_group);
                            $ticket->quota_achieve = $ticket->quota_achieve - count($groupParticipantsIds);
                            $ticket->save();
                        }

                        $ticket->quota_achieve = $ticket->quota_achieve - 1;
                        $ticket->save();
                    }

                    // save to transaction xendit
                    $transactionXenditId = TransactionXendit::savePayment($transaction->id, $transaction_code, $status);

                    // change transaction status
                    $transaction->transaction_code = $transaction_code;
                    $transaction->transaction_xendit_id = $transactionXenditId;
                    $transaction->status = $status;
                    $transaction->save();

                    $response['api_status'] = 1;
                    $response['api_message'] = 'Transaction has been updated';
                    $code = 202;

                    DB::commit();
                } else {
                    $error['code'] = 3;
                    $error['message'] = 'Payment method is ' . $transaction->payment_method;

                    $response['api_status'] = 0;
                    $response['api_message'] = 'Invalid payment method';
                    $response['error'] = $error;
                    $code = 400;
                    DB::rollBack();
                }

                // log payment
                $message = 'Invoice : ' . $transaction->invoice_code . ' is ' . $status;
                $data['payment_id'] = $transaction->id;
                $data['method'] = $transaction->payment_method;
                $data['url'] = request()->fullUrl();
                $data['params'] = request()->all();
                $data['response'] = $response;
                $data['code'] = $code;
                Eventy::LogFile($message, 'payment-callback', 'payment', $data);
            }
        } catch (Exception $e) {
            DB::rollBack();
            $error['code'] = $e->getCode();
            $error['message'] = $e->getMessage();

            $response['api_status'] = 0;
            $response['api_message'] = 'Something went wrong, please try again.';
            $response['error'] = $error;
            $code = 500;

            // log payment
            $message = 'Invoice : ' . $transaction->invoice_code . ' is ' . $status;
            $data['payment_id'] = $transaction->id;
            $data['method'] = $transaction->payment_method;
            $data['url'] = request()->fullUrl();
            $data['params'] = request()->all();
            $data['response'] = $response;
            $data['code'] = $code;
            Eventy::LogFile($message, 'payment-callback', 'payment', $data);
        }

        return response()->json($response, $code);
    }

    private function saveRegistration()
    {
        $ticket = Ticket::query()->firstWhere('id', $this->transaction->transactionTicket->ticket_id);

        // save participant
        if (!$this->participant->isActiveStatus()) {
            $this->participant->ticket_id = $this->transaction->transactionTicket->ticket_id;
            $this->participant->status = Participant::STATUS_ACTIVE;
            $this->participant->type_code = $ticket->type_index_ticket;
            $this->participant->registration_number = Eventy::registrationNumber();
            $this->participant->save();
        }

        // save schedule (classroom)
//        ParticipantSchedule::saveRegistration($this->transaction->id, $this->participant->id);
    }

    private function saveRegistrationGroup($participant_ids_group)
    {
        $ticket = Ticket::query()->firstWhere('id', $this->transaction->transactionTicket->ticket_id);
        foreach ($participant_ids_group as $memberId) {
            $findParticipant = Participant::query()->firstWhere('id', $memberId);
            if (!$findParticipant->isActiveStatus()) {
                $findParticipant->ticket_id = $this->transaction->transactionTicket->ticket_id;
                $findParticipant->status = Participant::STATUS_ACTIVE;
                $findParticipant->type_code = $ticket->type_index_ticket;
                $findParticipant->registration_number = Eventy::registrationNumber();
                $findParticipant->save();
            }
        }
    }


    /**
     * @throws Exception
     */
    private function sendEmailSuccess()
    {
        if ($this->transaction) {
            $this->transaction->load(['transactionDetails.participant', 'transactionTicket.ticket']);
        }
        // data
        $qr = Eventy::QRCode($this->participant->qr_code);
        $mail['event_name'] = preferGet('name_events');
        $mail['event_logo'] = preferGet('logo_events');
        $mail['event_start'] = date('F d, Y', strtotime(preferGet('date_start_events')));
        $mail['event_email'] = preferGet('contact_email_events');
        $mail['contact_name_1'] = preferGet('contact_name_1');
        $mail['contact_phone_1'] = preferGet('contact_phone_1');
        $mail['contact_name_2'] = preferGet('contact_name_2');
        $mail['contact_phone_2'] = preferGet('contact_phone_2');
        $mail['participant'] = $this->participant;
        $mail['qr_code'] = $qr;
        $mail['email'] = $this->participant->email;
        $mail['type'] = $this->participant->type;
        $mail['type_code'] = $this->participant->type_code;
        $mail['registration_number'] = $this->participant->registration_number;
        $mail['payment'] = $this->transaction;
        $mail['order_id'] = $this->transaction->invoice_code;
        $mail['url'] = action([LoginController::class, 'index'], ['token' => encrypt($this->participant->email)]);

        // Postmark SMTP TODO: addition class
        if ($this->transaction->transactionTicket) {
            $send = $this->participant->sendEmailRegistration($mail);
        }
//        else{
//            $send = $this->participant->sendEmailClassroomAddition($mail);
//        }

        if ($send['status'] == 2) {
            throw new Exception('Failed to send email success registration', 500);
        }
    }

    /**
     * @throws Exception
     */
    private function sendEmailSuccessGroup($participant_ids_group)
    {
        if ($this->transaction) {
            $this->transaction->load(['transactionDetails.participant', 'transactionTicket.ticket']);
        }

        foreach ($participant_ids_group as $id) {
            $participantGroup = Participant::find( $id);
            $qr = Eventy::QRCode($participantGroup->qr_code);
            $mail['event_name'] = preferGet('name_events');
            $mail['event_logo'] = preferGet('logo_events');
            $mail['event_start'] = date('F d, Y', strtotime(preferGet('date_start_events')));
            $mail['event_email'] = preferGet('contact_email_events');
            $mail['contact_name_1'] = preferGet('contact_name_1');
            $mail['contact_phone_1'] = preferGet('contact_phone_1');
            $mail['contact_name_2'] = preferGet('contact_name_2');
            $mail['contact_phone_2'] = preferGet('contact_phone_2');
            $mail['participant'] = $participantGroup;
            $mail['qr_code'] = $qr;
            $mail['email'] = $participantGroup->email;
            $mail['type'] = $participantGroup->type;
            $mail['type_code'] = $participantGroup->type_code;
            $mail['registration_number'] = $participantGroup->registration_number;
            $mail['payment'] = $this->transaction;
            $mail['order_id'] = $this->transaction->invoice_code;
            $mail['url'] = action([LoginController::class, 'index'], ['token' => encrypt($participantGroup->email)]);

            // Postmark SMTP TODO: addition class
            if ($this->transaction->transactionTicket) {
                $send = $participantGroup->sendEmailRegistration($mail);
            }

            if ($send['status'] == 2) {
                throw new Exception('Failed to send email success registration', 500);
            }
        }
    }

    private function sendEmailExpired()
    {
        // data
        $mail['event_name'] = preferGet('name_events');
        $mail['event_start'] = date('F d, Y', strtotime(preferGet('date_start_events')));
        $mail['event_email'] = preferGet('contact_email_events');
        $mail['event_logo'] = preferGet('logo_events');
        $mail['contact_name_1'] = preferGet('contact_name_1');
        $mail['contact_phone_1'] = preferGet('contact_phone_1');
        $mail['contact_name_2'] = preferGet('contact_name_2');
        $mail['contact_phone_2'] = preferGet('contact_phone_2');
        $mail['name'] = $this->participant->name;
        $mail['qr_code'] = $this->participant->present_qr_code;
        $mail['email'] = $this->participant->email;
        $mail['type'] = $this->participant->type;
        $mail['type_code'] = $this->participant->type_code;
        $mail['registration_number'] = $this->participant->registration_number;

        $send = $this->participant->sendEmailExpired($mail);

        // Send Email Invoice
        Eventy::LogFile('Email Send : `Expired`', 'logging', 'logging', [
            'email' => $this->participant->email,
            'status' => $send['status'],
            'message' => $send['message'],
        ]);

        if ($send['status'] == 2) {
            throw new Exception('Failed to send email expired', 500);
        }
    }
}
