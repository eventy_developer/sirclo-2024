<?php

namespace App\Traits;

use App\Helpers\Eventy;
use App\Models\Transaction;
use App\Models\TransactionXendit;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

trait EventyPayment
{
    private $base_url = 'https://payment.eventy.id';

    /**
     * @param Transaction $transaction
     * @param array $params
     *
     * @return mixed
     * @throws \Exception
     */
    private function Invoice(Transaction $transaction, array $params)
    {
        if (config('eventy.payment_debug') == true) {
            $this->base_url = 'https://test.payment.eventy.id';
        }

        // load participant relationship
        $transaction->load(['participant']);
        $participant = $transaction->participant;

        // api parameter
        $params['event_id'] = config('eventy.event_id');
        $params['invoice_code'] = $transaction->invoice_code;
        $params['payer_email'] = $participant->email;
        $params['description'] = 'Registration - ' . preferGet('name_events');
        $params['amount'] = $transaction->amount;
        if (config('eventy.payment_debug') == false){
            $params['invoice_duration'] = 3600;
        }else{
            $params['invoice_duration'] = 300;
        }
        // participant data
        $params['given_names'] = getFirstName($participant->name) . ' - ' . $transaction->invoice_code;
        $params['phone'] = str_replace('-', '', $participant->phone);

        // data for log
        $data['payment_id'] = $transaction->id;
        $data['method'] = $transaction->payment_method;
        $data['url'] = request()->fullUrl();
        $data['params'] = $params;

        try {
            $url = $this->base_url . '/invoice';

            // push to payment
            $client = new Client();
            $result = $client->post($url, [
                'form_params' => $params,
            ]);

            // xendit invoice create response from eventy invoice
            $response = json_decode($result->getBody(), true);

            if ($result->getStatusCode() === 200) {
                $res = (object)$response['data'];

                // debug status
                if (!$res->status) {
                    $res->status = Transaction::STATUS_WAITING_PAYMENT;
                }

                // save to payment xendit
                $transactionXenditId = TransactionXendit::savePayment($transaction->id, $res->external_id, $res->status);

                // update payment
                $transaction->checkout_url = $res->url;
                $transaction->transaction_code = $res->external_id;
                $transaction->transaction_xendit_id = $transactionXenditId;
                $transaction->status = $res->status;
                $transaction->save();

                // log payment
                $message = 'Invoice Create : ' . $transaction->invoice_code . ' is ' . $res->status;
                $data['response'] = $response;
                $data['code'] = $result->getStatusCode();
                Eventy::LogFile($message, 'payment', 'payment', $data);

                return $response;
            }
        } catch (BadResponseException $e) {
            // log payment
            $message = $e->getMessage();
            $data['response'] = $e->getResponse();
            $data['code'] = $e->getCode();
            Eventy::LogFile($message, 'payment', 'payment', $data, 'error');

            $response = json_decode($e->getResponse()->getBody());

            throw new Exception($response->api_message, $e->getCode());
        }
    }
}
