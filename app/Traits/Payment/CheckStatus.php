<?php

namespace App\Traits\Payment;

trait CheckStatus
{
    /**
     * Check if payment is empty
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return $this->id == '';
    }

    /**
     * Check if payment status is waiting payment
     *
     * @return boolean
     */
    public function isWaitingPayment(): bool
    {
        return $this->status === self::STATUS_WAITING_PAYMENT;
    }

    /**
     * Check if payment status is waiting confirmation
     *
     * @return boolean
     */
    public function isWaitingConfirmation(): bool
    {
        return $this->status === self::STATUS_WAITING_CONFIRMATION;
    }

    /**
     * Check if payment status is payment changed
     *
     * @return boolean
     */
    public function isPaymentChanged(): bool
    {
        return $this->status === self::STATUS_PAYMENT_CHANGED;
    }

    /**
     * Check if payment status is pending
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    /**
     * Check if payment status is success
     *
     * @return boolean
     */
    public function isSuccess(): bool
    {
        return $this->status === self::STATUS_SUCCESS;
    }

    /**
     * Check if payment status is failed
     *
     * @return boolean
     */
    public function isFailed(): bool
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * Check if payment status is expired
     *
     * @return boolean
     */
    public function isExpired(): bool
    {
        return $this->status === self::STATUS_EXPIRED;
    }
}
