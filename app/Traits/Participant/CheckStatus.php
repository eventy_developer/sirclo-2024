<?php

namespace App\Traits\Participant;

trait CheckStatus
{
    /**
     * Check if account is not available
     *
     * @return boolean
     */
    public function isNotAvailable(): bool
    {
        return $this->isAvailable() || $this->isWaiting();
    }

    /**
     * Check if participant is available
     *
     * @return boolean
     */
    public function isAvailable(): bool
    {
        return !$this->isEmpty();
    }

    /**
     * Check if participant is empty
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return empty($this->id);
    }

    /**
     * Check if participant status Active
     *
     * @return boolean
     */
    public function isActiveStatus(): bool
    {
        return in_array($this->status, [self::STATUS_ACTIVE]);
    }

    /**
     * Check if participant status is still waiting payment and waiting payment confirm
     *
     * @return boolean
     */
    public function isWaiting(): bool
    {
        return in_array($this->status, [self::STATUS_WAITING_TICKET, self::STATUS_WAITING_PAYMENT, self::STATUS_WAITING_CONFIRMATION]);
    }

    /**
     * Check if participant status is still waiting ticket
     *
     * @return boolean
     */
    public function isWaitingTicket(): bool
    {
        return in_array($this->status, [self::STATUS_WAITING_TICKET]);
    }

    /**
     * Check if participant status is still waiting payment
     *
     * @return boolean
     */
    public function isWaitingPayment(): bool
    {
        return in_array($this->status, [self::STATUS_WAITING_PAYMENT]);
    }

    /**
     * Check if participant status is still waiting confirmation
     *
     * @return boolean
     */
    public function isWaitingConfirmation(): bool
    {
        return in_array($this->status, [self::STATUS_WAITING_CONFIRMATION]);
    }

    /**
     * Check if participant status is change payment method
     *
     * @return boolean
     */
    public function isPaymentChanged(): bool
    {
        return in_array($this->status, [self::STATUS_PAYMENT_CHANGED]);
    }

    /**
     * Check if participant is pending
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return in_array($this->status, [self::STATUS_PENDING]);
    }

    /**
     * Check if participant is blocked
     *
     * @return boolean
     */
    public function isBlocked(): bool
    {
        return in_array($this->status, ['Blocked']);
    }

    /**
     * Check if participant ever login
     *
     * @return boolean
     */
    public function isEverLogin(): bool
    {
        return (bool)$this->getIsLogin();
    }
}
