<?php

namespace App\Traits\Participant;

use App\Helpers\SendEmail as Email;

trait SendEmail
{
    private $emailFrom = 'no-reply@eventy.id';
    private $emailTemplate;
    private $emailData;
    private $emailSubjectName;

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailRegistration(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.register';
        $this->emailSubjectName = 'E-Commerce Expo 2024 Ticket Purchase Confirmation [Order Id: ' . $data['order_id'] . ']';
        return $this->sendEmailNow();
    }

    public function sendEmailExpired(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.expired';
        $this->emailSubjectName = 'Ticket Purchase Expired';
        return $this->sendEmailNow();
    }

    private function sendEmailNow()
    {
        $sendEmail = new Email();
        $sendEmail->setEvent(preferGet('name_events'));
        $sendEmail->setFrom($this->emailFrom);
        $sendEmail->setTo($this->email);
        $sendEmail->setData($this->emailData);
        $sendEmail->setTemplate($this->emailTemplate);
        $sendEmail->setSubject($this->emailSubjectName);
        return $sendEmail->sendEmailSMTP();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailRegistrationForTicket(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.register_ticket';
        $this->emailSubjectName = 'Registration Ticket';
        return $this->sendEmailNow();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailInvoice(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.invoice2';
        $this->emailSubjectName = 'E-Commerce Expo 2024 INVOICE [Order Id: ' . $data['order_id'] . ']';
        return $this->sendEmailNow();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailForgotPassword(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.forgot_password';
        $this->emailSubjectName = 'Please reset your password';
        return $this->sendEmailNow();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailWaitingPayment(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.waiting';
        $this->emailSubjectName = 'Waiting Confirmation';
        return $this->sendEmailNow();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailRejectPayment(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.reject';
        $this->emailSubjectName = 'Payment Confirmation Rejected';
        return $this->sendEmailNow();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function sendEmailBlocked(array $data): array
    {
        $this->emailData = $data;
        $this->emailTemplate = 'web.Email.blocked';
        $this->emailSubjectName = 'Account Blocked';
        return $this->sendEmailNow();
    }
}
