<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponser
{
    protected function successResponse($data = [], $message = 'Success', $code = 200): JsonResponse
    {
        return response()->json([
            'code' => $code,
            'status' => Response::$statusTexts[$code],
            'message' => $message,
            'data' => $data
        ], $code);
    }

    protected function errorResponse($message = null, $trace = null, $code = 500): JsonResponse
    {
        $code = $code == 0 ? 500 : $code;
        return response()->json([
            'code' => $code,
            'status' => Response::$statusTexts[$code],
            'message' => $message,
            'trace' => $trace,
            'data' => null
        ], $code);
    }
}
