<?php

namespace App\Traits\Preference;

use crocodicstudio\crudbooster\helpers\CRUDBooster;

trait PreferencePage
{
    public function getEvents()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Events';

        return $this->view('admin.Preference.events', $data);
    }

    public function getZoom()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Zoom Settings';

        return $this->view('admin.Preference.zoom', $data);
    }

    public function getLandingPage()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Landing Page';

        return $this->view('admin.Preference.landing_page', $data);
    }

    public function getLoginPage()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Login Settings';

        return $this->view('admin.Preference.login', $data);
    }

    public function getRegisterPage()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Register Settings';

        return $this->view('admin.Preference.register', $data);
    }

    public function getWeb()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Web Settings';

        return $this->view('admin.Preference.web', $data);
    }

    public function getPayment()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Payment';

        return $this->view('admin.Preference.payment', $data);
    }

    public function getPlugin()
    {
        if (!CRUDBooster::isView()) CRUDBooster::redirect(CRUDBooster::adminPath(), trans('crudbooster.denied_access'));

        $data['page_title'] = 'Plugin';

        return $this->view('admin.Preference.plugin', $data);
    }
}
