<?php

namespace App\Traits\Preference;

use App\Helpers\Eventy;
use App\Models\Preference;
use App\Repositories\SpeakersRepository;
use App\Services\SpeakersService;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

trait UpdateConfig
{
    public function saveUpdatePreference($key, $value): bool
    {
        $check = Preference::where('key', $key)->first();
        if (!$check) {
            $save['created_at'] = dateTime();
            $save['name'] = ucwords(implode(' ', explode('_', $key)));
            $save['key'] = $key;
            $save['content'] = $value;
            $save['type'] = (strpos($key, 'image') !== false) ? 'Image' : 'Text';

            $update = DB::table('preferences')->insertGetId($save);

        } else {
            $id = ($check ? $check->id : null);

            $save['updated_at'] = dateTime();
            $save['content'] = $value;

            $update = DB::table('preferences')->where('id', $id)->update($save);
        }

        Cache::forget(prefixCacheName($key));

        return $update;
    }

    /**
     * post update plugin
     *
     */
    public function postPlugin()
    {
        $plugin_tawkto = request()->input('plugin_tawkto');
        $plugin_google_analytics = request()->input('plugin_google_analytics');

        $save = $this->saveUpdatePreference('plugin_tawkto', $plugin_tawkto);
        $save = $this->saveUpdatePreference('plugin_google_analytics', $plugin_google_analytics);

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update payment
     *
     */
    public function postPayment()
    {
        $ticket_title_text = request()->input('ticket_title_text');
        $ticket_caption_text = request()->input('ticket_caption_text');
        $classroom_title_text = request()->input('classroom_title_text');
        $classroom_caption_text = request()->input('classroom_caption_text');
        $order_summary_title_text = request()->input('order_summary_title_text');
        $order_summary_caption_text = request()->input('order_summary_caption_text');
        $payment_title_text = request()->input('payment_title_text');
        $payment_caption_text = request()->input('payment_caption_text');
        $payment_confirmation_title_text = request()->input('payment_confirmation_title_text');
        $payment_confirmation_caption_text = request()->input('payment_confirmation_caption_text');
        $payment_bank_name = request()->input('payment_bank_name');
        $payment_bank_branch = request()->input('payment_bank_branch');
        $payment_account_owner = request()->input('payment_account_owner');
        $payment_account_number = request()->input('payment_account_number');
        $contact_email_events = request()->input('contact_email_events');
        $contact_name_1 = request()->input('contact_name_1');
        $contact_phone_1 = request()->input('contact_phone_1');
        $contact_name_2 = request()->input('contact_name_2');
        $contact_phone_2 = request()->input('contact_phone_2');

        // payment methods
        $payment_method_va_status = request()->input('payment_method_va_status') ?? 'nonactive';
        $payment_method_ovo_status = request()->input('payment_method_ovo_status') ?? 'nonactive';
        $payment_method_cc_status = request()->input('payment_method_cc_status') ?? 'nonactive';
        $payment_method_manual_status = request()->input('payment_method_manual_status') ?? 'nonactive';
        $payment_method_qris_status = request()->input('payment_method_qris_status') ?? 'nonactive';

        if ($payment_method_va_status === 'nonactive' &&
            $payment_method_ovo_status === 'nonactive' &&
            $payment_method_cc_status === 'nonactive' &&
            $payment_method_manual_status === 'nonactive' &&
            $payment_method_qris_status === 'nonactive') {

            $payment_method_manual_status = 'active';
        }

        $save = $this->saveUpdatePreference('ticket_title_text', $ticket_title_text);
        $save = $this->saveUpdatePreference('ticket_caption_text', $ticket_caption_text);
        $save = $this->saveUpdatePreference('classroom_title_text', $classroom_title_text);
        $save = $this->saveUpdatePreference('classroom_caption_text', $classroom_caption_text);
        $save = $this->saveUpdatePreference('order_summary_title_text', $order_summary_title_text);
        $save = $this->saveUpdatePreference('order_summary_caption_text', $order_summary_caption_text);
        $save = $this->saveUpdatePreference('payment_title_text', $payment_title_text);
        $save = $this->saveUpdatePreference('payment_caption_text', $payment_caption_text);
        $save = $this->saveUpdatePreference('payment_confirmation_title_text', $payment_confirmation_title_text);
        $save = $this->saveUpdatePreference('payment_confirmation_caption_text', $payment_confirmation_caption_text);
        $save = $this->saveUpdatePreference('payment_bank_name', $payment_bank_name);
        $save = $this->saveUpdatePreference('payment_bank_branch', $payment_bank_branch);
        $save = $this->saveUpdatePreference('payment_account_owner', $payment_account_owner);
        $save = $this->saveUpdatePreference('payment_account_number', $payment_account_number);
        $save = $this->saveUpdatePreference('contact_email_events', $contact_email_events);
        $save = $this->saveUpdatePreference('contact_name_1', $contact_name_1);
        $save = $this->saveUpdatePreference('contact_phone_1', $contact_phone_1);
        $save = $this->saveUpdatePreference('contact_name_2', $contact_name_2);
        $save = $this->saveUpdatePreference('contact_phone_2', $contact_phone_2);

        // save payment methods
        $save = $this->saveUpdatePreference('payment_method_va_status', $payment_method_va_status);
        $save = $this->saveUpdatePreference('payment_method_ovo_status', $payment_method_ovo_status);
        $save = $this->saveUpdatePreference('payment_method_cc_status', $payment_method_cc_status);
        $save = $this->saveUpdatePreference('payment_method_manual_status', $payment_method_manual_status);
        $save = $this->saveUpdatePreference('payment_method_qris_status', $payment_method_qris_status);

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post remove landing page speaker image
     *
     */
    public function postSpeakerRemoveImage(): JsonResponse
    {
        try {
            $id = request('id');
            $speaker = SpeakersRepository::findById($id);

            removeFileS3($speaker->getImage());

            $speaker->setImage(null);
            $speaker->save();

            $response['item'] = [];

            return response()->json($response, 200);
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();

            return response()->json($response, 400);
        }
    }

    /**
     * post remove landing page speaker
     *
     */
    public function postSpeakerRemove(): JsonResponse
    {
        try {
            $id = request('id');
            $speaker = SpeakersRepository::findById($id);

            removeFileS3($speaker->getImage());

            $speaker->deleteById($id);

            Cache::forget(prefixCacheName('speakers'));

            $response['item'] = [];

            return response()->json($response, 200);
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();

            return response()->json($response, 400);
        }
    }

    /**
     * get list landing page speaker
     *
     */
    public function postSpeakerList(): JsonResponse
    {
        try {
            $speakers = SpeakersService::all();

            $response['item'] = $speakers;

            return response()->json($response, 200);
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();

            return response()->json($response, 400);
        }
    }

    /**
     * post update landing page speaker
     *
     */
    public function postSpeakerUpdate(): JsonResponse
    {
        try {
            $id = request('id');
            $name = request('name');
            $title = request('job_title');
            $photo = Eventy::uploadFileS3('image');

            $speaker = SpeakersRepository::findById($id);
            $speaker->setName($name);
            $speaker->setJobTitle($title);
            if ($photo) {
                $speaker->setImage($photo);
            }
            $speaker->save();

            Cache::forget(prefixCacheName('speakers'));

            $response['item'] = SpeakersService::all();

            return response()->json($response, 200);
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();

            return response()->json($response, 400);
        }
    }

    /**
     * post save landing page speaker
     *
     */
    public function postSpeakerSave(): JsonResponse
    {
        try {
            $name = request('speaker_name');
            $title = request('speaker_job_title');
            $photo = Eventy::uploadFileS3('speaker_image');
            $sort = SpeakersRepository::latestBySort()->getSort() + 1;

            $speaker = new SpeakersRepository();
            $speaker->setName($name);
            $speaker->setJobTitle($title);
            if ($photo) {
                $speaker->setImage($photo);
            }
            $speaker->setSort($sort);
            $speaker->save();

            Cache::forget(prefixCacheName('speakers'));

            $response['item'] = $speaker;

            return response()->json($response, 202);
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();

            return response()->json($response, 400);
        }
    }

    public function getSpeakerMoveUp($id)
    {
        $speaker = SpeakersRepository::findById($id);

        if ($speaker->getSort() == 1) {
            CRUDBooster::redirectBack('The data is already at the top of the list!', 'danger');
        } else if ($speaker->getSort() > 1) {
            $above = SpeakersRepository::findBySort($speaker->getSort() - 1);
            if ($above->getId() != '') {
                $above->setSort($speaker->getSort());
                $above->save();
            }

            $speaker->setSort($speaker->getSort() - 1);
            $speaker->save();
        }

        Cache::forget(prefixCacheName('speakers'));

        CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
    }

    public function getSpeakerMoveDown($id)
    {
        $speaker = SpeakersRepository::findById($id);

        $above = SpeakersRepository::findBySort($speaker->getSort() + 1);
        if ($above->getId() == '') {
            CRUDBooster::redirectBack('The data is already at the bottom of the list!', 'danger');
        } else if ($above->getId() != '') {
            if ($above->getId() != '') {
                $above->setSort($speaker->getSort());
                $above->save();
            }

            $speaker->setSort($speaker->getSort() + 1);
            $speaker->save();
        }

        Cache::forget(prefixCacheName('speakers'));

        CRUDBooster::redirectBack('The data order has been changed successfully!', 'success');
    }

    /**
     * post update landing page
     *
     */
    public function postLandingPage()
    {
        $landingpage_browser_popup_status = request()->input('landingpage_browser_popup_status') ?? 'nonactive';
        $landingpage_about_title_text = request()->input('landingpage_about_title_text');
        $landingpage_about_description_text = request()->input('landingpage_about_description_text');
        $landingpage_footer_text = request()->input('landingpage_footer_text');
        $landingpage_whatsapp = request()->input('landingpage_whatsapp');
        $landingpage_email = request()->input('landingpage_email');
        $landingpage_website = request()->input('landingpage_website');
        $landingpage_banner_image_desktop = Eventy::uploadFileS3('landingpage_banner_image_desktop');
        // $landingpage_banner_image_mobile = Eventy::uploadFileS3('landingpage_banner_image_mobile');

        if (CRUDBooster::isSuperadmin()) {
            $save = $this->saveUpdatePreference('landingpage_browser_popup_status', $landingpage_browser_popup_status);
        }
        $save = $this->saveUpdatePreference('landingpage_about_title_text', $landingpage_about_title_text);
        $save = $this->saveUpdatePreference('landingpage_about_description_text', $landingpage_about_description_text);
        $save = $this->saveUpdatePreference('landingpage_footer_text', $landingpage_footer_text);
        $save = $this->saveUpdatePreference('landingpage_whatsapp', $landingpage_whatsapp);
        $save = $this->saveUpdatePreference('landingpage_email', $landingpage_email);
        $save = $this->saveUpdatePreference('landingpage_website', $landingpage_website);

        if (!preferGet('landingpage_banner_image_desktop')) {
            $save = $this->saveUpdatePreference('landingpage_banner_image_desktop', $landingpage_banner_image_desktop);
        }
        // if (!preferGet('landingpage_banner_image_mobile')) {
        //     $save = $this->saveUpdatePreference('landingpage_banner_image_mobile', $landingpage_banner_image_mobile);
        // }

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update web
     *
     */
    public function postWeb()
    {
        $help_center_link = request()->input('help_center_link');
        $home_slider_type = request()->input('home_slider_type');
        $photobooth_link = request()->input('photobooth_link');
        $iframe_player_type = request()->input('iframe_player_type');
        $home_menu_text = request()->input('home_menu_text');
        $booth_menu_text = request()->input('booth_menu_text');
        $main_stage_menu_text = request()->input('main_stage_menu_text');
        $classroom_menu_text = request()->input('classroom_menu_text');
        $booth_menu_status = request()->input('booth_menu_status') ?? 'nonactive';
        $main_stage_menu_status = request()->input('main_stage_menu_status') ?? 'nonactive';
        $classroom_menu_status = request()->input('classroom_menu_status') ?? 'nonactive';
        $photobooth_status = request()->input('photobooth_status') ?? 'nonactive';

        $save = $this->saveUpdatePreference('help_center_link', $help_center_link);
        $save = $this->saveUpdatePreference('home_slider_type', $home_slider_type);
        $save = $this->saveUpdatePreference('photobooth_link', $photobooth_link);
        $save = $this->saveUpdatePreference('iframe_player_type', $iframe_player_type);

        // floating menu
        $save = $this->saveUpdatePreference('home_menu_text', $home_menu_text);
        $save = $this->saveUpdatePreference('booth_menu_text', $booth_menu_text);
        $save = $this->saveUpdatePreference('main_stage_menu_text', $main_stage_menu_text);
        $save = $this->saveUpdatePreference('classroom_menu_text', $classroom_menu_text);
        $save = $this->saveUpdatePreference('booth_menu_status', $booth_menu_status);
        $save = $this->saveUpdatePreference('main_stage_menu_status', $main_stage_menu_status);
        $save = $this->saveUpdatePreference('classroom_menu_status', $classroom_menu_status);
        $save = $this->saveUpdatePreference('photobooth_status', $photobooth_status);

        // icon active
        if (!preferGet('home_menu_icon_active')) {
            $home_menu_icon_active = Eventy::uploadFileS3('home_menu_icon_active');
            $save = $this->saveUpdatePreference('home_menu_icon_active', $home_menu_icon_active);
        }
        if (!preferGet('booth_menu_icon_active')) {
            $booth_menu_icon_active = Eventy::uploadFileS3('booth_menu_icon_active');
            $save = $this->saveUpdatePreference('booth_menu_icon_active', $booth_menu_icon_active);
        }
        if (!preferGet('main_stage_menu_icon_active')) {
            $main_stage_menu_icon_active = Eventy::uploadFileS3('main_stage_menu_icon_active');
            $save = $this->saveUpdatePreference('main_stage_menu_icon_active', $main_stage_menu_icon_active);
        }
        if (!preferGet('classroom_menu_icon_active')) {
            $classroom_menu_icon_active = Eventy::uploadFileS3('classroom_menu_icon_active');
            $save = $this->saveUpdatePreference('classroom_menu_icon_active', $classroom_menu_icon_active);
        }
        if (!preferGet('schedule_menu_icon_active')) {
            $schedule_menu_icon_active = Eventy::uploadFileS3('schedule_menu_icon_active');
            $save = $this->saveUpdatePreference('schedule_menu_icon_active', $schedule_menu_icon_active);
        }
        if (!preferGet('sponsor_menu_icon_active')) {
            $sponsor_menu_icon_active = Eventy::uploadFileS3('sponsor_menu_icon_active');
            $save = $this->saveUpdatePreference('sponsor_menu_icon_active', $sponsor_menu_icon_active);
        }
        if (!preferGet('booth_floor_menu_icon_active')) {
            $booth_floor_menu_icon_active = Eventy::uploadFileS3('booth_floor_menu_icon_active');
            $save = $this->saveUpdatePreference('booth_floor_menu_icon_active', $booth_floor_menu_icon_active);
        }
        if (!preferGet('booth_list_menu_icon_active')) {
            $booth_list_menu_icon_active = Eventy::uploadFileS3('booth_list_menu_icon_active');
            $save = $this->saveUpdatePreference('booth_list_menu_icon_active', $booth_list_menu_icon_active);
        }

        // icon nonactive
        if (!preferGet('home_menu_icon_nonactive')) {
            $home_menu_icon_nonactive = Eventy::uploadFileS3('home_menu_icon_nonactive');
            $save = $this->saveUpdatePreference('home_menu_icon_nonactive', $home_menu_icon_nonactive);
        }
        if (!preferGet('booth_menu_icon_nonactive')) {
            $booth_menu_icon_nonactive = Eventy::uploadFileS3('booth_menu_icon_nonactive');
            $save = $this->saveUpdatePreference('booth_menu_icon_nonactive', $booth_menu_icon_nonactive);
        }
        if (!preferGet('main_stage_menu_icon_nonactive')) {
            $main_stage_menu_icon_nonactive = Eventy::uploadFileS3('main_stage_menu_icon_nonactive');
            $save = $this->saveUpdatePreference('main_stage_menu_icon_nonactive', $main_stage_menu_icon_nonactive);
        }
        if (!preferGet('classroom_menu_icon_nonactive')) {
            $classroom_menu_icon_nonactive = Eventy::uploadFileS3('classroom_menu_icon_nonactive');
            $save = $this->saveUpdatePreference('classroom_menu_icon_nonactive', $classroom_menu_icon_nonactive);
        }

        // booth
        if (!preferGet('booth_side_banner')) {
            $booth_side_banner = Eventy::uploadFileS3('booth_side_banner');
            $save = $this->saveUpdatePreference('booth_side_banner', $booth_side_banner);
        }
        if (!preferGet('booth_audio')) {
            $booth_audio = Eventy::uploadFileS3('booth_audio');
            $save = $this->saveUpdatePreference('booth_audio', $booth_audio);
        }

        // photo booth
        if (!preferGet('photobooth_image')) {
            $photobooth_image = Eventy::uploadFileS3('photobooth_image');
            $save = $this->saveUpdatePreference('photobooth_image', $photobooth_image);
        }

        // design background
        if (!preferGet('home_background_image')) {
            $home_background_image = Eventy::uploadFileS3('home_background_image');
            $save = $this->saveUpdatePreference('home_background_image', $home_background_image);
        }
        if (!preferGet('booth_background_image')) {
            $booth_background_image = Eventy::uploadFileS3('booth_background_image');
            $save = $this->saveUpdatePreference('booth_background_image', $booth_background_image);
        }
        if (!preferGet('main_stage_background_image')) {
            $main_stage_background_image = Eventy::uploadFileS3('main_stage_background_image');
            $save = $this->saveUpdatePreference('main_stage_background_image', $main_stage_background_image);
        }
        if (!preferGet('classroom_background_image')) {
            $classroom_background_image = Eventy::uploadFileS3('classroom_background_image');
            $save = $this->saveUpdatePreference('classroom_background_image', $classroom_background_image);
        }

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update register page
     *
     */
    public function postRegister()
    {
        $register_title_text = request()->input('register_title_text');
        $register_caption_text = request()->input('register_caption_text');

        $save = $this->saveUpdatePreference('register_title_text', $register_title_text);
        $save = $this->saveUpdatePreference('register_caption_text', $register_caption_text);

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update login page
     *
     */
    public function postLogin()
    {
        $login_title_text = request()->input('login_title_text');
        $login_caption_text = request()->input('login_caption_text');
        $register_title_text = request()->input('register_title_text');
        $register_caption_text = request()->input('register_caption_text');
        $at_event_title_text = request()->input('at_event_title_text');
        $at_event_caption_text = request()->input('at_event_caption_text');

        $login_image = Eventy::uploadFileS3('login_image');

        $save = $this->saveUpdatePreference('login_title_text', $login_title_text);
        $save = $this->saveUpdatePreference('login_caption_text', $login_caption_text);
        $save = $this->saveUpdatePreference('register_title_text', $register_title_text);
        $save = $this->saveUpdatePreference('register_caption_text', $register_caption_text);
        $save = $this->saveUpdatePreference('at_event_title_text', $at_event_title_text);
        $save = $this->saveUpdatePreference('at_event_caption_text', $at_event_caption_text);

        if (!preferGet('login_image')) {
            $save = $this->saveUpdatePreference('login_image', $login_image);
        }

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update zoom
     *
     */
    public function postZoom()
    {
        $zoom_api_key = request()->input('zoom_api_key');
        $zoom_api_secret = request()->input('zoom_api_secret');

        $save = $this->saveUpdatePreference('zoom_api_key', $zoom_api_key);
        $save = $this->saveUpdatePreference('zoom_api_secret', $zoom_api_secret);

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    /**
     * post update event
     *
     */
    public function postEvents()
    {
        $pickup_words = request()->input('pickup_words');
        $pickup_words_grand = request()->input('pickup_words_grand');
        $name_events = request()->input('name_events');
        $date_start_events = request()->input('date_start_events');
        $date_end_events = request()->input('date_end_events');
        $timezone_events = request()->input('timezone_events');
        $free_events = request()->input('free_events') ?? 'no';
        $lock_login_wording_events = request()->input('lock_login_wording_events');
        $sirclo_information = Eventy::uploadFileS3('sirclo_information');
        $bg_ticket = Eventy::uploadFileS3('bg_ticket');
        $bg_sessions = Eventy::uploadFileS3('bg_sessions');
        $logo_events = Eventy::uploadFileS3('logo_events');
        $logo_landingpage = Eventy::uploadFileS3('logo_landingpage');
        $icon_events = Eventy::uploadFileS3('icon_events');
        $color_primary = request()->input('color_primary');
        $color_secondary = request()->input('color_secondary');
        $color_tertiary = request()->input('color_tertiary');
        // $color_quaternary = request()->input('color_quaternary');

        $save = $this->saveUpdatePreference('pickup_words', $pickup_words);
        $save = $this->saveUpdatePreference('pickup_words_grand', $pickup_words_grand);
        $save = $this->saveUpdatePreference('name_events', $name_events);
        $save = $this->saveUpdatePreference('date_start_events', $date_start_events);
        $save = $this->saveUpdatePreference('date_end_events', $date_end_events);
        $save = $this->saveUpdatePreference('timezone_events', $timezone_events);
        $save = $this->saveUpdatePreference('free_events', $free_events);
        $save = $this->saveUpdatePreference('lock_login_wording_events', $lock_login_wording_events);
        $save = $this->saveUpdatePreference('color_primary', $color_primary);
        $save = $this->saveUpdatePreference('color_secondary', $color_secondary);
        $save = $this->saveUpdatePreference('color_tertiary', $color_tertiary);
        // $save = $this->saveUpdatePreference('color_quaternary', $color_quaternary);

        if (!preferGet('sirclo_information')) {
            $save = $this->saveUpdatePreference('sirclo_information', $sirclo_information);
        }
        if (!preferGet('bg_ticket')) {
            $save = $this->saveUpdatePreference('bg_ticket', $bg_ticket);
        }
        if (!preferGet('bg_sessions')) {
            $save = $this->saveUpdatePreference('bg_sessions', $bg_sessions);
        }
        if (!preferGet('logo_events')) {
            $save = $this->saveUpdatePreference('logo_events', $logo_events);
        }
        if (!preferGet('logo_landingpage')) {
            $save = $this->saveUpdatePreference('logo_landingpage', $logo_landingpage);
        }
        if (!preferGet('icon_events')) {
            $save = $this->saveUpdatePreference('icon_events', $icon_events);
        }

        if ($save) {
            CRUDBooster::redirectBack('Successfully changed preferences', 'success');
        } else {
            CRUDBooster::redirectBack('Oops, something went wrong!', 'error');
        }
    }

    public function getDeleteImagePrefer($key)
    {
        $check = Preference::where('key', $key)->first();

        $id = ($check ? $check->id : null);
        $image = ($check ? $check->content : null);
        Eventy::removeFileS3($image);

        $save['updated_at'] = dateTime();
        $save['content'] = null;

        $update = DB::table('preferences')->where('id', $id)->update($save);

        if ($update) {
            Cache::forget(prefixCacheName($key));
        }

        CRUDBooster::redirectBack('Successfully deleted image', 'success');
    }
}
