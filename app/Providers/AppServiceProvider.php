<?php

namespace App\Providers;

use App\Models\BoothCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::preventLazyLoading(!app()->isProduction());

        if (!app()->runningInConsole()) {
            view()->share([
                'boothCategories' => BoothCategory::withCount('booths')->get(),
            ]);
        }
    }
}
