new Vue({
    el: '#root',
    mixins: [Helper],
    name: 'appPaymentConfirmation',
    data() {
        return {
            invoice,
            name,
            price: this.formatRupiah(amount),
            fileLocation: '',
            file: '',
        }
    },
    mounted() {
        this.loadClass();
        this.$nextTick(() => {
            setTimeout(() => {
                $('.login100-form').scrollTop(0);
            }, 500);
        });
    },
    methods: {
        changeFile(e) {
            let path = e.target.value,
                file = this.$refs.file.files[0];

            if (!(/\.jpe?g|JPE?G|PNG|png$/i.test(path))) {
                this.$swal.fire(
                    'Only support types : jpg, jpeg, png',
                    '',
                    'warning'
                );
                this.fileLocation = '';
                $('#file').val('');
            } else if (file.size > 3000000) {
                this.$swal.fire(
                    'Maximun size is 3MB',
                    '',
                    'warning'
                )
                this.fileLocation = '';
                $('#file').val('');
            } else {
                if (path !== '') {
                    this.file = file;

                    let slice = path.split('\\');
                    let filename = slice[slice.length - 1];

                    this.fileLocation = filename;
                } else {
                    this.fileLocation = '';
                    $('#file').val('');
                }
            }
        },
        changePriceFormat() {
            this.price = this.formatRupiah(this.price);
        },

        postFindInvoiceId() {
            this.price = "" + this.formatRupiah(this.price);

            if (this.invoice != '' && this.invoice.length >= 16) {
                this.loadShow();
                setTimeout(() => {
                    this.$http.post('/find-invoice', {
                        invoice: this.invoice
                    }).then(({data: {item}}) => {
                        this.loadHide();
                        this.price = this.formatRupiah("" + item.payment.amount);
                        this.name = item.payment.name;
                        $('input[name=date]').attr('min', item.payment.created_at);

                        this.$nextTick(() => {
                            $('input').focus();
                        });
                    }).catch((err) => {
                        this.loadHide();
                        this.emptyForm();
                        if (!err.response) {
                            console.log(err);
                            this.$toast.fire({
                                icon: 'error',
                                title: err.message,
                            });
                        }
                    });
                }, 200);
            } else {
                this.price = "" + this.formatRupiah('');
            }
        },
        emptyForm() {
            this.invoice = '';
            this.name = '';
            this.price = "" + this.formatRupiah('');
            this.fileLocation = '';
            this.file = '';
        },
        postSubmitPaymentConfirmation() {
            let input = $('.validate-input .input100');

            //check required column
            for (var i = 0; i < input.length; i++) {
                if (this.validate(input[i]) === false) {
                    input[i].focus();
                    this.showValidate(input[i]);

                    return false;
                }

                this.hideValidate(input[i]);
            }

            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure to complete payment confirmation?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    let formData = new FormData();
                    formData.append('no_invoice', this.invoice);
                    formData.append('account_name', this.name);
                    formData.append('paid_amount', this.price);
                    formData.append('paid_date', this.$refs.date.value);
                    formData.append('file', this.file);

                    this.loadShow();
                    this.$http.post('/confirmation', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(({ data }) => {
                        this.emptyForm();

                        this.$toast.fire({
                            icon: 'success',
                            title: data.message
                        });

                        setTimeout(() => {
                            window.location.href = data.data.redirect_url;
                        }, 4000);
                    }).catch((err) => {
                        this.loadHide();
                        if (!err.response) {
                            console.log(err);
                            this.$toast.fire({
                                icon: 'error',
                                title: err.message,
                            });
                        }
                    });
                }
            });
        },
    }
});
