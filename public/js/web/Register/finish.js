new Vue({
    name: 'appFinish',
    el: '#root',
    mixins: [Helper],
    data() {
        return {
            payment,
            limit: 0,
            interval: null,
            submitPayment: false,
        }
    },
    methods: {
        confirmPayment() {
            if (this.payment.method === 'OVO') {
                this.$swal.fire({
                    title: 'Enter your OVO Number',
                    input: 'number',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    inputValidator: (value) => {
                        if (!value) {
                            return 'Please input your number!'
                        }
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.loadShow();
                        this.$http.post(`/submit-payment/${this.payment.id}`, {
                            phone: result.value,
                        }).then(() => {
                            this.limit = 0;
                            this.checkPaymentStatus();
                        }).catch((err) => {
                            this.loadHide();
                            if (!err.response) {
                                console.log(err);
                                this.$toast.fire({
                                    icon: 'error',
                                    title: err.message,
                                });
                            }
                        })
                    }
                });
            } else {
                this.loadShow();
                window.location.href = confirmUrl;
                // this.$swal.fire({
                //     title: 'Confirmation',
                //     text: 'Are you sure to complete registration?',
                //     icon: 'warning',
                //     showCancelButton: true,
                //     confirmButtonColor: '#00B2EF',
                //     cancelButtonColor: '#FF4A4B',
                //     confirmButtonText: 'Yes',
                // }).then((confirm) => {
                //     if (confirm.value) {
                //
                //     }
                // });
            }
        },
        changePaymentMethod() {
            this.$swal.fire({
                title: 'Confirm change?',
                text: 'A new invoice will be issued, and the old one will expire.',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    this.loadShow();
                    this.$http.post(`/change-payment-method/${this.payment.id}`)
                        .then(({ data }) => {
                            this.$toast.fire({
                                icon: 'success',
                                title: 'You will be redirected to the payment method page.'
                            });

                            setTimeout(() => {
                                window.location.href = data.data.data.url;
                            }, 3000);
                        }).catch((err) => {
                            this.loadHide();
                            if (!err.response) {
                                console.log(err);
                                this.$toast.fire({
                                    icon: 'error',
                                    title: err.message,
                                });
                            }
                        });
                }
            });
        },
        checkPaymentStatus() {
            this.interval = setInterval(() => {
                if (!this.submitPayment) {
                    let limit = 12; // 1 minutes
                    if (this.limit >= limit) {
                        clearInterval(this.interval);
                        this.loadHide();
                        this.$toast.fire({
                            icon: 'warning',
                            title: 'Please click Pay Now again to repeat your transaction!'
                        });
                    } else {
                        this.loadShow();
                        this.$http.post(`/check-payment-status/${this.payment.id}`)
                            .then(({ data: { item } }) => {
                                if (!item.is_pending) {
                                    this.$toast.fire({
                                        icon: 'success',
                                        title: item.message
                                    });

                                    this.submitPayment = true;
                                    setTimeout(() => {
                                        window.location.href = item.data.url;
                                    }, 3000);
                                } else {
                                    var longToast = Swal.mixin({
                                        toast: true,
                                        position: 'top',
                                        showConfirmButton: false,
                                        timer: 60000,
                                        timerProgressBar: true,
                                        onOpen: (toast) => {
                                            toast.addEventListener('mouseenter', Swal.stopTimer);
                                            toast.addEventListener('mouseleave', Swal.resumeTimer);
                                        }
                                    });
                                    longToast.fire({
                                        icon: 'warning',
                                        title: 'Please open your ovo apps to continue the transaction for 1 minute!'
                                    });

                                    this.limit += 1;
                                }
                            }).catch((err) => {
                                this.loadHide();
                                if (!err.response) {
                                    console.log(err);
                                    this.$toast.fire({
                                        icon: 'error',
                                        title: err.message,
                                    });
                                }
                            });
                    }
                } else {
                    clearInterval(this.interval);
                }
            }, 5000);
        },

    }
});
