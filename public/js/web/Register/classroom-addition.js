new Vue({
    el: '#root',
    name: 'appTicket',
    mixins: [Helper],
    data() {
        return {
            participant,
            payment: {
                id: payment.id,
                amount: payment.amount,
                initial_amount: payment.initial_amount,
                ticket_id: payment.ticket_id
            },
            method_changed: method_changed,

            selectedIndexTicket: null,
            selectedIndexClassroom: [],
            tickets: tickets,
            ticketClassroom: [],
            ticketType: '',
            ticketOnly: 0,
            totalNominal: 'Rp 0',
            totalPaidNominal: 'Rp 0',
            paidAmount: 0,
            totalAmount: 0,
            varFee: 0,
            vatFee: 0,
            fixedFee: 0,
            totalFee: 'Rp 0',
            virtualAccountBanks: [
                {
                    id: 1,
                    name: 'BCA',
                    image: '/images/payment/img_bca_logo.svg',
                },
                {
                    id: 2,
                    name: 'BNI',
                    image: '/images/payment/img_bni_logo.svg',
                },
                {
                    id: 3,
                    name: 'MANDIRI',
                    image: '/images/payment/img_mandiri_logo.svg',
                },
                {
                    id: 4,
                    name: 'BRI',
                    image: '/images/payment/img_bri_logo.svg',
                },
                {
                    id: 5,
                    name: 'PERMATA',
                    image: '/images/payment/img_permata_logo.svg',
                }],
            indexVirtualAccount: '',

            paymentMethods: paymentMethods,
            selectedPaymentType: '',
            selectedPaymentMethod: '',
            selectBankCode: '',

            // plugin
            swiper: '',
            slidePrev: 0,
            slideCurrent: 0,
            slideNext: 1,
            slideLength: 4,
        }
    },
    mounted() {
        self = this;

        $(document).keydown(function (e) {
            if (e.which === 9 || e.keyCode === 9 || e.key === "Tab") {
                return false;
            }
        });
        $(document).keypress(function (e) {
            if (e.which === 13 || e.keyCode === 13 || e.key === "Enter") {
                return false;
            }
        });

        this.initSwiper();
        this.checkMethodChanged();

        $('.ic-caret-slide').click(function () {
            if ($(this).hasClass('rotate-up')) {
                $(this).removeClass('rotate-up');
                $(this).addClass('rotate-down');
            } else {
                $(this).removeClass('rotate-down');
                $(this).addClass('rotate-up');
            }

            $('.channel-list').slideToggle();

            setTimeout(() => {
                self.updateSwiperHeight();
            }, 500);
        });
    },
    computed: {
        currentTicket() {
            return this.tickets[this.selectedIndexTicket] || {};
        }
    },
    methods: {
        updateSwiperHeight: function () {
            this.swiper.updateAutoHeight(100);
        },
        checkMethodChanged: function () {
            if (this.method_changed) {
                this.participant.ticket_id = this.payment.ticket_id;
                this.participant.classroom = schedule_ids;
                this.participant.ticketClassroom = ticket_schedule_ids;
                this.paidAmount = parseInt(this.payment.initial_amount);
                this.totalNominal = this.formatRupiah('' + this.paidAmount);
                this.swiper.slideTo(3);
            } else {
                this.setDefaultData();
                this.$nextTick(function () {
                    this.swiper.slideTo(1);
                });
            }
        },
        logOut: function () {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure to logout ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then(function (confirm) {
                if (confirm.value) {
                    window.location.href = url_base + '/logout';
                }
            });
        },
        setActiveMethod: function (method, index = 0) {
            this.selectBankCode = '';

            if (method.name === 'VIRTUAL_ACCOUNT') {
                this.indexVirtualAccount = index;
                this.selectBankCode = this.virtualAccountBanks[index].name;
            }

            this.selectedPaymentType = method.type;
            this.selectedPaymentMethod = method.name;
            this.calculateFee(method.name);
        },
        initSwiper: function () {
            this.swiper = new window.Swiper('.swiper-ticket', {
                draggable: false,
                noSwiping: true,
                allowTouchMove: false,
                autoHeight: true,
                slidesPerView: 1,
                spaceBetween: 20,
            });

            this.swiper.on('slideChange', () => {
                this.ps.update(document.getElementById('scrollContainer').scrollTop = 0);
            });
        },

        // calculate total
        calculateNominal: function () {
            // setup variable
            let sumClassroom = 0;
            this.totalNominal = 'Rp 0';

            // setup ticket
            let ticket = this.tickets[this.selectedIndexTicket];

            this.$nextTick(() => {
                // setup classroom
                let selectedClassroom = this.selectedIndexClassroom;
                selectedClassroom.forEach((indexClassroom) => {
                    sumClassroom += parseInt(this.ticketClassroom[indexClassroom].price);
                });

                // set total nominal
                let totalNominal = parseInt(ticket.price) + sumClassroom;
                this.paidAmount = totalNominal;
                this.totalNominal = this.formatRupiah('' + totalNominal);
            });
        },
        calculateFee: function (method) {
            let vatFee = 0.11;

            let listMethod = {
                BANK_TRANSFER: {
                    fixedFee: 0,
                    varFee: 0
                },
                VIRTUAL_ACCOUNT: {
                    fixedFee: 6306,
                    varFee: 0
                },
                QRIS: {
                    fixedFee: 500,
                    varFee: 0.00730
                },
                CREDIT_CARD: {
                    fixedFee: 3500,
                    varFee: 0.030
                },
            }

            this.fixedFee = listMethod[method].fixedFee;
            let tax = listMethod[method].varFee * this.paidAmount;
            let taxFee = (this.fixedFee + tax) * vatFee;
            let totalTax = Math.round(tax + taxFee + this.fixedFee);

            this.varFee = tax;
            this.vatFee = taxFee;
            this.totalFee = this.formatRupiah('' + totalTax);
            this.totalAmount = this.paidAmount + totalTax;
            this.totalPaidNominal = this.formatRupiah('' + this.totalAmount)
        },
        removeSelectedClassroom(data) {
            this.selectedIndexClassroom = this.selectedIndexClassroom.filter(x => x !== data.i);
            this.participant.classroom = this.participant.classroom.filter(x => x !== data.id);
            this.participant.ticketClassroom = this.participant.ticketClassroom.filter(x => x !== data.ticket_schedule_id);
            this.calculateNominal();
        },
        setDefaultData: function () {
            if (this.tickets.length > 0) {
                this.setSelectedTicket(0);
            }
        },
        setSelectedTicket: function (index) {
            if (this.selectedIndexTicket !== index) {
                this.selectedIndexTicket = index;
                this.selectedIndexClassroom = [];
                this.participant.ticket_id = this.tickets[index].id;
                this.participant.classroom = [];
                this.participant.ticketClassroom = [];
                this.ticketClassroom = this.tickets[index].ticket_schedules;
                this.ticketType = this.tickets[index].type;
                this.ticketOnly = this.tickets[index].is_only_ticket;
                this.selectedPaymentMethod = '';
                this.selectedPaymentType = '';
                this.vatFee = 0;
                this.varFee = 0;
                this.fixedFee = 0;
                this.totalFee = 'Rp 0';
                this.totalPaidNominal = 'Rp 0';
                this.calculateNominal();
            }
        },
        setSelectedClassroom: function (e) {
            $(e.currentTarget).toggleClass('active');

            let selectedClassroom = [];
            let participantClassroom;
            let participantTicketClassroom;
            let listClassroom = this.ticketClassroom;

            // setup classroom
            participantClassroom = [];
            participantTicketClassroom = [];
            $(document).find('.btn-cart-classroom.active').each(function () {
                let index = $(this).data('index');
                selectedClassroom.push(index);
                participantClassroom.push(listClassroom[index].schedule_id);
                participantTicketClassroom.push(listClassroom[index].id);
            });

            this.selectedIndexClassroom = selectedClassroom;
            this.participant.classroom = participantClassroom;
            this.participant.ticketClassroom = participantTicketClassroom;
            this.calculateNominal();
        },
        slideToPrev: function () {
            if (this.slideCurrent === 2 && this.ticketOnly) {
                this.swiper.slideTo(0);
                this.slideCurrent = 0;
                this.slideNext = 2;
                this.slidePrev = 0;
            } else {
                this.swiper.slideTo(this.slidePrev);

                this.slideCurrent = this.slidePrev;
                let next = this.slidePrev + 1;
                this.slideNext = (next >= this.slideLength ? this.slideLength - 1 : next);
                let prev = this.slideNext - 2;
                this.slidePrev = (prev < 0 ? 0 : prev);
            }
        },
        slideToNext: function () {
            if (this.slideCurrent === 0) {
                if (this.ticketOnly) {
                    this.swiper.slideTo(2);
                    this.slideNext = 3;
                    this.slideCurrent = 2;
                    this.slidePrev = 0;
                } else {
                    this.swiper.slideTo(1);
                    this.slideNext = 2;
                    this.slideCurrent = 1;
                    this.slidePrev = 0;
                }
            } else if (this.slideCurrent === 1) {
                if (this.selectedIndexClassroom.length < 1) {
                    this.$toast.fire({
                        icon: 'error',
                        title: 'Sorry, you have to choose 1 for Session that has been provided',
                    });
                } else {
                    this.swiper.slideTo(2);
                    this.slideNext = 3;
                    this.slideCurrent = 2;
                    this.slidePrev = 1;
                }
            } else {
                this.swiper.slideTo(this.slideNext);
                this.slideCurrent = this.slideNext;

                let next = this.slideCurrent + 1;
                this.slideNext = (next >= this.slideLength ? this.slideLength - 1 : next);
                let prev = this.slideCurrent - 1;
                this.slidePrev = (prev < 0 ? 0 : prev);
            }

            if (this.slideNext === 3) {
                this.selectedPaymentMethod = '';
                this.selectedPaymentType = '';
                this.vatFee = 0;
                this.varFee = 0;
                this.fixedFee = 0;
                this.totalFee = 'Rp 0';
                this.totalPaidNominal = 'Rp 0';
            }
        },
        postSubmitNoClassroom: function () {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure to continue ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    let payload = {
                        email: this.participant.email,
                        ticket_id: this.participant.ticket_id,
                    };

                    $('#preloader-active').show();
                    this.$http.post('/checkout-no-classroom', payload)
                        .then(({data: {item}}) => {
                            this.$toast.fire({
                                icon: 'success',
                                title: item.message
                            });

                            if (item.redirect_url != undefined) {
                                setTimeout(function () {
                                    window.location.href = item.redirect_url;
                                }, 3000);
                            }
                        }).catch((err) => {
                        this.loadHide();
                        if (!err.response) {
                            console.log(err);
                            this.$toast.fire({
                                icon: 'error',
                                title: err.message,
                            });
                        }
                    });
                }
            });
        },
        postSubmitPayment: function () {
            if (this.selectedPaymentMethod) {
                this.$swal.fire({
                    title: 'Confirmation',
                    text: 'Are you sure to continue ?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#00B2EF',
                    cancelButtonColor: '#FF4A4B',
                    confirmButtonText: 'Yes',
                }).then((confirm) => {
                    if (confirm.value) {
                        let payload = {
                            email: this.participant.email,
                            schedule_ids: this.participant.classroom,
                            ticket_schedule_ids: this.participant.ticketClassroom,
                            type: this.selectedPaymentType,
                            payment_method: this.selectedPaymentMethod,
                            amount: this.totalAmount,
                            initial_amount: this.paidAmount,
                            fixed_fee: this.fixedFee,
                            var_fee: this.varFee,
                            vat_fee: this.vatFee,
                            payment_id: this.payment.id,
                            method_changed: this.method_changed,
                        };

                        if (this.selectedPaymentMethod === 'VIRTUAL_ACCOUNT') {
                            payload.payment_method = this.selectBankCode;
                        }

                        $('#preloader-active').show();
                        this.$http.post('/transactions', payload)
                            .then(({data: {data}}) => {
                                this.$toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });

                                if (data.redirect_url != undefined) {
                                    setTimeout(function () {
                                        window.location.href = data.redirect_url;
                                    }, 3000);
                                }
                            }).catch((err) => {
                            this.loadHide();
                            if (!err.response) {
                                console.log(err);
                                this.$toast.fire({
                                    icon: 'error',
                                    title: err.message,
                                });
                            }
                        });
                    }
                });
            } else {
                this.$toast.fire({
                    icon: 'warning',
                    title: `Please select an available payment method!`
                });
            }
        },
    }
});
