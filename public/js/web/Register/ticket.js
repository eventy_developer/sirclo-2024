new Vue({
    el: '#root', name: 'appTicket', mixins: [Helper], data() {
        return {
            // formGroup
            // participants: [
            //     { name: 'John Doe', email: 'john.doe@example.com' },
            //     { name: 'Jane Smith', email: 'jane.smith@example.com' },
            //     { name: 'Alice Johnson', email: 'alice.johnson@example.com' },
            //     { name: 'Bob Brown', email: 'bob.brown@example.com' },
            //     { name: 'Carol White', email: 'carol.white@example.com' }
            // ],
            participantIdsGroupExist: null,
            participants: [

            ],
            currentParticipant: {
                name: '',
                email: '',
                phone_prefix: '+62',
                phone: '',
                nationality: '',
                job_title: '',
                company: '',
                business_type: '',
                area_of_interests: [],
                password: '',
                retype_password: '',
                agree: false,
            },
            editIndex: null,
            selectize_country: '',
            businessTypes: ['Enterprise Brands / Principal', 'Local Brands / UMKM', 'E-Commerce', 'Government / BUMN', 'Information Technology', 'Transportation / Logistics', 'Media & Advertising', 'Finance', 'Others'],
            areaOfInterests: ['Market Dynamics', 'Consumer Insights', 'Omnichannel Strategy & Business Growth', 'Interactive Experiences', 'Marketing & Engagement', 'Sustainability Practices', 'Digital Transformation', 'Brand.com', 'Logistics & Operations', 'UMKM',], // formGroup

            couponCode: '',
            participant,
            payment: {
                id: payment.id,
                amount: payment.amount,
                initial_amount: payment.initial_amount,
                ticket_id: payment.ticket_id
            },
            method_changed: method_changed,
            selectedIndexTicket: null,
            selectedIndexClassroom: [],
            tickets: tickets,
            ticketClassroom: [],
            ticketType: '',
            groupNumber: 0,
            labelCategories: '',
            ticketOnly: 0,
            totalNominal: 'Rp 0',
            totalPaidNominal: 'Rp 0',
            paidAmount: 0,
            totalAmount: 0,
            varFee: 0,
            vatFee: 0,
            fixedFee: 0,
            totalFee: 'Rp 0',
            virtualAccountBanks: [
                // {
                //     id: 1,
                //     name: 'BCA',
                //     image: '/images/payment/img_bca_logo.svg',
                // },
                {
                    id: 2, name: 'BNI', image: '/images/payment/img_bni_logo.svg',
                }, {
                    id: 3, name: 'MANDIRI', image: '/images/payment/img_mandiri_logo.svg',
                }, {
                    id: 4, name: 'BRI', image: '/images/payment/img_bri_logo.svg',
                }, {
                    id: 5, name: 'PERMATA', image: '/images/payment/img_permata_logo.svg',
                }],
            indexVirtualAccount: '',

            paymentMethods: paymentMethods,
            selectedPaymentType: '',
            selectedPaymentMethod: '',
            selectBankCode: '',

            // plugin
            swiper: '',
            slidePrev: 0,
            slideCurrent: 0,
            slideNext: 1,
            slideLength: 5,
        }
    }, mounted() {
        self = this;

        this.$nextTick(function () {
            this.selectize_country = $('#nationality').selectize({
                onChange: function (value) {
                    self.currentParticipant.nationality = value;
                }
            });
        });

        $(document).keydown(function (e) {
            if (e.which === 9 || e.keyCode === 9 || e.key === "Tab") {
                return false;
            }
        });
        $(document).keypress(function (e) {
            if (e.which === 13 || e.keyCode === 13 || e.key === "Enter") {
                return false;
            }
        });

        this.initSwiper();
        this.checkMethodChanged();

        $('.ic-caret-slide').click(function () {
            if ($(this).hasClass('rotate-up')) {
                $(this).removeClass('rotate-up');
                $(this).addClass('rotate-down');
            } else {
                $(this).removeClass('rotate-down');
                $(this).addClass('rotate-up');
            }

            $('.channel-list').slideToggle();

            setTimeout(() => {
                self.updateSwiperHeight();
            }, 500);
        });
    }, computed: {
        currentTicket() {
            return this.tickets[this.selectedIndexTicket] || {};
        }
    }, watch: {
        'currentParticipant.nationality': function (newVal) {
            if (this.selectize_country) {
                const selectize = this.selectize_country[0].selectize;
                if (selectize.getValue() !== newVal) {
                    selectize.setValue(newVal, true);
                }
            }
        }
    }, methods: {
        checkMaxAreaOfInterests() {
            if (this.currentParticipant.area_of_interests.length > 3) {
                this.currentParticipant.area_of_interests.pop();
                this.$toast.fire({
                    icon: 'error', title: `You can only select up to 3 areas of interest.`,
                });
            }
        }, selectBusinessType(item) {
            this.currentParticipant.business_type = item;
        }, slideToAddParticipant() {
            this.resetCurrentParticipant();
            this.editIndex = null;
            if (this.slideCurrent === 1) {
                this.swiper.slideTo(2);
            }
        }, slideToBack() {
            this.swiper.slideTo(1);
        }, isValidEmail(email) {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(([^<>()[\]\.,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,})$/i;
            return re.test(email);
        }, async validateParticipant() {
            this.errors = {};

            $('#preloader-active').show();

            if (!this.currentParticipant.email) {
                this.errors.email = 'Email is required';
            } else if (!this.isValidEmail(this.currentParticipant.email)) {
                this.errors.email = 'Email is invalid';
            } else {
                try {
                    const {data} = await this.$http.post('/find-email', {email: this.currentParticipant.email});
                    if (data.data === 0) {
                        this.errors.email = 'Email already exists';
                    }
                } catch (err) {
                    if (err.response) {
                        const error = err.response;
                        console.error(error);
                    } else {
                        console.log(err);
                    }
                }
            }

            if (!this.currentParticipant.name) {
                this.errors.name = 'Name is required';
            }
            if (!this.currentParticipant.phone) {
                this.errors.phone = 'Phone is required';
            }
            if (!this.currentParticipant.nationality) {
                this.errors.nationality = 'Nationality is required';
            }
            if (!this.currentParticipant.job_title) {
                this.errors.job_title = 'Job title is required';
            }
            if (!this.currentParticipant.company) {
                this.errors.company = 'Company name is required';
            }
            if (!this.currentParticipant.business_type) {
                this.errors.business_type = 'Type of business is required';
            }
            if (this.currentParticipant.area_of_interests.length === 0) {
                this.errors.area_of_interests = 'At least one area of interest is required';
            } else if (this.currentParticipant.area_of_interests.length > 3) {
                this.errors.area_of_interests = 'You can select up to 3 areas of interest';
            }
            if (!this.currentParticipant.password) {
                this.errors.password = 'Password is required';
            }
            if (this.currentParticipant.password !== this.currentParticipant.retype_password) {
                this.errors.retype_password = 'Passwords do not match';
            }
            if (!this.currentParticipant.agree) {
                this.errors.agree = 'You must agree to the terms';
            }

            $('#preloader-active').hide();

            if (Object.keys(this.errors).length > 0) {
                this.showToastErrors();
                return false;
            }
            return true;
        }, showToastErrors() {
            for (const key in this.errors) {
                if (this.errors.hasOwnProperty(key)) {
                    this.$toast.fire({
                        icon: 'error', title: this.errors[key]
                    });
                }
            }
        }, async saveParticipant() {
            $('#preloader-active').show();
            const isValid = await this.validateParticipant();
            if (isValid) {
                if (this.editIndex !== null) {
                    this.$set(this.participants, this.editIndex, {...this.currentParticipant});
                } else {
                    this.participants.push({...this.currentParticipant});
                }
                this.swiper.slideTo(1);
            }
        }, editParticipant(index) {
            this.currentParticipant = {...this.participants[index]};
            this.editIndex = index;
            if (this.slideCurrent === 1) {
                this.swiper.slideTo(2);
            }
        }, deleteParticipant(index) {
            this.participants.splice(index, 1);
        }, resetCurrentParticipant() {
            this.currentParticipant = {
                name: '',
                email: '',
                phone_prefix: '+62',
                phone: '',
                nationality: '',
                job_title: '',
                company: '',
                business_type: '',
                area_of_interests: [],
                password: '',
                retype_password: '',
                agree: false,
            };
        }, updateSwiperHeight: function () {
            this.swiper.updateAutoHeight(100);
        }, checkMethodChanged: function () {
            if (this.method_changed) {
                this.participant.ticket_id = this.payment.ticket_id;
                this.participant.classroom = schedule_ids;
                this.participant.ticketClassroom = ticket_schedule_ids;
                this.paidAmount = parseInt(this.payment.initial_amount);
                this.totalNominal = this.formatRupiah('' + this.paidAmount);
                this.participantIdsGroupExist = participant_ids_group;
                this.swiper.slideTo(4);
            } else {
                this.setDefaultData();
                this.$nextTick(function () {
                    this.swiper.slideTo(0);
                });
            }
        }, logOut: function () {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure to logout ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then(function (confirm) {
                if (confirm.value) {
                    window.location.href = url_base + '/logout';
                }
            });
        }, setActiveMethod: function (method, index = 0) {
            this.selectBankCode = '';

            if (method.name === 'VIRTUAL_ACCOUNT') {
                this.indexVirtualAccount = index;
                this.selectBankCode = this.virtualAccountBanks[index].name;
            }

            this.selectedPaymentType = method.type;
            this.selectedPaymentMethod = method.name;
            this.calculateFee(method.name);
        }, initSwiper: function () {
            this.swiper = new window.Swiper('.swiper-ticket', {
                draggable: false,
                noSwiping: true,
                allowTouchMove: false,
                autoHeight: true,
                slidesPerView: 1,
                spaceBetween: 20,
            });

            this.swiper.on('slideChange', () => {
                this.ps.update(document.getElementById('scrollContainer').scrollTop = 0);
            });
        },

        // calculate total
        calculateNominal: function () {
            // setup variable
            let sumClassroom = 0;
            this.totalNominal = 'Rp 0';

            // setup ticket
            let ticket = this.tickets[this.selectedIndexTicket];

            this.$nextTick(() => {
                // setup classroom
                let selectedClassroom = this.selectedIndexClassroom;
                selectedClassroom.forEach((indexClassroom) => {
                    sumClassroom += parseInt(this.ticketClassroom[indexClassroom].price);
                });

                // set total nominal
                let totalNominal = parseInt(ticket.price) + sumClassroom;
                this.paidAmount = totalNominal;
                this.totalNominal = this.formatRupiah('' + totalNominal);
            });
        }, calculateFee: function (method) {
            let vatFee = 0.11;

            let listMethod = {
                BANK_TRANSFER: {
                    fixedFee: 0, varFee: 0
                }, VIRTUAL_ACCOUNT: {
                    fixedFee: 6306, varFee: 0
                }, QRIS: {
                    fixedFee: 500, varFee: 0.00730
                }, CREDIT_CARD: {
                    fixedFee: 3500, varFee: 0.030
                },
            }

            this.fixedFee = listMethod[method].fixedFee;
            let tax = listMethod[method].varFee * this.paidAmount;
            let taxFee = (this.fixedFee + tax) * vatFee;
            let totalTax = Math.round(tax + taxFee + this.fixedFee);

            this.varFee = tax;
            this.vatFee = taxFee;
            this.totalFee = this.formatRupiah('' + totalTax);
            this.totalAmount = this.paidAmount;
            this.totalPaidNominal = this.formatRupiah('' + this.totalAmount)
        }, removeSelectedClassroom(data) {
            this.selectedIndexClassroom = this.selectedIndexClassroom.filter(x => x !== data.i);
            this.participant.classroom = this.participant.classroom.filter(x => x !== data.id);
            this.participant.ticketClassroom = this.participant.ticketClassroom.filter(x => x !== data.ticket_schedule_id);
            this.calculateNominal();
        }, setDefaultData: function () {

        }, setResetData: function () {
            // Default values or initializations for each data property
            this.resetCurrentParticipant();
            this.participants = [];
            self = this;

            this.selectedIndexTicket = null;
            this.selectedIndexClassroom = [];
            this.ticketClassroom = [];
            this.ticketType = '';
            this.ticketOnly = 0;
            this.totalNominal = 'Rp 0';
            this.totalPaidNominal = 'Rp 0';
            this.paidAmount = 0;
            this.totalAmount = 0;
            this.varFee = 0;
            this.vatFee = 0;
            this.fixedFee = 0;
            this.totalFee = 'Rp 0';
            this.indexVirtualAccount = '';

            this.selectedPaymentType = '';
            this.selectedPaymentMethod = '';
            this.selectBankCode = '';

            this.slidePrev = 0;
            this.slideCurrent = 0;
            this.slideNext = 1;
            this.slideLength = 3;

            setTimeout(() => {
                self.updateSwiperHeight();
            }, 500);


        }, setSelectedTicket: function (index) {
            if (this.selectedIndexTicket !== index) {
                this.selectedIndexTicket = index;
                this.selectedIndexClassroom = [];
                this.participant.ticket_id = this.tickets[index].id;
                this.participant.classroom = [];
                this.participant.ticketClassroom = [];
                this.ticketClassroom = this.tickets[index].ticket_schedules;
                this.ticketType = this.tickets[index].type;
                this.labelCategories = this.tickets[index].label_categories;
                this.ticketOnly = this.tickets[index].is_only_ticket;
                this.groupNumber = this.tickets[index].group_number;
                this.selectedPaymentMethod = '';
                this.selectedPaymentType = '';
                this.vatFee = 0;
                this.varFee = 0;
                this.fixedFee = 0;
                this.totalFee = 'Rp 0';
                this.totalPaidNominal = 'Rp 0';
                this.calculateNominal();
            }
        }, setSelectedClassroom: function (e) {
            // reset if exists
            if (this.selectedIndexClassroom.length > 0) {
                $('.btn-cart-classroom').removeClass('active');
                this.selectedIndexClassroom = [];
                this.participant.classroom = [];
                this.participant.ticketClassroom = [];
            }

            $(e.currentTarget).toggleClass('active');

            let selectedClassroom = [];
            let participantClassroom;
            let participantTicketClassroom;
            let listClassroom = this.ticketClassroom;

            // setup classroom
            participantClassroom = [];
            participantTicketClassroom = [];

            $(document).find('.btn-cart-classroom.active').each(function () {
                let index = $(this).data('index');
                selectedClassroom.push(index);
                participantClassroom.push(listClassroom[index].schedule_id);
                participantTicketClassroom.push(listClassroom[index].id);
            });

            this.selectedIndexClassroom = selectedClassroom;
            this.participant.classroom = participantClassroom;
            this.participant.ticketClassroom = participantTicketClassroom;
            this.calculateNominal();
        }, slideToPrev: function () {
            if (this.slideCurrent === 3 && this.ticketOnly && this.labelCategories === 'Group') {
                this.swiper.slideTo(1);
                this.slideCurrent = 1;
                this.slideNext = 3;
                this.slidePrev = 0;
            } else if (this.slideCurrent === 3 && this.ticketOnly) {
                this.swiper.slideTo(0);
                this.slideCurrent = 0;
                this.slideNext = 3;
                this.slidePrev = 0;
            } else {
                this.swiper.slideTo(this.slidePrev);

                this.slideCurrent = this.slidePrev;
                let next = this.slidePrev + 1;
                this.slideNext = (next >= this.slideLength ? this.slideLength - 1 : next);
                let prev = this.slideNext - 2;
                this.slidePrev = (prev < 0 ? 0 : prev);
            }
        }, slideToNext: function () {
            if (this.slideCurrent === 0) {
                if (this.ticketOnly && this.labelCategories == 'Group') {
                    this.swiper.slideTo(1);
                    this.slideNext = 3;
                    this.slideCurrent = 1;
                    this.slidePrev = 0;
                } else if (this.ticketOnly) {
                    this.resetCurrentParticipant();
                    this.participants = [];
                    this.swiper.slideTo(3);
                    this.slideNext = 4;
                    this.slideCurrent = 3;
                    this.slidePrev = 0;
                } else if (!this.selectedIndexTicket) {
                    this.$toast.fire({
                        icon: 'error', title: `it is required to select a minimum 1 of tickets.`,
                    });
                    return false;
                } else {
                    this.resetCurrentParticipant();
                    this.participants = [];
                    this.swiper.slideTo(3);
                    this.slideNext = 4;
                    this.slideCurrent = 3;
                    this.slidePrev = 0;
                }
            } else if (this.slideCurrent === 1) {
                if (this.groupNumber > this.participants.length){
                    this.$toast.fire({
                        icon: 'error', title: `it is required to add a minimum ${this.groupNumber} of participants.`,
                    });
                }else{
                    this.swiper.slideTo(3);
                    this.slideNext = 4;
                    this.slideCurrent = 3;
                    this.slidePrev = 1;
                }
            } else {
                this.swiper.slideTo(this.slideNext);
                this.slideCurrent = this.slideNext;
                let next = this.slideCurrent + 1;
                this.slideNext = (next >= this.slideLength ? this.slideLength - 1 : next);
                let prev = this.slideCurrent - 1;
                this.slidePrev = (prev < 0 ? 0 : prev);
            }
            if (this.slideNext === 4) {
                this.selectedPaymentMethod = '';
                this.selectedPaymentType = '';
                this.vatFee = 0;
                this.varFee = 0;
                this.fixedFee = 0;
                this.totalFee = 'Rp 0';
                this.totalPaidNominal = 'Rp 0';
            }
        }, getTicketFree() {
            if (this.couponCode.trim() === '') {
                this.$toast.fire({
                    icon: 'warning', title: 'Please enter a coupon code!'
                });
                return;
            }

            $('#preloader-active').show();

            this.$http.get('/ticket', {params: {coupon: this.couponCode}})
                .then(({data}) => {
                    this.tickets = data.ticket;
                    this.setResetData()
                    $('#preloader-active').hide();
                })
                .catch((err) => {
                    if (err.response) {
                        const error = err.response;
                        this.tickets = error.data.ticket;
                        this.couponCode = '';
                        this.setResetData()
                        console.error(error);
                        $('#preloader-active').hide();
                        this.$toast.fire({
                            icon: 'error', title: error.data.message,
                        });
                    } else {
                        $('#preloader-active').hide();
                        this.$toast.fire({
                            icon: 'error', title: 'An error occurred while processing your request.',
                        });
                    }
                });
        }, postSubmitNoClassroom: function () {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Do you want to continue with this action?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    let payload = {
                        email: this.participant.email,
                        ticket_id: this.participant.ticket_id,
                        schedule_ids: this.participant.classroom,
                        ticket_schedule_ids: this.participant.ticketClassroom,
                        coupon: this.couponCode,
                    };

                    $('#preloader-active').show();
                    this.$http.post('/ticket-free', payload)
                        .then(({data: {data}}) => {
                            this.$toast.fire({
                                icon: 'success', title: data.message
                            });

                            if (data.redirect_url != undefined) {
                                setTimeout(function () {
                                    window.location.href = data.redirect_url;
                                }, 3000);
                            }
                        }).catch((err) => {
                        this.loadHide();
                        if (!err.response) {
                            console.log(err);
                            this.$toast.fire({
                                icon: 'error', title: err.message,
                            });
                        }
                    });
                }
            });
        }, postSubmitPayment: function () {
            if (this.selectedPaymentMethod) {
                this.$swal.fire({
                    title: 'Confirmation',
                    text: 'Do you want to continue with this action?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#00B2EF',
                    cancelButtonColor: '#FF4A4B',
                    confirmButtonText: 'Yes',
                }).then((confirm) => {
                    if (confirm.value) {
                        let payload = {
                            participant_ids_group : this.participantIdsGroupExist,
                            participants_group : this.participants,
                            email: this.participant.email,
                            ticket_id: this.participant.ticket_id,
                            schedule_ids: this.participant.classroom,
                            ticket_schedule_ids: this.participant.ticketClassroom,
                            type: this.selectedPaymentType,
                            payment_method: this.selectedPaymentMethod,
                            amount: this.totalAmount,
                            initial_amount: this.paidAmount,
                            fixed_fee: this.fixedFee,
                            var_fee: this.varFee,
                            vat_fee: this.vatFee,
                            payment_id: this.payment.id,
                            method_changed: this.method_changed,
                        };

                        if (this.selectedPaymentMethod === 'VIRTUAL_ACCOUNT') {
                            payload.payment_method = this.selectBankCode;
                        }

                        $('#preloader-active').show();
                        this.$http.post('/transactions', payload)
                            .then(({data: {data}}) => {
                                this.$toast.fire({
                                    icon: 'success', title: 'Processing your request.'
                                });

                                if (data.redirect_url != undefined) {
                                    setTimeout(function () {
                                        window.location.href = data.redirect_url;
                                    }, 3000);
                                }
                            }).catch((err) => {
                            this.loadHide();
                            console.log(err)
                            const errRes = err.response.data.data
                            if (!err.response) {
                                console.log(err);
                                this.$toast.fire({
                                    icon: 'error', title: err.message,
                                });
                            }
                            if (errRes.redirect_url != undefined) {
                                setTimeout(function () {
                                    window.location.href = errRes.redirect_url;
                                }, 3000);
                            }
                        });
                    }
                });
            } else {
                this.$toast.fire({
                    icon: 'warning', title: `Please select an available payment method!`
                });
            }
        },
    }
})

