new Vue({
    el: '#root',
    mixins: [Helper],
    data() {
        return {
            password: null,
            re_password: null,
        }
    },
    mounted() {
        this.loadClass();
    },
    methods: {
        Change() {
            var input = $('.validate-input .input100');

            check = true;
            for (var i = 0; i < input.length; i++) {
                if (this.validate(input[i]) == false) {
                    input[i].focus();
                    this.showValidate(input[i]);
                    check = false;

                    return check;
                }

                this.hideValidate(input[i]);
            }

            if (this.re_password != this.password) {
                $('input[name=password], input[name=re_password]').parent().attr('data-validate', valid_match);
                this.showValidate($('input[name=password]'));
                this.showValidate($('input[name=re_password]'));

                check = false;
            }

            if (check) {
                this.loadShow();

                this.$http.post('/reset-password', {
                    'key': key,
                    'password': this.password,
                    're_password': this.re_password,
                }).then(({ data: { data } }) => {
                    this.$toast.fire({
                        icon: 'success',
                        title: data.message
                    });

                    setTimeout(() => {
                        window.location.href = url_base + '/login';
                    }, 3000);
                }).catch((err) => {
                    this.empty();
                    this.loadHide();
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                });
            }
        },
        empty() {
            this.password = null;
            this.re_password = null;

            var thisAlert = $('.validate-input .input100');
            $(thisAlert).removeClass('has-val');
        },
    }
});
