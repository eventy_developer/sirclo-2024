new Vue({
    el: '#root',
    mixins: [Helper],
    data() {
        return {
            email: null,
        }
    },
    mounted() {
        this.loadClass();
    },
    methods: {
        Forgot() {
            var input = $('.validate-input .input100');

            check = true;
            for (var i = 0; i < input.length; i++) {
                if (this.validate(input[i]) == false) {
                    input[i].focus();
                    this.showValidate(input[i]);
                    check = false;

                    return check;
                }

                this.hideValidate(input[i]);
            }

            if (check) {
                this.loadShow();

                this.$http.post('/forgot-password', {
                    'email': this.email,
                }).then(({ data: { data } }) => {
                    this.$toast.fire({
                        icon: 'success',
                        title: data.message
                    });

                    setTimeout(() => {
                        window.location.href = url_base + '/login';
                    }, 3000);
                }).catch((err) => {
                    this.empty();
                    this.loadHide();
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                });
            }
        },
        empty() {
            this.email = null;
            var thisAlert = $('.validate-input .input100');
            $(thisAlert).removeClass('has-val');
        },
    }
});
