new Vue({
    el: '#root',
    mixins: [Helper],
    data() {
        return {
            agree : 'agree',
            // form
            participant: {
                name: '',
                email: '',
                password: '',
                retype_password: '',
                company: '',
                job_title: '',
                phone_prefix: '+62',
                phone: '',
                business_type: '',
                nationality: '',
                area_of_interests: [],
            },

            selectize_business_type: '',
            selectize_country: '',
            selectize_phone_prefix: '',
            tnc: true,
            type_of_business: [
                'Enterprise Brands / Principal',
                'Local Brands / UMKM',
                'E-Commerce',
                'Government / BUMN',
                'Information Technology',
                'Transportation / Logistics',
                'Media & Advertising',
                'Finance',
                'Others'
            ],

            area_of_interests: [
                'Market Dynamics',
                'Consumer Insights',
                'Omnichannel Strategy & Business Growth',
                'Interactive Experiences',
                'Marketing & Engagement',
                'Sustainability Practices',
                'Digital Transformation',
                'Brand.com',
                'Logistics & Operations',
                'UMKM',
            ],
        };
    },
    mounted() {
        self = this;
        this.selectize_country = $("#nationality").selectize({
            onChange: function(value) {
                self.participant.nationality = value;
            }
        });

        if (debug) {
            this.autoCompleteForm();
        }
        this.loadClass();
    },
    methods: {
        autoCompleteForm() {
            if (['127.0.0.1', '::1', 'localhost'].indexOf(ipAddress) > -1) {
                this.participant.name = 'Waya';
                this.participant.email = 'waya@eventy.id';
                this.participant.password = '1212';
                this.participant.retype_password = '1212';
                this.participant.company = 'Eventy';
                this.participant.job_title = '-';
                this.participant.phone = '088225261904';
            }
        },
        isSelectedBusiness(item) {
            return this.participant.business_type === item;
        },
        toggleSelectionBusiness(item) {
            if (this.participant.business_type === item) {
                // Deselect the item if it is already selected
                this.participant.business_type = '';
            } else {
                // Select the item and deselect others
                this.participant.business_type = item;
            }
        },
        postRegister() {
            let input = $('.validate-input .input100.validated, .validate-input.validated');

            //check required column
            for (var i = 0; i < input.length; i++) {
                if (this.validate(input[i]) === false) {
                    input[i].focus();
                    this.showValidate(input[i]);

                    return false;
                }

                this.hideValidate(input[i]);
            }

            if (this.participant.area_of_interests.length == 0) {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Please select the Area Interests',
                });
                return false;
            }

            if (this.participant.area_of_interests.length > 3) {
                console.log(agree)
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Please select a maximum of 3 Area Interests',
                });
                return false;
            }

            if (!this.participant.business_type) {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Please select the Type of Business',
                });
                return false;
            }

            //check same password
            if (this.participant.password !== this.participant.retype_password) {
                $('input[name=password], input[name=retype_password]').parent().attr('data-validate', valid_match_password);
                this.showValidate($('input[name=password]'));
                this.showValidate($('input[name=retype_password]'));

                return false;
            }

            if (!this.agree) {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'You must check the box to continue.',
                });
                return false;
            }

            this.loadShow();
            this.$http.post('/register', this.participant)
                .then(({ data: { data } }) => {
                    this.$toast.fire({
                        icon: 'success',
                        title: data.message,
                    });

                    setTimeout(() => {
                        window.location.href = data.redirect_url;
                    }, 3000);
                }).catch((err) => {
                this.loadHide();
                if (!err.response) {
                    console.log(err);
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                }
            });
        },
    }
});
