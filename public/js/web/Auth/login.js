new Vue({
    el: '#root',
    mixins: [Helper],
    data() {
        return {
            email: null,
            password: null,
            agree: null,
        }
    },
    mounted() {
        localStorage.setItem('is_event_locked', is_event_locked);
        localStorage.setItem('date_start', date_start);
        localStorage.setItem('date_start_message', date_start_message);
        localStorage.removeItem('is_popup_login');

        this.loadClass();
    },
    methods: {
        Login() {
            let input = $('.validate-input .input100');

            check = true;
            for (let i = 0; i < input.length; i++) {
                if (this.validate(input[i]) == false) {
                    input[i].focus();
                    this.showValidate(input[i]);
                    check = false;

                    return check;
                }

                this.hideValidate(input[i]);
            }

            if (check) {
                this.loadShow();

                this.$http.post('/login', {
                    '_token': token,
                    'email': this.email,
                    'password': this.password
                }).then(({ data }) => {
                    this.$toast.fire({
                        icon: 'success',
                        title: data.message,
                    });

                    if (data.message !== undefined) {
                        window.location.href = url_base + '/home';
                    }
                }).catch((err) => {
                    this.empty();
                    this.loadHide();
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                });
            }
        },
        empty() {
            this.password = null;

            var thisAlert = $('.validate-input .input100');
            $(thisAlert).removeClass('has-val');
        },
    }
});
