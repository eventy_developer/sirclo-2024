let Classroom = new Vue({
    el: '#appEventy',
    name: 'appClassroom',
    mixins: [CheckMobile, Visitor, Schedule],
    created() {
        this.getSchedule('Class Room');
    },
});
