let MainStage = new Vue({
    el: '#appEventy',
    name: 'appMainStage',
    mixins: [CheckMobile, Visitor, Schedule],
    created() {
        this.getSchedule('Main Stage');
    },
});
