function frame16_9(target) {
    let targetEl = document.querySelector(target);

    function setHeight() {
        let targetWidth = targetEl.clientWidth;
        targetEl.style.height = (targetWidth / 16) * 9 + 'px';
    }

    setHeight();
    window.addEventListener('resize', function() {
        setHeight();
    })
}

function setWrapperHeight(elSelector, targetQuery) {
    let wrapperEl = document.querySelector(elSelector);
    let targetRefEl = document.querySelector(targetQuery);

    function setHeight() {
        let height = targetRefEl.clientHeight;

        if (height < 100) {
            height = 'calc(100vh - 375px)';
        } else {
            height = height + 'px';
        }

        if (screen.width < 992) {
            height = 'auto';
        }
        wrapperEl.style.height = height;
    }

    if ($(targetRefEl).css('display') !== 'none') {
        setHeight();
        $(window).on('window:resize', function (e) {
            setHeight();
        })
    }

}

function asset_path(url) {
    return url_base + '/' + url;
}

function checkIsMobile() {
    if (window.matchMedia(`(min-width: ${breakpoints.xlg}px)`).matches) {
        EventBus.$emit('is_mobile', false);
        // console.log('is desktop')
    } else {
        EventBus.$emit('is_mobile', true);
        // console.log('is mobile');
    }
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function hideLoader() {
    $('#preloader-active').fadeOut();
    $('body').delay(300).css({
        'overflow': 'visible'
    });
}

function showLoader() {
    $('#preloader-active').show();
    $('body').css({
        'overflow': 'visible'
    });
}

function urlify(text) {
    return text.replace(/(https?:\/\/[^\s]+)/g, function(url) {
        return '<a href="' + url + '" target="_blank">' + url + '</a>';
    });
}

function strip_tags(text) {
    return text.replace(/<[^>]+>/g, '');
}

function dateTimeNow() {
    return dayjs().utcOffset(420).format('YYYY-MM-DD HH:mm:ss');
}
