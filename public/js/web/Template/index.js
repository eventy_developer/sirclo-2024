'use strict';
;( function ( document, window, index )
{
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            readURL(this);
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $('#name_'+input.id).text(fileName);
            else
                label.innerHTML = labelVal;
        });

        function readURL(input) {
            if (input.files) {
                console.log(input.files)
                var reader = new FileReader();
                reader.onload = function(e) {
                    console.log(e.target.result)
                    // $('#show_'+input.id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });

    function showPassword(button) {
        var inputPassword = $(button).parent().find('input');
        if (inputPassword.attr('type') === 'password') {
            inputPassword.attr('type', 'text');
        } else {
            inputPassword.attr('type','password');
        }
    }

    function getChatStorageName() {
        return app_name + '-chat-' + window.location.pathname.replaceAll('/', '');
    }

    $(document).ready(function() {
        $('[data-toggle=tooltip]').tooltip({
            trigger: 'hover'
        });

        if (!is_mobile) {
            if (!localStorage.getItem(getChatStorageName())) {
                $('.floating-chat').animate({
                    height: 'toggle',
                    opacity: 'toggle'
                }).find('input').focus();
                localStorage.setItem(getChatStorageName(), 'disabled');
            }
        }

        checkIsMobile();
        $(window).resize(function () {
            $(window).trigger('window:resize');
            checkIsMobile();
        });

        $('.show-password').on('click', function(){
            showPassword(this);
        });

        $(document).on('click', '.btn-float .icon', function () {
            $('.floating-chat').animate({
                height: 'toggle',
                opacity: 'toggle'
            }).find('input').focus();
        });

        $(document).on('click', '.floating-chat .floating-chat-header .btn-close', function () {
            $('.floating-chat').animate({
                height: 'toggle',
                opacity: 'toggle'
            });
        });
    });

}( document, window, 0 ));
