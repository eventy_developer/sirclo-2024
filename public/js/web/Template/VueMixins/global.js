let CheckMobile = {
    data() {
        return {
            is_mobile,
        }
    },
    mounted() {
        EventBus.$on('is_mobile', (val) => {
            this.is_mobile = val;
        });
    },
};

let Visitor = {
    data() {
        return {
            participant,
            currentVisitor: 0,
        }
    },
    created() {
        this.postCurrentVisitor();
        setInterval(() => {
            this.postCurrentVisitor();
        }, 60000);
    },
    methods: {
        postCurrentVisitor() {
            console.log('visitor: 0');
            // this.$http.post('/count-visitor')
            //     .then(({data: {item}}) => {
            //         this.currentVisitor = item.total;
            //     })
            //     .catch((err) => {
            //         console.log(err);
            //         if (!err.response) {
            //             this.$toast.fire({
            //                 icon: 'error',
            //                 title: err.message,
            //             });
            //         }
            //     });
        },
    }
}

let Schedule = {
    data() {
        return {
            is_event_locked: localStorage.getItem('is_event_locked'),
            dates: [],
            dateIndex: 0,
            selectedDate: '',
            locationType: 'All',
            isIndexActiveSchedule: null,
            isActiveScheduleId: null,
            isAnyActiveSchedule: false,
            schedules: [{id: 111}],
            is_more: false,
            detail: {
                id: '',
                images: [],
                videos: [],
                documents: [],
                chats: [],
                pinned_message: '',
            },

            listVideoId: [],
            message: '',
            isHaveNewMessage: false,
            current_channel: '',
            last_channel: '',
            external_zoom_link: '',

            // autoplay
            currentDateTime: '',
            currentInterval: '',
            isLastSchedule: false,
        }
    },
    computed: {
        pinned_message() {
            if (this.detail.pinned_message) {
                return this.is_more ? this.detail.pinned_message : this.detail.pinned_message.substring(0, 50);
            }

            return '';
        },
        scheduleList() {
            if (this.schedules.length > 0) {
                return this.schedules.filter((item) => {
                    let sameDate = item.date === this.selectedDate;
                    if (this.locationType !== 'All') {
                        return (item.schedule.location.name === this.locationType) && sameDate;
                    }

                    return sameDate;
                });
            }

            return [];
        },
    },
    methods: {
        clearNextSchedule() {
            console.log('%cclear at ' + dateTimeNow(), 'color: red;');
            clearInterval(this.currentInterval);
        },
        checkNextSchedule() {
            this.clearNextSchedule();

            let isActive = this.isIndexActiveSchedule;
            if (!this.isLastSchedule) {
                let dateEndSchedule = this.scheduleList[this.isIndexActiveSchedule].datetime_end;

                this.currentInterval = setInterval(() => {
                    this.currentDateTime = dateTimeNow();

                    console.log('current :', this.currentDateTime);
                    console.log('%ccurrent next end : ' + dateEndSchedule, 'color: #2da5ff;');

                    if (this.currentDateTime > dateEndSchedule) {
                        let nextIsActive = isActive + 1;

                        if (nextIsActive < this.scheduleList.length) {
                            let nextSchedule = this.scheduleList[nextIsActive];
                            let dateStartNextSchedule = nextSchedule.datetime_start;

                            console.log('next start :', dateStartNextSchedule);

                            if (this.currentDateTime > dateStartNextSchedule) {
                                this.isIndexActiveSchedule++;
                                this.postFindDetailSchedule(nextIsActive, nextSchedule.id);
                            }
                        } else {
                            this.isLastSchedule = true;
                            this.clearNextSchedule();
                        }
                    }

                }, 1000);
            }
        },
        autoPlaySchedule(auto) {
            if (this.scheduleList.length > 0) {
                let lastScheduleId = this.scheduleList.length - 1;

                if (this.scheduleList.length === this.isIndexActiveSchedule) {
                    if (dateTimeNow() > this.scheduleList[lastScheduleId].datetime_end) {
                        this.isLastSchedule = true;
                        this.clearNextSchedule();
                    }
                }

                if (this.detail.isNowdate) {
                    if (auto || this.detail.isNow || (dateTimeNow() < this.detail.datetime_end)) {
                        this.checkNextSchedule();
                    } else {
                        this.clearNextSchedule();
                    }
                } else {
                    this.clearNextSchedule();
                }
            }
        },
        isHomepage() {
            return path === '/';
        },
        toScheduleLocation(index) {
            this.scheduleList[index].type === 'Classroom' ? window.location.href = '/classroom' :
                window.location.href = '/main-stage';
        },
        searchActiveSchedule() {
            this.isIndexActiveSchedule = 0;
            let isActiveId = null;
            isActiveId = this.schedules.length <= 0 ? '' :
                this.scheduleList.length > 0 ? this.scheduleList[this.isIndexActiveSchedule].id : '';

            for (let i = 0; i < this.schedules.length; i++) {
                if (!this.schedules[i].schedules_on && i === 0) {
                    isActiveId = this.schedules.length <= 0 ? '' :
                        this.scheduleList.length > 0 ? this.scheduleList[this.isIndexActiveSchedule].id : '';

                    this.isActiveScheduleId = isActiveId;
                } else {
                    if (this.schedules[i].isNowdate) {
                        this.dateIndex = i;
                        if (this.scheduleList.length > 0) {
                            isActiveId = this.scheduleList[this.isIndexActiveSchedule].id;
                            for (let [x, xrow] of this.scheduleList.entries()) {
                                if (xrow.isNow) {
                                    isActiveId = xrow.id;
                                    this.isAnyActiveSchedule = true;
                                    this.isActiveScheduleId = isActiveId;
                                    this.isIndexActiveSchedule = x;
                                }
                            }
                        }

                        break;
                    }
                }
            }

            return isActiveId;
        },
        listenNewScheduleMessage() {
            this.isHaveNewMessage = false;
            if (this.detail.chats.length > 0 && (this.detail.chats[this.detail.chats.length - 1].participant_id !== this.participant.id)) {
                this.isHaveNewMessage = true;
            }
            this.last_channel = this.current_channel;

            Echo.channel(this.current_channel)
                .listen('.NewScheduleMessage', ({message}) => {
                    this.isHaveNewMessage = true;
                    this.detail.chats.push(message);

                    let scrollEl = $('.nav-news-list');

                    scrollEl.animate({
                        scrollTop: scrollEl.prop('scrollHeight')
                    }, 700);
                });
        },
        getSchedule(type = '') {
            this.$http.get('/schedule-list', {
                params: {
                    type
                }
            }).then(({data}) => {
                // default loader
                // if (type) {
                //     this.$refs.loadingChatSchedule.classList.add('loader-send');
                //     this.$refs.loadingChatSchedule.classList.add('d-block');
                //     this.$refs.contentChatSchedule.classList.add('loader-show');
                // }

                this.dates = data.data.dates;
                this.schedules = data.data.item;

                if (this.schedules.length > 0) {
                    this.selectedDate = this.dates[0].date;
                }

                let isActiveId = this.searchActiveSchedule();
                if (!this.isHomepage()) {
                    if (this.schedules.length > 0 && isActiveId != '')
                        this.postFindDetailSchedule(this.isIndexActiveSchedule, isActiveId);
                } else {
                    this.isIndexActiveSchedule = null;
                    this.isActiveScheduleId = null;
                }
            }).catch((err) => {
                console.log(err);
                if (!err.response) {
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                }
            });
        },
        setPlatformType(item) {
            if (item.platform_type === 'Youtube') {
                if (iframe_player_type !== 'Plyr') {
                    this.$refs.embedZoom.src = youtubeUrl + item.youtube_url + '?autoplay=1&modestbranding=1&rel=0&loop=1&disablekb=1&playlist=' + item.youtube_url;
                } else {
                    // zoom
                    this.$refs.embedZoom.src = '';
                    $('#embedZoom').hide();

                    // youtube
                    $('#embedVideo').show();
                    this.$refs.plyr.player.embed.loadVideoById(item.youtube_url);
                    this.$refs.plyr.player.embed.playVideo();
                }
            } else if (item.platform_type === 'Zoom') {
                if (iframe_player_type === 'Plyr') {
                    // youtube
                    this.$refs.plyr.player.embed.stopVideo();
                    $('#embedVideo').hide();
                }

                // zoom
                $('#embedZoom').show();
                this.$refs.embedZoom.src = zoomUrl + item.zoom_conference_id + `?schedule_id=${item.id}`;
                if (item.zoom_conference_password !== null) {
                    this.$refs.embedZoom.src = zoomUrl + item.zoom_conference_id + '/' + item.zoom_conference_password + `?schedule_id=${item.id}`;
                }
            }
        },
        postFindDetailSchedule(index, id, auto = true) {
            $('#preloader-active').show();
            if (this.isHomepage()) {
                this.toScheduleLocation(index);
            } else {
                this.isIndexActiveSchedule = index;
                this.isActiveScheduleId = id;

                this.$http.post('/schedule-find', {
                    id
                }).then(({data: {data}}) => {
                    $('#preloader-active').delay(100).fadeOut('slow');

                    // hide loader
                    // this.$refs.loadingChatSchedule.classList.remove('loader-send');
                    // this.$refs.loadingChatSchedule.classList.add('d-none');
                    // this.$refs.loadingChatSchedule.classList.remove('d-block');
                    // this.$refs.contentChatSchedule.classList.remove('loader-show');

                    this.detail = data;
                    this.detail.images = data.attachments.filter(el => el.type === 'Image');
                    this.detail.videos = data.attachments.filter(el => el.type === 'Video');
                    this.detail.documents = data.attachments.filter(el => el.type === 'Document');
                    this.listVideoId = [];

                    delete this.detail.attachments;

                    // set external zoom link
                    this.external_zoom_link = data.zoom_link;


                    // start autoplay
                    if (this.isAnyActiveSchedule || (dateTimeNow() >= this.detail.datetime_start && dateTimeNow() <= this.detail.datetime_end)) {
                        this.autoPlaySchedule(auto);
                    }

                    this.$nextTick(() => {
                        if (this.detail.videos.length > 0) {
                            this.detail.videos.forEach((vid) => {
                                if (!this.listVideoId.includes(vid.id)) {
                                    new ModalVideo(`#play-modal-${vid.id}`);
                                    this.listVideoId.push(vid.id);
                                }
                            });
                        }

                        // let scrollEl = $('.nav-news-list');
                        // setTimeout(function () {
                        //     self.setPlatformType(item);
                        //
                        //     scrollEl.animate({
                        //         scrollTop: scrollEl.prop('scrollHeight')
                        //     }, 500);
                        // }, 2000);
                    });

                    this.current_channel = `${event_channel}.schedule.${this.detail.id}`;
                    Echo.leave(this.last_channel);
                    // this.listenNewScheduleMessage();
                }).catch((err) => {
                    $('#preloader-active').delay(100).fadeOut('slow');
                    console.log(err);
                    if (!err.response) {
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                });
            }
        },
        sendMessage() {
            if (this.$refs.loadingChatSchedule.classList.contains('loader-send')) {
                return false;
            }

            if (this.message.trim() === '') {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Message can\'t be empty!',
                });

                return false;
            }

            if (this.message.length >= 255) {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Message can\'t be longer than 255 characters!',
                });

                return false;
            }

            this.$refs.loadingChatSchedule.classList.add('loader-send');
            this.$refs.loadingChatSchedule.classList.add('d-block');
            this.$refs.contentChatSchedule.classList.add('loader-show');

            this.message = strip_tags(this.message);
            let sending = {
                message: urlify(this.message),
                participant_id: this.participant.id,
                participant_name: 'You',
                participant_image: this.participant.image,
                created_at: dayjs().format('HH:mm A'),
            };
            this.message = '';

            this.$http.post('/schedule-send-message', {
                schedule_id: this.detail.id,
                participants_id: this.participant.id,
                message: sending.message,
            }).then(() => {
                this.isHaveNewMessage = false;
                this.detail.chats.push(sending);

                this.$refs.loadingChatSchedule.classList.remove('loader-send');
                this.$refs.loadingChatSchedule.classList.add('d-none');
                this.$refs.loadingChatSchedule.classList.remove('d-block');
                this.$refs.contentChatSchedule.classList.remove('loader-show');

                $('.nav-news-list').animate({
                    scrollTop: $('.nav-news-list').prop('scrollHeight')
                }, 500);
            }).catch((err) => {
                console.log(err);
                if (!err.response) {
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                }
            });
        },
    },
}

let Sponsor = {
    data() {
        return {
            is_event_locked: localStorage.getItem('is_event_locked'),
            sponsors,
        }
    }
}

let Profile = {
    data() {
        return {
            profile_name: '',
            profile_phone: '',
            profile_email: '',
            profile_image: '',
            profile_company: '',
            profile_job_title: '',
            errors_upload: [],
            current_password: null,
            new_password: null,
            retype_password: null,
            errors_editpass: [],
            uploadCrop: '',
        }
    },
    mounted() {
        this.fillValue();
        this.initCroppie();
    },
    created() {
        self = this;
        $(document).on('click', '#saveCropie', function () {
            self.uploadImageProfile();
        });

        $('#profileModal').on('hidden.bs.modal', function (e) {
            $('#imgHeading').val('');
        });
    },
    methods: {
        fillValue() {
            this.profile_name = entrat_name;
            this.profile_phone = entrat_phone;
            this.profile_email = entrat_email;
            this.profile_image = entrat_image;
            this.profile_company = entrat_company;
            this.profile_job_title = entrat_job_title;
        },
        showHideProfile(val) {
            if (val === true) {
                $("#profile-info").fadeIn();
            } else {
                $("#profile-info").fadeOut(0);
            }
        },
        showHidePassword(val) {
            if (val === true) {
                $("#profile-password").fadeIn();
            } else {
                $("#profile-password").fadeOut(0);
            }
        },
        showEditPassword() {
            this.showHideProfile(false);
            this.showHidePassword(true);
        },
        showProfile() {
            this.showHideProfile(true);
            this.showHidePassword(false);
        },
        loadShow(type) {
            let styles = {
                opacity: "0.4",
                pointerEvents: 'none',
                userSelect: 'none',
            };
            if (type !== 'password') {
                $("#profile-info").css(styles);
            } else {
                $("#profile-password").css(styles);
            }
        },
        loadHide(type) {
            let styles = {
                opacity: "1",
                pointerEvents: 'all',
            };
            if (type !== 'password') {
                $("#profile-info").css(styles);
            } else {
                $("#profile-password").css(styles);
            }
        },

        logOut() {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure to logout ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    window.location.href = url_base + '/logout';
                }
            });
        },

        empty() {
            this.errors_editpass = [];
            this.errors_upload = [];
            this.current_password = null;
            this.retype_password = null;
            this.new_password = null;
        },
        previewImage(event) {
            self = this;
            // Reference to the DOM input element
            let input = event.target;

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    this.uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function () {
                        $('.cr-slider').attr({'min': 0.1000, 'max': 5.0000});
                    });
                    $('#box-profile').fadeToggle(100);
                    $('#profileModal').modal('show');
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        initCroppie() {
            this.uploadCrop = $('#croppie-img').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square'
                },
                boundary: {
                    width: 300,
                    height: 300
                }
            });
        },
        uploadImageProfile() {
            this.loadShow('profile');

            let formData = new FormData();
            this.uploadCrop.croppie('result', {
                type: 'blob',
                // format: 'png'
            }).then((blob) => {
                formData.append('image', blob);
                formData.append('_token', token);
                formData.append('participant_id', entrat_id);

                this.postUploadImage(formData);
            });
        },
        postUploadImage(payload) {
            $('#preloader-active').show();
            this.$http.post('/profile', payload)
                .then(({data}) => {
                    $('#imgHeading').val('');
                    $('#preloader-active').hide();
                    $('#profileModal').modal('hide');

                    this.loadHide('profil');
                    this.empty();

                    this.$toast.fire({
                        icon: 'success',
                        title: data.message,
                    });

                    this.profile_image = data.data.image;
                }).catch((err) => {
                    $('#preloader-active').hide();
                    $('#profileModal').modal('hide');

                    this.loadHide('profil');

                    if (err.response.status === 422) {
                        this.errors_upload = err.response.data.errors;
                    } else {
                        if (!err.response) {
                            console.log(err);
                            this.$toast.fire({
                                icon: 'error',
                                title: err.message,
                            });
                        }
                    }
                });
        },
        editPassword() {
            this.loadShow('password');

            this.$http.post('/profile', {
                _token: token,
                current_password: this.current_password,
                new_password: this.new_password,
                retype_password: this.retype_password,
                participant_id: entrat_id
            }).then(({data}) => {
                this.loadHide('password');
                this.empty();

                this.$toast.fire({
                    icon: 'success',
                    title: data.message,
                });
            }).catch((err) => {
                this.loadHide('password');

                if (err.response.status === 422) {
                    let errors = err.response.data.errors;
                    if (!errors) {
                        errors = {
                            current_password: [err.response.data.message],
                        }
                    }

                    this.errors_editpass = errors;
                } else {
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                }
            });
        }
    }
}

let Feedback = {
    data() {
        return {
            rating: 0,
            suggestion: '',
            is_filled: false,
            emoji_rating: [{
                rating: 1,
                name: 'Tidak Puas',
            }, {
                rating: 2,
                name: 'Kurang Puas',
            }, {
                rating: 3,
                name: 'Biasa Saja',
            }, {
                rating: 4,
                name: 'Puas',
            }, {
                rating: 5,
                name: 'Sangat Puas',
            }],
            emojis: [
                asset_path('example/emoji/ic_angry_standart.png'),
                asset_path('example/emoji/ic_sad_standart.png'),
                asset_path('example/emoji/ic_neutral_standart.png'),
                asset_path('example/emoji/ic_happy_standart.png'),
                asset_path('example/emoji/ic_smile_standart.png')
            ],
            reviews: [
                {
                    name: 'Tidak Puas', rating: 1, sum: 0, percentage: 0
                },
                {
                    name: 'Kurang Puas', rating: 2, sum: 0, percentage: 0
                },
                {
                    name: 'Biasa Saja', rating: 3, sum: 0, percentage: 0
                },
                {
                    name: 'Puas', rating: 4, sum: 0, percentage: 0
                },
                {
                    name: 'Sangat Puas', rating: 5, sum: 0, percentage: 0
                }
            ]
        }
    },
    computed: {
        highlightRating() {
            return this.reviews.reduce(function (max, obj) {
                return max.percentage > obj.percentage ? max : obj
            })
        }
    },
    mounted() {
        self = this;

        if (is_event_locked) {
            this.setCookie(exdays);
        }

        if (!getCookie(app_name + '_hideFeedback' + entrat_id) && entrat_is_filled_feedback != 1 && localStorage.getItem(app_name + '_introStatus' + entrat_id)) {
            $('#feedbackModal').modal('show');
        }
        $('#feedbackModal').on('hidden.bs.modal', function () {
            self.setCookie();// 5 hour
        });
    },
    methods: {
        getEmoji(rating) {
            switch (rating) {
                case 1:
                    return this.emojis[0];
                case 2:
                    return this.emojis[1];
                case 3:
                    return this.emojis[2];
                case 4:
                    return this.emojis[3];
                case 5:
                    return this.emojis[4];
            }
        },
        setRating(rating) {
            this.rating = rating;
        },
        setCookie(exdays = 0.13) {
            setCookie(app_name + '_hideFeedback' + entrat_id, true, exdays);
        },
        avoidFeedback(type) {
            // feedback will show up again until exdays
            exdays = 0.13; // 3 hours
            if (type === 'later') {
                exdays = 0.0837; // 2 hours
            }

            this.setCookie(exdays);
            $('#feedbackModal').modal('hide');
        },
        saveFeedback() {
            if (this.rating === 0) {
                this.$toast.fire({
                    icon: 'warning',
                    title: 'Please tell how you feel',
                });
                return;
            }

            showLoader();
            this.$http.post('/feedback', {
                _token: token,
                rating: this.rating,
                suggestion: this.suggestion,
                participants_id: entrat_id
            }).then(({data}) => {
                hideLoader();
                this.is_filled = true;
                this.reviews = data.item;
                this.$toast.fire({
                    icon: 'success',
                    title: data.message,
                });
            }).catch((err) => {
                console.log(err);
                if (!err.response) {
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                }
            });
        }
    }
}
