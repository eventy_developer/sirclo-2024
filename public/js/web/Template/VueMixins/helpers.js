let Helper = {
    data() {
        return {
            ps: '',
        }
    },
    mounted() {
        if (document.getElementById('scrollContainer')) {
            this.ps = new PerfectScrollbar('#scrollContainer', {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
            });
        }
    },
    methods: {
        loadHide() {
            $('#preloader-active').fadeOut();
            $('body').delay(300).css({
                'overflow': 'visible'
            });
        },
        loadShow() {
            $('#preloader-active').show();
            $('body').css({
                'overflow': 'visible'
            });
        },
        numberOnly(event) {
            // Get the value from the input field
            let value = event.target.value;

            // Remove any leading '0'
            if (value.startsWith('0')) {
                value = value.substring(1);
            }

            // Remove any non-numeric characters
            value = value.replace(/[^0-9]/g, '');

            // Update the v-model
            this.participant.phone = value;
        },
        numberOnlyInTicket(event) {
            // Get the value from the input field
            let value = event.target.value;

            // Remove any leading '0'
            if (value.startsWith('0')) {
                value = value.substring(1);
            }

            // Remove any non-numeric characters
            value = value.replace(/[^0-9]/g, '');

            // Update the v-model
            this.currentParticipant.phone = value;
        },
        formatRupiah(angka, prefix = 'Rp ') {
            // let number_string = angka.replace(/[^,\d]/g, '').toString();
            let number_string = '' + parseInt(angka.replace(/[^,\d]/g, '').toString());
            let split = number_string.split(',');
            let sisa = split[0].length % 3;
            let rupiah = split[0].substr(0, sisa);
            let ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix + (rupiah === '' ? '0' : rupiah);
        },
        validate(input) {
            if($(input).attr('type') == 'email' || $(input).attr('name') == 'email' && $(input).val().trim() != '') {
                if($(input).val().trim() == ''){
                    return false;
                } else {
                    if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                        this.setValidate(input, 'Email format is incorrect');
                        return false;
                    }
                }
            } else {
                if (input.id === 'wrap-nationality' || input.id === 'wrap-business_type') {
                    if ($('#' + input.id + ' select').val().trim() == '') return false;
                } else {
                    if($(input).val().trim() == ''){
                        return false;
                    }
                }
            }
        },
        setValidate(input, message) {
            var thisAlert = $(input).parent();
            $(thisAlert).attr('data-validate', message);
        },
        showValidate(input) {
            if (input.id === 'wrap-nationality' || input.id === 'wrap-business_type') {
                $(input).addClass('alert-validate');
            } else {
                var thisAlert = $(input).parent();
                $(thisAlert).addClass('alert-validate');
            }
        },
        hideValidate(input) {
            if (input.id === 'wrap-nationality' || input.id === 'wrap-business_type') {
                $(input).removeClass('alert-validate');
            } else {
                var thisAlert = $(input).parent();
                $(thisAlert).removeClass('alert-validate');
            }
        },
        loadClass(){
            $('.input100').each(function(){
                $(this).on('blur', function(){
                    if($(this).val().trim() != '') {
                        $(this).addClass('has-val');
                    }
                    else {
                        $(this).removeClass('has-val');
                    }
                })
            });
            $('.validate-form .input100').each(function(){
                $(this).focus(function(){
                    var thisAlert = $(this).parent();
                    $(thisAlert).removeClass('alert-validate');
                });
            });
        },
    },
}
