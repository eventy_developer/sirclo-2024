if (document.getElementById('appHeader')) {
    let appHeader = new Vue({
        name: 'appHeader',
        mixins: [CheckMobile, Profile],
        el: '#appHeader',
        data() {
            return {
                notifications: [],
                blacklistUrl: ['register-ticket', 'register-finish', 'confirmation', 'classroom-addition']
            }
        },
        computed: {
            notReadLatest() {
                return (this.notifications.length - 1) - this.notifications.filter(x => x.detail.is_read === 1).length;
            },
            countNotRead() {
                return this.notifications.filter(x => x.detail.is_read === 0).length;
            }
        },
        created() {
            if (!this.blacklistUrl.includes(path)) {
                // this.logNotify();
                this.getNotificationList();
            }
            setInterval(() => {
                this.TimeNow();
            }, 1000);
        },
        methods: {
            TimeNow() {
                let abbreviation = 'WIB';
                switch (timezone_events) {
                    case 'Asia/Makassar':
                        abbreviation = 'WITA';
                        break;
                    case 'Asia/Jayapura':
                        abbreviation = 'WIT';
                        break;
                    default:
                        abbreviation = 'WIB';
                        break;
                }
                let dayjsTZ = dayjs().utcOffset(420).format('HH:mm');
                let dateTime = `${dayjsTZ} ${abbreviation}`;

                $('#time-map').empty().html(dateTime);
            },
            getNotificationList() {
                this.$http.get(`/notifications`, {
                    headers: {
                        'X-Socket-ID': Echo.socketId()
                    }
                }).then(({data: {data}}) => {
                    this.notifications = data;
                }).catch((err) => {
                    console.log(err);
                    if (!err.response) {
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                });
            },
            readNotification() {
                if (this.countNotRead > 0) {
                    this.$http.post(`/notifications`, {
                        headers: {
                            'X-Socket-ID': Echo.socketId()
                        }
                    }).then(() => {
                        this.notifications.forEach((item) => {
                            item.detail.is_read = 1;
                        });
                    }).catch((err) => {
                        console.log(err);
                        if (!err.response) {
                            this.$toast.fire({
                                icon: 'error',
                                title: err.message,
                            });
                        }
                    });
                }
            },
            logNotify() {
                Echo.channel(`${event_channel}.sendNotification`)
                    .listen('.NewSendNotif', ({message}) => {
                        text = message.message;

                        this.notifications.unshift(message);

                        if (!('Notification' in window)) {
                            alert('Web Notification is not supported');
                            return;
                        }

                        Notification.requestPermission(permission => {
                            let notification = new Notification('New notification!', {
                                body: text, // content for the alert
                                icon: "{{ preferGet('icon_events')?getS3EndpointFile(preferGet('icon_events')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" // optional image url
                            });

                            // link to page on clicking the notification
                            notification.onclick = () => {
                                window.open(window.location.href);
                            };
                        });
                    });
            },
            togglePopupMenu(target) {
                $(target).fadeToggle(100);
            }
        }
    });
}
