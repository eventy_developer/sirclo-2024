toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
});

axios = axios.create({
    baseURL: url_base,
    headers: {
        'X-CSRF-TOKEN': token,
        'X-Requested-With': 'XMLHttpRequest'
    }
});

axios.interceptors.request.use((config) => {
    config.headers['X-Socket-ID'] = window.Echo.socketId() // Echo instance
    return config;
});

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (typeof error.response.status != 'undefined' && error.response.status != null) {
        let status = error.response.status.toString();
        if (status === '419') {
            window.location.reload();
        }

        $('#preloader-active').delay(450).fadeOut('slow');
        if (error.response && !status.startsWith('5')) {
            toast.fire({
                icon: 'error',
                title: error.response.data.message,
            });
        } else {
            toast.fire({
                icon: 'error',
                title: error.message,
            });
        }

        return Promise.reject(error);
    }
});

Vue.prototype.$http = axios;
Vue.prototype.$toast = toast;
Vue.prototype.$swal = Swal;
// Vue.use(VuePlyr, {
//     plyr: {
//         controls: [
//             'play-large', // The large play button in the center
//             // 'restart', // Restart playback
//             // 'rewind', // Rewind by the seek time (default 10 seconds)
//             'play', // Play/pause playback
//             // 'fast-forward', // Fast forward by the seek time (default 10 seconds)
//             'progress', // The progress bar and scrubber for playback and buffering
//             // 'current-time', // The current time of playback
//             // 'duration', // The full duration of the media
//             'mute', // Toggle mute
//             'volume', // Volume control
//             'captions', // Toggle captions
//             'settings', // Settings menu
//             // 'pip', // Picture-in-picture (currently Safari only)
//             // 'airplay', // Airplay (currently Safari only)
//             // 'download', // Show a download button with a link to either the current source or a custom URL you specify in your options
//             'fullscreen', // Toggle fullscreen
//         ],
//         autoplay: true,
//         loop: {
//             active: true,
//         },
//         youtube: {
//             noCookie: false,
//         },
//         invertTime: false,
//         toggleInvert: false,
//         tooltips: { controls: false, seek: false }
//     }
// });
let EventBus = new Vue();

// dayjs
dayjs.extend(dayjs_plugin_utc);
dayjs.extend(dayjs_plugin_timezone);
dayjs.extend(dayjs_plugin_advancedFormat);
dayjs.tz.setDefault(timezone_events);
