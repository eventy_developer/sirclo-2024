let Booth = {
    mixins: [CheckMobile],
    data() {
        return {
            selectedCategoryId: '',
            categories: [],
            searchBooth: ''
        }
    },
    computed: {
        indexCategory() {
            return this.categories.findIndex(val => val.id === this.selectedCategoryId);
        },
        filterBooths() {
            if (this.selectedCategoryId !== '' && this.categories.length > 0) {
                return this.categories[this.indexCategory].booths
                    .filter(booth => booth.name.toLowerCase().includes(this.searchBooth.toLowerCase()));
            }

            return [];
        },
    },
    created() {
        // this.dummyData();
        this.getBoothCategoryWithBooths();
    },
    mounted() {
        this.checkAudioStatus();
    },
    methods: {
        initSelectedBooth() {
            let url = new URLSearchParams(window.location.search);
            this.selectedCategoryId = url.get('category_id') ? parseInt(url.get('category_id')) : this.categories[0].id;
            this.isActiveId = url.get('booth_id') ? parseInt(url.get('booth_id')) : this.categories[0].booths[0].id;
        },
        checkAudioStatus() {
            if (localStorage.getItem(app_name + '-audioStatus') === 'paused') {
                this.stopAudio();
            }
        },
        playAudio() {
            localStorage.setItem(app_name + '-audioStatus', 'played');
            this.$refs.audioPlayer.play();
            $('#icon-sound').attr('src', '/template/images/icon_sound_pack/sound_gray_bg_shadow-2.png');
        },
        stopAudio() {
            localStorage.setItem(app_name + '-audioStatus', 'paused');
            this.$refs.audioPlayer.pause();
            $('#icon-sound').attr('src', '/template/images/icon_sound_pack/mute_gray_bg_shadow-2.png');
        },
        toggleAudio() {
            if (this.$refs.audioPlayer.paused) {
                this.playAudio();
            } else {
                this.stopAudio();
            }
        },
        getBoothCategoryWithBooths() {
            this.$http.get('/booth-list')
                .then(({ data }) => {
                    this.categories = data.data;
                    if (data.data.length > 0) {
                        this.selectedCategoryId = data.data[0].id;
                    }
                })
                .catch((err) => {
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                    console.log(err);
                });
        },
        dummyData() {
            let categories = ['food', 'drink', 'motorcycle', 'car', 'laptop', 'computer', 'handphone', 'house'];
            categories.forEach((x, i) => {
                this.categories.push({
                    id: i + 1,
                    name: x,
                    booths: [],
                });
            });

            let id = 1;
            let images = [
                '/example/image/default_booth.png',
                '/example/image/booth_type_1.png',
                '/example/image/booth_type_2.png'
            ];

            this.categories.forEach(data => {
                for (let i = 1; i <= 6; i++) {
                    let random = Math.floor(Math.random() * 3);
                    data.booths.push({
                        id: id++,
                        image: '/example/image/icon.jpg',
                        name: `Eventy Booth ${data.id} (${data.name})`,
                        description: 'Testin hehe',
                        stand_image: images[random],
                    });
                }
            });
        },
    },
}
