new Vue({
    el: '#appEventy',
    name: 'appBooth',
    mixins: [Booth],
    watch: {
        categories() {
            this.initSelectedBooth();
        },
    },
});
