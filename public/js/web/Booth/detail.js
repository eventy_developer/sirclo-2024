let detailBooth = new Vue({
    el: '#appEventy',
    name: 'appDetailBooth',
    mixins: [Booth],
    data() {
        return {
            participant,
            youtubeUrl,
            booth: {
                id: null,
                present_icon: null,
                name: null,
                desc: null,
                category: '',
                products: [],
                images: [],
                documents: [],
                videos: [],
                chats: [],
            },

            listVideoId: [],
            indexProductActive: 0,
            isActiveId: 0,
            swiperBooth: '',
            swiperProduct: '',
        }
    },
    watch: {
        categories() {
            this.initSelectedBooth();
            this.initSwiperBooth();

            this.$nextTick(() => {
                let category = this.findCategoryById(this.selectedCategoryId);
                if (category) {
                    if (category.booths.length > 0) {
                        for (const i in category.booths) {
                            if (category.booths[i].id === this.isActiveId) {
                                this.postFindDetailBooth(this.isActiveId, i);
                                break;
                            }
                        }
                    }
                }
            });
        },
        searchBooth(val) {
            this.initSwiperBooth();

            if (val === '') {
                this.isActiveId = this.findCategoryById(this.selectedCategoryId).booths[0].id;
            }
        },
    },
    computed: {
        currentProduct() {

            return {
                banners: [],
            }
        },
    },
    mounted() {
        this.registerDropdownBoothEvents();
    },
    methods: {
        registerDropdownBoothEvents: function () {
            let dropdownBooth = document.getElementById('dropdownBooth')
            dropdownBooth.addEventListener('show.bs.dropdown', function () {
                // (this) on dropdown context
                this.classList.add('expanded');

                // append backdrop element
                let backdrop = document.createElement('div');
                backdrop.classList.add('modal-backdrop', 'fade');
                document.body.appendChild(backdrop);
                setTimeout(function () {
                    backdrop.classList.add('show');
                }, 100);
            });
            dropdownBooth.addEventListener('hide.bs.dropdown', function () {
                // (this) on dropdown context
                this.classList.remove('expanded');

                // remove backdrop element
                let backdrop = document.querySelector('.modal-backdrop');
                backdrop.classList.remove('show');
                setTimeout(function () {
                    document.body.removeChild(backdrop);
                }, 150);
            });
        },
        findCategoryById(category_id) {
            return this.categories.find(category => category.id === category_id);
        },
        findBoothById(booth_id) {
            return this.findBoothById(this.selectedCategoryId).booths.find(booth => booth.id === booth_id);
        },
        initSwiperBooth() {
            if (this.swiperBooth != '') {
                this.swiperBooth.removeAllSlides();
                this.swiperBooth.removeAllSlides();
                this.swiperBooth.update();
                this.filterBooths.forEach(obj => {
                    let html = `<div class="swiper-slide">
                                    <img src="${obj.present_image}" alt="Booth" class="w-100">
                                </div>`;
                    this.swiperBooth.appendSlide(html);
                });
                this.swiperBooth.update();
            } else {
                this.$nextTick(() => {
                    this.swiperBooth = new Swiper('.swiper-booth', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        loop: false,
                        centeredSlides: true,
                        noSwiping: true,
                        navigation: {
                            nextEl: '.swiper-button-next__booth',
                            prevEl: '.swiper-button-prev__booth',
                        },
                        preloadImages: false,
                        lazy: {
                            loadPrevNext: true,
                        },
                    });

                    this.swiperBooth.navigation.nextEl.addEventListener('click', () => {
                        let index = this.swiperBooth.realIndex;
                        this.postFindDetailBooth(this.filterBooths[index].id, index);
                    });
                    this.swiperBooth.navigation.prevEl.addEventListener('click', () => {
                        let index = this.swiperBooth.realIndex;
                        this.postFindDetailBooth(this.filterBooths[index].id, index);
                    });
                });
            }
        },
        initSwiperProduct() {
            if (this.swiperProduct != '') {
                this.swiperProduct.removeAllSlides();
                this.swiperProduct.removeAllSlides();
                this.swiperProduct.update();
                this.currentProduct.banners.forEach(obj => {
                    let html = `<div class="swiper-slide">`;
                    if (obj.type === 'Image') {
                        html += `<a data-lightbox="${obj.image}" data-title="Booth"
                                href="${obj.image}" class="product-slide">
                                <img src="${obj.image}" alt="Booth" width="100%"
                                    class="pointer-events-none">
                            </a>`;
                    } else {
                        html += `<iframe
                                src="${this.youtubeUrl + obj.youtube_url}" frameborder="0"
                                allow="encrypted-media"
                                allowfullscreen class="product-slide"></iframe>`;
                    }
                    html += `</div>`;

                    this.swiperProduct.appendSlide(html);
                });
                this.swiperProduct.update();
            } else {
                this.$nextTick(() => {
                    this.swiperProduct = new Swiper('#slider-product', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        loop: false,
                        centeredSlides: true,
                        noSwiping: true,
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                        preloadImages: false,
                        lazy: {
                            loadPrevNext: true,
                        },
                    });
                });
            }
        },
        emptyChild() {
            this.booth.id = null;
            this.booth.present_icon = null;
            this.booth.name = null;
            this.booth.desc = null;
            this.booth.category = null;
            this.booth.products = [];
            this.booth.documents = [];
            this.booth.images = [];
            this.booth.videos = [];
            this.listVideoId = [];
        },
        modifyCurrentUrlParams() {
            let params = new URLSearchParams(window.location.search);
            params.set('category_id', this.selectedCategoryId);
            params.set('booth_id', this.isActiveId);
            window.history.replaceState({}, '', `${window.location.pathname}?${params}`);
        },
        postFindDetailBooth(id, i) {
            this.emptyChild();
            this.isActiveId = id;

            // set swiper active index
            if (this.swiperBooth) {
                this.swiperBooth.slideTo(i);
            }

            $('#preloader-active').show();
            this.$http.post('/booth-find', {
                booth_id: id,
                participants_id: this.participant.id,
            }).then(({ data: { data } }) => {
                $('#preloader-active').delay(100).fadeOut('slow');

                this.modifyCurrentUrlParams();

                this.booth.id = data.id;
                this.booth.present_icon = data.present_icon;
                this.booth.name = data.name;
                this.booth.category = this.findCategoryById(this.selectedCategoryId).name;
                this.booth.desc = data.description;
                this.booth.products = data.products;
                this.booth.images = data.attachments.filter(el => el.type === 'Image');
                this.booth.documents = data.attachments.filter(data => data.type === 'Document');
                this.booth.videos = data.attachments.filter(data => data.type === 'Video');

                this.$nextTick(() => {
                    if (this.booth.videos.length > 0) {
                        this.booth.videos.forEach((vid) => {
                            if (!this.listVideoId.includes(vid.id)) {
                                new ModalVideo(`#play-modal-${vid.id}`);
                                this.listVideoId.push(vid.id);
                            }
                        });
                    }

                    setTimeout(() => {
                        $('.nav-news-list').animate({
                            scrollTop: $('.nav-news-list').prop('scrollHeight')
                        }, 500);
                    }, 1000);
                });

            }).catch((err) => {
                $('#preloader-active').delay(100).fadeOut('slow');
                console.log(err);
                if (!err.response) {
                    this.$toast.fire({
                        icon: 'error',
                        title: err.message,
                    });
                }
            });
        },
    }
});
