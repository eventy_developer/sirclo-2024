$(document).ready(function () {
    let boothElement = document.querySelector('.nav--step[data-href="/booth"]');
    let stageElement = document.querySelector('.nav--step[data-href="/main-stage"]');
    let classroomElement = document.querySelector('.nav--step[data-href="/classroom"]');
    let scheduleElement = document.querySelector('.nav--step-schedule');
    let sponsorElement = document.querySelector('.nav--step-sponsor');

    let steps = [{
        title: "Hello Welcome!",
        intro: "Let's explore this page",
    }, {
        element: document.querySelector('.nav--step[data-href="/"]'),
        intro: "The main page that displays <strong>General Information</strong>",
        position: "right",
    }];

    if (boothElement) {
        steps.push({
            element: boothElement,
            intro: "Display a <strong>Sponsor Booth</strong> and you can also interact with the owner",
            position: "right",
        });
    }
    if (stageElement) {
        steps.push({
            element: stageElement,
            intro: "Watch <strong>Public Shows</strong> and interact with other participant",
            position: "right",
        });
    }
    if (classroomElement) {
        steps.push({
            element: classroomElement,
            intro: "You can join <strong>Private Sessions</strong> on this menu",
            position: "right",
        });
    }
    if (scheduleElement) {
        steps.push({
            element: document.querySelector('.nav--step-schedule'),
            intro: "Contains the <strong>Overall Schedule</strong> of events",
            position: "right",
        });
    }
    if (sponsorElement) {
        steps.push({
            element: document.querySelector('.nav--step-sponsor'),
            intro: "Display a list of <strong>Sponsors</strong> who support this event",
            position: "right",
        });
    }

    if (!is_mobile && !localStorage.getItem(app_name + '_introStatus' + entrat_id)) {
        // set position to absolute
        introJs().setOptions({
            steps: steps
        }).start().onexit(function() {
            localStorage.setItem(app_name + '_introStatus' + entrat_id, 'done');
            setCookie(app_name + '_hideFeedback' + entrat_id, true, 0.13);
        });
    }
});
