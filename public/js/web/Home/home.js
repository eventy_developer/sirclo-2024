let appHome = new Vue({
    name: 'appHome',
    el: '#appEventy',
    mixins: [CheckMobile, Visitor, Schedule, Sponsor, Booth],
    data() {
        return {
            is_event_locked: localStorage.getItem('is_event_locked'),
            participant,
        }
    },
    created() {
        this.getSchedule();
    },
    mounted() {
        this.initSwiper();

        // if (is_event_locked) {
        //     this.intervalEvent = setInterval(() => {
        //         this.checkEventStarted();
        //     }, 1000);
        // }
    },
    methods: {
        checkEventStarted() {
            let now = dayjs(dayjs().format('YYYY-MM-DD HH:mm:ss')).tz(timezone_events).format('YYYY-MM-DD HH:mm:ss');
            if (now >= date_start) {
                clearInterval(this.intervalEvent);
                window.location.reload();
            }
        },
        initSwiper() {
            if (!is_video) {
                new Swiper('#slider-home', {
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                    },
                    autoplay: {
                        delay: 4000,
                        disableOnInteraction: false,
                    },
                    slidesPerView: 'auto',
                    preloadImages: false,
                    lazy: true,
                });
            }
        },
    }
});
