let appLanding = new Vue({
    name: 'appLanding',
    el: '#root',
    data() {
        return {
            schedule_sessions: schedule_sessions,
            currentLocation: 'All',
            currentDate: 'All',
            locations: [
                {
                    option: 'All',
                    value: 'All'
                },
            ],
            partnerCollapsed: true,
        }
    },
    mounted() {
        if (youtube_url) {
            this.initModalVideo();
        }

        this.initCountdown();
        this.initSwiperBanner();
        this.initSwiperSpeaker();
        this.initChoices();
        document.addEventListener('scroll', this.onScrollChangeNavbar)
    },
    created() {
        this.getSchedules();
    },
    computed: {
        dates() {
            const uniqueDates = this.toUniqueArray(this.schedule_sessions, 'date')
            const dates = []

            uniqueDates.forEach(function (item) {
                dates.push({
                    option: item.present_date_text,
                    value: item.date
                })
            })

            return dates
        },
        filterSchedules() {
            if (this.currentLocation === 'All') {
                return this.schedule_sessions.filter(item => item.date === this.currentDate)
            }

            return this.schedule_sessions.filter(item =>
                item.date === this.currentDate && item.schedule.location.name === this.currentLocation
            )
        }
    },
    methods: {
        toUniqueArray(item, key) {
            return [...new Map(item.map(item => [item[key], item])).values()]
        },
        changeSelectDate(event) {
            this.currentDate = event.target.value;
        },
        changeSelectLocation(event) {
            this.currentLocation =  event.target.value;
        },
        getSchedules() {
            this.currentDate = this.schedule_sessions[0]?.date;

            // location
            locations.forEach((item) => {
                this.locations.push({
                    option: item,
                    value: item
                });
            });
        },
        initChoices() {
            const choiceOpt = {
                searchEnabled: false,
                searchChoices: false,
                itemSelectText: '',
                shouldSort: false,
            }
            new Choices(document.getElementById('selectDateHome'), choiceOpt)
            new Choices(document.getElementById('selectLocationHome'), choiceOpt)
        },
        onScrollChangeNavbar() {
            const navbar = document.querySelector('.navbar-eventy')
            if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
                navbar.classList.add('with-shadow')
            } else {
                navbar.classList.remove('with-shadow')
            }
        },
        countDownClock(selectors, number = 100, format = 'seconds') {
            if (number < 0) return false;

            const daysElement = selectors.days;
            const hoursElement = selectors.hours;
            const minutesElement = selectors.minutes;
            const secondsElement = selectors.seconds;
            let countdown;
            convertFormat(format);


            function convertFormat(format) {
                switch (format) {
                    case 'seconds':
                        return timer(number);
                    case 'minutes':
                        return timer(number * 60);
                    case 'hours':
                        return timer(number * 60 * 60);
                    case 'days':
                        return timer(number * 60 * 60 * 24);
                }
            }

            function timer(seconds) {
                const now = Date.now();
                const then = now + seconds * 1000;

                countdown = setInterval(() => {
                    const secondsLeft = Math.round((then - Date.now()) / 1000);

                    if (secondsLeft < 0) {
                        clearInterval(countdown);
                        window.location.reload();
                        return;
                    }


                    displayTimeLeft(secondsLeft);

                }, 1000);
            }

            function displayTimeLeft(seconds) {
                const days = Math.floor(seconds / 86400);
                const hours = Math.floor((seconds % 86400) / 3600);
                const minutes = Math.floor((seconds % 86400) % 3600 / 60);

                daysElement.textContent = days < 10 ? `0${days}` : days;
                hoursElement.textContent = hours < 10 ? `0${hours}` : hours;
                minutesElement.textContent = minutes < 10 ? `0${minutes}` : minutes;
                secondsElement.textContent = seconds % 60 < 10 ? `0${seconds % 60}` : seconds % 60;
            }
        },
        initCountdown() {
            /*
              start countdown
              enter number and format
              days, hours, minutes or seconds
            */
            const selectors = {
                days: document.getElementById('countdown-days'),
                hours: document.getElementById('countdown-hours'),
                minutes: document.getElementById('countdown-minutes'),
                seconds: document.getElementById('countdown-seconds'),
            }
            this.countDownClock(selectors, numberOfCountdown, 'seconds')
        },
        initSwiperBanner() {
            new Swiper('.swiper-banners', {
                slidesPerView: 1,
                spaceBetween: 20,

                autoplay: {
                    delay: 4000,
                    disableOnInteraction: false,
                },

                // And if we need scrollbar
                scrollbar: {
                    el: '.swiper-scrollbar',
                    draggable: true,
                    dragSize: 40
                },
            });
        },
        initSwiperSpeaker() {
            new Swiper('.swiper-speakers', {
                slidesPerView: 1,
                spaceBetween: 20,

                autoplay: {
                    delay: 4000,
                    disableOnInteraction: false,
                },

                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },

                // And if we need scrollbar
                scrollbar: {
                    el: '.swiper-scrollbar',
                    draggable: true,
                    dragSize: 40
                },

                breakpoints: {
                    // when window width is >= 576px
                    576: {
                        slidesPerView: 2
                    },
                    // when window width is >= 992px
                    992: {
                        slidesPerView: 3
                    },
                }
            });
        },
        initModalVideo: function() {
            new ModalVideo('.about__icon-play');
        },
        togglePartner: function() {
            let self = this;
            let partnerCol = Array.from(document.querySelectorAll('.partner-list .collapsed'));

            partnerCol.forEach(function(item) {
                if (self.partnerCollapsed) {
                    item.classList.remove('d-none');
                } else {
                    item.classList.add('d-none');
                }
            });

            this.partnerCollapsed = !this.partnerCollapsed;
        },
    }
});
