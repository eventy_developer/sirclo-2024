let type = $('#type').val();
let label_categories = $('#label_categories').val();
if (label_categories != 'Group'){
    $('#form-group-group_number').hide();
}else{
    $('#form-group-group_number').show();
}
if (type === 'Free') {
    $('#form-group-price').hide();
    $('#price').prop('required', false);
    $('#form-group-is_only_ticket').hide();
    $('#form-group-coupon').show();
    $('#form-group-label_categories').hide();
} else {
    $('#form-group-price').show();
    $('#price').prop('required', true);
    $('#form-group-is_only_ticket').show();
    $('#form-group-coupon').hide();
    $('#form-group-label_categories').show();
}


$('#label_categories').on('change', function(){
   if (this.value == 'Group'){
       $('#form-group-group_number').show();
   }else{
       $('#form-group-group_number').hide();
   }
});

$('#type').on('change', function(){
    if (this.value === 'Free') {
        $('#form-group-price').hide();
        $('#price').prop('required', false);
        $('#form-group-is_only_ticket').hide();
        $('#form-group-coupon').show();
        $('#form-group-label_categories').hide();
    } else {
        $('#form-group-price').show();
        $('#price').prop('required', true);
        $('#form-group-is_only_ticket').show();
        $('#form-group-coupon').hide();
        $('#form-group-label_categories').show();
    }
});
