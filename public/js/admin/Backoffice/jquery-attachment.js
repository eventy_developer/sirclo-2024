$(document).ready(function () {
    let type = $('#type').val();
    if (type === 'video') {
        $('#form-group-video_url').show();
        $('#video_url').prop('required', true);
        $('#form-group-video_name').show();
        $('#video_name').prop('required', true);
        $('#form-group-file').hide();
        $('#file').prop('required', false);
        $('#form-group-image').hide();
        $('#image').prop('required', false);
    } else if (type === 'document') {
        $('#form-group-file').show();
        $('#file').prop('required', true);
        $('#form-group-image').hide();
        $('#image').prop('required', false);
        $('#form-group-video_url').hide();
        $('#video_url').prop('required', false);
        $('#form-group-video_name').hide();
        $('#video_name').prop('required', false);
    } else if (type === 'image') {
        $('#form-group-image').show();
        $('#image').prop('required', true);
        $('#form-group-file').hide();
        $('#file').prop('required', false);
        $('#form-group-video_url').hide();
        $('#video_url').prop('required', false);
        $('#form-group-video_name').hide();
        $('#video_name').prop('required', false);
    }

    $('#type').on('change', function(){
        if (this.value === 'video') {
            $('#form-group-video_url').show();
            $('#video_url').prop('required', true);
            $('#form-group-video_name').show();
            $('#video_name').prop('required', true);
            $('#form-group-file').hide();
            $('#file').prop('required', false);
            $('#form-group-image').hide();
            $('#image').prop('required', false);
        } else if (this.value === 'document') {
            $('#form-group-file').show();
            $('#file').prop('required', true);
            $('#form-group-image').hide();
            $('#image').prop('required', false);
            $('#form-group-video_url').hide();
            $('#video_url').prop('required', false);
            $('#form-group-video_name').hide();
            $('#video_name').prop('required', false);
        } else if (this.value === 'image') {
            $('#form-group-image').show();
            $('#image').prop('required', true);
            $('#form-group-file').hide();
            $('#file').prop('required', false);
            $('#form-group-video_url').hide();
            $('#video_url').prop('required', false);
            $('#form-group-video_name').hide();
            $('#video_name').prop('required', false);
        }
    });
});