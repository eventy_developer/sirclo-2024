new Vue({
    name: 'appSidebar',
    el: '#rootProfile',
    data() {
        return {
            profile_id: admin.id,
            profile_name: admin.name,
            profile_email: admin.email,
            profile_image: admin.image,
            errors_upload: [],
            current_password: null,
            new_password: null,
            retype_password: null,
            errors_editpass: [],
        };
    },
    methods: {
        showHideProfile(val) {
            if (val === true) {
                $('#side-profil').show(200);
            } else {
                $('#side-profil').hide(0);
            }
        },
        showHidePassword(val) {
            if (val === true) {
                $('#side-password').show(200);
            } else {
                $('#side-password').hide(0);
            }
        },
        showEditPassword() {
            this.showHideProfile(false);
            this.showHidePassword(true);
        },
        showProfil() {
            this.showHideProfile(true);
            this.showHidePassword(false);
        },
        loadShow(type) {
            var styles = {
                opacity: '0.4',
                pointerEvents: 'none',
                userSelect: 'none',
            };
            $('#preloader-active').show();
            if (type != 'password') {
                $('#side-profil').css(styles);
            } else {
                $('#side-password').css(styles);
            }
        },
        loadHide(type) {
            var styles = {
                opacity: '1',
                pointerEvents: 'all',
            };
            $('#preloader-active').hide();
            if (type != 'password') {
                $('#side-profil').css(styles);
            } else {
                $('#side-password').css(styles);
            }
        },
        logOut() {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure want to logout ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then(
                function (confirm) {
                    if (confirm.value) {
                        return window.location.href = `${url_base}/admin/logout`;
                    }
                }
            );

        },
        empty() {
            this.errors_editpass = [];
            this.errors_upload = [];
            this.current_password = null;
            this.retype_password = null;
            this.new_password = null;
        },
        previewImage: function (event) {
            self = this;
            // Reference to the DOM input element
            var input = event.target;

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    self.profile_image = e.target.result;
                    this.uploadImageProfile();
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        uploadImageProfile() {
            this.loadShow('profile');

            this.$http.post('/profile-upload-image', {
                image: this.profile_image,
                user_id: this.profile_id
            }).then(({ data }) => {
                this.loadHide('profil');
                this.empty();

                $('#header_profile_image').attr('src', data.item.image);

                this.profile_image = data.item.image;
                this.$toast.fire({
                    icon: 'success',
                    title: data.message,
                });
            }).catch((err) => {
                this.loadHide('profil');

                if (err.response.status == 422) {
                    this.errors_upload = err.response.data.errors;
                } else {
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                }
            });
        },
        editPassword() {
            this.loadShow('password');

            this.$http.post('/profile-edit-password', {
                current_password: this.current_password,
                new_password: this.new_password,
                retype_password: this.retype_password,
                user_id: this.profile_id,
            }).then(({ data }) => {
                this.loadHide('password');
                this.empty();

                this.$toast.fire({
                    icon: 'success',
                    title: data.message,
                });
            }).catch((err) => {
                this.loadHide('password');

                if (err.response.status == 422) {
                    let errors = err.response.data.errors;
                    if (!errors) {
                        errors = {
                            current_password: [err.response.data.message],
                        }
                    }

                    this.errors_editpass = errors;
                } else {
                    if (!err.response) {
                        console.log(err);
                        this.$toast.fire({
                            icon: 'error',
                            title: err.message,
                        });
                    }
                }
            });
        }
    }
});
