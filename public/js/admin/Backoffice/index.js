new Vue({
    name: 'appBackoffice',
    el: '#boothAdminApp',
    data() {
        return {
            is_load_message: false,
            booth,
            total_visitor: 0,
            admin: '',
            attachments: [],
            products: [],
        };
    },
    created() {
        this.getListItems(this.booth.id);
        this.getTotalVisitor();
        setInterval(() => this.getTotalVisitor(), 60000);
    },
    methods: {
        /**
         * HTTP GET request total visitor
         */
        getTotalVisitor() {
            this.$http.get(`/total-visitor/${this.booth.id}`)
                .then(({data}) => {
                    this.total_visitor = data.total;
                })
                .catch(e => {
                    this.$toast.fire({
                        icon: 'error',
                        title: e.message,
                    });
                    console.log(e);
                });
        },

        /**
         * HTTP GET request list items
         *
         * @param {number} booth_id
         */
        getListItems(booth_id) {
            this.$http.get(`/list-items/${booth_id}`)
                .then(({data: {data}}) => {
                    this.attachments = data.attachments;
                    this.products = data.products;

                    if (data.attachments.length > 0) {
                        this.$nextTick(() => {
                            new ModalVideo('.play-modal');
                        });
                    }

                })
                .catch((e) => {
                    this.$toast.fire({
                        icon: 'error',
                        title: e.message,
                    });
                    console.log(e);
                });
        },

        /**
         * HTTP POST request delete attachment files
         *
         * @param {number} id
         */
        deleteAttachment(id) {
            this.$swal.fire({
                title: 'Confirmation',
                text: 'Are you sure want to delete this data ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#00B2EF',
                cancelButtonColor: '#FF4A4B',
                confirmButtonText: 'Yes',
            }).then((confirm) => {
                if (confirm.value) {
                    $('#preloader-active').show();
                    this.$http.post(`/delete-attachment/${id}`)
                        .then(res => {
                            $('#preloader-active').hide();
                            this.$swal.fire('Success', 'Delete attachment successfully', 'success').then(() => {
                                location.reload();
                            });
                        })
                        .catch((e) => {
                            $('#preloader-active').hide();
                            this.$toast.fire({
                                icon: 'error',
                                title: e.message,
                            });
                            console.log(e);
                        });
                }
            });
        },
    },
});
