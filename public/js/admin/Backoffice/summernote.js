var summernote = $('#textarea_description').summernote({
    height: 300,
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['codeview']],
    ],
    callbacks: {
        onImageUpload: function (image) {
            uploadImage(image[0]);
        }
    },
    disableResizeEditor: true,
    prettifyHtml: true,
    codemirror: {
        theme: 'monokai',
        mode: 'text/html',
        htmlMode: true,
        lineNumbers: true,
    },
});

function uploadImage(image) {
    var data = new FormData();
    data.append('userfile', image);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
    $.ajax({
        url: summernoteUploadUrl,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: 'post',
        success: function (url) {
            var image = $('<img>').attr('src', url);
            summernote.summernote('insertNode', image[0]);
        },
        error: function (data) {
            console.log(data);
        }
    });
}
