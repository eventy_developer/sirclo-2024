new Vue({
    el: '#appBoothAdmin',
    data() {
        return {
            booth,
            type: 'add',
            name: '',
            email: '',
            password: '',
            adminId: '',
        };
    },
    methods: {
        getListAdmin() {
            this.$http.get(`/list-admin/${this.booth.id}`)
                .then(({ data: { item } }) => {
                    this.booth.admin = item;
                })
                .catch((e) => {
                    swal({
                        title: 'Oops!',
                        text: e.message,
                        icon: 'error',
                        button: 'OK!',
                    });
                });
        },
        validate(event) {
            if (event.target.value === '') {
                event.target.style.borderColor = 'red';
                return false;
            }

            event.target.style.borderColor = '#d2d6de';
        },
        addToTable() {
            if (this.name === '') {
                this.$refs.name.style.borderColor = 'red';
                this.$refs.name.focus();
                return false;
            }

            if (this.email === '') {
                this.$refs.email.style.borderColor = 'red';
                this.$refs.email.focus();
                return false;
            }

            this.saveToDatabase();
        },
        saveToDatabase() {
            let formData = new FormData();
            formData.append('name', this.name);
            formData.append('email', this.email);
            formData.append('password', this.password);
            formData.append('booth_id', this.booth.id);

            this.$http.post('/add-admin-booth', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                })
                .then(() => {
                    this.resetForm();
                    this.getListAdmin();
                    swal({
                        title: 'Good job!',
                        text: 'Admin added successfully',
                        icon: 'success',
                        button: 'OK!',
                    });
                })
                .catch((e) => {
                    if (e.response.status === 422) {
                        swal({
                            title: 'Oops!',
                            text: e.response.data.errors.email[0],
                            icon: 'warning',
                            button: 'OK!',
                        });
                    } else {
                        swal({
                            title: 'Oops!',
                            text: e.message,
                            icon: 'error',
                            button: 'OK!',
                        });
                    }
                });

        },
        editToDatabase() {
            let formData = new FormData();
            formData.append('name', this.name);
            formData.append('email', this.email);
            formData.append('password', this.password);
            formData.append('booth_id', this.booth.id);
            formData.append('id', this.adminId);

            this.$http.post('/edit-admin-booth', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                })
                .then(() => {
                    this.getListAdmin();
                    this.resetForm();
                    swal({
                        title: 'Good job!',
                        text: 'Admin updated successfully',
                        icon: 'success',
                        button: 'OK!',
                    });
                })
                .catch((error) => {
                    if (error.response.status === 422) {
                        swal({
                            title: 'Oops!',
                            text: error.response.data.errors.email[0],
                            icon: 'warning',
                            button: 'OK!',
                        });
                    } else {
                        swal({
                            title: 'Oops!',
                            text: error.message,
                            icon: 'error',
                            button: 'OK!',
                        });
                    }
                });
        },
        deleteAdmin(id) {
            swal({
                title: 'Are you sure ?',
                text: 'You will not be able to recover this record data!',
                icon: 'warning',
                buttons: {
                    confirm: {
                        text : 'Yes!',
                        className : 'btn btn-primary'
                    },
                    cancel: {
                        visible: true,
                        text : 'No',
                        className: 'btn btn-danger'
                    }
                }
            }).then((isConfirm) => {
                if (isConfirm) {
                    this.$http.post(`/delete-admin/${id}`)
                        .then(() => {
                            this.getListAdmin();
                            swal({
                                title: 'Good job!',
                                text: 'Admin deleted successfully',
                                icon: 'success',
                                button: 'OK!',
                            });
                        })
                        .catch((e) => {
                            swal({
                                title: 'Oops!',
                                text: e.message,
                                icon: 'warning',
                                button: 'OK!',
                            });
                        });
                }
            });
        },
        formEdit(item) {
            this.type = 'edit';
            this.name = item.name;
            this.email = item.email;
            this.adminId = item.id;
            this.$refs.name.focus();
        },
        resetForm() {
            this.name = '';
            this.email = '';
            this.password = '';
            this.adminId = '';
            this.type = 'add';
        },
    },
});
