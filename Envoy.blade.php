@setup
    $env = $env ? $env : "production";
    $branch = $branch ? $branch : "master";
    $domain_name = str_replace(['https://', 'https', '://'], '', $url);

    function logMessage($message) {
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }
@endsetup

@servers(['dev_server' => "$dev_user@$dev_host", 'production_server' => "$prod_user@$prod_host"])

@story('deploy_dev', ['on' => 'dev_server'])
    clone_repository
    dev_script
    compiling
@endstory

@story('deploy_production', ['on' => 'production_server'])
    clone_repository
    production_script
    compiling
@endstory

@task('clone_repository')
    cd /www/wwwroot
    {{ logMessage("=== Clone repository if doesnt exist ===") }}
    [ -d {{ $domain_name }} ] || git clone {{ $repo }} {{ $domain_name }}

    cd /www/wwwroot/{{ $domain_name }}
    composer install
@endtask

@task('compiling')
    export PATH=$PATH:/www/server/nvm/versions/node/v16.13.2/bin
    cd /www/wwwroot/{{ $domain_name }}
    {{ logMessage("=== Compiling assets ===") }}
    [ -d "node_modules" ] || npm install
    npm run production
@endtask

@task('dev_script')
    cd /www/wwwroot/{{ $domain_name }}
    [ -f .env ] || ( cp .env.example .env && php artisan key:generate )
    {{ logMessage('=== Pulling update from branch ' . $branch . ' ===') }}

    git fetch origin

    {{ logMessage('=== Switch branch to ' . $branch . ' ===') }}
@if($branch === 'dev')
    git switch -C dev && git merge origin/dev
@else
    git switch -C {{ $branch }} && git merge origin/{{ $branch }}
@endif

{{ logMessage('=== Clearing cache ===') }}
    php artisan cache:clear
    php artisan route:clear
    php artisan view:clear
    sudo chown -R www:www storage
@endtask

@task('production_script')
    cd /www/wwwroot/{{ $domain_name }}
    [ -f .env ] || ( cp .env.example .env && php artisan key:generate )
    {{ logMessage('=== Pulling update from branch ' . $branch . ' ===') }}
    git fetch origin
    git merge origin/{{ $branch }}
    {{ logMessage('=== Clearing cache ===') }}
    php artisan cache:clear
    php artisan route:clear
    php artisan view:clear
@endtask

@finished
    echo "Envoy deployment script finished.\r\n";
@endfinished
