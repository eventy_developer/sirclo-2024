<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoorprizeIdOnScanHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_histories', function (Blueprint $table) {
            $table->foreignId('doorprize_id')->nullable()->after('scan_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_histories', function (Blueprint $table) {
            $table->dropColumn(['doorprize_id']);
        });
    }
}
