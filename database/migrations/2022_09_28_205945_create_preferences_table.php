<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('key', 50)->unique()->comment('Untuk pemanggilan helper menggunakan key');
            $table->string('name', 50)->comment('Nama yang digunakan untuk menampilkan di backend');
            $table->text('content')->nullable()->comment('Value preference');
            $table->string('type', 50)->comment('Content Type: Text, Image, Link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}
