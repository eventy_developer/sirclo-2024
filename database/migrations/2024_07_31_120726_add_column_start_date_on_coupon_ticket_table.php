<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnStartDateOnCouponTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupon_tickets', function (Blueprint $table) {
            $table->date('start_date')->default(now());
            $table->renameColumn('avail_date', 'end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupon_tickets', function (Blueprint $table) {
            $table->dropColumn(['start_date']);
            $table->renameColumn('end_date', 'avail_date');
        });
    }
}
