<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDrawTypeOnDoorprizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doorprizes', function (Blueprint $table) {
            $table->string('draw_type')->default('General');
            $table->dateTime('available_at')->nullable();
            $table->dateTime('draw_at')->nullable();
            $table->integer('minutes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doorprizes', function (Blueprint $table) {
            $table->dropColumn(['draw_type', 'available_at', 'draw_at', 'minutes']);
        });
    }
}
