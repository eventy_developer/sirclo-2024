<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoorprizeTypeOnDoorprizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doorprizes', function (Blueprint $table) {
            $table->string('doorprize_type')->nullable()->default('Doorprize 1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doorprizes', function (Blueprint $table) {
            $table->dropColumn(['doorprize_type']);
        });
    }
}
