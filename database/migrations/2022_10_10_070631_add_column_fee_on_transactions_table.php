<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFeeOnTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('initial_amount')->default(0)->after('invoice_code');
            $table->integer('vat_fee')->default(0);
            $table->integer('var_fee')->default(0);
            $table->integer('fixed_fee')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn(['initial_amount', 'vat_fee', 'var_fee', 'fixed_fee']);
        });
    }
}
