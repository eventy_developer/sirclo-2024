<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_confirmations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('transaction_id');
            $table->foreignId('participant_id');
            $table->string('invoice_code');
            $table->string('account_name');
            $table->date('transfer_date');
            $table->string('nominal');
            $table->string('picture');
            $table->string('status', 10)->comment('Waiting, Approved, Rejected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_confirmations');
    }
}
