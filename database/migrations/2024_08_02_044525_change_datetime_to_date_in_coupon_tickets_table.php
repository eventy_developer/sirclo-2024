<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDatetimeToDateInCouponTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupon_tickets', function (Blueprint $table) {
            // Change start_date column type from datetime to date
            $table->dateTime('start_date')->change();

            // Rename and change avail_date column type from datetime to date
            $table->dateTime('end_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupon_tickets', function (Blueprint $table) {
            // Revert the columns back to datetime type
            $table->date('start_date')->change();
            $table->date('end_date')->change();
        });
    }
}
