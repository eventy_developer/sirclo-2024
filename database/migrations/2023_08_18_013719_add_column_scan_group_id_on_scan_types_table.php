<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnScanGroupIdOnScanTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scan_types', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
            $table->foreignId('scan_group_id')->after('deleted_at');
            $table->string('type')->comment('Checkin, Pickup Doorprize, Point Doorprize');
            $table->integer('point')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scan_types', function (Blueprint $table) {
            $table->dropColumn(['deleted_at', 'scan_group_id', 'type', 'point']);
        });
    }
}
