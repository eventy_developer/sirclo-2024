<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('participant_id');
            $table->string('invoice_code', 50)->unique();
            $table->integer('amount')->default(0);
            $table->string('type')->comment('Virtual Accounts, Retail Outlets, eWallets, Cards, QR Codes');
            $table->string('payment_method', 50)->comment('BRI, MANDIRI, BNI, PERMATA, BCA, SAHABAT_SAMPOERNA, CREDIT_CARD, QRIS, ALFAMART, INDOMARET, OVO, DANA, LINKAJA');
            $table->string('status')->default('WAITING_PAYMENT')->comment('WAITING_PAYMENT, WAITING_CONFIRMATION, PENDING, SUCCESS, FAILED');
            $table->string('transaction_code', 50)->nullable()->comment('Unique Code Transaksi');
            $table->string('virtual_account_number', 50)->nullable()->comment('Untuk Virtual Accounts');
            $table->text('checkout_url')->nullable()->comment('Untuk Invoice Url');
            $table->integer('transaction_xendit_id')->nullable()->comment('Xendit Helper');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
