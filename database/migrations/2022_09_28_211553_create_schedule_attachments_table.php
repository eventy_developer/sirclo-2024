<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_attachments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('schedule_session_id');
            $table->string('type', 20)->comment('Image, Document, Video');
            $table->string('file')->nullable()->comment('Image & Document');
            $table->string('file_name')->nullable()->comment('Image & Document');
            $table->string('file_size')->nullable()->comment('Image & Document');
            $table->string('extension')->nullable()->comment('Image & Document');
            $table->string('video_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_attachments');
    }
}
