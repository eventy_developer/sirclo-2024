<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_sessions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('schedule_id');
            $table->string('image')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->date('date');
            $table->time('time_start');
            $table->time('time_end');
            $table->string('platform_type')->comment('Youtube, Zoom');
            $table->string('youtube_id')->nullable();
            $table->string('zoom_api_key')->nullable();
            $table->string('zoom_api_secret')->nullable();
            $table->string('zoom_conference_id')->nullable()->comment('Zoom meeting / webinar ID');
            $table->string('zoom_conference_password')->nullable()->comment('Zoom meeting / webinar Password');
            $table->string('zoom_link')->nullable()->comment('Zoom Link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_sessions');
    }
}
