<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('ticket_id')->nullable();
            $table->string('image')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('company')->nullable();
            $table->string('status', 20)->default('Waiting Ticket')->comment('Waiting Ticket, Waiting Payment, Waiting Confirmation, Active, Blocked');
            $table->tinyInteger('is_invitation')->default(0);
            $table->tinyInteger('is_admin')->default(0);
            $table->tinyInteger('is_feedback_filled')->default(0);
            $table->tinyInteger('is_activated_manually')->default(0);
            $table->dateTime('last_login')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
