<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreeeeColumnDorprizeOnParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('participants', function (Blueprint $table) {
            $table->tinyInteger('is_pickup_doorprize')->default(0);
            $table->tinyInteger('is_pickup_doorprize2')->default(0);
            $table->tinyInteger('is_pickup_doorprize3')->default(0);
            $table->tinyInteger('exclude_doorprize')->default(0);
            $table->tinyInteger('exclude_doorprize2')->default(0);
            $table->tinyInteger('exclude_doorprize3')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('participants', function (Blueprint $table) {
            $table->dropColumn([
                'is_pickup_doorprize',
                'is_pickup_doorprize3',
                'is_pickup_doorprize3',
                'exclude_doorprize',
                'exclude_doorprize2',
                'exclude_doorprize3',
            ]);
        });
    }
}
