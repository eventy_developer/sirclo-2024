<?php

namespace Database\Seeders;

use App\Models\ScheduleLocation;
use Illuminate\Database\Seeder;

class ScheduleLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (ScheduleLocation::count() === 0) {
            ScheduleLocation::insert([
                ['created_at' => date('Y-m-d H:i:s'), 'name' => 'Main Stage'],
                ['created_at' => date('Y-m-d H:i:s'), 'name' => 'Class Room'],
            ]);
        }
    }
}
