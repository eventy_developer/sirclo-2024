<?php

namespace Database\Seeders;

use App\Models\Sponsor;
use App\Models\SponsorCategory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class SponsorCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (SponsorCategory::count() === 0) {
            SponsorCategory::factory()
                ->count(3)
                ->state(new Sequence(
                    ['name' => 'Gold', 'column_size' => 2, 'sort' => 2],
                    ['name' => 'Silver', 'column_size' => 3, 'sort' => 1],
                    ['name' => 'Bronze', 'column_size' => 4, 'sort' => 3],
                ))
                ->has(
                    Sponsor::factory()
                        ->count(7)
                        ->state(new Sequence(
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_crocodic.png', 'sort' => 1],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_taaruf.png', 'sort' => 2],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_reprime.png', 'sort' => 3],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_salor.png', 'sort' => 4],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_couries.png', 'sort' => 5],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_crocodic.png', 'sort' => 6],
                            ['image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/img_logo_couries.png', 'sort' => 7],
                        ))
                    , 'sponsors')
                ->create();
        }
    }
}
