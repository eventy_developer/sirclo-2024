<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('preferences')->count() <= 0) {

            // init timezone
            DB::table('preferences')->insert([
                'key' => 'timezone_events',
                'name' => 'Timezone',
                'content' => 'Asia/Jakarta',
                'type' => 'Text',
            ]);

            $prefs = [
                // Main Settings
                [
                    'key' => 'name_events',
                    'name' => 'Event Name',
                    'content' => 'Eventy Virtual Event ' . date('Y'),
                    'type' => 'Text',
                ],
                [
                    'key' => 'logo_events',
                    'name' => 'Logo',
                    'content' => 'virtual-event/virtualeventv3/example/image/logo.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'icon_events',
                    'name' => 'Icon',
                    'content' => 'virtual-event/virtualeventv3/example/image/icon_v3.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'date_start_events',
                    'name' => 'Date Start',
                    'content' => date('Y-m-d H:i:s', strtotime("+2 days", strtotime(dateTime()))),
                    'type' => 'Text',
                ],
                [
                    'key' => 'date_end_events',
                    'name' => 'Date End',
                    'content' => date('Y-m-d  H:i:s', strtotime("+5 days", strtotime(dateTime()))),
                    'type' => 'Text',
                ],
                [
                    'key' => 'time_start_events',
                    'name' => 'Time Start',
                    'content' => date('H:i:s'),
                    'type' => 'Text',
                ],
                [
                    'key' => 'time_end_events',
                    'name' => 'Time End',
                    'content' => date('H:i:s', strtotime("+3 hours", strtotime(dateTime()))),
                    'type' => 'Text',
                ],
                [
                    'key' => 'free_events',
                    'name' => 'Free Events ?',
                    'content' => 'no',
                    'type' => 'Text',
                ],
                [
                    'key' => 'lock_login_wording_events',
                    'name' => 'Locked login message',
                    'content' => '',
                    'type' => 'Text',
                ],
                [
                    'key' => 'bg_ticket',
                    'name' => 'Background Ticket',
                    'content' => '',
                    'type' => 'Image',
                ],
                [
                    'key' => 'bg_sessions',
                    'name' => 'Background Sessions',
                    'content' => '',
                    'type' => 'Image',
                ],
                [
                    'key' => 'lang_events',
                    'name' => 'Event Language',
                    'content' => 'en',
                    'type' => 'Text',
                ],
                [
                    'key' => 'contact_email_events',
                    'name' => 'Contact Email Events',
                    'content' => 'contact@eventy.id',
                    'type' => 'Text',
                ],
                [
                    'key' => 'contact_name_1',
                    'name' => 'Contact Name 1',
                    'content' => 'Contact Name One',
                    'type' => 'Text',
                ],
                [
                    'key' => 'contact_phone_1',
                    'name' => 'Contact Phone 1',
                    'content' => '08128162812',
                    'type' => 'Text',
                ],
                [
                    'key' => 'contact_name_2',
                    'name' => 'Contact Name 2',
                    'content' => 'Contact Name Two',
                    'type' => 'Text',
                ],
                [
                    'key' => 'contact_phone_2',
                    'name' => 'Contact Phone 2',
                    'content' => '081204589485',
                    'type' => 'Text',
                ],

                // Font & Color
                [
                    'key' => 'font_style',
                    'name' => 'Font Style',
                    'content' => 'Inter',
                    'type' => 'Text',
                ],
                [
                    'key' => 'font_url',
                    'name' => 'Font Url',
                    'content' => 'https://fonts.googleapis.com/css2?family=Inter&display=swap',
                    'type' => 'Link',
                ],
                [
                    'key' => 'color_primary',
                    'name' => 'Primary Color',
                    'content' => '#070060',
                    'type' => 'Text',
                ],
                [
                    'key' => 'color_secondary',
                    'name' => 'Secondary Color',
                    'content' => '#28B7FB',
                    'type' => 'Text',
                ],
                [
                    'key' => 'color_tertiary',
                    'name' => 'Tertiary Color',
                    'content' => '#28B7FB',
                    'type' => 'Text',
                ],
                [
                    'key' => 'color_quaternary',
                    'name' => 'Quaternary Color',
                    'content' => '#EF476F',
                    'type' => 'Text',
                ],

                // Zoom
                [
                    'key' => 'zoom_api_key',
                    'name' => 'Zoom API Key',
                    'content' => 'xiVv2ZbKScaRXbW4dibC-Q',
                    'type' => 'Text',
                ],
                [
                    'key' => 'zoom_api_secret',
                    'name' => 'Zoom API Secret',
                    'content' => '2VlV8Y62Yu2Edv8wzKMGaLlvE2GGXgGm4H8I',
                    'type' => 'Text',
                ],

                // Landing Page
                [
                    'key' => 'logo_landingpage',
                    'name' => 'Logo Landingpage',
                    'content' => 'virtual-event/virtualeventv3/example/image/logo_landingpage.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'landingpage_browser_popup_status',
                    'name' => 'Landingpage Browser Popup Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],
                [
                    'key' => 'landingpage_about_title_text',
                    'name' => 'Landingpage About Title Text',
                    'content' => 'Back Story of Virtual Eventy 2023',
                    'type' => 'Text',
                ],
                [
                    'key' => 'landingpage_about_description_text',
                    'name' => 'Landingpage About Description Text',
                    'content' => 'Eventy.id merupakan teknologi event management berbasis platform. Menyediakan layanan dan fasilitas untuk berbagai kebutuhan event, baik yang diselenggarakan secara Offline, Virtual maupun Hybrid.',
                    'type' => 'Text',
                ],
                [
                    'key' => 'landingpage_whatsapp',
                    'name' => 'Landingpage Whatsapp',
                    'content' => 'https://wa.me/+62817179898989',
                    'type' => 'Text',
                ],
                [
                    'key' => 'landingpage_email',
                    'name' => 'Landingpage Email',
                    'content' => 'contact@eventy.id',
                    'type' => 'Text',
                ],
                [
                    'key' => 'landingpage_website',
                    'name' => 'Landingpage Website',
                    'content' => 'https://eventy.id',
                    'type' => 'Text',
                ],

                // Login
                [
                    'key' => 'login_title_text',
                    'name' => 'Login Title Text',
                    'content' => 'Welcome to the Login Page',
                    'type' => 'Text',
                ],
                [
                    'key' => 'login_caption_text',
                    'name' => 'Login Caption Text',
                    'content' => 'Please enter your registered email and password to join this event now',
                    'type' => 'Text',
                ],
                [
                    'key' => 'login_image',
                    'name' => 'Login Image',
                    'content' => 'virtual-event/virtualeventv3/example/image/poster_auth.png',
                    'type' => 'Image',
                ],

                // Register
                [
                    'key' => 'register_title_text',
                    'name' => 'Register Title Text',
                    'content' => 'Create your account',
                    'type' => 'Text',
                ],
                [
                    'key' => 'register_caption_text',
                    'name' => 'Register Caption Text',
                    'content' => 'Register your valid personal data to create a new account and immediately join this event',
                    'type' => 'Text',
                ],

                // Forgot Password
                [
                    'key' => 'reset_password_title_text',
                    'name' => 'Reset Password Title Text',
                    'content' => 'Reset Password',
                    'type' => 'Text',
                ],
                [
                    'key' => 'reset_password_caption_text',
                    'name' => 'Reset Password Caption Text',
                    'content' => 'Enter the email you previously registered for further verification',
                    'type' => 'Text',
                ],
                [
                    'key' => 'change_password_title_text',
                    'name' => 'Change Password Title Text',
                    'content' => 'Update Password',
                    'type' => 'Text',
                ],
                [
                    'key' => 'change_password_caption_text',
                    'name' => 'Change Password Caption Text',
                    'content' => 'Please create your new password',
                    'type' => 'Text',
                ],

                // Ticket
                [
                    'key' => 'ticket_status',
                    'name' => 'Ticket Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],
                [
                    'key' => 'ticket_title_text',
                    'name' => 'Ticket Title Text',
                    'content' => 'Order Ticket',
                    'type' => 'Text',
                ],
                [
                    'key' => 'ticket_caption_text',
                    'name' => 'Ticket Caption Text',
                    'content' => 'To continue registration, please book a ticket to join the session at this event',
                    'type' => 'Text',
                ],

                // Classroom
                [
                    'key' => 'classroom_title_text',
                    'name' => 'Classroom Title Text',
                    'content' => 'Order Workshop Ticket',
                    'type' => 'Text',
                ],
                [
                    'key' => 'classroom_caption_text',
                    'name' => 'Classroom Caption Text',
                    'content' => 'To join workshop, please book a ticket to join the session at this event',
                    'type' => 'Text',
                ],

                // Order Summary
                [
                    'key' => 'order_summary_title_text',
                    'name' => 'Order Summary Title Text',
                    'content' => 'Order Summary',
                    'type' => 'Text',
                ],
                [
                    'key' => 'order_summary_caption_text',
                    'name' => 'Order Summary Caption Text',
                    'content' => 'Please check your ticket order before making payment',
                    'type' => 'Text',
                ],

                // Payment
                [
                    'key' => 'payment_title_text',
                    'name' => 'Payment Title Text',
                    'content' => 'Payment',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_caption_text',
                    'name' => 'Payment Caption Text',
                    'content' => 'Pay your ticket fee to finish event registration',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_bank_name',
                    'name' => 'Payment Bank Name',
                    'content' => 'Mandiri',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_bank_branch',
                    'name' => 'Payment Bank Branch',
                    'content' => 'KCP Banyumanik',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_account_owner',
                    'name' => 'Payment Account Owner',
                    'content' => 'Demo Account',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_account_number',
                    'name' => 'Payment Account Number',
                    'content' => '012391792381723',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_confirmation_title_text',
                    'name' => 'Payment Confirmation Title Text',
                    'content' => 'Payment Confirmation',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_confirmation_caption_text',
                    'name' => 'Payment Confirmation Caption Text',
                    'content' => 'Please fill the form below to confirm your payment. Thank you!',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_method_va_status',
                    'name' => 'Payment Method VA Status',
                    'content' => 'nonactive',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_method_qris_status',
                    'name' => 'Payment Method QRIS Status',
                    'content' => 'nonactive',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_method_cc_status',
                    'name' => 'Payment Method CC Status',
                    'content' => 'nonactive',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_method_ovo_status',
                    'name' => 'Payment Method OVO Status',
                    'content' => 'nonactive',
                    'type' => 'Text',
                ],
                [
                    'key' => 'payment_method_manual_status',
                    'name' => 'Payment Method Manual Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],

                // Menu
                [
                    'key' => 'booth_menu_status',
                    'name' => 'Booth Menu Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],
                [
                    'key' => 'main_stage_menu_status',
                    'name' => 'Main Stage Menu Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],
                [
                    'key' => 'classroom_menu_status',
                    'name' => 'Classroom Menu Status',
                    'content' => 'active',
                    'type' => 'Text',
                ],
                [
                    'key' => 'home_menu_text',
                    'name' => 'Home Menu Text',
                    'content' => 'Home',
                    'type' => 'Text',
                ],
                [
                    'key' => 'home_menu_icon_active',
                    'name' => 'Home Menu Icon Active',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_homepage_active.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'home_menu_icon_nonactive',
                    'name' => 'Home Menu Icon Nonactive',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_homepage_nonactive.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_menu_text',
                    'name' => 'Booth Menu Text',
                    'content' => 'Exhibition',
                    'type' => 'Text',
                ],
                [
                    'key' => 'booth_menu_icon_active',
                    'name' => 'Booth Menu Icon Active',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_exhibition_active.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_menu_icon_nonactive',
                    'name' => 'Booth Menu Icon Nonactive',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_exhibition_nonactive.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'main_stage_menu_text',
                    'name' => 'Main Stage Menu Text',
                    'content' => 'Main Stage',
                    'type' => 'Text',
                ],
                [
                    'key' => 'main_stage_menu_icon_active',
                    'name' => 'Main Stage Menu Icon Active',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_mainstage_active.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'main_stage_menu_icon_nonactive',
                    'name' => 'Main Stage Menu Icon Nonactive',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_mainstage_nonactive.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'classroom_menu_text',
                    'name' => 'Classroom Menu Text',
                    'content' => 'Classroom',
                    'type' => 'Text',
                ],
                [
                    'key' => 'classroom_menu_icon_active',
                    'name' => 'Classroom Menu Icon Active',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_classroom_active.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'classroom_menu_icon_nonactive',
                    'name' => 'Classroom Menu Icon Nonactive',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/menu/img_classroom_nonactive.png',
                    'type' => 'Image',
                ],

                // Sub Menu
                [
                    'key' => 'schedule_menu_icon_active',
                    'name' => 'Schedule Menu Icon',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/submenu/img_schedule.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'sponsor_menu_icon_active',
                    'name' => 'Sponsor Menu Icon',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/submenu/img_sponsor.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_floor_menu_icon_active',
                    'name' => 'Booth Floor Plan Menu Icon',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/submenu/img_floorplan.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_list_menu_icon_active',
                    'name' => 'Booth List Menu Icon',
                    'content' => 'virtual-event/virtualeventv2/example/image/icon/submenu/img_booth_list.png',
                    'type' => 'Image',
                ],

                // Homepage
                [
                    'key' => 'home_background_image',
                    'name' => 'Home Background Image',
                    'content' => 'virtual-event/virtualeventv2/example/background/homepage.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'help_center_link',
                    'name' => 'Help Center Whatsapp',
                    'content' => 'https://wa.me/+62817171989898',
                    'type' => 'Text',
                ],
                [
                    'key' => 'home_slider_type',
                    'name' => 'Home Slider Type',
                    'content' => 'Image',
                    'type' => 'Text',
                ],
                [
                    'key' => 'iframe_player_type',
                    'name' => 'Player Type',
                    'content' => 'Default',
                    'type' => 'Text',
                ],

                // Booth
                [
                    'key' => 'booth_side_banner',
                    'name' => 'Booth Side Banner',
                    'content' => 'virtual-event/virtualeventv2/example/poster/poster_floor_plan.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_background_image',
                    'name' => 'Booth Background Image',
                    'content' => 'virtual-event/virtualeventv2/example/background/exhibition.png',
                    'type' => 'Image',
                ],
                [
                    'key' => 'booth_audio',
                    'name' => 'Booth Audio',
                    'content' => 'virtual-event/virtualeventv2/example/sound/sample_sound.mp3',
                    'type' => 'Audio',
                ],

                // Main Stage
                [
                    'key' => 'main_stage_background_image',
                    'name' => 'Main Stage Background Image',
                    'content' => 'virtual-event/virtualeventv2/example/background/mainstage.png',
                    'type' => 'Image',
                ],

                // Classroom
                [
                    'key' => 'classroom_background_image',
                    'name' => 'Classroom Background Image',
                    'content' => 'virtual-event/virtualeventv2/example/background/classroom.png',
                    'type' => 'Image',
                ],
            ];

            DB::table('preferences')->insert($prefs);
        }
    }
}
