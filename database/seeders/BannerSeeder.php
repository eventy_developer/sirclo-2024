<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Banner::count() === 0) {
            $data = [
                [
                    'type' => 'Image',
                    'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_landing_page_01.png',
                    'youtube_url' => null,
                    'sort' => 1,
                ],
                [
                    'type' => 'Image',
                    'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_landing_page_02.png',
                    'youtube_url' => null,
                    'sort' => 2,
                ],
                [
                    'type' => 'Image',
                    'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_landing_page_03.png',
                    'youtube_url' => null,
                    'sort' => 3,
                ],
                [
                    'type' => 'Image',
                    'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_landing_page_04.png',
                    'youtube_url' => null,
                    'sort' => 3,
                ],
                [
                    'type' => 'Youtube',
                    'image' => 'https://img.youtube.com/vi/QjJHNI8EWIQ/maxresdefault.jpg',
                    'youtube_url' => 'QjJHNI8EWIQ',
                    'sort' => 1,
                ],
            ];

            Banner::insert($data);
        }
    }
}
