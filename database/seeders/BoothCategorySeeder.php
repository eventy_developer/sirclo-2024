<?php

namespace Database\Seeders;

use App\Models\Booth;
use App\Models\BoothAttachment;
use App\Models\BoothCategory;
use App\Models\BoothContact;
use App\Models\BoothProduct;
use App\Models\BoothProductCategory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class BoothCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (BoothCategory::count() === 0) {
            BoothCategory::factory()
                ->count(4)
                ->state(new Sequence(
                    ['sort' => 1],
                    ['sort' => 2],
                    ['sort' => 3],
                    ['sort' => 4],
                ))
                ->has(
                    Booth::factory()
                        ->count(12)
                        ->state(new Sequence(
                            ['sort' => 1],
                            ['sort' => 2],
                            ['sort' => 3],
                            ['sort' => 4],
                            ['sort' => 5],
                            ['sort' => 6],
                            ['sort' => 7],
                            ['sort' => 8],
                            ['sort' => 9],
                            ['sort' => 10],
                            ['sort' => 11],
                            ['sort' => 12],
                        ))
                        ->has(BoothAttachment::factory()->count(7), 'attachments')
                        ->has(BoothContact::factory()
                            ->count(3)
                            ->state(new Sequence(
                                ['icon' => '', 'content' => 'Phone'],
                                ['icon' => '', 'content' => 'Website'],
                                ['icon' => '', 'content' => 'Email'],
                            )), 'contacts')
                        ->has(BoothProductCategory::factory()
                            ->count(3)
                            ->state(new Sequence(
                                ['name' => 'Handphone'],
                                ['name' => 'Laptop'],
                                ['name' => 'Speaker'],
                            ))
                            ->has(BoothProduct::factory()
                                ->count(6)
                                ->state(new Sequence(
                                    ['sort' => 1],
                                    ['sort' => 2],
                                    ['sort' => 3],
                                    ['sort' => 4],
                                    ['sort' => 5],
                                    ['sort' => 6],
                                )), 'products')
                            , 'productCategories'),
                    'booths')
                ->create();
        }
    }
}
