--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Participants', 'Route', 'AdminParticipantControllerGetIndex', NULL, 'fas fa-users', 0, 1, 0, 1, 7, '2022-09-30 00:26:06', NULL),
(2, 'Preferences', 'Route', 'AdminPreferenceControllerGetIndex', NULL, 'fas fa-cogs', 0, 1, 0, 1, 2, '2022-09-30 01:16:00', NULL),
(3, 'Notifications', 'Route', 'AdminNotificationControllerGetIndex', NULL, 'fas fa-bell', 0, 1, 0, 1, 3, '2022-09-30 05:26:46', NULL),
(4, 'Ticket', 'Route', 'AdminTicketControllerGetIndex', NULL, 'fas fa-ticket-alt', 0, 1, 0, 1, 4, '2022-09-30 05:27:49', NULL),
(6, 'Transactions', 'Route', 'AdminTransactionControllerGetIndex', NULL, 'far fa-circle', 14, 1, 0, 1, 1, '2022-09-30 05:35:37', NULL),
(7, 'Confirmation', 'Route', 'AdminTransactionConfirmationControllerGetIndex', 'normal', 'far fa-circle', 14, 1, 0, 1, 2, '2022-09-30 05:36:57', '2022-11-25 05:45:55'),
(8, 'Sponsors', 'Route', 'AdminSponsorCategoryControllerGetIndex', 'normal', 'fas fa-gem', 0, 1, 0, 1, 11, '2022-09-30 05:38:06', '2022-10-01 04:12:13'),
(9, 'Location', 'Route', 'AdminScheduleLocationControllerGetIndex', NULL, 'far fa-circle', 15, 1, 0, 1, 1, '2022-09-30 05:41:23', NULL),
(10, 'Booth', 'Route', 'AdminBoothCategoryControllerGetIndex', 'normal', 'far fa-circle', 16, 1, 0, 1, 2, '2022-10-01 01:09:55', '2022-11-25 05:47:13'),
(11, 'Booth Templates', 'Route', 'AdminBoothTemplateControllerGetIndex', 'normal', 'far fa-circle', 16, 1, 0, 1, 1, '2022-10-01 01:15:16', '2022-11-25 05:47:31'),
(12, 'Schedules', 'Route', 'AdminScheduleControllerGetIndex', NULL, 'far fa-circle', 15, 1, 0, 1, 2, '2022-10-01 04:00:42', NULL),
(13, 'Speakers', 'Route', 'AdminSpeakerControllerGetIndex', NULL, 'fas fa-user', 0, 1, 0, 1, 10, '2022-10-01 04:10:53', NULL),
(14, 'Transactions', 'URL', '#', 'normal', 'fas fa-money-bill', 0, 1, 0, 1, 5, '2022-10-03 01:31:38', '2022-10-03 01:36:02'),
(15, 'Schedules', 'URL', '#', 'normal', 'fas fa-calendar', 0, 1, 0, 1, 6, '2022-10-03 01:36:38', NULL),
(16, 'Booths', 'URL', '#', 'normal', 'fas fa-th-large', 0, 1, 0, 1, 8, '2022-10-03 01:37:09', NULL),
(17, 'Dashboard', 'Route', 'AdminIndexControllerGetIndex', 'normal', 'fas fa-tachometer-alt', 0, 1, 1, 1, 1, '2022-10-11 14:37:25', '2022-10-11 16:56:43'),
(18, 'Image', 'URL', '/dashboard/banner?type=Image', 'normal', 'far fa-circle', 19, 1, 0, 1, 1, '2022-11-24 20:14:03', '2022-11-25 05:51:57'),
(19, 'Banners', 'URL', '#', 'normal', 'fas fa-image', 0, 1, 0, 1, 9, '2022-11-25 05:48:06', NULL),
(20, 'Youtube', 'URL', '/dashboard/banner?type=Youtube', 'normal', 'far fa-circle', 19, 1, 0, 1, 2, '2022-11-25 05:52:28', NULL),
(21, 'Report Registrations', 'Route', 'AdminReportRegistrationControllerGetIndex', NULL, 'far fa-circle', 24, 1, 0, 1, 1, '2023-04-10 01:59:50', NULL),
(22, 'Report Transactions', 'Route', 'AdminReportTransactionControllerGetIndex', NULL, 'far fa-circle', 24, 1, 0, 1, 2, '2023-04-10 02:02:45', NULL),
(23, 'Report Transaction Confirmations', 'Route', 'AdminReportTransactionConfirmationControllerGetIndex', NULL, 'far fa-circle', 24, 1, 0, 1, 3, '2023-04-10 02:11:42', NULL),
(24, 'Report', 'URL', 'javascript:;', 'normal', 'fas fa-file', 0, 1, 0, 1, 12, '2023-04-10 02:40:12', NULL),
(25, 'Scan Types', 'Route', 'AdminScanTypeControllerGetIndex', NULL, 'fas fa-qrcode', 0, 1, 0, 1, 13, '2023-07-24 16:35:44', NULL),
(26, 'Email Templates', 'Route', 'AdminEmailTemplateControllerGetIndex', NULL, 'fas fa-envelope', 0, 1, 0, 1, 14, '2023-07-24 16:52:08', NULL),
(27, 'Bulk Email', 'Route', 'AdminBulkSendControllerGetIndex', NULL, 'fas fa-th-large', 0, 1, 0, 1, 15, '2023-07-24 16:53:39', NULL);
