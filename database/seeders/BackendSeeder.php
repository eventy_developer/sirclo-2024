<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BackendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms_users')->truncate();
        DB::table('cms_moduls')->truncate();
        DB::table('cms_privileges')->truncate();
        DB::table('cms_privileges_roles')->truncate();
        DB::table('cms_email_templates')->truncate();

        $this->command->info('Please wait updating the data...');

        $this->generateCmsUsers();
        $this->generateCmsModuls();
        $this->generateCmsPrivileges();
        $this->generateCmsMenus();
        $this->generateCmsSetting();
        $this->generateCmsEmailTemplates();

        $this->command->info('All cb seeders completed !');
    }

    public function generateCmsUsers()
    {
        if (DB::table('cms_users')->count() == 0) {
            $password = Hash::make('akuanakevent' . date('Y'));
            DB::table('cms_users')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Super Admin',
                'email' => 'eventysuperhebat@eventy.id',
                'password' => Hash::make('eventySuperHebatV3!'),
                'id_cms_privileges' => 1,
                'status' => 'Active',
            ]);

            DB::table('cms_users')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Admin Eventy',
                'email' => 'admindefault@eventy.id',
                'password' => Hash::make('eventyHebatV3!'),
                'id_cms_privileges' => 2,
                'status' => 'Active',
            ]);

            DB::table('cms_users')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Admin',
                'email' => 'admin@eventy.id',
                'password' => Hash::make('virtualevent' . date('Y')),
                'id_cms_privileges' => 2,
                'status' => 'Active',
            ]);
        }

        $this->command->info("Create cb users completed");
    }

    public function generateCmsModuls()
    {
        if (DB::table('cms_moduls')->count() == 0) {
            $sql = file_get_contents(database_path() . '/seeders/sql/cms_moduls.sql');

            DB::statement($sql);
        }

        $this->command->info("Create default cb modules completed");
    }

    public function generateCmsPrivileges()
    {
        if (DB::table('cms_privileges')->where('name', 'Super Administrator')->count() == 0) {
            DB::table('cms_privileges')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Super Administrator',
                'is_superadmin' => 1,
                'theme_color' => 'skin-blue-light',
            ]);

            DB::table('cms_privileges')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Admin',
                'is_superadmin' => 0,
                'theme_color' => 'skin-blue-light',
            ]);
        }

        if (DB::table('cms_privileges_roles')->count() == 0) {
            $modules = DB::table('cms_moduls')->get();
            $cms_privileges = DB::table('cms_privileges')->get();

            foreach ($cms_privileges as $cms_privilege) {
                $exceptionIds = [3, 4]; // Booth, Help Center

                foreach ($modules as $module) {

                    $is_visible = 1;
                    $is_create = 1;
                    $is_read = 1;
                    $is_edit = 1;
                    $is_delete = 1;

                    if (in_array($cms_privilege->id, $exceptionIds)) {
                        if ($module->path === 'dashboard') {
                            DB::table('cms_privileges_roles')->insert([
                                'created_at' => date('Y-m-d H:i:s'),
                                'is_visible' => $is_visible,
                                'is_create' => $is_create,
                                'is_edit' => $is_edit,
                                'is_delete' => $is_delete,
                                'is_read' => $is_read,
                                'id_cms_privileges' => $cms_privilege->id,
                                'id_cms_moduls' => $module->id,
                            ]);
                        }
                    } else {
                        $moduleSuperadminIds = [1, 2, 3, 5, 6, 7, 8, 9, 10, 11];
                        switch ($module->table_name) {
                            case 'cms_logs':
                                $is_create = 0;
                                $is_edit = 0;
                                break;
                            case 'cms_apicustom':
                            case 'cms_privileges_roles':
                                $is_visible = 0;
                                break;
                            case 'cms_notifications':
                                $is_create = $is_read = $is_edit = $is_delete = 0;
                                break;
                        }

                        if ($cms_privilege->id == 2 && $module->id == 4) {
                            $is_delete = 1;
                            $is_create = 0;
                        }

                        if ($cms_privilege->id == 1 || ($cms_privilege->id == 2 && !in_array($module->id, $moduleSuperadminIds))) {
                            DB::table('cms_privileges_roles')->insert([
                                'created_at' => date('Y-m-d H:i:s'),
                                'is_visible' => $is_visible,
                                'is_create' => $is_create,
                                'is_edit' => $is_edit,
                                'is_delete' => $is_delete,
                                'is_read' => $is_read,
                                'id_cms_privileges' => $cms_privilege->id,
                                'id_cms_moduls' => $module->id,
                            ]);
                        }
                    }
                }
            }
        }

        $this->command->info("Create cb roles completed");
    }

    public function generateCmsMenus()
    {
        if (DB::table('cms_menus')->count() == 0) {
            $sql = file_get_contents(database_path() . '/seeders/sql/cms_menus.sql');

            DB::statement($sql);
        }

        if (DB::table('cms_menus_privileges')->count() == 0) {
            $cms_menus = DB::table('cms_menus')->get();

            foreach ($cms_menus as $cms_menu) {
                if ($cms_menu->path === 'dashboard') {
                    for ($i = 1; $i <= 4; $i++) {
                        DB::table('cms_menus_privileges')->insert([
                            'id_cms_menus' => $cms_menu->id,
                            'id_cms_privileges' => $i
                        ]);
                    }
                } else {

                    // only role superadmin & admin
                    for ($i = 1; $i <= 2; $i++) {
                        DB::table('cms_menus_privileges')->insert([
                            'id_cms_menus' => $cms_menu->id,
                            'id_cms_privileges' => $i
                        ]);
                    }
                }
            }
        }

        $this->command->info("Create cb menus completed");
    }

    public function generateCmsSetting()
    {
        $data = [

            //LOGIN REGISTER STYLE
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'login_background_color',
                'label' => 'Login Background Color',
                'content' => null,
                'content_input_type' => 'text',
                'group_setting' => cbLang('login_register_style'),
                'dataenum' => null,
                'helper' => 'Input hexacode',
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'login_font_color',
                'label' => 'Login Font Color',
                'content' => null,
                'content_input_type' => 'text',
                'group_setting' => cbLang('login_register_style'),
                'dataenum' => null,
                'helper' => 'Input hexacode',
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'login_background_image',
                'label' => 'Login Background Image',
                'content' => null,
                'content_input_type' => 'upload_image',
                'group_setting' => cbLang('login_register_style'),
                'dataenum' => null,
                'helper' => null,
            ],

            //EMAIL SETTING
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'email_sender',
                'label' => 'Email Sender',
                'content' => 'support@crudbooster.com',
                'content_input_type' => 'text',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'smtp_driver',
                'label' => 'Mail Driver',
                'content' => 'mail',
                'content_input_type' => 'select',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => 'smtp,mail,sendmail',
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'smtp_host',
                'label' => 'SMTP Host',
                'content' => '',
                'content_input_type' => 'text',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'smtp_port',
                'label' => 'SMTP Port',
                'content' => '25',
                'content_input_type' => 'text',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => null,
                'helper' => 'default 25',
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'smtp_username',
                'label' => 'SMTP Username',
                'content' => '',
                'content_input_type' => 'text',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'smtp_password',
                'label' => 'SMTP Password',
                'content' => '',
                'content_input_type' => 'text',
                'group_setting' => cbLang('email_setting'),
                'dataenum' => null,
                'helper' => null,
            ],

            //APPLICATION SETTING
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'appname',
                'label' => 'Application Name',
                'group_setting' => cbLang('application_setting'),
                'content' => 'CRUDBooster',
                'content_input_type' => 'text',
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'default_paper_size',
                'label' => 'Default Paper Print Size',
                'group_setting' => cbLang('application_setting'),
                'content' => 'Legal',
                'content_input_type' => 'text',
                'dataenum' => null,
                'helper' => 'Paper size, ex : A4, Legal, etc',
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'logo',
                'label' => 'Logo',
                'content' => '',
                'content_input_type' => 'upload_image',
                'group_setting' => cbLang('application_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'favicon',
                'label' => 'Favicon',
                'content' => '',
                'content_input_type' => 'upload_image',
                'group_setting' => cbLang('application_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'api_debug_mode',
                'label' => 'API Debug Mode',
                'content' => 'true',
                'content_input_type' => 'select',
                'group_setting' => cbLang('application_setting'),
                'dataenum' => 'true,false',
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'google_api_key',
                'label' => 'Google API Key',
                'content' => '',
                'content_input_type' => 'text',
                'group_setting' => cbLang('application_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
            [
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'google_fcm_key',
                'label' => 'Google FCM Key',
                'content' => '',
                'content_input_type' => 'text',
                'group_setting' => cbLang('application_setting'),
                'dataenum' => null,
                'helper' => null,
            ],
        ];

        foreach ($data as $row) {
            $count = DB::table('cms_settings')->where('name', $row['name'])->count();
            if ($count) {
                if ($count > 1) {
                    $newsId = DB::table('cms_settings')->where('name', $row['name'])->orderby('id', 'asc')->take(1)->first();
                    DB::table('cms_settings')->where('name', $row['name'])->where('id', '!=', $newsId->id)->delete();
                }
                continue;
            }
            DB::table('cms_settings')->insert($row);
        }

        $this->command->info("Create cb settings completed");
    }

    public function generateCmsEmailTemplates()
    {
        if (DB::table('cms_email_templates')->count() == 0) {
            DB::table('cms_email_templates')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Email Template Forgot Password Backend',
                'slug' => 'forgot_password_backend',
                'content' => '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>',
                'description' => '[password]',
                'from_name' => 'System',
                'from_email' => 'system@crudbooster.com',
                'cc_email' => null,
            ]);
        }

        $this->command->info("Create email templates completed");
    }
}
