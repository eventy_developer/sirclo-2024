<?php

namespace Database\Seeders;

use App\Models\Schedule;
use App\Models\ScheduleAttachment;
use App\Models\ScheduleSession;
use App\Models\ScheduleSessionSpeaker;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schedule::count() === 0) {
            Schedule::factory()
                ->count(4)
                ->state(new Sequence(
                    ['schedule_location_id' => 1],
                    ['schedule_location_id' => 2],
                    ['schedule_location_id' => 1],
                    ['schedule_location_id' => 2],
                ))
                ->has(ScheduleSession::factory()
                    ->count(5)
                    ->state(new Sequence(
                        ['time_start' => '07:00', 'time_end' => '08:00'],
                        ['time_start' => '08:00', 'time_end' => '09:00'],
                        ['time_start' => '09:00', 'time_end' => '10:00'],
                        ['time_start' => '10:00', 'time_end' => '11:30'],
                        ['time_start' => '13:00', 'time_end' => '17:00'],
                    ))
                    ->has(ScheduleSessionSpeaker::factory()->count(1))
                    ->has(ScheduleAttachment::factory()->count(6), 'attachments')
                    , 'sessions')
                ->create();
        }
    }
}
