<?php

namespace Database\Seeders;

use App\Models\ScanGroup;
use App\Models\ScanType;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ScanGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (ScanGroup::count() === 0) {
            ScanGroup::factory()
                ->count(21)
                ->state(new Sequence(
                    ['name' => 'E-Commerce Expo 2023: powered by SIRCLO', 'cms_user_id' => 3, 'sort' => 1],
                    ['name' => 'Booth 1', 'cms_user_id' => 4, 'sort' => 2],
                    ['name' => 'Booth 2', 'cms_user_id' => 5, 'sort' => 3],
                    ['name' => 'Booth 3', 'cms_user_id' => 6, 'sort' => 4],
                    ['name' => 'Booth 4', 'cms_user_id' => 7, 'sort' => 5],
                    ['name' => 'Booth 5', 'cms_user_id' => 8, 'sort' => 6],
                    ['name' => 'Booth 6', 'cms_user_id' => 9, 'sort' => 7],
                    ['name' => 'Booth 7', 'cms_user_id' => 10, 'sort' => 8],
                    ['name' => 'Booth 8', 'cms_user_id' => 11, 'sort' => 9],
                    ['name' => 'Booth 9', 'cms_user_id' => 12, 'sort' => 10],
                    ['name' => 'Booth 10', 'cms_user_id' => 13, 'sort' => 11],
                    ['name' => 'Booth 11', 'cms_user_id' => 14, 'sort' => 12],
                    ['name' => 'Booth 12', 'cms_user_id' => 15, 'sort' => 13],
                    ['name' => 'Booth 13', 'cms_user_id' => 16, 'sort' => 14],
                    ['name' => 'Booth 14', 'cms_user_id' => 17, 'sort' => 15],
                    ['name' => 'Booth 15', 'cms_user_id' => 18, 'sort' => 16],
                    ['name' => 'Booth 16', 'cms_user_id' => 19, 'sort' => 17],
                    ['name' => 'Booth 17', 'cms_user_id' => 20, 'sort' => 18],
                    ['name' => 'Booth 18', 'cms_user_id' => 21, 'sort' => 19],
                    ['name' => 'Booth 19', 'cms_user_id' => 22, 'sort' => 20],
                    ['name' => 'Booth 20', 'cms_user_id' => 23, 'sort' => 21],
                ))
                ->has(
                    ScanType::factory()
                        ->count(2)
                        ->state(new Sequence(
                            ['name' => 'Check-In Kunjungan', 'type' => 'Checkin', 'point' => 1],
                            ['name' => 'Point Doorprize', 'type' => 'Point', 'point' => 1],
                        ))
                , 'scanTypes')
                ->create();

        }
    }
}
