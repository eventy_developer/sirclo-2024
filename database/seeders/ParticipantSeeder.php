<?php

namespace Database\Seeders;

use App\Models\Participant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create demo account
        $check = Participant::whereEmail('demo@eventy.id')->first();

        if (!$check) {
            $save['created_at'] = date('Y-m-d H:i:s');
            $save['ticket_id'] = 1;
            $save['name'] = 'Demo Eventy';
            $save['first_name'] = 'Demo';
            $save['last_name'] = 'Eventy';
            $save['email'] = 'demo@eventy.id';
            $save['password'] = Hash::make('virtualevent' . date('Y'));
            $save['phone'] = '08123456789';
            $save['company'] = 'PT. Taman Event Indonesia';
            $save['job_title'] = 'Tester';
            $save['business_type'] = 'Individual';
            $save['nationality'] = 'INDONESIA';
            $save['status'] = Participant::STATUS_ACTIVE;
            $save['is_invitation'] = 1;
            $save['is_admin'] = 1;
            $save['is_feedback_filled'] = 0;
            $save['is_activated_manually'] = 0;
            DB::table('participants')->insert($save);
        }
    }
}
