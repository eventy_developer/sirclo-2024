<?php

namespace Database\Seeders;

use App\Models\Ticket;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Ticket::count() === 0) {
            $tickets = [
                [
                    'name' => 'Free Ticket',
                    'price' => 0,
                    'is_only_ticket' => 1,
                    'description' => '',
                ],
                [
                    'name' => 'Paid Ticket',
                    'price' => 100000,
                    'is_only_ticket' => 0,
                    'description' => 'lorem ipsum dolor sir amet lorem ipsum dolor sir ametlorem ipsum dolor sir amet.',
                ],
            ];

            foreach ($tickets as $row) {
                $row = array_merge($row, [
                    'type' => $row['price'] > 0 ? 'Paid' : 'Free',
                ]);

                $ticket_id = DB::table('tickets')->insertGetId($row);

                if ($row['is_only_ticket'] === 0) {
                    $schedules = DB::table('schedules')->where('schedule_location_id', 2)->limit(4)->get();

                    foreach ($schedules as $xrow) {
                        $insert = [
                            'ticket_id' => $ticket_id,
                            'schedule_id' => $xrow->id,
                            'price' => round(rand(100000, 900000)),
                        ];

                        DB::table('ticket_schedules')->insert($insert);
                    }
                }
            }
        }
    }
}
