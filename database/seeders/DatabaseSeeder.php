<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BackendSeeder::class,
            PreferenceSeeder::class,
            ParticipantSeeder::class,
            CountrySeeder::class,
            NotificationSeeder::class,
            BoothTemplateSeeder::class,
            BoothCategorySeeder::class,
            BannerSeeder::class,
            SpeakerSeeder::class,
            SponsorCategorySeeder::class,
//            ScheduleLocationSeeder::class,
//            ScheduleSeeder::class,
//            TicketSeeder::class,
        ]);
    }
}
