<?php

namespace Database\Seeders;

use App\Models\Speaker;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class SpeakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Speaker::count() === 0) {
            Speaker::factory()
                ->count(4)
                ->state(new Sequence(
                    ['sort' => 1, 'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_speaker_01.png'],
                    ['sort' => 2, 'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_speaker_02.png'],
                    ['sort' => 3, 'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_speaker_03.png'],
                    ['sort' => 4, 'image' => 'virtual-event/virtualeventv3/example/placeholder/landingpage/kv_platform_v3_speaker_04.png'],
                ))
                ->create();
        }
    }
}
