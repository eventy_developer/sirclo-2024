<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BoothSircloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 7; $i <= 20; $i++) {
            DB::table('cms_users')->insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Booth ' . $i,
                'email' => 'booth' . $i . '@eventy.id',
                'password' => Hash::make('sirclo2023!'),
                'id_cms_privileges' => 3,
                'status' => 'Active',
            ]);
        }
    }
}
