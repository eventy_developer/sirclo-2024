<?php

namespace Database\Seeders;

use App\Models\BoothTemplate;
use Illuminate\Database\Seeder;

class BoothTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (BoothTemplate::count() === 0) {
            BoothTemplate::insert([
                'created_at' => date('Y-m-d H:i:s'),
                'name' => 'Booth Template 01',
            ]);
        }
    }
}
