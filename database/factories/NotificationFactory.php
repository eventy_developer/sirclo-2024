<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(50),
            'message' => $this->faker->text,
            'send_at' => dateTime(),
            'link' => null,
            'is_active' => 1,
            'is_send' => 0,
        ];
    }
}
