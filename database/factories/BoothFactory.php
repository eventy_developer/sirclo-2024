<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BoothFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'booth_template_id' => 1,
            'name' => $this->faker->text(50),
            'description' => $this->faker->text(1000),
        ];
    }
}
