<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ScheduleAttachmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $file = null;
        $doce[0] = null;
        $doce[1] = null;
        $url = null;
        $size = null;
        $name = null;
        $types = ['Image', 'Document', 'Video'];
        $documents = [
            'virtual-event/virtualeventv3/example/pdf/best_selling.pdf',
            'virtual-event/virtualeventv3/example/pdf/personal_branding.pdf',
            'virtual-event/virtualeventv3/example/pdf/powerful_negotiation.pdf',
            'virtual-event/virtualeventv3/example/pdf/quick_selling.pdf',
        ];
        $youtube_ids = ['nU307tV32B0', '2M3jN5VTsIY', 'YdWJmKSVf5c'];


        $getType = $types[$this->faker->numberBetween(0, 2)];

        if (in_array($getType, ['Image', 'Document'])) {
            $file = $documents[$this->faker->numberBetween(0, 3)];
            if ($getType === 'Image') {
                $file = 'virtual-event/virtualeventv3/example/placeholder/ph_image.png';
            }

            $doce = explode('.', explode('/', $file)[4]);
            $size = round(rand(1000, 10000));
            $name = $doce[0] . '.' . $doce[1];

        } else {
            $url = $youtube_ids[$this->faker->numberBetween(0, 2)];
            $name = 'Sample Activity Video 2022';
        }

        return [
            'type' => $getType,
            'file' => $file,
            'file_name' => $name,
            'file_size' => $size,
            'extension' => $doce[1],
            'video_url' => $url,
        ];
    }
}
