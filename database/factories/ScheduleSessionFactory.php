<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ScheduleSessionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dates = ['2022-09-30', '2022-10-01', '2022-10-02'];
        $platform_types = ['Youtube', 'Zoom'];
        $youtube_ids = ['nU307tV32B0', '2M3jN5VTsIY', 'YdWJmKSVf5c'];
        $youtube_id = null;
        $zoom_conference_id = null;
        $zoom_conference_password = null;
        $platform_selected = $platform_types[$this->faker->numberBetween(0, 1)];

        if ($platform_selected === 'Youtube') {
            $youtube_id = $youtube_ids[$this->faker->numberBetween(0, 2)];
        } else {
            $zoom_conference_id = 888888;
            $zoom_conference_password = 123123;
        }

        return [
            'title' => $this->faker->text(50),
            'description' => $this->faker->text,
            'date' => $dates[$this->faker->numberBetween(0, 2)],
            'platform_type' => $platform_selected,
            'youtube_id' => $youtube_id,
            'zoom_conference_id' => $zoom_conference_id,
            'zoom_conference_password' => $zoom_conference_password,
        ];
    }
}
