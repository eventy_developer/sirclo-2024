<nav class="bottom-navigation">
    <ul class="nav-bottom-menu">
        <li class="nav-bottom-item nav--step active" data-href="">
            <a href="{{ url('/lobby') }}" class="menu-item">
                <svg width="38" height="38" viewBox="0 0 38 38" fill="#A7ACB4" xmlns="http://www.w3.org/2000/svg">
                    <path d="M30.4404 18.734L30.1515 19.0514C29.7392 19.5051 29.1953 19.6874 28.9365 19.4599C28.6766 19.2316 28.4669 19.0477 28.4669 19.0477V28.8914L28.4671 28.8911C28.4669 29.1852 28.3501 29.4672 28.1422 29.6753C27.9341 29.8832 27.6521 30 27.358 29.9998H23.148C22.8607 29.9987 22.5862 29.8791 22.3894 29.6696C22.1924 29.4601 22.0903 29.1787 22.1069 28.8918V21.9064H17.0401V28.8918C17.0403 29.1856 16.9235 29.4677 16.7156 29.6755C16.5078 29.8834 16.226 30.0002 15.9319 30H11.9595C11.6655 30.0002 11.3834 29.8834 11.1755 29.6755C10.9677 29.4677 10.8508 29.1858 10.8511 28.8918V18.9071L10.0892 19.589V19.5893C9.86778 19.769 9.58571 19.857 9.30114 19.8354C9.01654 19.8135 8.7513 19.6833 8.55982 19.4715L8.30426 19.2056V19.2058C8.10101 18.9961 7.99132 18.7131 8.00054 18.4211C8.00999 18.1294 8.13742 17.8538 8.35358 17.6577L18.5442 8.31602C18.7716 8.11577 19.0631 8.00354 19.3662 8.00008C19.6692 7.99663 19.9632 8.10217 20.1951 8.29713L30.3515 17.1839H30.3517C30.5725 17.3754 30.7066 17.6483 30.7234 17.9402C30.7402 18.2322 30.6381 18.5187 30.4406 18.7344L30.4404 18.734Z"/>
                </svg>
            </a>
            <span class="label-menu-mini">Lobby</span>
        </li>
        <li class="nav-bottom-item nav--step" data-href="">
            <a href="#" class="menu-item" data-bs-toggle="modal" data-bs-target="#boothCategoryModal">
                <svg width="38" height="38" viewBox="0 0 38 38" fill="#A7ACB4" xmlns="http://www.w3.org/2000/svg">
                    <path d="M26.5818 28.1866C26.5818 29.188 25.7699 29.9997 24.7687 29.9997C23.7673 29.9997 22.9556 29.188 22.9556 28.1866C22.9556 27.1854 23.7673 26.3737 24.7687 26.3737C25.7699 26.3737 26.5818 27.1853 26.5818 28.1866Z"/>
                    <path d="M19.3293 28.1866C19.3293 29.1881 18.5174 30 17.5161 30C16.5145 30 15.7028 29.1881 15.7028 28.1866C15.7028 27.1852 16.5145 26.3733 17.5161 26.3733C18.5174 26.3733 19.3293 27.1852 19.3293 28.1866Z"/>
                    <path d="M30.0871 11.2636H12.9438L12.3657 8.6625C12.2798 8.27537 11.9366 8 11.5399 8H7.8462C7.37852 8 7 8.37869 7 8.8462C7 9.31332 7.37851 9.69239 7.8462 9.69239H10.861L14.2054 24.7436C14.2914 25.1308 14.6349 25.4065 15.0318 25.4065H27.1864C27.6539 25.4065 28.0326 25.0275 28.0326 24.5603C28.0326 24.0928 27.6541 23.7141 27.1864 23.7141H15.71L15.3607 22.1429H27.6699C28.0704 22.1429 28.4652 21.826 28.552 21.4349L30.6551 11.972C30.7421 11.5805 30.4878 11.2636 30.0869 11.2636H30.0871Z"/>
                </svg>
            </a>
            <span class="label-menu-mini">Exhibition</span>
        </li>
        <li class="nav-bottom-item nav--step" data-href="">
            <a href="{{ url('mainstage') }}" class="menu-item">
                <svg width="38" height="38" viewBox="0 0 38 38" fill="#A7ACB4" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.3102 8C10.3683 8 8 10.3686 8 13.3102V24.6895C8 27.6315 10.3686 30 13.3102 30H25.0344C27.9763 30 30.3446 27.6314 30.3446 24.6895V13.3102C30.3446 10.3683 27.976 8 25.0344 8H13.3102ZM16.1378 14.0691L24.4826 19.0002L16.1378 23.9313V14.0691Z" />
                </svg>
            </a>
            <span class="label-menu-mini">Main Stage</span>
        </li>
        <li class="nav-bottom-item nav--step" data-href="">
            <a href="{{ url('classroom') }}" class="menu-item">
                <svg width="38" height="38" viewBox="0 0 38 38" fill="#A7ACB4" xmlns="http://www.w3.org/2000/svg">
                    <path d="M31.3374 23.8417L31.3374 26.44C31.3374 28.4651 29.6958 30.1067 27.6707 30.1067L10.56 30.1067C8.53493 30.1067 6.89331 28.4651 6.89331 26.44L6.89331 23.8417C9.00185 23.2988 10.56 21.3847 10.56 19.1067C10.56 16.8287 9.00185 14.9146 6.89331 14.3717L6.89331 11.7734C6.89331 9.74831 8.53493 8.10669 10.56 8.10669L27.6707 8.10669C29.6958 8.10669 31.3374 9.74831 31.3374 11.7734L31.3374 14.3717C29.2289 14.9145 27.6707 16.8287 27.6707 19.1067C27.6707 21.3847 29.2289 23.2988 31.3374 23.8417ZM22.782 22.7734L22.782 15.44C22.782 14.765 22.2349 14.2179 21.5599 14.2179C20.8848 14.2179 20.3375 14.765 20.3375 15.44L20.3375 22.7734C20.3375 23.4484 20.8848 23.9955 21.5599 23.9955C22.2349 23.9955 22.782 23.4484 22.782 22.7734ZM17.8932 22.7734L17.8932 15.44C17.8932 14.765 17.3459 14.2179 16.6708 14.2179C15.9958 14.2179 15.4487 14.765 15.4487 15.44L15.4487 22.7734C15.4487 23.4484 15.9958 23.9955 16.6708 23.9955C17.3459 23.9955 17.8932 23.4484 17.8932 22.7734Z" />
                </svg>
            </a>
            <span class="label-menu-mini">Class Room</span>
        </li>
    </ul>
</nav>

<!-- Modal Booth Category -->
<div class="modal modal-booth-category fade" id="boothCategoryModal" tabindex="-1" aria-labelledby="boothCategoryModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <h5 class="modal-title">Booth Categories</h5>
                <p class="modal-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis et posuere neque neque integer.</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Slider main container -->
                <div class="swiper swiper-booth-categories">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->

                        @foreach($boothCategories as $category)
                        <div class="swiper-slide">
                            <div class="card-category"
                                style="background-image: url('{{ $category->present_thumbnail }}')">
                                <div class="card-category__inner">
                                    <h5 class="card-category__inner__title">{{ $category->name }}1</h5>
                                    <p class="card-category__inner__total">{{ $category->booths_count }} Booths</p>

                                    <a href="{{ url('booth?category_id=' . $category->id) }}" class="btn-explore">Explore</a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>
                </div>

                <div class="swiper-booth-category-navigation" role="navigation">
                    <div class="swiper-button-prev">
                        <img src="{{ asset('images/icon/icon_arrow_left.svg') }}" alt="Icon Arrow Left">
                    </div>
                    <div class="swiper-button-next">
                        <img src="{{ asset('images/icon/icon_arrow_left.svg') }}" alt="Icon Arrow Right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
