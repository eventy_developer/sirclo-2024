<!-- Modal Profile -->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog"
     aria-labelledby="profileModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="profileModalLabel">Upload Profile Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="croppie-img" src="/example/image/icon.jpg" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-primary text-white" id="saveCropie">Save changes</button>
            </div>
        </div>
    </div>
</div>

<nav class="navbar fixed-top d-flex" id="appHeader">
    <div class="container-fluid container-header position-relative">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a href="https://expo.sirclo.com/" class="navbar-brand brand-logo">
                <img src="{{ preferGet('logo_events') }}" alt="logo"/>
            </a>
            <a href="https://expo.sirclo.com/" class="navbar-brand brand-logo-mini">
                <img src="{{ preferGet('icon_events') }}" alt="icon"/>
            </a>
        </div>
        <div class="navbar-nav-center">
            <h3 class="event-title">{{ preferGet('name_events') }}</h3>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
            <ul class="navbar-nav navbar-nav-right" id="navbarNav">
                <li class="nav-item position-relative">
{{--                    <button role="button" class="notif-app" @click="togglePopupMenu('#box-notification')">--}}
{{--                        <img src="{{ asset('images/example/ellipse_new_notif.png') }}"--}}
{{--                             alt="Icon New Notif" class="notif-badge-new"--}}
{{--                             v-if="countNotRead">--}}
{{--                        <img src="{{ asset('images/button/btn_notification.svg') }}" alt="Icon Bell">--}}
{{--                    </button>--}}
{{--                    <div class="box-notification" id="box-notification">--}}
{{--                        <div class="notification-header">--}}
{{--                            <h5>Today Notifications <span class="total-badge">(@{{ countNotRead }})</span></h5>--}}
{{--                            <p class="mark-as-read" role="button" @click="readNotification">--}}
{{--                                Mark as read--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                        <div class="notification-empty mt-3" v-if="notifications.length <= 0">--}}
{{--                            <img src="{{ asset('template/images/empty_states/empty_states_notification@2x.png') }}" alt="Empty Notification"--}}
{{--                                 class="w-100">--}}
{{--                        </div>--}}
{{--                        <ul class="notification-list scroll-list">--}}
{{--                            <li class="notification-item" v-for="(item, i) in notifications" :key="item.id"--}}
{{--                                :class="{ 'has-read' : item.detail.is_read, 'bottom-rounded' : notReadLatest === i }">--}}
{{--                                <h5 class="notification-title">@{{ item.title }}</h5>--}}
{{--                                <p class="notification-message">@{{ item.message }}</p>--}}
{{--                                <a :href="item.link" target="_blank" class="notification-attachment" v-if="item.link">--}}
{{--                                    <svg width="10" height="10" viewBox="0 0 10 10" fill="#28B7FB" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                        <path d="M0.723914 5.32199L4.58975 1.45615C4.98416 1.06174 5.51909 0.840163 6.07687 0.840163C6.63465 0.840163 7.16958 1.06174 7.56399 1.45615C7.9584 1.85056 8.17998 2.38549 8.17998 2.94327C8.17998 3.50105 7.9584 4.03598 7.56399 4.43039L3.10263 8.8909C2.94399 9.04412 2.73152 9.1289 2.51098 9.12699C2.29044 9.12507 2.07947 9.03661 1.92352 8.88066C1.76757 8.72471 1.67911 8.51374 1.67719 8.2932C1.67527 8.07266 1.76005 7.86018 1.91327 7.70155L6.37379 3.24019C6.4504 3.16087 6.49279 3.05463 6.49183 2.94436C6.49087 2.83409 6.44664 2.72861 6.36867 2.65063C6.29069 2.57265 6.18521 2.52842 6.07494 2.52747C5.96467 2.52651 5.85843 2.5689 5.77911 2.64551L1.31859 7.10771C1.15792 7.26289 1.02976 7.44852 0.941597 7.65376C0.853431 7.859 0.807024 8.07975 0.805083 8.30312C0.803142 8.52649 0.845706 8.74801 0.930291 8.95475C1.01488 9.1615 1.13979 9.34932 1.29774 9.50728C1.45569 9.66523 1.64352 9.79014 1.85027 9.87473C2.05701 9.95931 2.27853 10.0019 2.5019 9.99994C2.72527 9.998 2.94601 9.95159 3.15126 9.86342C3.3565 9.77526 3.54213 9.6471 3.69731 9.48643L8.15867 5.02591C8.7108 4.47378 9.02098 3.72493 9.02098 2.94411C9.02098 2.16328 8.7108 1.41444 8.15867 0.862309C7.60654 0.310182 6.8577 0 6.07687 0C5.29605 0 4.5472 0.310182 3.99507 0.862309L0.128394 4.72731C0.0882253 4.7661 0.0561856 4.81251 0.0341442 4.86382C0.0121028 4.91513 0.000501135 4.97032 1.58793e-05 5.02616C-0.000469376 5.082 0.0101716 5.13738 0.031318 5.18907C0.0524644 5.24075 0.0836927 5.28771 0.123181 5.3272C0.162669 5.36669 0.209626 5.39791 0.261312 5.41906C0.312998 5.44021 0.368377 5.45085 0.42422 5.45036C0.480062 5.44988 0.535249 5.43828 0.58656 5.41623C0.63787 5.39419 0.684277 5.36215 0.723073 5.32199H0.723914Z"/>--}}
{{--                                    </svg>--}}
{{--                                    @{{ item.link_name }}--}}
{{--                                </a>--}}
{{--                                <span>@{{ item.time_elapsed }}</span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
                </li>
{{--                <li class="nav-item dropdown position-relative">--}}
{{--                    <a role="button" class="nav-link dropdown-toggle nav-profile py-0"--}}
{{--                       href="javascript:void(0)"--}}
{{--                       id="settings-trigger"--}}
{{--                       @click="togglePopupMenu('#box-profile')">--}}
{{--                        <div class="wrapper d-flex align-items-center text-right">--}}
{{--                            <div class="wrapper me-2">--}}
{{--                                <h6 class="profile-name">--}}
{{--                                    <strong v-cloak>--}}
{{--                                        @{{ is_mobile ? profile_name.slice(0, 10) + '...' : profile_name }}--}}
{{--                                    </strong>--}}
{{--                                </h6>--}}
{{--                                <small class="profile-company" v-cloak>--}}
{{--                                    @{{ is_mobile ? profile_company.slice(0, 15) + '...' : profile_company }}--}}
{{--                                </small>--}}
{{--                            </div>--}}
{{--                            <img :src="profile_image" id="header_profile_image" alt="profile">--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <div class="box-profile" id="box-profile">--}}
{{--                        <div id="profile-info">--}}
{{--                            <div class="profile-header">--}}
{{--                                <h5 class="profile-header__title">Profile Account</h5>--}}
{{--                                <div class="img-heading">--}}
{{--                                    <input type="file" id="imgHeading" class="upload-heading"--}}
{{--                                           @change="previewImage" accept="image/*"/>--}}
{{--                                    <img :src="profile_image" alt="img Heading" id="show_imgHeading">--}}
{{--                                    <i class="fas fa-pencil-alt"></i>--}}
{{--                                </div>--}}
{{--                                <p v-if="errors_upload.image" class="text-danger font-weight-bold text-alert">--}}
{{--                                    @{{ errors_upload.image[0] }}--}}
{{--                                </p>--}}
{{--                                <h5 class="profile-header__name">@{{ profile_name }}</h5>--}}
{{--                                <p class="profile-header__company">@{{ profile_company }}</p>--}}
{{--                            </div>--}}
{{--                            <ul class="profile-body">--}}
{{--                                <li>--}}
{{--                                    <div class="profile-body__icon">--}}
{{--                                        <img src="{{ asset('images/icon/header/icon_email.svg') }}" alt="Icon Email">--}}
{{--                                    </div>--}}
{{--                                    <div class="profile-body__info">--}}
{{--                                        <span class="profile-body__label">Email</span>--}}
{{--                                        <span class="profile-body__value">@{{ profile_email }}</span>--}}
{{--                                    </div>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <div class="profile-body__icon">--}}
{{--                                        <img src="{{ asset('images/icon/header/icon_password.svg') }}" alt="Icon Password">--}}
{{--                                    </div>--}}
{{--                                    <div class="profile-body__info">--}}
{{--                                        <span class="profile-body__label">Password</span>--}}
{{--                                        <span class="profile-body__value">******</span>--}}
{{--                                    </div>--}}
{{--                                    <button class="btn-edit-password" @click="showEditPassword">Edit</button>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <button class="btn-logout" @click="logOut">@lang('eventy/website_config.profile_btn_logout')</button>--}}
{{--                        </div>--}}
{{--                        <div id="profile-password">--}}
{{--                            <div class="profile-password-header">--}}
{{--                                <a role="button" href="javascript:void(0)" @click="showProfile" class="btn-side-back">--}}
{{--                                    <img src="{{ asset('images/icon/header/icon_arrow_left.svg') }}" alt="Icon Arrow Left">--}}
{{--                                </a>--}}
{{--                                <h5 class="profile-password-header__title">@lang('eventy/website_config.title_edit_pass')</h5>--}}
{{--                            </div>--}}

{{--                            <div class="profile-password-form">--}}
{{--                                <div class="form-group mb-3">--}}
{{--                                    <label>@lang('eventy/website_config.edit_pass_label_current_pass')</label>--}}
{{--                                    <div class="position-relative d-flex">--}}
{{--                                        <input type="password" id="current_password" class="form-control" style="font-size: 1.2rem;" v-model="current_password">--}}
{{--                                        <div class="show-password small" style="top: 50%;">--}}
{{--                                            <i class="fa fa-eye"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p v-if="errors_editpass.current_password" class="text-danger font-weight-bold text-alert">--}}
{{--                                        @{{ errors_editpass.current_password[0] }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                                <div class="form-group mb-3">--}}
{{--                                    <label>@lang('eventy/website_config.edit_pass_label_new_pass')</label>--}}
{{--                                    <div class="position-relative d-flex">--}}
{{--                                        <input type="password" id="new_password" class="form-control" style="font-size: 1.2rem;" v-model="new_password">--}}
{{--                                        <div class="show-password small" style="top: 50%;">--}}
{{--                                            <i class="fa fa-eye"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p v-if="errors_editpass.new_password" class="text-danger font-weight-bold text-alert">--}}
{{--                                        @{{ errors_editpass.new_password[0] }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label>@lang('eventy/website_config.edit_pass_label_retype_pass')</label>--}}
{{--                                    <div class="position-relative d-flex">--}}
{{--                                        <input type="password" id="retype_password" class="form-control" style="font-size: 1.2rem;" v-model="retype_password">--}}
{{--                                        <div class="show-password small" style="top: 50%;">--}}
{{--                                            <i class="fa fa-eye"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p v-if="errors_editpass.retype_password" class="text-danger font-weight-bold text-alert">--}}
{{--                                        @{{ errors_editpass.retype_password[0] }}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <button @click="editPassword()" class="btn-edit-pass">--}}
{{--                                @lang('eventy/website_config.profile_btn_save_pass')--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="nav-item position-relative">
                    <button class="btn-logout" @click="logOut">@lang('eventy/website_config.profile_btn_logout')</button>
                </li>
            </ul>
        </div>
    </div>
</nav>
