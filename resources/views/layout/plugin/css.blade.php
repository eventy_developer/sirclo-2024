<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('packages/bootstrap5/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('packages/swiper/swiper-bundle.min.css') }}">
<link rel="stylesheet" href="{{ asset('packages/perfect-scrollbar/css/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('packages/modal-video/modal-video.min.css') }}">
<link rel="stylesheet" href="{{ asset('packages/croppie/croppie.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome/css/all.min.css') }}">
<link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css"
/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap4.min.css">
<!-- end plugin css -->

<link rel="stylesheet" href="{{ asset('css/web/global.css') }}">
<link rel="stylesheet" href="{{ asset('css/web/global-responsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/web/load.css') }}">
