<script>
    app_name = '{{ config("app.name") }}';
    url_base = '{{ url('/') }}';
    apiBaseUrl = '{{ url('/api/v1') }}';
    token = '{{ csrf_token() }}';
    event_channel = '{{ config("eventy.event_channel") }}';
    timezone_events = '';
    S3Endpoint = '';
    zoomUrl = '{{ url("zoom-meet") }}/';
    youtubeUrl = 'https://www.youtube.com/embed/';
    is_mobile = 'false';
    debug = '{{ config("app.debug") }}';
    ipAddress = "{{ request()->ip() }}";
    path = "{{ request()->path() }}";
    breakpoints = {
        sm: 576,
        md: 768,
        lg: 992,
        xlg: 1200,
    };

    entrat_id = '{{ gSession("id") }}';
    entrat_name = '{{ gSession("name") }}';
    entrat_image = '{{ gSession("present_image") }}';
    entrat_phone = '{{ gSession("phone") }}';
    entrat_email = '{{ gSession("email") }}';
    entrat_company = '{{ gSession("company") }}';
    entrat_is_filled_feedback = '{{ gSession("is_feedback_filled") }}';
    entrat_job_title = '';

    participant = {
        id: entrat_id,
        name: entrat_name,
        image: entrat_image,
        phone: entrat_phone,
        email: entrat_email,
        company: entrat_company,
        job_title: entrat_job_title
    };
</script>

<!-- plugin -->
<script src="{{ asset('packages/jquery/jquery-3.6.1.min.js') }}"></script>
<script src="{{ asset('packages/bootstrap5/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('packages/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('packages/sweetalert@2/sweetalert2@9.js') }}"></script>
<script src="{{ asset('packages/modal-video/modal-video.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.min.js"></script>
@if(config('app.debug'))
<script src="{{ asset('packages/vue@2/vue.js') }}"></script>
@else
<script src="{{ asset('packages/vue@2/vue.min.js') }}"></script>
@endif
<script src="{{ asset('packages/axios/axios.min.js') }}"></script>
<script src="{{ asset('packages/croppie/croppie.min.js') }}"></script>
<script src="{{ asset('packages/dayjs/dayjs-1.11.4.min.js') }}"></script>
<script src="{{ asset('packages/dayjs/plugin/utc-1.11.4.min.js') }}"></script>
<script src="{{ asset('packages/dayjs/plugin/timezone-1.11.4.min.js') }}"></script>
<script src="{{ asset('packages/dayjs/plugin/advancedFormat-1.11.4.min.js') }}"></script>
<script src="{{ asset('packages/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset(mix('js/laravel-echo-setup.js')) }}"></script>
<!-- end plugin -->

<script src="{{ asset('js/web/Template/plugin.js') }}"></script>
<script src="{{ asset('js/web/Template/functions.js') }}"></script>

<!-- Vue Mixins -->
<script src="{{ asset('js/web/Template/VueMixins/helpers.js') }}"></script>
<script src="{{ asset('js/web/Template/VueMixins/global.js') }}"></script>
<!-- End Vue Mixins -->

<!-- Vue Instance -->
<script src="{{ asset('js/web/Template/index.js') }}"></script>
<script src="{{ asset('js/web/Template/load.js') }}"></script>
<script src="{{ asset('js/web/Template/header.js') }}"></script>
<!-- End Vue Instance -->

<script>
    document.addEventListener('DOMContentLoaded', function () {
        new Swiper('.swiper-booth-categories', {
            slidesPerView: 1,
            spaceBetween: 20,

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // Pagination
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },

            breakpoints: {
                // when window width is >= 576px
                576: {
                    slidesPerView: 2
                },

                // when window width is >= 1200px
                1200: {
                    slidesPerView: 3
                }
            }
        });
    });
</script>
