<!DOCTYPE html>
<html lang="en" class="notranslate" translate="no">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google" content="notranslate">

    <link rel="shortcut icon" href="{{ preferGet('icon_events') }}">

    <title>@yield('page_title')</title>

    <style>
        :root {
            /* color */
            --eventy-color-primary: {{ preferGet('color_primary') }}!important;
            --eventy-color-secondary: {{ preferGet('color_secondary') }}!important;
            --eventy-color-primary-light: {{ adjustColorBrightness(preferGet('color_primary'), 0.2) }}!important;

            --eventy-font-family: 'Inter-Regular';
            --eventy-font-family-bold: 'Inter-Bold';
            --eventy-font-family-semibold: 'Inter-SemiBold';
            --eventy-font-family-medium: 'Inter-Medium';

            --header-height: 50px;
        }
        /*  */
    </style>

    @include('layout.plugin.css')

    @stack('head')

</head>
<body>

    <x-preloader/>

    @yield('content')

    @include('layout.plugin.js')

    @stack('bottom')

</body>
</html>
