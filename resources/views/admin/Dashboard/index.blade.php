@extends('crudbooster::admin_template')

@push('head')
    <style>
        .small-box {
            background-color: var(--base-color);
            color: #fff;
        }

        .small-box .icon {
            color: var(--base-color-dark);
        }

        .box-green {
            background-color: #07CB97;
            border-radius: 15px;
            padding: 30px 20px;
            border: 2px solid #07CB97;
            margin-bottom: 15px;
        }
        .box-green-outline {
            background-color: #fff;
        }
        .box-green-outline * {
            color: #07CB97;
        }

        .box-green .icon {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            background-color: #fff;
            display: flex;
            margin-left: auto;
            margin-bottom: 10px;
        }
        .box-green .icon > * {
            margin: auto;
            color: #07CB97;
            font-size: 28px;
        }
        .table-custom {
            max-height: 350px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header mb-2">
                        <h4>Total Participant Registered</h4>
                    </div>
                    <div class="card-body">
                        <h3 class="mb-0">{{ $total_register }}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header mb-2">
                        <h4>Total Participant Registered (Success)</h4>
                    </div>
                    <div class="card-body">
                        <h3 class="mb-0">{{ $total_register_success }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(preferGet('free_events') === 'no' && in_array(\crocodicstudio\crudbooster\helpers\CRUDBooster::myPrivilegeId(), [1,2]))
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title font-weight-bold" style="color: #34395e;">Ticket</h4>
                    </div>
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-md-6">
                                <div class="box-green box-green-outline py-3">
                                    <h4 class="card-title font-weight-bold mb-0"
                                        style="font-size: 40px;">{{ number_format($total_ticket_sales,0,'.','.') }}</h4>
                                    <p class="mb-0">Ticket Sold</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-green py-3">
                                    <p class="text-white mb-1">Total Income</p>
                                    <h4 class="card-title font-weight-bold text-white mb-2"
                                        style="font-size: 32px;">Rp{{ number_format($total_income,0,'.','.') }}</h4>
                                </div>
                            </div>
                            @if($total_withdrawal)
                                <div class="col-md-6">
                                    <div class="box-green py-3">
                                        <p class="text-white mb-1">Total Withdraw {{$total_withdrawal->description}}</p>
                                        <h4 class="card-title font-weight-bold text-white mb-2"
                                            style="font-size: 32px;">Rp{{ number_format($total_withdrawal->nominal,0,'.','.') }}</h4>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            @foreach($total_income_methods as $method)
                                <div class="{{ count($total_income_methods) > 3 ? 'col-md-3' : 'col-md-4' }}">
                                    <div class="box-green">
                                        <div class="icon">
                                            @if($method->type === 'Manual')
                                                <i class="fa fa-exchange-alt"></i>
                                            @elseif($method->type === 'eWallets')
                                                <img src="{{ asset('template/images/payment/img_ovo_logo.png') }}"
                                                     width="30">
                                            @elseif($method->type === 'QR Codes')
                                                <i class="fa fa-qrcode"></i>
                                            @elseif($method->type === 'Virtual Accounts')
                                                <i class="fab fa-google-wallet"></i>
                                            @else
                                                <i class="fa fa-credit-card"></i>
                                            @endif
                                        </div>
                                        <p class="card-category text-white mb-0">{{ $method->name }}</p>
                                        <h4 class="card-title font-weight-bold text-white mb-0"
                                            style="font-size: {{ count($total_income_methods) > 3 ? '20px' : '24px' }};">Rp{{ number_format($method->amount,0,'.','.') }}</h4>
                                        <hr>
                                        <p class="card-category text-white">Fee</p>
                                        <h5 class="card-title font-weight-bold"
                                            style="font-size: {{ count($total_income_methods) > 3 ? '18px' : '22px' }};color:#fff6cb;">
                                            -Rp{{ number_format($method->fee,0,'.','.') }}</h5>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row justify-content-center">
                        <div class="card-title">New Registration</div>
                    </div>
                </div>
                <div class="card-body">
                    <canvas id="statisticsChart" height="375"></canvas>
                    <div id="statisticsChartLegend"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="50%">Type</th>
                            <th width="50%" class="text-center">Total Participant</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($registers as $item)
                            <tr>
                                <td>{{ $item->type }}</td>
                                <td class="text-center">{{ $item->count }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Total Participants Check-in Registration Booth</h6>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="50%">Type</th>
                            <th width="25%" class="text-center">Day 1</th>
                            <th width="25%" class="text-center">Day 2</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalDay1 = 0;
                                $totalDay2 = 0;
                            @endphp
                            @foreach($registers as $item)
                            <tr>
                                <td>{{ $item->type }}</td>
                                <td class="text-center">{{ $item->day1 }}</td>
                                <td class="text-center">{{ $item->day2 }}</td>
                            </tr>
                            @php
                                $totalDay1 += $item->day1;
                                $totalDay2 += $item->day2;
                            @endphp
                            @endforeach
                            <tr class="table-active">
                                <td><strong>Total</strong></td>
                                <td class="text-center"><strong>{{ $totalDay1 }}</strong></td>
                                <td class="text-center"><strong>{{ $totalDay2 }}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if (in_array(\crocodicstudio\crudbooster\helpers\CRUDBooster::myPrivilegeId(), [1,2]))
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">

                        <!-- Example single danger button -->
                        <div class="d-flex justify-content-between">
                            <div class="btn-group mb-3">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Shows Top Point : {{ $top_point }}
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?top_point=20') }}">20</a></li>
                                    <li><a class="dropdown-item" href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?top_point=30') }}">30</a></li>
                                    <li><a class="dropdown-item" href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?top_point=50') }}">50</a></li>
                                </ul>
                            </div>

                            <div>
                                <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?top_point=' . g('top_point')) }}" class="btn btn-warning">Reload Page</a>
                            </div>
                        </div>

                        <div class="table-responsive table-custom">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th width="40%">Leaderboard</th>
                                        <th>Phone</th>
                                        <th width="15%" class="text-center">Point</th>
                                        <th width="15%" class="text-center">Eligible</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($participants as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td><a target="_blank" href="https://wa.me/{{ str_replace('-', '', $item->phone) }}">https://wa.me/{{ str_replace('-', '', $item->phone) }}</a></td>
                                        <td class="text-center">{{ $item->total_point }}</td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <button class="btn btn-sm btn-{{ $item->is_skip_doorprize === 0 ? 'primary' : 'danger' }} dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    {{ $item->is_skip_doorprize === 0 ? 'Eligible' : 'Skip' }}
                                                </button>
                                                <div class="dropdown-menu">
                                                    @if($item->is_skip_doorprize === 0)
                                                    <a class="dropdown-item" onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'This will make account skip rolling door prize.',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                window.location.href = '{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('eligible?is_skip_doorprize=1&id=' . $item->id) }}';
                                                            }
                                                        })" href="#">Skip</a>
                                                    @else
                                                    <a class="dropdown-item" onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'This will make account eligible for prize again.',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                            }).then((result) => {
                                                            if (result.isConfirmed) {
                                                            window.location.href = '{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('eligible?is_skip_doorprize=0&id=') . $item->id }}';
                                                        }
                                                        })" href="#">Eligible</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">

                        <div class="d-flex justify-content-between mb-3">
                            <h6 class="card-title align-self-center">Skipped Draw</h6>
                            <div>
                                <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?top_point=' . g('top_point')) }}" class="btn btn-warning">Reload Page</a>
                            </div>
                        </div>

                        <div class="table-responsive table-custom">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="40%">Name</th>
                                    <th>Phone</th>
                                    <th width="15%" class="text-center">Point</th>
                                    <th width="15%" class="text-center">Eligible</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($participants_skip as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td><a target="_blank" href="https://wa.me/{{ str_replace('-', '', $item->phone) }}">https://wa.me/{{ str_replace('-', '', $item->phone) }}</a></td>
                                        <td class="text-center">{{ $item->total_point }}</td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <button class="btn btn-sm btn-{{ $item->is_skip_doorprize === 0 ? 'primary' : 'danger' }} dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    {{ $item->is_skip_doorprize === 0 ? 'Eligible' : 'Skip' }}
                                                </button>
                                                <div class="dropdown-menu">
                                                    @if($item->is_skip_doorprize === 0)
                                                        <a class="dropdown-item" onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'This will make account skip rolling door prize.',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                            }).then((result) => {
                                                            if (result.isConfirmed) {
                                                            window.location.href = '{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('eligible?is_skip_doorprize=1&id=' . $item->id) }}';
                                                            }
                                                            })" href="#">Skip</a>
                                                    @else
                                                        <a class="dropdown-item" onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'This will make account eligible for prize again.',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                            }).then((result) => {
                                                            if (result.isConfirmed) {
                                                            window.location.href = '{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('eligible?is_skip_doorprize=0&id=') . $item->id }}';
                                                            }
                                                            })" href="#">Eligible</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif



    {{--  TODO: scan types  --}}
    {{--    <div class="row">--}}
    {{--        <div class="col-lg-12">--}}
    {{--            <div class="card">--}}
    {{--                <div class="card-body">--}}
    {{--                    <table class="table table-bordered">--}}
    {{--                        <thead>--}}
    {{--                        <tr>--}}
    {{--                            <th>Scan Type</th>--}}
    {{--                            <th>Total Scan</th>--}}
    {{--                        </tr>--}}
    {{--                        </thead>--}}
    {{--                        <tbody>--}}
    {{--                        @foreach($scan_types as $item)--}}
    {{--                            <tr>--}}
    {{--                                <td>{{ $item->name }}</td>--}}
    {{--                                <td>{{ $item->history_count }}</td>--}}
    {{--                            </tr>--}}
    {{--                        @endforeach--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

@endsection

@push('bottom')
    <script>
        let dataRegisterIntervalDateGeneral = {!! json_encode($dataRegisterIntervalDateGeneral) !!};
        let dataRegisterIntervalDateEntrepreneur = {!! json_encode($dataRegisterIntervalDateEntrepreneur) !!};
        let dataRegisterIntervalInvitation = {!! json_encode($dataRegisterIntervalInvitation) !!};
        let dataRegisterIntervalGroup = {!! json_encode($dataRegisterIntervalGroup) !!};
        let dates = dataRegisterIntervalDateGeneral.map(item => item.date);
        dataRegisterIntervalDateGeneral = dataRegisterIntervalDateGeneral.map(item => item.total);
        dataRegisterIntervalDateEntrepreneur = dataRegisterIntervalDateEntrepreneur.map(item => item.total);
        dataRegisterIntervalInvitation = dataRegisterIntervalInvitation.map(item => item.total);
        dataRegisterIntervalGroup = dataRegisterIntervalGroup.map(item => item.total);

        $(document).ready(function () {
            // Line Chart
            var ctx = document.getElementById('statisticsChart').getContext('2d');
            var statisticsChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [
                        {
                            label: "General Pass",
                            borderColor: '#9E6FD9',
                            pointBackgroundColor: 'rgba(158,111,217, 0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(158,111,217, 0.1)',
                            legendColor: '#9E6FD9',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalDateGeneral
                        }, {
                            label: "VIP",
                            borderColor: '#FF748D',
                            pointBackgroundColor: 'rgba(255, 116, 141, 0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(255, 116, 141, 0.1)',
                            legendColor: '#FF748D',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalDateEntrepreneur
                        },
                        {
                            label: "Invitation",
                            borderColor: '#1952bb',
                            pointBackgroundColor: 'rgba(116,155,255,0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(116,123,255,0.1)',
                            legendColor: '#748bff',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalInvitation
                        },
                        {
                            label: "Group",
                            borderColor: '#ff8974',
                            pointBackgroundColor: 'rgba(255,174,116,0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(255,165,116,0.1)',
                            legendColor: '#ffae74',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalGroup
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontSize: 14,
                            padding: 20
                        },
                    },
                    tooltips: {
                        bodySpacing: 4,
                        mode: "nearest",
                        intersect: 0,
                        position: "nearest",
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    layout: {
                        padding: {left: 15, right: 15, top: 15, bottom: 15}
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: "rgba(0,0,0,0.5)",
                                fontStyle: "500",
                                beginAtZero: false,
                                maxTicksLimit: 5,
                                padding: 20
                            },
                            gridLines: {
                                drawTicks: false,
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                zeroLineColor: "transparent"
                            },
                            ticks: {
                                padding: 20,
                                fontColor: "rgba(0,0,0,0.5)",
                                fontStyle: "500"
                            }
                        }]
                    },
                }
            });
        });
    </script>
@endpush
