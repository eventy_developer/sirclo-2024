@extends('crudbooster::admin_template')

@push('head')
    <style>
        .small-box {
            background-color: var(--base-color);
            color: #fff;
        }

        .small-box .icon {
            color: var(--base-color-dark);
        }

        .box-green {
            background-color: #07CB97;
            border-radius: 15px;
            padding: 30px 20px;
            border: 2px solid #07CB97;
            margin-bottom: 15px;
        }
        .box-green-outline {
            background-color: #fff;
        }
        .box-green-outline * {
            color: #07CB97;
        }

        .box-green .icon {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            background-color: #fff;
            display: flex;
            margin-left: auto;
            margin-bottom: 10px;
        }
        .box-green .icon > * {
            margin: auto;
            color: #07CB97;
            font-size: 28px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header mb-2">
                        <h4>Total Participant Registered</h4>
                    </div>
                    <div class="card-body">
                        <h3 class="mb-0">{{ $total_register }}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="far fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header mb-2">
                        <h4>Total Participant Registered (Success)</h4>
                    </div>
                    <div class="card-body">
                        <h3 class="mb-0">{{ $total_register_success }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-head-row justify-content-center">
                        <div class="card-title">Participant Statistics</div>
                    </div>
                </div>
                <div class="card-body">
                    <canvas id="statisticsChart" height="375"></canvas>
                    <div id="statisticsChartLegend"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('bottom')
    <script>
        let dataRegisterIntervalDateGeneral = {!! json_encode($dataRegisterIntervalDateGeneral) !!};
        let dataRegisterIntervalDateEntrepreneur = {!! json_encode($dataRegisterIntervalDateEntrepreneur) !!};
        let dates = dataRegisterIntervalDateGeneral.map(item => item.date);
        dataRegisterIntervalDateGeneral = dataRegisterIntervalDateGeneral.map(item => item.total);
        dataRegisterIntervalDateEntrepreneur = dataRegisterIntervalDateEntrepreneur.map(item => item.total);

        $(document).ready(function () {
            // Line Chart
            var ctx = document.getElementById('statisticsChart').getContext('2d');
            var statisticsChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [
                        {
                            label: "General Pass",
                            borderColor: '#9E6FD9',
                            pointBackgroundColor: 'rgba(158,111,217, 0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(158,111,217, 0.1)',
                            legendColor: '#9E6FD9',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalDateGeneral
                        }, {
                            label: "Entrepreneur Pass",
                            borderColor: '#FF748D',
                            pointBackgroundColor: 'rgba(255, 116, 141, 0.2)',
                            pointRadius: 0,
                            backgroundColor: 'rgba(255, 116, 141, 0.1)',
                            legendColor: '#FF748D',
                            fill: true,
                            borderWidth: 3,
                            data: dataRegisterIntervalDateEntrepreneur
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontSize: 14,
                            padding: 20
                        },
                    },
                    tooltips: {
                        bodySpacing: 4,
                        mode: "nearest",
                        intersect: 0,
                        position: "nearest",
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    layout: {
                        padding: {left: 15, right: 15, top: 15, bottom: 15}
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: "rgba(0,0,0,0.5)",
                                fontStyle: "500",
                                beginAtZero: false,
                                maxTicksLimit: 5,
                                padding: 20
                            },
                            gridLines: {
                                drawTicks: false,
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                zeroLineColor: "transparent"
                            },
                            ticks: {
                                padding: 20,
                                fontColor: "rgba(0,0,0,0.5)",
                                fontStyle: "500"
                            }
                        }]
                    },
                }
            });
        });
    </script>
@endpush
