@extends('admin.Backoffice.template.index')

@section('content')
<div class="main-panel" id="boothAdminApp">

    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 stretch-card">
                    <h3 class="card-title d-flex">
                        <a class="icon-back" href="{{ CRUDBooster::mainpath('product') }}">
                            <i class="fa fa-caret-left"></i>
                        </a> &nbsp;&nbsp;&nbsp; {{ $page_title }}

                        <a href="{{ CRUDBooster::mainpath('banner-add/' . request()->segment(3)) }}" id="btn_add_new_data"
                        class="btn btn-sm ml-3 btn-add-data" title="Add Data">
                            <i class="fa fa-plus-circle"></i> Add Data
                        </a>
                    </h3>
                </div>
            </div>
            <div class="row grid-margin">
                <div class="col-12 stretch-card">
                    <div class="card w-100 overflow-auto">
                        <div class="card-body">
                            <div class="dropdown mb-4">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Bulk Actions <i class="fa fa-caret-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item btn-action-deleted" href="javascript:void(0)">
                                        <i class="fa fa-trash"></i> Delete Selected</a>
                                    </a>
                                </div>
                            </div>
                            <form action="{{ action('BackOffice\BackOfficeController@postActionSelected', [
                                    'table' => 'booth_product_banners'
                                ]) }}"
                                method="POST" id="form-table">
                                @csrf
                                <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" id="checkall">
                                            </th>
                                            <th>Type</th>
                                            <th>Image</th>
                                            <th>Youtube Url</th>
                                            <th>Order</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($items as $item)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="checkbox"
                                                    name="checkbox[]" value="{{ $item->id }}">
                                            </td>
                                            <td>{{ $item->type }}</td>
                                            <td>
                                                <a href="{{ $item->image }}" data-lightbox="roadtrip" title="{{ $item->name }}">
                                                    <img src="{{ $item->image }}" alt="Image" width="50">
                                                </a>
                                            </td>
                                            <td>{{ $item->youtube_url }}</td>
                                            <td>{{ $item->sort }}</td>
                                            <td>
                                                <div class="button_action" style="text-align:right">
                                                    <a class="btn btn-xs btn-info" title="" target="_self"
                                                        href="{{ CRUDBooster::mainpath('banner-move-up/' . $item->id) }}">
                                                        <i class="fa fa-arrow-up"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-info" title="" target="_self"
                                                        href="{{ CRUDBooster::mainpath('banner-move-down/' . $item->id) }}">
                                                        <i class="fa fa-arrow-down"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-primary btn-detail" title="Detail Data"
                                                        href="{{ CRUDBooster::mainpath('banner-detail/' . request()->segment(3) . '/' . $item->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-success btn-edit" title="Edit Data"
                                                        href="{{ CRUDBooster::mainpath('banner-edit/' . request()->segment(3) . '/' .$item->id) }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-danger btn-delete" title="Delete" href="javascript:;"
                                                        onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'You will not be able to recover this record data!',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                window.location.href = '{{ action('BackOffice\BackOfficeController@getBannerDelete', ['id' => $item->id]) }}'
                                                            }
                                                        });">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Type</th>
                                            <th>Image</th>
                                            <th>Youtube Url</th>
                                            <th>Sort</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
