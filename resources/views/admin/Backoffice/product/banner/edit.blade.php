@extends('admin.Backoffice.template.index')

@section('content')
<form action="{{ CRUDBooster::mainpath('banner-edit') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="booth_product_id" value="{{ request()->segment(3) }}">
    <input type="hidden" name="id" value="{{ request()->segment(4) }}">
    <div class="content-center">
        <div class="row">
            <div class="col-12 stretch-card">
                <h3 class="card-title d-flex">
                    <a class="icon-back" href="{{ CRUDBooster::mainpath('banner/' . request()->segment(3)) }}">
                        <i class="fa fa-caret-left"></i>
                    </a> &nbsp;&nbsp;&nbsp; Edit Banner
                </h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="form-control-label">
                                Type
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <select name="type" id="type" required class="form-control input-custom">
                                <option value="">* Please Select a Type *</option>
                                <option value="Image" {{ $row->getType() === 'Image' ? 'selected' : '' }} selected>Image</option>
                                <option value="Youtube" {{ $row->getType() === 'Youtube' ? 'selected' : '' }}>Youtube</option>
                            </select>
                            <div class="invalid-feedback">
                                {{ $errors->first('type') }}
                            </div>
                        </div>
                        <div class="form-group" id="form-group-image">
                            <label for="image" class="form-control-label">
                                Image
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="d-flex">
                                <div class="image-landscape-box">
                                    <input type="file" name="image" id="image"
                                        accept="image/*" ref="image" required
                                        class="form-control inputfile dropify"
                                        data-default-file="{{ $row->getImage() }}"
                                        data-max-file-size="400K"
                                        data-allowed-file-extensions="jpg jpeg png"
                                        data-delete-url=""
                                        data-allowed-formats="landscape"
                                        data-height="300">
                                </div>

                                <p class="text-help">
                                    <em>* Max size 400KB</em><br>
                                    <em>&nbsp; Landscape dimensions with ratio 16:9</em>
                                </p>
                            </div>
                        </div>
                        <div class="form-group" id="form-group-youtube_url">
                            <label for="name" class="form-control-label">
                                Youtube Url
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="youtube_url" id="youtube_url" minlength="10" maxlength="50"
                                class="form-control input-custom {{ $errors->first('youtube_url') ? 'is-invalid' : '' }}"
                                required value="{{ $row->getYoutubeUrl() }}"
                                placeholder="Youtube Url">
                            <div class="invalid-feedback">
                                {{ $errors->first('youtube_url') }}
                            </div>
                            <div class="text-help">
                                Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, yang di input adalah Fd17fEB-9kk
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 50px;">
            <a href="{{ CRUDBooster::mainpath('product') }}" class="btn btn-save btn-save-outline mb-3">Back</a>
            <button type="submit" class="btn btn-save mb-3 ml-2">Save</button> <br>
        </div>
    </div>
</form>
@push('js')
<script>
    $(document).ready(function() {
        let type = $('#type').val();

        if (type === 'Youtube') {
            $('#form-group-youtube_url').show();
            $('#youtube_url').prop('required', true);

            $('#form-group-image').hide();
            $('#image').prop('required', false);
        } else {
            $('#form-group-image').show();
            $('#image').prop('required', true);

            $('#form-group-youtube_url').hide();
            $('#youtube_url').prop('required', false);
        }

        $('#type').on('change', function(){
            console.log(this.value);
            if (this.value === 'Youtube') {
                $('#form-group-youtube_url').show();
                $('#youtube_url').prop('required', true);

                $('#form-group-image').hide();
                $('#image').prop('required', false);
            } else {
                $('#form-group-image').show();
                $('#image').prop('required', true);

                $('#form-group-youtube_url').hide();
                $('#youtube_url').prop('required', false);
            }
        });
    });
</script>
@endpush
@endsection
