@extends('admin.Backoffice.template.index')

@section('content')
<form action="{{ CRUDBooster::mainpath('') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{ $row->getId() }}">
    <div class="content-center">
        <div class="row">
            <div class="col-12 stretch-card">
                <h3 class="card-title d-flex">
                    <a class="icon-back" href="{{ CRUDBooster::mainpath('banner/' . request()->segment(3)) }}">
                        <i class="fa fa-caret-left"></i>
                    </a> &nbsp;&nbsp;&nbsp; Detail Product
                </h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body overflow-auto">
                <div class="row">
                    <div class="col-md-12">
                        
                        <table class="table table-striped table-bordered table-detail" style="width:100%">
                            <tr>
                                <td>Type</td>
                                <td>{{ $row->getType() }}</td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td>
                                    <a data-lightbox="roadtrip" 
                                        href="{{ $row->getImage() }}">
                                        <img style="max-width: 160px;" title="Image" 
                                        src="{{ $row->getImage() }}">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Youtube Url</td>
                                <td>{{ $row->getYoutubeUrl() }}</td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection