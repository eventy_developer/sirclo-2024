@extends('admin.Backoffice.template.index')

@section('content')
<div class="modal fade" id="modalEcommerce" tabindex="-1" role="dialog"
    aria-labelledby="modalEcommerceLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background-color: #fff;">
            <form action="{{ action('BackOffice\BackOfficeController@postEcommerceEdit') }}" method="POST">
                @csrf
                <input type="hidden" name="booth_product_id" id="booth_product_id_hidden">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-pencil-alt"></i> Edit Marketplace Link
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="form-group-tokopedia">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="tokopedia">Tokopedia Link</label> &nbsp;
                                    <img src="{{ asset('template/images/ecommerce/icon_tokopedia.png') }}" alt="Icon"/>
                                </div>
                                <div class="col-lg-8">
                                    <input type="url" class="form-control" name="types[Tokopedia]" id="tokopedia"
                                        autocomplete="off" minlength="3" maxlength="50">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-shopee">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="shopee">Shopee Link</label> &nbsp;
                                    <img src="{{ asset('template/images/ecommerce/icon_shopee.png') }}" alt="Icon"/>
                                </div>
                                <div class="col-lg-8">
                                    <input type="url" class="form-control" name="types[Shopee]" id="shopee"
                                        autocomplete="off" minlength="3" maxlength="50">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-general">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="general">General Link (Other platform)</label> &nbsp;
                                    {{-- <img src="{{ asset('template/images/ecommerce/icon_general.png') }}" alt="Icon"/> --}}
                                </div>
                                <div class="col-lg-8">
                                    <input type="url" class="form-control" name="types[General]" id="general"
                                        autocomplete="off" minlength="3" maxlength="50">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('eventy/backoffice.text_btn_save_changes')</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('eventy/backoffice.text_btn_close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="main-panel" id="boothAdminApp">

    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 stretch-card">
                    <h3 class="card-title d-flex">
                        <a class="icon-back" href="{{ CRUDBooster::mainpath() }}">
                            <i class="fa fa-caret-left"></i>
                        </a> &nbsp;&nbsp;&nbsp; {{ $page_title }}

                        <a href="{{ CRUDBooster::mainpath('product-add') }}" id="btn_add_new_data"
                        class="btn btn-sm ml-3 btn-add-data" title="Add Data">
                            <i class="fa fa-plus-circle"></i> Add Data
                        </a>
                    </h3>
                </div>
            </div>
            <div class="row grid-margin">
                <div class="col-12 stretch-card">
                    <div class="card w-100 overflow-auto">
                        <div class="card-body">
                            <div class="dropdown mb-4">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Bulk Actions <i class="fa fa-caret-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item btn-action-deleted" href="javascript:void(0)">
                                        <i class="fa fa-trash"></i> Delete Selected</a>
                                    </a>
                                </div>
                            </div>
                            <form action="{{ action('BackOffice\BackOfficeController@postActionSelected', [
                                    'table' => 'booth_product'
                                ]) }}"
                                method="POST" id="form-table">
                                @csrf
                                <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" id="checkall">
                                            </th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                            <th>Order</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($items as $item)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="checkbox"
                                                    name="checkbox[]" value="{{ $item->id }}">
                                            </td>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                <a href="{{ $item->image }}" data-lightbox="roadtrip" title="{{ $item->name }}">
                                                    <img src="{{ $item->image }}" alt="Image" width="50">
                                                </a>
                                            </td>
                                            <td>{{ $item->description_summary }}</td>
                                            <td>Rp{{ $item->price }}</td>
                                            <td>{{ $item->sort }}</td>
                                            <td>
                                                <div class="button_action" style="text-align:right">
                                                    <a class="btn btn-xs btn-info" title="" target="_self"
                                                       href="{{ CRUDBooster::mainpath('product-move-up/' . $item->id) }}">
                                                        <i class="fa fa-arrow-up"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-info" title="" target="_self"
                                                       href="{{ CRUDBooster::mainpath('product-move-down/' . $item->id) }}">
                                                        <i class="fa fa-arrow-down"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-primary btn-detail" title="Detail Data"
                                                        href="{{ CRUDBooster::mainpath('banner/' . $item->id) }}">
                                                        <i class="fa fa-image"></i> &nbsp; Banners
                                                    </a>
                                                    <a class="btn btn-xs btn-success btn-ecommerce" title="Detail Data" href="javascript:void(0)"
                                                        data-toggle="modal" data-target="#modalEcommerce" data-product-id="{{ $item->id }}"
                                                        data-tokopedia="{{ $item->tokopedia->getExternalLink() }}"
                                                        data-shopee="{{ $item->shopee->getExternalLink() }}"
                                                        data-general="{{ $item->general->getExternalLink() }}">
                                                        <i class="fa fa-external-link-alt"></i> &nbsp; Marketplace
                                                    </a>
                                                    <a class="btn btn-xs btn-primary btn-detail" title="Detail Data"
                                                        href="{{ CRUDBooster::mainpath('product-detail/' . $item->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-success btn-edit" title="Edit Data"
                                                        href="{{ CRUDBooster::mainpath('product-edit/' . $item->id) }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>

                                                    <a class="btn btn-xs btn-danger btn-delete" title="Delete" href="javascript:;"
                                                        onclick="Swal.fire({
                                                            title: 'Are you sure ?',
                                                            text: 'You will not be able to recover this record data!',
                                                            icon: 'warning',
                                                            showCancelButton: true,
                                                            confirmButtonColor: '#3085d6',
                                                            cancelButtonColor: '#d33',
                                                            confirmButtonText: 'Yes!'
                                                        }).then((result) => {
                                                            if (result.isConfirmed) {
                                                                window.location.href = '{{ action('BackOffice\BackOfficeController@getProductDelete', ['id' => $item->id]) }}'
                                                            }
                                                        });">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function() {
        $('.btn-ecommerce').on('click', function() {
            let productId = $(this).data('product-id'),
                tokopedia = $(this).data('tokopedia'),
                shopee = $(this).data('shopee')
                general = $(this).data('general');
            $('#booth_product_id_hidden').val(productId);
            $('#tokopedia').val(tokopedia);
            $('#shopee').val(shopee);
            $('#general').val(general);
        });
    });
</script>
@endpush
@endsection
