@extends('admin.Backoffice.template.index')

@section('content')
<form action="{{ CRUDBooster::mainpath('product-edit') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{ $product->getId() }}">
    <div class="content-center">
        <div class="row">
            <div class="col-12 stretch-card">
                <h3 class="card-title d-flex">
                    <a class="icon-back" href="{{ CRUDBooster::mainpath('product') }}">
                        <i class="fa fa-caret-left"></i>
                    </a> &nbsp;&nbsp;&nbsp; Edit Product
                </h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="image" class="form-control-label">Image (optional)</label>
                            <div class="d-flex">
                                <div class="image-square-box">
                                    <input type="file" name="image" id="image"
                                        accept="image/*" ref="image"
                                        class="form-control inputfile dropify"
                                        data-default-file="{{ $product->getImage() }}"
                                        data-max-file-size="400K"
                                        data-allowed-file-extensions="jpg jpeg png"
                                        data-delete-url=""
                                        data-allowed-formats="square">
                                </div>

                                <p class="text-help">
                                    <em>* Max size 400KB</em><br>
                                    <em>&nbsp; Square dimentions</em>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="form-control-label">
                                Name
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="name" id="name" minlength="5" maxlength="50"
                                class="form-control input-custom {{ $errors->first('name') ? 'is-invalid' : '' }}"
                                required value="{{ $product->getName() }}"
                                placeholder="Name">
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="form-control-label">
                                Description
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <textarea type="text" name="description" id="description"
                            minlength="5" maxlength="1000" class="form-control input-custom {{ $errors->first('description') ? 'is-invalid' : '' }}"
                            required style="min-width: 100%;max-width: 100%;"
                            rows="8" placeholder="Description">{{ $product->getDescription() }}</textarea>
                            <div class="invalid-feedback">
                                <i class="fa fa-info-circle"></i> {{ $errors->first('description') }}
                            </div>
                            <p class="text-help">
                                <em>* Max length 1000 character</em><br>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-control-label">
                                Price (IDR)
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="price" id="price" maxlength="11" min="1"
                                class="form-control input-custom inputMoney" placeholder="Price (IDR)" required
                                value="{{ $product->getPrice() }}">
                        </div>

                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 50px;">
            <a href="{{ CRUDBooster::mainpath('product') }}" class="btn btn-save btn-save-outline mb-3">Back</a>
            <button type="submit" class="btn btn-save mb-3 ml-2">Save</button> <br>
        </div>
    </div>
</form>
@endsection
