@extends('admin.Backoffice.template.index')

@section('content')
    @include('admin.Backoffice.template.modal')

    <div class="row">
        <div class="col-lg-12">
            <div class="dashboard-info">
                <div class="dashboard-cog">
                    <i class="fa fa-cog"></i>
                </div>
                <div class="ms-lg-4">
                    <h2 class="dashboard-title">Welcome Back in Dashboard {{ $booth->name }}</h2>
                    <p class="dashboard-caption">Everything you need is available on this page</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-lg-8">
            <div class="display mb-5">
                <div class="display__information">
                    <div class="icon-group-default">
                        <img src="{{ asset('images/icon/backoffice-booth/ic_display_booth.svg') }}" alt="Icon Display Booth">
                    </div>
                    <div class="ms-3">
                        <h5 class="display__information__title">Display Virtual Booth</h5>
                        <p class="display__information__desc mb-0">Click button (+) on booth to upload & change attachment</p>
                    </div>
                </div>
                <div class="display__preview">
                    <div class="display__preview__btn-group">
                        <a href="" class="display__preview__btn">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_preview_booth.svg') }}" alt=""> <span class="ms-3">Preview Booth</span>
                        </a>
                        <a href="" class="display__preview__btn">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_size_detail.svg') }}" alt=""> <span class="ms-3">Size Detail</span>
                        </a>
                    </div>
                </div>
            </div>

            <a href="{{ $booth->present_image }}"
               data-lightbox="{{ $booth->present_image }}"
               data-title="{{ $booth->name }}">
                <img src="{{ $booth->present_image }}" alt="Booth Stand" class="img-booth-stand">
            </a>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-content-center mb-4">
                        <div class="icon-group-default">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_detail_information.svg') }}" alt="Icon Information">
                        </div>
                        <div class="ms-3">
                            <h5 class="display__information__title">Detail Information Booth</h5>
                            <p class="display__information__desc mb-0">Manage your information Booth</p>
                        </div>
                    </div>
                    <div class="mb-4">
                        <div class="mb-4">
                            <h5 class="placeholder-form__label">@lang('eventy/backoffice.text_information_name') <span>*</span></h5>
                            <div class="placeholder-form__inner">
                                <p>{{ $booth->name }}</p>
                            </div>
                        </div>
                        <div class="mb-3">
                            <h5 class="placeholder-form__label">@lang('eventy/backoffice.text_description_title') <span>*</span></h5>
                            <div class="placeholder-form__inner placeholder-form__inner--description">
                                <p>{!! str_limit(strip_tags($booth->description), 600) !!}</p>
                            </div>
                        </div>
                        <span class="placeholder-form__alert">*Can add image on description</span>
                    </div>


                    <div class="d-flex">
                        <button type="button" class="btn-edit-desc ms-auto" data-bs-toggle="modal"
                                data-bs-target="#modalEdit">
                            <i class="fa fa-pencil-alt"></i> &nbsp;@lang('eventy/backoffice.text_btn_edit')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex card-item-list align-items-start">
                        <div class="icon-group-default icon-group-default--visitor">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_total_visitor.svg') }}" alt="Icon Total Visitor">
                        </div>
                        <div class="ms-lg-5">
                            <p class="visitor__title-text">@lang('eventy/backoffice.text_total_visitor')</p>
                            <div class="text-caption">
                                <span class="total-text" v-cloak>@{{ total_visitor }}</span>
                                <span class="line-text">@lang('eventy/backoffice.text_attendance')</span>
                            </div>

                            <a href="{{ CRUDBooster::mainpath('export-booth-visitor') }}"
                               id="btn_export_data"
                               data-url-parameter="" title="Export Data"
                               class="btn btn-sm btn-primary btn-export-data">
                                <img src="{{ asset('images/icon/backoffice-booth/ic_export_data.svg') }}" alt="Icon Export Data"> Export Data
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-content-center mb-5">
                        <div class="icon-group-default">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_my_product.svg') }}" alt="Icon Product">
                        </div>
                        <div class="ms-3">
                            <h5 class="display__information__title">@lang('eventy/backoffice.my_product_title')</h5>
                            <p class="display__information__desc mb-0">@lang('eventy/backoffice.my_product_text_manage')</p>
                        </div>
                    </div>

                    <div class="card-list">
                        <h5 class="card-list__title mb-4">@lang('eventy/backoffice.my_product_text_list')</h5>
                        <ul class="card-list__item">
                            <li class="card-list__detail"
                                v-for="(item, i) in products">
                                <div class="icon-group-default icon-group-default--item icon-group-default--product">
                                    <img src="{{ asset('images/placeholder/ph_product@2x.png') }}" alt="Icon Attachment"
                                        class="w-100 img-fluid" style="object-fit: contain;">
                                </div>
                                <div class="card-list__detail__info">
                                    <h5 class="card-list__detail__info__title" v-cloak>@{{ item.name }}</h5>
                                    <h5 class="card-list__detail__info__cat" v-cloak>@{{ item.category.name }}</h5>
                                    <h5 class="card-list__detail__info__subtitle" v-cloak>@{{ item.present_price }}</h5>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a class="btn-manage" title="@lang('eventy/backoffice.my_product_text_button')"
                        href="{{ CRUDBooster::mainpath('product') }}">
                        <i class="fa fa-tags"></i> &nbsp; @lang('eventy/backoffice.my_product_text_button')
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card mb-lg-0">
                <div class="card-body card-body--attachment">
                    <div class="d-flex align-content-center mb-5">
                        <div class="icon-group-default">
                            <img src="{{ asset('images/icon/backoffice-booth/ic_attachment_booth.svg') }}" alt="Icon Attachment">
                        </div>
                        <div class="ms-3">
                            <h5 class="display__information__title">@lang('eventy/backoffice.attachment_title')</h5>
                            <p class="display__information__desc mb-0">@lang('eventy/backoffice.attachment_text_manage')</p>
                        </div>
                    </div>

                    <div class="card-list">
                        <h5 class="card-list__title mb-4">@lang('eventy/backoffice.attachment_text_list')</h5>
                        <ul class="card-list__item">
                            <li class="card-list__detail"
                                v-for="(item, i) in attachments">
                                <div class="icon-group-default icon-group-default--item">
                                    <img src="{{ asset('images/icon/backoffice-booth/ic_image_attach.svg') }}" alt="Icon Attachment"
                                         v-if="item.type === 'Image'">
                                    <img src="{{ asset('images/icon/backoffice-booth/ic_docs_attach.svg') }}" alt="Icon Attachment"
                                         v-else-if="item.type === 'Document'">
                                    <div style="cursor: pointer" :data-video-id="item.video_url" class="play-modal" v-else>
                                        <img src="{{ asset('images/icon/backoffice-booth/ic_video_attach.svg') }}" alt="Icon Attachment">
                                    </div>
                                </div>
                                <div class="card-list__detail__info">
                                    <h5 class="card-list__detail__info__title" v-cloak>@{{ item.file_name }}</h5>
                                    <h5 class="card-list__detail__info__subtitle" v-cloak v-if="item.type !== 'Video'">@{{ item.file_size }}</h5>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <button class="btn-manage" data-bs-toggle="modal"
                            data-bs-target="#modalUpload" title="@lang('eventy/backoffice.attachment_text_upload')">
                        <i class="fa fa-upload"></i> &nbsp; @lang('eventy/backoffice.attachment_text_upload')
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
