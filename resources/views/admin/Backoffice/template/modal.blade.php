<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background-color: #fff;">
            <form action="{{ url('update-description') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-pencil-alt"></i> @lang('eventy/backoffice.text_modal_title_description')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <textarea id="textarea_description" required name="description" class="form-control" rows="5"
                        style="display: none;">{!! $booth->description !!}</textarea>
                    <input type="hidden" name="booth_id" value="{{ $booth->id }}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('eventy/backoffice.text_btn_save_changes')</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">@lang('eventy/backoffice.text_btn_close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background-color: #fff;">
            <form action="{{ url('upload-attachment') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-upload"></i> @lang('eventy/backoffice.text_modal_title_upload')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                <label for="type">Type <span class="text-danger"
                                        title="This field is required">*</span></label>
                            </div>
                            <div class="col-lg-8">
                                <select name="type" class="form-control" id="type">
                                    <option value="document">Document</option>
                                    <option value="image">Image</option>
                                    <option value="video">Video</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-file">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="file">File <span class="text-danger"
                                            title="This field is required">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="file" name="file" id="file" class="form-control"
                                    accept=".csv, application/msword,
                                        application/vnd.ms-excel,
                                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,
                                        application/vnd.ms-powerpoint,
                                        application/mspowerpoint,
                                        application/powerpoint,
                                        application/x-mspowerpoint,
                                        application/vnd.openxmlformats-officedocument.presentationml.presentation,
                                        application/vnd.openxmlformats-officedocument.presentationml.slideshow,
                                        .ppt, .pptx,
                                        application/pdf">
                                    <p class="help-block">@lang('eventy/backoffice.text_modal_help_file_types'): pdf, xls, xlsx, csv, docx, pdf, ppt, pptx with maximum size 2MB
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-image">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="file">Image <span class="text-danger"
                                            title="This image is required">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="file" name="image" id="image" class="form-control"
                                        accept="image/*">
                                    <p class="help-block">@lang('eventy/backoffice.text_modal_help_file_types'): jpg, jpeg, png with maximum size 1MB</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-video_name">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="video_name">Video Short Name <span class="text-danger"
                                            title="This field is required">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="video_name" id="video_name"
                                        autocomplete="off" minlength="3" maxlength="50">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="form-group-video_url">
                        <div class="row">
                            <div class="input-group">
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <label for="video_url">Video Url <span class="text-danger"
                                            title="This field is required">*</span></label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="video_url" id="video_url"
                                        autocomplete="off" minlength="10" maxlength="15">
                                    <p class="help-block">Contoh: https://www.youtube.com/watch?v=Fd17fEB-9kk, maka yang di input adalah Fd17fEB-9kk</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="booth_id" value="{{ $booth->id }}">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('eventy/backoffice.text_btn_submit')</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">@lang('eventy/backoffice.text_btn_close')</button>
                </div>
            </form>
        </div>
    </div>
</div>
