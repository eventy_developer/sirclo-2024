<div class="theme-setting-wrapper" id="rootProfile">
    <div id="settings-trigger"></div>
    <div id="theme-settings" class="settings-panel">

        <div class="square-box-loader" id="loading-setting" style="display:none;pointer-events: none;">
            <div class="square-box-loader-container">
                <div class="square-box-loader-corner-top"></div>
                <div class="square-box-loader-corner-bottom"></div>
            </div>
            <div class="square-box-loader-square"></div>
        </div>

        <div id="side-profil">
            <b class="settings-close">&times;</b>

            <p class="settings-heading">@lang('eventy/website_config.title_profile')</p>
            <div class="profil-heading">
                <div class="img-heading">
                    <input type="file" id="imgHeading" class="upload-heading" @change="previewImage" accept="image/*" />
                    <img :src="profile_image" alt="img Heading" id="show_imgHeading">
                    <span class="fa fa-camera"></span>
                </div>
                <p v-if="errors_upload.image" class="text-danger font-weight-bold text-alert">
                    @{{ errors_upload.image[0] }}</p>
                <p class="name">@{{ profile_name }}</p>
                <p class="design">Eventy</p>

                <div class="p-profil">
                    <span class="title">@lang('eventy/website_config.profile_text_username')</span>
                    <span class="caption">@{{ profile_email }}</span>
                </div>
            </div>
            <p class="settings-heading border-top">@lang('eventy/website_config.profile_text_account_sett')</p>
            <ul class="menu-heading">
                <li>
                    <a href="javascript:void(0)" @click="showEditPassword"><span class="fa fa-lock"></span>
                        &nbsp;@lang('eventy/website_config.profile_text_edit_pass')</a>
                </li>
            </ul>
            <div class="profil-heading">
                <button class="btn-logout" @click="logOut">@lang('eventy/website_config.profile_btn_logout')</button>
            </div>
            <div class="profil-footer">{{ $event_name }}</div>
        </div>
        <div id="side-password">
            <div class="wrapper d-flex align-items-center py-2 ml-5 mt-4">
                <div class="wrapper">
                    <a href="javascript:void(0)" @click="showProfil" class="btn-side-back">
                        <span class="fa fa-arrow-left"></span>
                    </a>
                </div>
                <h4 class="card-title mb-0 ml-3">@lang('eventy/website_config.title_edit_pass')</h4>
                </h4>
            </div>

            <div class="form-side">
                <div class="form-group mb-4">
                    <label>@lang('eventy/website_config.edit_pass_label_current_pass')</label>
                    <div class="position-relative d-flex">
                        <input type="password" id="current_password" class="form-control"
                               placeholder="@lang('eventy/website_config.edit_pass_placeholder_current_pass')"
                               v-model="current_password">
                        <div class="show-password">
                            <i class="fa fa-eye"></i>
                        </div>
                    </div>
                    <p v-if="errors_editpass.current_password" class="text-danger font-weight-bold text-alert">
                        @{{ errors_editpass.current_password[0] }}</p>
                </div>
                <div class="form-group mb-4">
                    <label>@lang('eventy/website_config.edit_pass_label_new_pass')</label>
                    <div class="position-relative d-flex">
                        <input type="password" id="new_password" class="form-control"
                               placeholder="@lang('eventy/website_config.edit_pass_placeholder_new_pass')"
                               v-model="new_password">
                        <div class="show-password">
                            <i class="fa fa-eye"></i>
                        </div>
                    </div>
                    <p v-if="errors_editpass.new_password" class="text-danger font-weight-bold text-alert">
                        @{{ errors_editpass.new_password[0] }}</p>
                </div>
                <div class="form-group">
                    <label>@lang('eventy/website_config.edit_pass_label_retype_pass')</label>
                    <div class="position-relative d-flex">
                        <input type="password" id="retype_password" class="form-control"
                               placeholder="@lang('eventy/website_config.edit_pass_placeholder_retype_pass')"
                               v-model="retype_password">
                        <div class="show-password">
                            <i class="fa fa-eye"></i>
                        </div>
                    </div>
                    <p v-if="errors_editpass.retype_password" class="text-danger font-weight-bold text-alert">
                        @{{ errors_editpass.retype_password[0] }}</p>
                </div>
            </div>
            <div class="profil-heading">
                <button @click="editPassword()"
                    class="btn-edit-pass">@lang('eventy/website_config.profile_btn_save_pass')</button>
            </div>
        </div>

    </div>
</div>
