<!DOCTYPE html>
<html lang="en" class="notranslate" translate="no">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google" content="notranslate">
    <link rel="shortcut icon" href="{{ preferGet('icon_events') }}">

    <style>
        :root {
            /* color */
            --eventy-color-primary: {{ preferGet('color_primary') }}!important;
            --eventy-color-primary-light: {{ adjustColorBrightness(preferGet('color_primary'), 0.2) }}!important;
            --eventy-color-primary-dark: {{ adjustColorBrightness(preferGet('color_primary'), -0.2) }}!important;
            --eventy-color-secondary: {{ preferGet('color_secondary') }}!important;
            --eventy-color-tertiary: {{ preferGet('color_tertiary') }}!important;
            --eventy-color-quaternary: {{ preferGet('color_quaternary') }}!important;
            --eventy-color-red: #EF476F!important;
            --eventy-color-green: #06D6A0!important;
            --eventy-color-lightgrey: #F5F5F5!important;

            --eventy-font-family: 'Inter-Regular';
            --eventy-font-family-bold: 'Inter-Bold';
            --eventy-font-family-semibold: 'Inter-SemiBold';
            --eventy-font-family-medium: 'Inter-Medium';

            --header-height: 50px;
        }
        /*  */
    </style>

    <title>@lang('eventy/backoffice.page_title', ['EVENT_NAME' => preferGet('name_events')])</title>
    @include("admin.Backoffice.plugin.css")

    @stack('css')
</head>

<body class="sidebar-toggle-display sidebar-hidden">
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{ preferGet('icon_events') }}" alt="Eventy Virtual Events">
                </div>
            </div>
        </div>
    </div>

    <div class="container-scroller">

        @include('admin.Backoffice.template.header')

        <div class="container-fluid page-body-wrapper">

            @include('admin.Backoffice.template.sidebar')

            <div class="main-panel" id="boothAdminApp">
                <div class="content-wrapper" style="padding-bottom: 4rem; padding-top: 4rem;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                @if(session()->has('message'))
                                <div class="alert alert-{{ session('message_type') }}">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <h4>
                                        <i class="icon fa fa-info"></i>
                                        @if(Session::get('message_type') === 'success')
                                        Good
                                        @else
                                        Oops
                                        @endif
                                    </h4>
                                    {!! session('message') !!}
                                </div>
                                @endif

                                @if ($errors->any())
                                    @php
                                    $message = implode(', ', $errors->all())
                                    @endphp
                                    <div class="alert alert-warning">
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">×</button>
                                        <h4>
                                            <i class="icon fa fa-info"></i>
                                            {{ $message }}
                                        </h4>
                                        {!! session('message') !!}
                                    </div>
                                @endif
                            </div>
                        </div>

                        @yield('content')

                    </div>
                </div>
            </div>

        </div>

    </div>


    @include('admin.Backoffice.plugin.js')


    @stack('js')
</body>

</html>
