<nav class="navbar fixed-top d-flex" id="rootHeader">
    <div class="container-fluid container-header position-relative">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a href="{{ url('/backoffice') }}" class="navbar-brand brand-logo">
                <img src="{{ preferGet('logo_events') }}" alt="logo"/>
            </a>
            <a href="{{ url('/backoffice') }}" class="navbar-brand brand-logo-mini">
                <img src="{{ preferGet('icon_events') }}" alt="icon"/>
            </a>
        </div>
        <div class="navbar-nav-center">
            <h3 class="event-title">{{ preferGet('name_events') }}</h3>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
            <ul class="navbar-nav navbar-nav-right" id="navbarNav">
                <li class="nav-item dropdown position-relative">
                    <a class="nav-link dropdown-toggle nav-profile" href="#" id="settings-trigger">
                        <div class="wrapper d-flex align-items-center text-right">
                            <div class="wrapper me-2">
                                <h6 class="mb-0" v-cloak>@{{ admin_name }}</h6>
                                <small class="mb-0">Admin Booth</small>
                            </div>
                            <img :src="image_profile" id="header_profile_image" alt="profile">
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
