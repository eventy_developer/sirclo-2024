<!-- template:css -->
<link rel="stylesheet" href="{{ asset('css/font-awesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('packages/bootstrap5/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin/Backoffice/backoffice.css') . '?v=1.0.0' }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/web/Register/spinner.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/web/load.css') }}">
<!-- template:css -->

<!-- summernote codemirror -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.css">
<!-- summernote codemirror -->

<!-- plugin:css -->
<link rel="stylesheet" href="{{ asset('packages/modal-video/modal-video.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<link rel='stylesheet' href="{{ asset('crudbooster/assets/lightbox/dist/css/lightbox.min.css') }}"/>
<link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
<!-- plugin:css -->

<link rel="shortcut icon" href="{{ asset($event_icon) }}">

<style>
    .dataTables_wrapper {
        width: 100%;
    }
</style>
