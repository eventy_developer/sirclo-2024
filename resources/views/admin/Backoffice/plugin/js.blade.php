<script>
    url_base = '{{ url('/') }}';
    token = '{{ csrf_token() }}';
    event_channel = '{{ config("eventy.event_channel") }}';
    timezone_events = '{{ preferGet("timezone_events") }}';
    zoomUrl = '{{ url("zoom-meet") }}/';
    youtubeUrl = 'https://www.youtube.com/embed/';
    is_mobile = '';
    debug = '{{ config("app.debug") }}';
    ipAddress = "{{ request()->ip() }}";
    path = "{{ request()->path() }}";
    breakpoints = {
        sm: 576,
        md: 768,
        lg: 992,
        xlg: 1200,
    };
</script>
<!-- template:js -->
<script src="{{ asset('packages/jquery/jquery-3.6.1.min.js') }}"></script>
<!-- template:js -->

<!-- summernote codemirror -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/codemirror.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.js"></script>
<!-- summernote codemirror -->

<!-- plugin:js -->
<script src="{{ asset('packages/bootstrap5/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('packages/modal-video/modal-video.js') }}"></script>
<script src="{{ asset('crudbooster/assets/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('packages/sweetalert@2/sweetalert2@9.js') }}"></script>
<script src="{{ asset('crudbooster/assets/lightbox/dist/js/lightbox.min.js') }}"></script>
<script src="{{ asset('packages/axios/axios.min.js') }}"></script>
@if(config('app.debug'))
    <script src="{{ asset('packages/vue@2/vue.js') }}"></script>
@else
    <script src="{{ asset('packages/vue@2/vue.min.js') }}"></script>
@endif
<script src="{{ asset('packages/dayjs/dayjs-1.11.4.min.js') }}"></script>
<script src="{{ asset('js/web/Template/load.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script src="{{ asset('crudbooster/jquery.price_format.2.0.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<!-- plugin:js -->

<script src="{{ asset(mix('js/laravel-echo-setup.js')) }}"></script>
<script>
function urlify(text) {
    return text.replace(/(https?:\/\/[^\s]+)/g, function(url) {
        return '<a href="' + url + '" target="_blank">' + url + '</a>';
    });
}

function strip_tags(text) {
    return text.replace(/<[^>]+>/g, '');
}

summernoteUploadUrl = '{{ url('upload-summernote') }}';

toast = Swal.mixin({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
});

axios = axios.create({
    baseURL: url_base + '/backoffice',
    headers: {
        'X-CSRF-TOKEN': token,
        'X-Requested-With': 'XMLHttpRequest'
    }
});

axios.interceptors.request.use((config) => {
    config.headers['X-Socket-ID'] = window.Echo.socketId() // Echo instance
    return config;
});

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    let status = error.response.status.toString();
    if (status === '419') {
        window.location.reload();
    }

    $('#preloader-active').delay(450).fadeOut('slow');
    if (error.response && !status.startsWith('5')) {
        toast.fire({
            icon: 'error',
            title: error.response.data.message,
        });
    } else {
        toast.fire({
            icon: 'error',
            title: error.message,
        });
    }

    return Promise.reject(error);
});

Vue.prototype.$http = axios;
Vue.prototype.$toast = toast;
Vue.prototype.$swal = Swal;
let EventBus = new Vue();
</script>
<script>
    $(document).on('click', '#btn-edit-pass', function() {
        $('#side-profil').hide();
        $('#side-password').show(450);
    });

    let admin = {
        id: '{{ $admin->id }}',
        name: '{{ $admin->name }}',
        image: '{{ $admin->photo }}',
        email: '{{ $admin->email }}',
    };

    let booth = {
        id: '{{ $booth->id }}',
        name: '{{ $booth->name }}',
    };
</script>
<script>
    function showPassword(button) {
        var inputPassword = $(button).parent().find('input');
        if (inputPassword.attr('type') === 'password') {
            inputPassword.attr('type', 'text');
        } else {
            inputPassword.attr('type','password');
        }
    }

    $(document).ready(function() {
        $('.show-password').on('click', function(){
            showPassword(this);
        });

        $('#dataTable').DataTable({
            'ordering': false
        });

        $("#dataTable .checkbox").click(function () {
            var is_any_checked = $("#dataTable .checkbox:checked").length;
            if (is_any_checked) {
                $(".btn-delete-selected").removeClass("disabled");
            } else {
                $(".btn-delete-selected").addClass("disabled");
            }
        });

        $("#dataTable #checkall").click(function () {
            var is_checked = $(this).is(":checked");
            $("#dataTable .checkbox").prop("checked", !is_checked).trigger("click");
        });

        $('.btn-action-deleted').click(function () {
            Swal.fire({
                title: 'Are you sure ?',
                text: 'You will not be able to recover this record data!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#form-table').submit();
                }
            });
        });

        $('.inputMoney').priceFormat({
            'prefix': '',
            'thousandsSeparator': ',',
            'centsLimit': '0',
            'clearOnEmpty': false
        });

        let dropify = $('.dropify').dropify();
        dropify.on('dropify.beforeClear', function(event, element){
            return confirm('Are you sure to delete ?');
        });

        dropify.on('dropify.afterClear', function(event, element){
            let deleteUrl = element.element.dataset.deleteUrl;
            axios.get(deleteUrl)
                .then(() => {
                    console.log('success');
                })
                .catch(() => {
                    swal('Failed to delete image!', '', 'error');
                });
        });
    });
</script>
<script src="{{ asset('js/admin/Backoffice/header.js') . '?v=1.0.0' }}"></script>
<script src="{{ asset('js/admin/Backoffice/profile.js') . '?v=1.0.0' }}"></script>
<script src="{{ asset('js/admin/Backoffice/index.js') . '?v=1.0.0' }}"></script>
<script src="{{ asset('js/admin/Backoffice/summernote.js') }}"></script>
<script src="{{ asset('js/admin/Backoffice/jquery-attachment.js') }}"></script>
