<table border="1" cellpadding="10" cellspacing="0">
    <thead>
        <tr>
            <th>Ticket</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Company</th>
            <th>Job Position</th>
            <th>City</th>
            <th>Date Visit</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($participants as $item)
        <tr>
            <td>{{ $item->ticket }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->company }}</td>
            <td>{{ $item->job_title }}</td>
            <td>{{ $item->master_city_name }}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
