@push('head')
<style>
    :root {
        --base-color: #00b2f9!important;
        --base-color-dark: #1e9ee0!important;
        --base-color-secondary: #06d6a0!important;
        --base-color-tertiary: #ffb628!important;
        --base-color-light: #E5F1F5!important;
        --base-color-red: #EF476F!important;
        --base-color-green: #06D6A0!important;
        --base-color-lightgrey: #F5F5F5!important;
        --color-dark: #092B49;
    }
    a {
        font-weight: 500;
        transition: all .5s;
        -webkit-transition: all .5s;
        -o-transition: all .5s;
        color: var(--base-color);
    }
    [v-cloak] {
        display: none;
    }
    .dropify {
        height: auto!important;
    }
    .dropify-wrapper .dropify-message p {
        font-size: 14px;
    }

    .bootstrap-timepicker .dropdown-menu {
        left: 185px !important;
        box-shadow: 0 0 20px #aaaaaa;
    }

    .alert-validate::before {
        content: attr(data-validate);
        position: absolute;
        z-index: 100;
        max-width: 70%;
        background-color: #fff;
        border: 1px solid #c80000;
        border-radius: 2px;
        padding: 4px 25px 4px 10px;
        top: 50%;
        transform: translateY(-50%);
        right: 12px;
        pointer-events: none;
        color: #c80000;
        font-size: 13px;
        line-height: 1.4;
        text-align: left;

        visibility: hidden;
        opacity: 0;
        transition: opacity 0.4s;
    }
    .alert-validate::after {
        content: "\2757";
        font-family: FontAwesome;
        display: block;
        position: absolute;
        z-index: 110;
        color: #c80000;
        font-size: 16px;
        top: 50%;
        transform: translateY(-50%);
        right: 18px;
    }
    .alert-validate:hover:before {
        visibility: visible;
        opacity: 1;
    }

    .color-dark {
        color: var(--color-dark);
    }
    .color-green {
        color: #06D6A0;
    }
    .color-red {
        color: #FF748D;
    }
    .color-blue {
        color: #36a3f7;
    }
    .content-center {
        width: 95%;
        margin: 20px auto auto;
    }
    .btn-return {
        background-color: var(--base-color);
        border-radius: 50%;
        width: 30px;
        height: 30px;
        display: flex;
        font-size: 24px;
        margin-right: 10px;
        align-items: center;
    }
    .btn-return > * {
        color: #fff;
        width: 100%;
        height: 100%;
        display: flex;
        margin-left: -1px;
    }
    .btn-return  > *:hover {
        color: #fff;
        text-decoration: none;
    }
    .btn-return > * > * {
        margin: auto;
    }

    .btn.active.focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn:active:focus, .btn:focus {
        outline: none;
    }
    .btn-save {
        background-color: var(--base-color);
        border: 1px solid var(--base-color);
        color: #fff;
        border-radius: 15px!important;
        font-weight: bold!important;
        box-shadow: rgba(0, 0, 0, 0.05) 0px 5px 10px;
        margin: auto auto 50px;
        font-size: 16px!important;
        padding: 7px 15px!important;
        outline: none;
    }
    .btn-save:hover,
    .btn-save:focus {
        background-color: var(--base-color)!important;
        color: #fff;
    }
    .btn-save-outline {
        color: var(--base-color);
        background-color: transparent;
    }
    .btn-save-more,
    .btn-save-more:focus,
    .btn-save-more:hover,
    .btn-save-more:active {
        background-color: transparent;
        color: var(--base-color-green);
        border-radius: 15px!important;
        font-weight: bold!important;
        margin: auto auto 50px;
        font-size: 16px!important;
        padding: 7px 15px!important;
        outline: none;
    }
    .btn-cancel {
        border: 1px solid var(--base-color)!important;
        background-color: #fff;
        color: var(--base-color);
        border-radius: 15px!important;
        font-weight: bold!important;
        box-shadow: rgba(0, 0, 0, 0.05) 0px 5px 10px;
        margin: auto auto 50px;
        font-size: 16px!important;
        padding: 7px 15px!important;
        outline: none;
    }
    .btn-add,
    .btn-add:focus,
    .btn-add:active,
    .btn-add:hover {
        background-color: var(--base-color-red);
        color: #fff;
        border-radius: 15px!important;
        font-weight: bold!important;
        box-shadow: rgba(0, 0, 0, 0.05) 0px 5px 10px;
        margin: auto auto 30px;
        font-size: 16px!important;
        padding: 7px 20px!important;
        outline: none;
    }
    .btn-eventy,
    .btn-eventy:hover {
        background: var(--base-color)!important;
        border-color: var(--base-color)!important;
    }
    .btn-save.btn-red,
    .btn-save.btn-red:hover,
    .btn-save.btn-red:active {
        background-color: var(--base-color-red);
        border-color: var(--base-color-red);
    }
    .btn-option,
    .btn-option:hover {
        background-color: #9E6FD9;
        color: #fff!important;
        cursor: default!important;
        border-radius: 15px;
        margin-bottom: 0!important;
    }
    .btn-option > * {
        font-size: 18px;
        font-weight: bold;
    }
    .btn-answer {
        background-color: #fff;
        color: var(--color-dark);
        border-color: var(--color-dark);
        cursor: default;
        border-radius: 15px;
        font-weight: bold;
        font-size: 18px;
    }
    .btn-answer.active {
        background-color: var(--base-color-green);
        color: #fff;
        border-color: var(--base-color-green);
    }
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .form-control-label {
        font-weight: 500;
        font-size: 15px;
        margin-bottom: 10px;
        width: 100%;
    }
    .input-custom-wrapper {
        position: relative;
        width: 100%;
    }
    .input-custom,
    .input-custom:focus,
    .input_date,
    .input_date:focus,
    .form-control:disabled,
    .form-control[readonly],
    .select2,
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        background-color: #F5F5FB!important;
        color: var(--color-dark);
        border-radius: 15px;
        border: 1px solid #F5F5FB;
        box-shadow: 5px 10px 5px #F1F1F7 inset;
        height: auto;
        font-size: 16px;
        font-weight: 700;
        opacity: 1!important;
    }
    .select2,
    .select2-container--default .select2-selection--single .select2-selection__arrow,
    .select2-container--default .select2-selection--single {
        height: 44px;
    }
    #schedule_id.select2 ~ .select2-container--default .select2-selection--multiple,
    #schedule_id.select2 ~ .select2-container--default {
        height: 100px!important;
    }
    .select2-container--default .select2-selection--single {
        border: none;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 40px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .select2-container select2-container--default select2-container--open {
        top: 240px;
    }
    .select2-container .select2-selection {
        height: 50px;
        overflow: scroll;
    }
    .input-custom {
        padding: 10px 15px;
    }
    .input-custom:focus {
        outline: none;
        border: none;
        box-shadow: 5px 10px 5px #F1F1F7 inset;
    }
    .input-group-custom {
        display: flex;
    }
    select.input-custom {
        width: 100%;
    }
    .text-muted {
        line-height: 20px;
    }
    .color-wrapper {
        background-color: #fff;
        width: 45px;
        height: 40px;
        border-radius: 10px;
        padding: 3px;
        margin-left: 10px;
        box-shadow: 0px 0px 15px #F1F1F7;
        display: flex;
    }
    .color-preview {
        border-radius: inherit;
        display: block;
        width: 100%;
        height: 100%;
        margin: auto;
    }
    .text-info,
    .text-info:hover {
        color: #FF748D!important;
        font-weight: 700;
        line-height: 18px;
        margin-top: 5px;
        margin-left: 10px;
    }
    .image-square-box {
        width: 200px;
        height: 200px;
    }
    .group-flex {
        display: flex;
    }
    .speaker-list {
        max-height: 580px;
        padding-right: 10px;
    }
    .speaker-list .card {
        margin-bottom: 15px;
    }
    .speaker-list .card .card-body {
        min-height: 70px;
        display: flex;
        align-items: center;
        padding: 10px 20px;
    }
    .speaker-list .card .card-body .speaker-name {
        font-size: 15px;
        color: var(--color-dark);
        margin-left: 10px;
    }
    .scroll-list {
        overflow-x: auto;
    }
    .scroll-list::-webkit-scrollbar {
        width: 6px;
    }
    .scroll-list::-webkit-scrollbar-track {
        background-color: #f5f5f5;
        border-radius: 4px;
    }
    .scroll-list::-webkit-scrollbar-thumb {
        background-color: #e2e2e2;
        border-radius: 4px;
    }
    .tools-icon {
        font-size: 20px;
        margin-left: 10px;
        cursor: pointer;
    }
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: var(--base-color);
    }

    input:focus + .slider {
        box-shadow: 0 0 1px var(--base-color);
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }
    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }
    .slider.round:before {
        border-radius: 50%;
    }

    /* Custom Radio Button */
    [type="radio"]:checked,
    [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    [type="radio"]:checked + label,
    [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    [type="radio"]:checked + label:before,
    [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 20px;
        height: 20px;
        border: 1px solid #ddd;
        border-radius: 100%;
        background: #fff;
    }
    [type="radio"]:checked + label:after,
    [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 12px;
        height: 12px;
        background: var(--base-color);
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }

    .input-group>.input-group-prepend>.btn,
    .input-group>.input-group-prepend>.input-group-text,
    .input-group>.input-group-append:not(:last-child)>.btn,
    .input-group>.input-group-append:not(:last-child)>.input-group-text,
    .input-group>.input-group-append:last-child>.btn:not(:last-child):not(.dropdown-toggle),
    .input-group>.input-group-append:last-child>.input-group-text:not(:last-child) {
        border-top-left-radius: 15px;
        border-bottom-left-radius: 15px;
    }
    .card {
        box-shadow: rgba(0, 0, 0, 0.05) 0px 5px 20px;
        border-radius: 15px;
        border: none;
        min-width: 0;
        word-wrap: break-word;
        background-clip: border-box;
        width: 100%;
        color: #092B49;
    }
    .card-box-upload {
        background-color: #F5F5FB;
        border-radius: 15px;
        box-shadow: 5px 5px 5px #E1E1E1 inset;
    }
    .card-box-upload-label {

    }
    .card.card-large-icons {
        display: flex;
        flex-direction: row;
    }
    .card.card-large-icons .card-icon {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        width: 150px;
        border-radius: 3px 0 0 3px;
        background-color: var(--base-color);
    }
    .card .card-header,
    .card .card-body,
    .card .card-footer {
        background-color: transparent;
        padding: 20px 25px;
    }

    .card .card-header {
        border-bottom: 1px solid rgba(0,0,0,.125);
        border-bottom-color: #f9f9f9;
        margin-bottom: 0;
        line-height: 30px;
        -ms-grid-row-align: center;
        align-self: center;
        width: 100%;
        min-height: 70px;
        padding: 15px 25px;
        display: flex;
        align-items: center;
    }
    .card-header:first-child {
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }
    .card.card-large-icons .card-body {
        padding: 25px 30px;
    }
    .card.card-large-icons .card-body h4 {
        font-size: 18px;
    }
    .card.card-large-icons .card-body p {
        opacity: .6;
        font-weight: 500;
    }
    .card.card-large-icons .card-body a.card-cta {
        text-decoration: none;
        color: var(--base-color);
    }
    .card.card-large-icons .card-body a.card-cta i {
        margin-left: 7px;
    }
    .card.card-large-icons .card-icon .ion,
    .card.card-large-icons .card-icon .fas,
    .card.card-large-icons .card-icon .far,
    .card.card-large-icons .card-icon .fab,
    .card.card-large-icons .card-icon .fa,
    .card.card-large-icons .card-icon .fal {
        font-size: 60px;
    }


    p, ul:not(.list-unstyled), ol {
        line-height: 28px;
    }
    .flex-column {
        -ms-flex-direction: column!important;
        flex-direction: column!important;
    }
    .nav {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    .nav-pills .nav-item .nav-link {
        color: var(--base-color);
        padding-left: 15px !important;
        padding-right: 15px !important;
        font-size: 16px;
    }
    .nav-pills .nav-link {
        border-radius: .25rem;
        border: none!important;
    }
    .nav-pills .nav-item .nav-link.active {
        box-shadow: 0 2px 6px #acb5f6;
        color: #fff;
        background-color: var(--base-color);
        border-radius: 5px!important;
    }

    .section-header-back {
        margin-right: 15px;
    }
    .section-header-back .btn.btn-icon {
        color: var(--base-color);
        display: flex;
    }
    .section-header-back .btn.btn-icon * {
        margin: auto;
    }
    .section-header-back .btn:hover {
        background-color: var(--base-color);
        color: #fff;
    }

    .card-custom {
        border-radius: 15px;
        box-shadow: rgba(0, 0, 0, 0.16) 0px 5px 40px;
        background-color: #fff;
    }

    @media (min-width: 768px) {
        .col-md-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .col-md-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }
    }

    @media (min-width: 992px) {
        .col-lg-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
    }

    @media (max-width: 991.98px) {
        .content-center {
            width: auto;
        }
    }
</style>
@endpush
