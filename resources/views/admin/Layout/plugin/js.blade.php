@push('bottom')
<script>
    $(document).ready(function () {
        $('.checkbox-toggle-1').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).val('active');
            } else {
                $(this).val('nonactive');
            }
        });

        $('.checkbox-toggle-2').on('change', function () {
            if ($(this).is(':checked')) {
                $(this).val('yes');
            } else {
                $(this).val('no');
            }
        });

        $('.select2').select2();

        let lang = '{{ App::getLocale() }}';
        $('.inputMoney').priceFormat({
            "prefix": "",
            "thousandsSeparator": ",",
            "centsLimit": "0",
            "clearOnEmpty": false
        });
        $('.input_datetime').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePickerIncrement: 1,
            timePicker24Hour: true,
            autoApply: false,
            showDropdowns: true,
            locale: {
                format: "YYYY-MM-DD HH:mm:ss",
            },
        });
        $('.input_date').datepicker({
            format: 'yyyy-mm-dd',
            @if (in_array(App::getLocale(), ['ar', 'fa']))
            rtl: true,
            @endif
            language: lang
        });
        $('#textarea_description').summernote({
            height: ($(window).height() - 300),
            toolbar: [
                ['style', ['style']],
                ['fontname', ['fontname']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['insert', ['link']],
                ['view', ['fullscreen', 'codeview']],
            ],
            prettifyHtml: true,
            codemirror: {
                theme: 'monokai',
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
            },
            callbacks: {
                onImageUpload: function (image) {
                    uploadImagedescription(image[0]);
                }
            },
        });

        function uploadImagedescription(image) {
            let data = new FormData();
            data.append("userfile", image);
            $.ajax({
                url: '{{ CRUDBooster::mainpath("upload-summernote") }}',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function (url) {
                    var image = $('<img>').attr('src', url);
                    $('#textarea_description').summernote("insertNode", image[0]);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        let dropify = $('.dropify').dropify();
        dropify.on('dropify.beforeClear', function(event, element){
            return confirm('Are you sure to delete ?');
        });

        dropify.on('dropify.afterClear', function(event, element){
            let deleteUrl = element.element.dataset.deleteUrl;
            axios.get(deleteUrl)
                .then(() => {
                    console.log('success');
                })
                .catch(() => {
                    swal('Failed to delete image!', '', 'error');
                });
        });
    });

    function numberOnly(el) {
        let element = document.getElementById(el.getAttribute('id'));
        element.value = element.value.replace(/[^0-9]/gi, "");
    }

    // vue global mixin
    Vue.mixin({
        mounted() {
            if (document.querySelectorAll('.vue-dropify').length > 0) {
                $('.vue-dropify').dropify();
            }
        },
        methods: {
            formatRupiah(angka, prefix = 'Rp ') {
                angka = angka.toString();
                // let number_string = angka.replace(/[^,\d]/g, '').toString();
                let number_string = '' + parseInt(angka.replace(/[^,\d]/g, '').toString());
                let split = number_string.split(',');
                let sisa = split[0].length % 3;
                let rupiah = split[0].substr(0, sisa);
                let ribuan = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix + (rupiah === '' ? '0' : rupiah);
            },

            // validate required form
            validateForm() {
                let input = $('.form-group .input-custom-wrapper .input-custom');

                //check required column
                for (let i = 0; i < input.length; i++) {
                    if (this.validate(input[i]) === false) {
                        input[i].focus();
                        this.showValidate(input[i]);

                        return false;
                    }

                    this.hideValidate(input[i]);
                }

                return true;
            },

            // helpers
            validate(input) {
                if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email' && $(input).val().trim() != '') {
                    if ($(input).val().trim() == ''){
                        return false;
                    } else {
                        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                            this.setValidate(input, 'Email format is incorrect');
                            return false;
                        }
                    }
                } else {
                    if($(input).val().trim() == ''){
                        return false;
                    }
                }
            },
            setValidate(input, message) {
                var thisAlert = $(input).parent();
                $(thisAlert).attr('data-validate', message);
            },
            showValidate(input) {
                var thisAlert = $(input).parent();
                $(thisAlert).addClass('alert-validate');
            },
            hideValidate(input) {
                var thisAlert = $(input).parent();
                $(thisAlert).removeClass('alert-validate');
            },
        },
    });
</script>
@endpush
