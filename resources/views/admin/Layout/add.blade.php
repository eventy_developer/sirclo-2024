@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')

    <form action="{{ CRUDBooster::adminPath($path . '/add-save') }}" method="POST" enctype="multipart/form-data">
        @csrf

        @if(g('parent_field'))
        <input type="hidden" name="{{ g('parent_field') }}" value="{{ g('parent_id') }}">
        @endif
        <input type="hidden" name="return_url" value="{{ g('return_url') }}">
        <input type="hidden" name="ref_mainpath" value="{{ g('return_url') }}">
        <input type="hidden" name="ref_parameter" value="return_url={{ g('return_url') }}">

        @include($template)

        <div class="text-center" style="margin-top: 50px;">
            @if(g('return_url'))
                <a href="{{ g('return_url') }}" class="btn btn-save btn-save-outline mb-3"
                   style="padding-left: 40px!important;padding-right: 40px!important;">Back</a>
            @else
                <a href="{{ CRUDBooster::mainpath('?' . http_build_query(@$_GET)) }}" class="btn btn-save btn-save-outline mb-3"
                   style="padding-left: 40px!important;padding-right: 40px!important;">Back</a>
            @endif

            <button type="submit" class="btn btn-save mb-3 ml-2"
                    style="padding-left: 40px!important;padding-right: 40px!important;">Save</button> <br>
            <input type="submit" name="submit" class="btn btn-save-more" value="Save & Add More">
        </div>

    </form>

@endsection
