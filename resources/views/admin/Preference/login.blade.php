@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/login') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="login_image" class="form-control-label">
                                Login Image
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            @if(!preferGet('login_image'))
                            <input type="file" name="login_image" id="login_image"
                            class="form-control inputfile dropify" accept="image/*"
                            data-default-file="{{ preferGet('login_image') }}"
                            data-max-file-size="2M"
                            data-allowed-file-extensions="jpg jpeg png"
                            data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/login_image') }}"
                            data-height="600">

                            <p class="text-info"><em>* Max size 2MB</em></p>
                            @else
                            <p>
                                <a data-lightbox="roadtrip"
                                    href="{{ preferGet('login_image') }}">
                                    <img title="Home Background" width="100%"
                                        src="{{ preferGet('login_image') }}">
                                </a>
                            </p>
                            <p>
                                <a class="btn btn-danger btn-delete btn-sm"
                                    onclick="if(!confirm('Are you sure ?')) return false"
                                    href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/login_image') }}">
                                    <i class="fa fa-ban"></i> Delete
                                </a>
                            </p>
                            <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Login</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="login_title_text" class="form-control-label">
                                Login Title Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="login_title_text" id="login_title_text" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('login_title_text') }}">
                        </div>
                        <div class="form-group">
                            <label for="login_caption_text" class="form-control-label">
                                Login Caption Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="login_caption_text" id="login_caption_text" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('login_caption_text') }}">
                        </div>
                    </div>
                </div>
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Register</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="register_title_text" class="form-control-label">
                                Register Title Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="register_title_text" id="register_title_text" maxlength="100" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('register_title_text') }}">
                        </div>
                        <div class="form-group">
                            <label for="register_caption_text" class="form-control-label">
                                Register Caption Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="register_caption_text" id="register_caption_text" maxlength="100" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('register_caption_text') }}">
                        </div>
                    </div>
                </div>
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Register (At the Event)</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="at_event_title_text" class="form-control-label">
                                Register Title (At the Event)
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="at_event_title_text" id="at_event_title_text" maxlength="100" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('at_event_title_text') }}">
                        </div>
                        <div class="form-group">
                            <label for="at_event_caption_text" class="form-control-label">
                                Register Caption (At the Event)
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="at_event_caption_text" id="at_event_caption_text" maxlength="100" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('at_event_caption_text') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection
