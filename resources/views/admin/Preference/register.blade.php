@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/register') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="register_title_text" class="form-control-label">
                                {{ preferGet('register_title_text', true) }}
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="register_title_text" id="register_title_text" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('register_title_text') }}">
                        </div>
                        <div class="form-group">
                            <label for="register_caption_text" class="form-control-label">
                                {{ preferGet('register_caption_text', true) }}
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="register_caption_text" id="register_caption_text" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('register_caption_text') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection
