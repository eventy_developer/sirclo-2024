@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/payment') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Ticket</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="ticket_title_text" class="form-control-label">
                                        Ticket Title Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="ticket_title_text" id="ticket_title_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('ticket_title_text') }}">
                                </div>
                                <div class="form-group">
                                    <label for="ticket_caption_text" class="form-control-label">
                                        Ticket Caption Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="ticket_caption_text" id="ticket_caption_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('ticket_caption_text') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Classroom</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="classroom_title_text" class="form-control-label">
                                        Classroom Title Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="classroom_title_text" id="classroom_title_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('classroom_title_text') }}">
                                </div>
                                <div class="form-group">
                                    <label for="classroom_caption_text" class="form-control-label">
                                        Classroom Caption Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="classroom_caption_text" id="classroom_caption_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('classroom_caption_text') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Order Summary</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="order_summary_title_text" class="form-control-label">
                                        Order Summary Title Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="order_summary_title_text" id="order_summary_title_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('order_summary_title_text') }}">
                                </div>
                                <div class="form-group">
                                    <label for="order_summary_caption_text" class="form-control-label">
                                        Order Summary Caption Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="order_summary_caption_text" id="order_summary_caption_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('order_summary_caption_text') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Payment</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="payment_title_text" class="form-control-label">
                                        Payment Title Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="payment_title_text" id="payment_title_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('payment_title_text') }}">
                                </div>
                                <div class="form-group">
                                    <label for="payment_caption_text" class="form-control-label">
                                        Payment Caption Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="payment_caption_text" id="payment_caption_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('payment_caption_text') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Payment Confirmation</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="payment_confirmation_title_text" class="form-control-label">
                                        Payment Confirmation Title Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="payment_confirmation_title_text" id="payment_confirmation_title_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('payment_confirmation_title_text') }}">
                                </div>
                                <div class="form-group">
                                    <label for="payment_confirmation_caption_text" class="form-control-label">
                                        Payment Confirmation Caption Text
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="payment_confirmation_caption_text" id="payment_confirmation_caption_text" maxlength="100" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('payment_confirmation_caption_text') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Contact Settings</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="contact_email_events" class="form-control-label">
                                Contact Email Events
                            </label>
                            <input type="text" name="contact_email_events" id="contact_email_events" maxlength="50" minlength="5"
                                class="form-control input-custom" value="{{ preferGet('contact_email_events') }}">
                        </div>
                        <div class="form-group">
                            <label for="contact_name_1" class="form-control-label">
                                Contact Name 1
                            </label>
                            <input type="text" name="contact_name_1" id="contact_name_1" maxlength="100" minlength="3"
                                class="form-control input-custom" value="{{ preferGet('contact_name_1') }}">
                        </div>
                        <div class="form-group">
                            <label for="contact_phone_1" class="form-control-label">
                                Contact Phone 1
                            </label>
                            <input type="text" name="contact_phone_1" id="contact_phone_1" maxlength="100" minlength="5"
                                class="form-control input-custom" value="{{ preferGet('contact_phone_1') }}">
                        </div>
                        <div class="form-group">
                            <label for="contact_name_2" class="form-control-label">
                                Contact Name 2 (Optional)
                            </label>
                            <input type="text" name="contact_name_2" id="contact_name_2" maxlength="100"
                                class="form-control input-custom" value="{{ preferGet('contact_name_2') }}">
                        </div>
                        <div class="form-group">
                            <label for="contact_phone_2" class="form-control-label">
                                Contact Phone 2 (Optional)
                            </label>
                            <input type="text" name="contact_phone_2" id="contact_phone_2" maxlength="100"
                                class="form-control input-custom" value="{{ preferGet('contact_phone_2') }}">
                        </div>
                    </div>
                </div>
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Bank Transfer Settings</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="payment_bank_name" class="form-control-label">
                                Payment Bank Name
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="payment_bank_name" id="payment_bank_name" maxlength="100" minlength="3"
                                class="form-control input-custom" required value="{{ preferGet('payment_bank_name') }}">
                        </div>
                        <div class="form-group">
                            <label for="payment_bank_branch" class="form-control-label">
                                Payment Bank Branch
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="payment_bank_branch" id="payment_bank_branch" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('payment_bank_branch') }}">
                        </div>
                        <div class="form-group">
                            <label for="payment_account_owner" class="form-control-label">
                                Payment Account Owner
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="payment_account_owner" id="payment_account_owner" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('payment_account_owner') }}">
                        </div>
                        <div class="form-group">
                            <label for="payment_account_number" class="form-control-label">
                                Payment Account Number
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="payment_account_number" id="payment_account_number" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('payment_account_number') }}">
                        </div>
                    </div>
                </div>
                @if(\crocodicstudio\crudbooster\helpers\CRUDBooster::isSuperadmin())
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Payment Method Settings</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="payment_method_manual_status" class="form-control-label">
                                        Bank Transfer Status
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="payment_method_manual_status"
                                               value="{{ preferGet('payment_method_manual_status') }}"
                                            {{ preferGet('payment_method_manual_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="payment_method_qris_status" class="form-control-label">
                                        QRIS Status
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="payment_method_qris_status"
                                               value="{{ preferGet('payment_method_qris_status') }}"
                                            {{ preferGet('payment_method_qris_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="payment_method_ovo_status" class="form-control-label">
                                        OVO Status
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="payment_method_ovo_status"
                                               value="{{ preferGet('payment_method_ovo_status') }}"
                                            {{ preferGet('payment_method_ovo_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="payment_method_cc_status" class="form-control-label">
                                        Credit Card Status
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="payment_method_cc_status"
                                               value="{{ preferGet('payment_method_cc_status') }}"
                                            {{ preferGet('payment_method_cc_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="payment_method_va_status" class="form-control-label">
                                        Virtual Account Status (<i>Beta</i>)
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="payment_method_va_status"
                                               value="{{ preferGet('payment_method_va_status') }}"
                                            {{ preferGet('payment_method_va_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection
