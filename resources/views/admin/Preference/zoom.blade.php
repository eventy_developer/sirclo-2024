@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/zoom') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Zoom</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="zoom_api_key" class="form-control-label">
                                Zoom API Key
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="zoom_api_key" id="zoom_api_key" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('zoom_api_key') }}">
                        </div>
                        <div class="form-group">
                            <label for="zoom_api_secret" class="form-control-label">
                                Zoom API Secret
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="zoom_api_secret" id="zoom_api_secret" maxlength="100" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('zoom_api_secret') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 30px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection
