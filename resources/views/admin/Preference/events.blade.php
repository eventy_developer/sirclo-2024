@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/events') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Events</h5>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="sirclo_information" class="form-control-label">
                                        Image Sirclo Information
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('sirclo_information'))
                                        <input type="file" name="sirclo_information" id="sirclo_information" accept="image/*"
                                               class="form-control inputfile dropify"
                                               data-default-file="{{ preferGet('sirclo_information') }}"
                                               data-max-file-size="1000K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/sirclo_information') }}">

                                        <p class="text-info">
                                            <em>* Max size 500KB</em><br>
                                        </p>
                                    @else
                                        <p>
                                            <a data-lightbox="roadtrip"
                                               href="{{ preferGet('sirclo_information') }}">
                                                <img style="max-width: 160px;" title="Image"
                                                     src="{{ preferGet('sirclo_information') }}">
                                            </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-danger btn-delete btn-sm"
                                               onclick="if(!confirm('Are you sure ?')) return false"
                                               href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/sirclo_information') }}">
                                                <i class="fa fa-ban"></i> Delete
                                            </a>
                                        </p>
                                        <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pickup_words" class="form-control-label">
                                Doorprize Pengambilan Words (General)
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="pickup_words" id="pickup_words" maxlength="255" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('pickup_words') }}">
                        </div>
                        <div class="form-group">
                            <label for="pickup_words_grand" class="form-control-label">
                                Doorprize Pengambilan Words (Grand Prize)
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="pickup_words_grand" id="pickup_words_grand" maxlength="255" minlength="5"
                                   class="form-control input-custom" required value="{{ preferGet('pickup_words_grand') }}">
                        </div>
                        <div class="form-group">
                            <label for="name_events" class="form-control-label">
                                Event Name {{ preferGet('name_events') }}
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="name_events" id="name_events" maxlength="50" minlength="5"
                                class="form-control input-custom" required value="{{ preferGet('name_events') }}">
                        </div>
                        <div class="form-group">
                            <label for="date_start_events" class="form-control-label">
                                Date Start
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="date_start_events" id="date_start_events" maxlength="30" min="1"
                                class="form-control input-custom input_datetime" required
                                value="{{ preferGet('date_start_events') }}">
                        </div>
                        <div class="form-group">
                            <label for="date_end_events" class="form-control-label">
                                Date End
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="date_end_events" id="date_end_events" maxlength="30" min="1"
                                class="form-control input-custom input_datetime" required
                                value="{{ preferGet('date_end_events') }}">
                        </div>
                        <div class="form-group">
                            <label for="timezone_events" class="form-control-label">
                                Timezone
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <select name="timezone_events" id="timezone_events" required class="form-control input-custom">
                                <option value="">* Please select a Timezone *</option>
                                <option value="Asia/Jakarta" {{ preferGet('timezone_events') === 'Asia/Jakarta' ? 'selected' : '' }}>Asia/Jakarta</option>
                                <option value="Asia/Makassar" {{ preferGet('timezone_events') === 'Asia/Makassar' ? 'selected' : '' }}>Asia/Makassar</option>
                                <option value="Asia/Jayapura" {{ preferGet('timezone_events') === 'Asia/Jayapura' ? 'selected' : '' }}>Asia/Jayapura</option>
                            </select>
                        </div>
                        @if(\crocodicstudio\crudbooster\helpers\CRUDBooster::isSuperadmin())
                        <div class="form-group">
                            <label for="free_events" class="form-control-label">
                                Free Events ?
                            </label>
                            <label class="switch">
                                <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-2" name="free_events"
                                       value="{{ preferGet('free_events') }}"
                                    {{ preferGet('free_events') === 'yes' ? 'checked' : '' }}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="lock_login_wording_events" class="form-control-label">
                                Wording Locked Login (<i>Filled when you want to lock the login</i>)
                            </label>
                            <input type="text" name="lock_login_wording_events" id="lock_login_wording_events" maxlength="50" minlength="5"
                                   class="form-control input-custom" value="{{ preferGet('lock_login_wording_events') }}">
                        </div>
                        @endif

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="bg_ticket" class="form-control-label">
                                        Background Ticket
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('bg_ticket'))
                                        <input type="file" name="bg_ticket" id="bg_ticket" accept="image/*"
                                               class="form-control inputfile dropify"
                                               data-default-file="{{ preferGet('bg_ticket') }}"
                                               data-max-file-size="500K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/bg_ticket') }}"
                                               data-allowed-formats="landscape square">

                                        <p class="text-info">
                                            <em>* Max size 500KB</em><br>
                                            <em>&nbsp; Landscape or square dimentions</em>
                                        </p>
                                    @else
                                        <p>
                                            <a data-lightbox="roadtrip"
                                               href="{{ preferGet('bg_ticket') }}">
                                                <img style="max-width: 160px;" title="Image"
                                                     src="{{ preferGet('bg_ticket') }}">
                                            </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-danger btn-delete btn-sm"
                                               onclick="if(!confirm('Are you sure ?')) return false"
                                               href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/bg_ticket') }}">
                                                <i class="fa fa-ban"></i> Delete
                                            </a>
                                        </p>
                                        <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="bg_sessions" class="form-control-label">
                                        Background Sessions
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('bg_sessions'))
                                        <input type="file" name="bg_sessions" id="bg_sessions" accept="image/*"
                                               class="form-control inputfile dropify"
                                               data-default-file="{{ preferGet('bg_sessions') }}"
                                               data-max-file-size="500K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/bg_sessions') }}"
                                               data-allowed-formats="landscape square">

                                        <p class="text-info">
                                            <em>* Max size 500KB</em><br>
                                            <em>&nbsp; Landscape or square dimentions</em>
                                        </p>
                                    @else
                                        <p>
                                            <a data-lightbox="roadtrip"
                                               href="{{ preferGet('bg_sessions') }}">
                                                <img style="max-width: 160px;" title="Image"
                                                     src="{{ preferGet('bg_sessions') }}">
                                            </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-danger btn-delete btn-sm"
                                               onclick="if(!confirm('Are you sure ?')) return false"
                                               href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/bg_sessions') }}">
                                                <i class="fa fa-ban"></i> Delete
                                            </a>
                                        </p>
                                        <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Logo & Icon</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="logo_events" class="form-control-label">
                                        Logo
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('logo_events'))
                                    <input type="file" name="logo_events" id="logo_events" accept="image/*"
                                    class="form-control inputfile dropify"
                                    data-default-file="{{ preferGet('logo_events') }}"
                                    data-max-file-size="500K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/logo_events') }}"
                                    data-allowed-formats="landscape square">

                                    <p class="text-info">
                                        <em>* Max size 500KB</em><br>
                                        <em>&nbsp; Landscape or square dimentions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('logo_events') }}">
                                            <img style="max-width: 160px;" title="Image"
                                            src="{{ preferGet('logo_events') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/logo_events') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="logo_landingpage" class="form-control-label">
                                        Logo Landingpage
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('logo_landingpage'))
                                        <input type="file" name="logo_landingpage" id="logo_landingpage" accept="image/*"
                                               class="form-control inputfile dropify"
                                               data-default-file="{{ preferGet('logo_landingpage') }}"
                                               data-max-file-size="500K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/logo_landingpage') }}"
                                               data-allowed-formats="landscape square">

                                        <p class="text-info">
                                            <em>* Max size 500KB</em><br>
                                            <em>&nbsp; Landscape or square dimentions</em>
                                        </p>
                                    @else
                                        <p>
                                            <a data-lightbox="roadtrip"
                                               href="{{ preferGet('logo_landingpage') }}">
                                                <img style="max-width: 160px;" title="Image"
                                                     src="{{ preferGet('logo_landingpage') }}">
                                            </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-danger btn-delete btn-sm"
                                               onclick="if(!confirm('Are you sure ?')) return false"
                                               href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/logo_landingpage') }}">
                                                <i class="fa fa-ban"></i> Delete
                                            </a>
                                        </p>
                                        <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="icon_events" class="form-control-label">
                                        Icon
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('icon_events'))
                                    <input type="file" name="icon_events" id="icon_events"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('icon_events') }}"
                                    data-max-file-size="100K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/icon_events') }}"
                                    data-allowed-formats="square">

                                    <p class="text-info">
                                        <em>* Max size 100KB</em> <br>
                                        <em>&nbsp; Square dimentions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('icon_events') }}">
                                            <img style="max-width: 160px;" title="Image"
                                            src="{{ preferGet('icon_events') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/icon_events') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Color</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="color_primary" class="form-control-label">
                                Primary Color
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="input-group-custom">
                                <input type="text" name="color_primary" id="color_primary" maxlength="30" min="1"
                                class="form-control input-custom color" required value="{{ preferGet('color_primary') }}"
                                autocomplete="off" style="color: {{ preferGet('color_primary') }};">
                                <div class="color-wrapper">
                                    <span class="color-preview"
                                    style="background-color: {{ preferGet('color_primary') }}"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color_secondary" class="form-control-label">
                                Secondary Color
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="input-group-custom">
                                <input type="text" name="color_secondary" id="color_secondary" maxlength="30" min="1"
                                class="form-control input-custom color" required value="{{ preferGet('color_secondary') }}"
                                autocomplete="off" style="color: {{ preferGet('color_secondary') }};">
                                <div class="color-wrapper">
                                    <span class="color-preview"
                                    style="background-color: {{ preferGet('color_secondary') }}"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color_tertiary" class="form-control-label">
                                Tertiary Color
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="input-group-custom">
                                <input type="text" name="color_tertiary" id="color_tertiary" maxlength="30" min="1"
                                class="form-control input-custom color" required value="{{ preferGet('color_tertiary') }}"
                                autocomplete="off" style="color: {{ preferGet('color_tertiary') }};">
                                <div class="color-wrapper">
                                    <span class="color-preview"
                                    style="background-color: {{ preferGet('color_tertiary') }}"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 50px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection

@push('bottom')
<script>
    $('.date').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 1,
        timePicker24Hour: true,
        timePickerSeconds: false,
        autoApply: false,
        showDropdowns: true,
        locale: {
            format: "YYYY-MM-DD HH:mm:ss",
        },
    });
    $('.color').colorpicker({ format: 'hex' }).on('changeColor',function () {
        $(this).css('color', $(this).val());
        $(this).next().children().css('background-color', $(this).val());
    });
</script>
@endpush
