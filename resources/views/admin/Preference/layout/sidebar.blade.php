<div class="card">
    <div class="card-header">
        <div class="section-header-back">
            <a href="{{ CRUDBooster::adminPath('preference') }}" class="btn btn-icon">
                <i class="fa fa-arrow-left"></i>
            </a>
        </div>
        <h4>Back to Preferences</h4>
    </div>
    <div class="card-body">
        <ul class="nav nav-pills">
            @foreach ($menus as $menu)
            <li class="nav-item">
                <a href="{{ CRUDBooster::adminPath('preference/' . $menu->slug) }}" 
                    class="nav-link {{ request()->segment(3) == $menu->slug ? 'active' : '' }}">
                    {{ $menu->name }}
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</div>