@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
    <div class="content-center">
        @include('admin.Preference.layout.sidebar')
        <form action="{{ CRUDBooster::adminpath('preference/plugin') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-6">
                    <h5 class="text-center color-dark" style="margin-bottom: 20px;">Tawk.to</h5>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="plugin_tawkto" class="form-control-label">
                                    Tawk.to
                                </label>
                                <textarea type="text" name="plugin_tawkto" id="plugin_tawkto"
                                      min="1" class="form-control input-custom" style="min-width: 100%;max-width: 100%;"
                                      rows="10" placeholder="Tawk.to Script">{{ preferGet('plugin_tawkto') }}</textarea>

                                <p class="text-info" style="font-size: 16px;">
                                    <br>
                                    How to: <br>
                                    <a href="https://help.tawk.to/article/adding-a-widget-to-your-website"
                                        target="_blank">
                                        https://help.tawk.to/article/adding-a-widget-to-your-website
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h5 class="text-center color-dark" style="margin-bottom: 20px;">Google Analytics Tracking ID</h5>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="plugin_google_analytics" class="form-control-label">
                                    Google Analytics Tracking ID
                                </label>
                                <input type="text" name="plugin_google_analytics" id="plugin_google_analytics" maxlength="100" min="5"
                                       class="form-control input-custom" value="{{ preferGet('plugin_google_analytics') }}"
                                        placeholder="Tracking ID">

                                <p class="text-info" style="font-size: 16px;">
                                    <br>
                                    How to: <br>
                                    <a href="https://support.google.com/analytics/answer/1008080?hl=en"
                                       target="_blank">
                                        https://support.google.com/analytics/answer/1008080?hl=en
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center" style="margin-top: 30px;">
                <button type="submit" class="btn btn-save">Save Changes</button>
            </div>
        </form>
    </div>
@endsection
