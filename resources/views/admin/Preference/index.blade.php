@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')

@section('content')
<div class="row">
    @foreach ($menus as $menu)
    <div class="col-lg-6">
        <div class="card card-large-icons">
            <div class="card-icon bg-primary text-white">
                <i class="{{ $menu->fa_icon }}"></i>
            </div>
            <div class="card-body">
                <h4>{{ $menu->name }}</h4>
                <p>{{ $menu->description }}</p>
                <a href="{{ CRUDBooster::adminPath('preference/' . $menu->slug) }}" class="card-cta">
                    Change Setting <i class="fa fa-chevron-right"></i>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
