@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/web') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Floating Menu</h5>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center color-dark" style="margin: auto;">Home</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="home_menu_text" class="form-control-label">
                                Home Menu Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="home_menu_text" id="home_menu_text" maxlength="10" minlength="3"
                                class="form-control input-custom" required value="{{ preferGet('home_menu_text') }}">
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="home_menu_icon_active" class="form-control-label">
                                    Home Menu Icon Active
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('home_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="home_menu_icon_active" id="home_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('home_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('home_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('home_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="home_menu_icon_nonactive" class="form-control-label">
                                    Home Menu Icon Nonactive
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('home_menu_icon_nonactive'))
                                <div class="image-square-box">
                                    <input type="file" name="home_menu_icon_nonactive" id="home_menu_icon_nonactive"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('home_menu_icon_nonactive') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_menu_icon_nonactive') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('home_menu_icon_nonactive') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('home_menu_icon_nonactive') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_menu_icon_nonactive') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center color-dark" style="margin: auto;">Booth</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="booth_menu_text" class="form-control-label">
                                Booth Menu Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="booth_menu_text" id="booth_menu_text" maxlength="10" minlength="3"
                                class="form-control input-custom" required value="{{ preferGet('booth_menu_text') }}">
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="booth_menu_icon_active" class="form-control-label">
                                    Booth Menu Icon Active
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('booth_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="booth_menu_icon_active" id="booth_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('booth_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('booth_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('booth_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="booth_menu_icon_nonactive" class="form-control-label">
                                    Booth Menu Icon Nonactive
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('booth_menu_icon_nonactive'))
                                <div class="image-square-box">
                                    <input type="file" name="booth_menu_icon_nonactive" id="booth_menu_icon_nonactive"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('booth_menu_icon_nonactive') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_menu_icon_nonactive') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('booth_menu_icon_nonactive') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('booth_menu_icon_nonactive') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_menu_icon_nonactive') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center color-dark" style="margin: auto;">Main Stage</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="main_stage_menu_text" class="form-control-label">
                                Main Stage Menu Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="main_stage_menu_text" id="main_stage_menu_text" maxlength="10" minlength="3"
                                class="form-control input-custom" required value="{{ preferGet('main_stage_menu_text') }}">
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="main_stage_menu_icon_active" class="form-control-label">
                                    Main Stage Menu Icon Active
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('main_stage_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="main_stage_menu_icon_active" id="main_stage_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('main_stage_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('main_stage_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('main_stage_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="main_stage_menu_icon_nonactive" class="form-control-label">
                                    Main Stage Menu Icon Nonactive
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('main_stage_menu_icon_nonactive'))
                                <div class="image-square-box">
                                    <input type="file" name="main_stage_menu_icon_nonactive" id="main_stage_menu_icon_nonactive"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('main_stage_menu_icon_nonactive') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_menu_icon_nonactive') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('main_stage_menu_icon_nonactive') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('main_stage_menu_icon_nonactive') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_menu_icon_nonactive') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center color-dark" style="margin: auto;">Classroom</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="classroom_menu_text" class="form-control-label">
                                Classroom Menu Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="classroom_menu_text" id="classroom_menu_text" maxlength="10" minlength="3"
                                class="form-control input-custom" required value="{{ preferGet('classroom_menu_text') }}">
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="classroom_menu_icon_active" class="form-control-label">
                                    Classroom Menu Icon Active
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('classroom_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="classroom_menu_icon_active" id="classroom_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('classroom_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('classroom_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('classroom_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="classroom_menu_icon_nonactive" class="form-control-label">
                                    Classroom Menu Icon Nonactive
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('classroom_menu_icon_nonactive'))
                                <div class="image-square-box">
                                    <input type="file" name="classroom_menu_icon_nonactive" id="classroom_menu_icon_nonactive"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('classroom_menu_icon_nonactive') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_menu_icon_nonactive') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('classroom_menu_icon_nonactive') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('classroom_menu_icon_nonactive') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_menu_icon_nonactive') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="text-center color-dark" style="margin: auto;">Sub Menu</h5>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="schedule_menu_icon_active" class="form-control-label">
                                    Schedule Icon
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('schedule_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="schedule_menu_icon_active" id="schedule_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('schedule_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/schedule_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('schedule_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('schedule_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/schedule_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="sponsor_menu_icon_active" class="form-control-label">
                                    Sponsor Icon
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('sponsor_menu_icon_active'))
                                <div class="image-square-box">
                                    <input type="file" name="sponsor_menu_icon_active" id="sponsor_menu_icon_active"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('sponsor_menu_icon_active') }}"
                                    data-max-file-size="50K"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/sponsor_menu_icon_active') }}"
                                    data-allowed-formats="square"
                                    required>
                                </div>

                                <p class="text-info">
                                    <em>* Max size 50KB</em><br>
                                    <em>&nbsp; Square dimensions</em>
                                </p>
                                @else
                                <p>
                                    <a data-lightbox="roadtrip"
                                        href="{{ preferGet('sponsor_menu_icon_active') }}">
                                        <img title="Home Background" style="max-width: 50px;"
                                            src="{{ preferGet('sponsor_menu_icon_active') }}">
                                    </a>
                                </p>
                                <p>
                                    <a class="btn btn-danger btn-delete btn-sm"
                                        onclick="if(!confirm('Are you sure ?')) return false"
                                        href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/sponsor_menu_icon_active') }}">
                                        <i class="fa fa-ban"></i> Delete
                                    </a>
                                </p>
                                <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-group">
                                <label for="booth_floor_menu_icon_active" class="form-control-label">
                                    Booth Floor Plan Icon
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('booth_floor_menu_icon_active'))
                                    <div class="image-square-box">
                                        <input type="file" name="booth_floor_menu_icon_active" id="booth_floor_menu_icon_active"
                                               class="form-control inputfile dropify" accept="image/*"
                                               data-default-file="{{ preferGet('booth_floor_menu_icon_active') }}"
                                               data-max-file-size="50K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_floor_menu_icon_active') }}"
                                               data-allowed-formats="square"
                                               required>
                                    </div>

                                    <p class="text-info">
                                        <em>* Max size 50KB</em><br>
                                        <em>&nbsp; Square dimensions</em>
                                    </p>
                                @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                           href="{{ preferGet('booth_floor_menu_icon_active') }}">
                                            <img title="Home Background" style="max-width: 50px;"
                                                 src="{{ preferGet('booth_floor_menu_icon_active') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                           onclick="if(!confirm('Are you sure ?')) return false"
                                           href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_floor_menu_icon_active') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="booth_list_menu_icon_active" class="form-control-label">
                                    Booth List icon
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                @if(!preferGet('booth_list_menu_icon_active'))
                                    <div class="image-square-box">
                                        <input type="file" name="booth_list_menu_icon_active" id="booth_list_menu_icon_active"
                                               class="form-control inputfile dropify" accept="image/*"
                                               data-default-file="{{ preferGet('booth_list_menu_icon_active') }}"
                                               data-max-file-size="50K"
                                               data-allowed-file-extensions="jpg jpeg png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_list_menu_icon_active') }}"
                                               data-allowed-formats="square"
                                               required>
                                    </div>

                                    <p class="text-info">
                                        <em>* Max size 50KB</em><br>
                                        <em>&nbsp; Square dimensions</em>
                                    </p>
                                @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                           href="{{ preferGet('booth_list_menu_icon_active') }}">
                                            <img title="Home Background" style="max-width: 50px;"
                                                 src="{{ preferGet('booth_list_menu_icon_active') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                           onclick="if(!confirm('Are you sure ?')) return false"
                                           href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_list_menu_icon_active') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Booth</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="booth_audio" class="form-control-label">
                                Booth Audio
                            </label>
                            @if(!preferGet('booth_audio'))
                            <div class="image-square-box">
                                <input type="file" name="booth_audio" id="booth_audio"
                                class="form-control inputfile dropify"
                                data-default-file="{{ preferGet('booth_audio') }}"
                                data-max-file-size="3M"
                                data-allowed-file-extensions="mp3"
                                data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_audio') }}">
                            </div>

                            <p class="text-info">
                                <em>* Max size 3MB</em>
                            </p>
                            @else
                            <p>
                                <audio ref="audioPlayer" controls loop>
                                    <source src="{{ preferGet('booth_audio') }}" type="audio/mp3">
                                </audio>
                            </p>
                            <p>
                                <a class="btn btn-danger btn-delete btn-sm"
                                    onclick="if(!confirm('Are you sure ?')) return false"
                                    href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_audio') }}">
                                    <i class="fa fa-ban"></i> Delete
                                </a>
                            </p>
                            <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="booth_side_banner" class="form-control-label">
                                Booth Side Banner
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            @if(!preferGet('booth_side_banner'))
                            <input type="file" name="booth_side_banner" id="booth_side_banner"
                            class="form-control inputfile dropify" accept="image/*"
                            data-default-file="{{ preferGet('booth_side_banner') }}"
                            data-max-file-size="1M"
                            data-allowed-file-extensions="jpg jpeg png"
                            data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_side_banner') }}"
                            data-height="300"
                            data-allowed-formats="portrait square"
                            required>

                            <p class="text-info">
                                <em>* Max size 1MB</em><br>
                                <em>&nbsp; Portrait or Square dimensions</em>
                            </p>
                            @else
                            <p>
                                <a data-lightbox="roadtrip"
                                    href="{{ preferGet('booth_side_banner') }}">
                                    <img title="Home Background" width="100%"
                                        src="{{ preferGet('booth_side_banner') }}">
                                </a>
                            </p>
                            <p>
                                <a class="btn btn-danger btn-delete btn-sm"
                                    onclick="if(!confirm('Are you sure ?')) return false"
                                    href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_side_banner') }}">
                                    <i class="fa fa-ban"></i> Delete
                                </a>
                            </p>
                            <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Main Menu</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="booth_menu_status" class="form-control-label">
                                                Booth Menu Status
                                            </label>
                                            <label class="switch">
                                                <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="booth_menu_status"
                                                    value="{{ preferGet('booth_menu_status') }}"
                                                    {{ preferGet('booth_menu_status') === 'active' ? 'checked' : '' }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="main_stage_menu_status" class="form-control-label">
                                                Main Stage Menu Status
                                            </label>
                                            <label class="switch">
                                                <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="main_stage_menu_status"
                                                    value="{{ preferGet('main_stage_menu_status') }}"
                                                    {{ preferGet('main_stage_menu_status') === 'active' ? 'checked' : '' }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="classroom_menu_status" class="form-control-label">
                                                Classroom Menu Status
                                            </label>
                                            <label class="switch">
                                                <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="classroom_menu_status"
                                                    value="{{ preferGet('classroom_menu_status') }}"
                                                    {{ preferGet('classroom_menu_status') === 'active' ? 'checked' : '' }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="text-center color-dark" style="margin-bottom: 20px;">Homepage</h5>
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="help_center_link" class="form-control-label">
                                        Help Center Whatsapp
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="text" name="help_center_link" id="help_center_link" maxlength="255" minlength="5"
                                        class="form-control input-custom" required value="{{ preferGet('help_center_link') }}">
                                </div>
                                <div class="form-group">
                                    <label for="home_slider_type" class="form-control-label">
                                        Home Slider Type
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <select name="home_slider_type" id="home_slider_type" required class="form-control input-custom">
                                        <option value="">* Select Slider Type *</option>
                                        <option value="Image" {{ preferGet('home_slider_type') === 'Image' ? 'selected' : '' }}>Image</option>
                                        <option value="Youtube" {{ preferGet('home_slider_type') === 'Youtube' ? 'selected' : '' }}>Youtube</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="iframe_player_type" class="form-control-label">
                                        Player Type
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <select name="iframe_player_type" id="iframe_player_type" required class="form-control input-custom">
                                        <option value="">* Select Player Type *</option>
                                        <option value="Default" {{ preferGet('iframe_player_type') === 'Default' ? 'selected' : '' }}>Default</option>
                                        <option value="Plyr" {{ preferGet('iframe_player_type') === 'Plyr' ? 'selected' : '' }}>Plyr</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="photobooth_status" class="form-control-label">
                                        Photobooth Status
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="photobooth_status"
                                               value="{{ preferGet('photobooth_status') }}"
                                            {{ preferGet('photobooth_status') === 'active' ? 'checked' : '' }}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="photobooth_link" class="form-control-label">
                                        Photo Booth Link
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    <input type="url" name="photobooth_link" id="photobooth_link" maxlength="255" minlength="5"
                                           class="form-control input-custom" required value="{{ preferGet('photobooth_link') }}">
                                </div>
                                <div class="form-group">
                                    <label for="photobooth_image" class="form-control-label">
                                        Photo Booth Background Image
                                    </label>
                                    @if(!preferGet('photobooth_image'))
                                        <input type="file" name="photobooth_image" id="photobooth_image"
                                               class="form-control inputfile dropify" accept="image/*"
                                               data-default-file="{{ preferGet('photobooth_image') }}"
                                               data-max-file-size="50K"
                                               data-allowed-file-extensions="png"
                                               data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/photobooth_image') }}"
                                               data-height="500"
                                               data-allowed-formats="portrait">
                                        <p class="text-info">
                                            <em>* Max size 50KB</em><br>
                                            <em>&nbsp; Portrait dimensions</em>
                                        </p>
                                    @else
                                        <p>
                                            <a data-lightbox="roadtrip"
                                               href="{{ preferGet('photobooth_image') }}">
                                                <img title="Home Background" width="30%"
                                                     src="{{ preferGet('photobooth_image') }}">
                                            </a>
                                        </p>
                                        <p>
                                            <a class="btn btn-danger btn-delete btn-sm"
                                               onclick="if(!confirm('Are you sure ?')) return false"
                                               href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/photobooth_image') }}">
                                                <i class="fa fa-ban"></i> Delete
                                            </a>
                                        </p>
                                        <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Design</h5>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="home_background_image" class="form-control-label">
                                        Home Background Image
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('home_background_image'))
                                    <input type="file" name="home_background_image" id="home_background_image"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('home_background_image') }}"
                                    data-max-file-size="2M"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_background_image') }}"
                                    data-height="300"
                                    data-allowed-formats="landscape"
                                           required>

                                    <p class="text-info">
                                        <em>* Max size 2MB</em><br>
                                        <em>&nbsp; Landscape dimensions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('home_background_image') }}">
                                            <img title="Home Background" width="100%"
                                                src="{{ preferGet('home_background_image') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/home_background_image') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="booth_background_image" class="form-control-label">
                                        Booth Background Image
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('booth_background_image'))
                                    <input type="file" name="booth_background_image" id="booth_background_image"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('booth_background_image') }}"
                                    data-max-file-size="2M"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_background_image') }}"
                                    data-height="300"
                                    data-allowed-formats="landscape"
                                           required>

                                    <p class="text-info">
                                        <em>* Max size 2MB</em><br>
                                        <em>&nbsp; Landscape dimensions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('booth_background_image') }}">
                                            <img title="Home Background" width="100%"
                                                src="{{ preferGet('booth_background_image') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/booth_background_image') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="main_stage_background_image" class="form-control-label">
                                        Main Stage Background Image
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('main_stage_background_image'))
                                    <input type="file" name="main_stage_background_image" id="main_stage_background_image"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('main_stage_background_image') }}"
                                    data-max-file-size="2M"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_background_image') }}"
                                    data-height="300"
                                    data-allowed-formats="landscape"
                                           required>

                                    <p class="text-info">
                                        <em>* Max size 2MB</em><br>
                                        <em>&nbsp; Landscape dimensions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('main_stage_background_image') }}">
                                            <img title="Home Background" width="100%"
                                                src="{{ preferGet('main_stage_background_image') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/main_stage_background_image') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="classroom_background_image" class="form-control-label">
                                        Classroom Background Image
                                        <span class="text-danger" title="This field is required">*</span>
                                    </label>
                                    @if(!preferGet('classroom_background_image'))
                                    <input type="file" name="classroom_background_image" id="classroom_background_image"
                                    class="form-control inputfile dropify" accept="image/*"
                                    data-default-file="{{ preferGet('classroom_background_image') }}"
                                    data-max-file-size="2M"
                                    data-allowed-file-extensions="jpg jpeg png"
                                    data-delete-url="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_background_image') }}"
                                    data-height="300"
                                    data-allowed-formats="landscape"
                                           required>

                                    <p class="text-info">
                                        <em>* Max size 2MB</em><br>
                                        <em>&nbsp; Landscape dimensions</em>
                                    </p>
                                    @else
                                    <p>
                                        <a data-lightbox="roadtrip"
                                            href="{{ preferGet('classroom_background_image') }}">
                                            <img title="Home Background" width="100%"
                                                src="{{ preferGet('classroom_background_image') }}">
                                        </a>
                                    </p>
                                    <p>
                                        <a class="btn btn-danger btn-delete btn-sm"
                                            onclick="if(!confirm('Are you sure ?')) return false"
                                            href="{{ CRUDBooster::adminpath('preference/delete-image-prefer/classroom_background_image') }}">
                                            <i class="fa fa-ban"></i> Delete
                                        </a>
                                    </p>
                                    <p class="text-muted"><em>* If you want to upload other file, please first delete the file.</em></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 50px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@endsection
