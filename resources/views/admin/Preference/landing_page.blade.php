@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    @include('admin.Preference.layout.sidebar')
    <form action="{{ CRUDBooster::adminpath('preference/landing-page') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if(\crocodicstudio\crudbooster\helpers\CRUDBooster::isSuperadmin())
                            <div class="form-group">
                                <label for="landingpage_browser_popup_status" class="form-control-label">
                                    Activate popup browser info ?
                                </label>
                                <label class="switch">
                                    <input type="checkbox" class="checkbox checkbox-toggle checkbox-toggle-1" name="landingpage_browser_popup_status"
                                           value="{{ preferGet('landingpage_browser_popup_status') }}"
                                        {{ preferGet('landingpage_browser_popup_status') === 'active' ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="landingpage_about_title_text" class="form-control-label">
                                Landingpage About Title Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <input type="text" name="landingpage_about_title_text" id="landingpage_about_title_text" maxlength="100" min="5"
                                class="form-control input-custom" required value="{{ preferGet('landingpage_about_title_text') }}">
                        </div>
                        <div class="form-group">
                            <label for="landingpage_about_description_text" class="form-control-label">
                                Landingpage About Description Text
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <textarea type="text" name="landingpage_about_description_text" id="landingpage_about_description_text"
                            min="1" class="form-control input-custom" required style="min-width: 100%;max-width: 100%;height: auto;"
                            rows="8">{{ preferGet('landingpage_about_description_text') }}</textarea>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="landingpage_whatsapp" class="form-control-label">
                                        Whatsapp
                                    </label>
                                    <input type="text" name="landingpage_whatsapp" id="landingpage_whatsapp" maxlength="100" min="5"
                                           class="form-control input-custom" value="{{ preferGet('landingpage_whatsapp') }}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="landingpage_email" class="form-control-label">
                                        Email
                                    </label>
                                    <input type="email" name="landingpage_email" id="landingpage_email" maxlength="100" min="5"
                                           class="form-control input-custom" value="{{ preferGet('landingpage_email') }}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="landingpage_website" class="form-control-label">
                                        Website
                                    </label>
                                    <input type="url" name="landingpage_website" id="landingpage_website" maxlength="100" min="5"
                                           class="form-control input-custom" value="{{ preferGet('landingpage_website') }}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="margin-top: 50px;">
            <button type="submit" class="btn btn-save">Save Changes</button>
        </div>
    </form>
</div>
@push('bottom')
<script src="{{ asset('js/plugin/jquery-uploadPreview/jquery.uploadPreview.js') }}"></script>
<script>
    // $.uploadPreview({
    //     input_field: "#splashScreen",
    //     preview_box: "#imageSplashScreen",
    //     label_field: "#splashscreenLabel",
    //     label_default: "375 : 740",
    //     label_selected: "375 : 740",
    //     no_label: false,
    //     default_image: "http://pertaminaapq.eventy.id/uploads/1/2020-03/splashscreen_apq_awards_2020at2x.png"
    // });
</script>
<script>
    moveUpPath = "{{ CRUDBooster::mainpath('speaker-move-up') }}";
    moveDownPath = "{{ CRUDBooster::mainpath('speaker-move-down') }}";

    new Vue({
        el: '#form-speakers',
        data() {
            return {
                speaker: {
                    id: '',
                    name: '',
                    job_title: '',
                    image: '',
                },
                speakers: [],
                speaker_name: '',
                speaker_job_title: '',
                speaker_image: '',
                dropify: '',
            }
        },
        created() {
            this.list();
        },
        mounted() {
            this.dropify = $('.vue-dropify').dropify();
        },
        watch: {
            speakers(newVal) {
                this.dropify = $('.vue-dropify').dropify();
            },
            speaker(newVal) {
                this.$nextTick(() => {
                    this.dropify = $('.vue-dropify').dropify();
                });
            },
        },
        methods: {
            moveUp(index) {
                window.location.href = moveUpPath + '/' + this.speakers[index].id;
            },
            moveDown(index) {
                window.location.href = moveDownPath + '/' + this.speakers[index].id;
            },
            openModalUpdate(index) {
                this.speaker = {
                    id: this.speakers[index].id,
                    name: this.speakers[index].name,
                    job_title: this.speakers[index].job_title ?? '',
                    image: this.speakers[index].image,
                };

                $('#modal-speaker-update').modal('show');
            },
            clearData() {
                this.speaker_name = '';
                this.speaker_job_title = '';
                this.speaker_image = '';
                $('.vue-dropify').val('');
                $('.dropify-clear').click();
                drEvent = this.dropify.data('dropify');
                drEvent.resetPreview();
                drEvent.clearElement();
            },
            removeFile(id) {
                if (confirm('Are you sure ?')) {
                    axios.post('{{ CRUDBooster::adminPath('preference/speaker-remove-image') }}', { id })
                    .then(() => {
                        this.list();
                        this.speaker.image = '';
                        swal('Successfully deleted image!', '', 'success');
                        this.$nextTick(() => {
                            $('.vue-dropify').dropify();
                        });
                    }).catch((err) => {
                        console.log(err);
                        swal('Failed to delete image!', '', 'error');
                    });
                }
            },
            list() {
                axios.post('{{ CRUDBooster::adminPath('preference/speaker-list') }}')
                    .then(({ data: { item } }) => {
                        this.speakers = item;
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            },
            add() {
                let input = $('#card-add .validate');
                for (var i = 0; i < input.length; i++) {
                    if (this.validate(input[i]) === false) {
                        input[i].focus();

                        return false;
                    }
                }

                let formData = new FormData();
                formData.append('speaker_name', this.speaker_name);
                formData.append('speaker_job_title', this.speaker_job_title);
                formData.append('speaker_image', $('#speaker_image').prop('files')[0]);

                axios.post('{{ CRUDBooster::adminPath('preference/speaker-save') }}', formData)
                    .then(({ data: { item } }) => {
                        this.clearData();
                        this.list();
                        swal('Successfully added data!', '', 'success');
                    })
                    .catch((err) => {
                        console.log(err);
                        swal('Failed to add data!', '', 'error');
                    });
            },
            remove(index) {
                swal({
                    title: 'Are you sure?',
                    text: 'You will not be able to recover this record data!',
                    icon: 'warning',
                    buttons:{
                        confirm: {
                            text : 'Yes!',
                            className : 'btn btn-primary'
                        },
                        cancel: {
                            visible: true,
                            text : 'Cancel',
                            className: 'btn btn-danger'
                        }
                    }
                }).then((isConfirm) => {
                    if (isConfirm) {
                        axios.post('{{ CRUDBooster::adminPath('preference/speaker-remove') }}', {
                            id: this.speakers[index].id,
                        }).then(() => {
                            swal('Successfully deleted data!', '', 'success');
                            this.list();
                        }).catch((err) => {
                            console.log(err);
                            swal('Failed to remove data!', '', 'error');
                        });
                    }
                });
            },
            update() {
                let input = $('#modal-speaker-update .validate');
                for (var i = 0; i < input.length; i++) {
                    if (this.validate(input[i]) === false) {
                        input[i].focus();

                        return false;
                    }
                }

                let formData = new FormData();
                formData.append('id', this.speaker.id);
                formData.append('name', this.speaker.name);
                formData.append('job_title', this.speaker.job_title);
                if (!this.speaker.image) {
                    formData.append('image', $('#modal__speaker_image').prop('files')[0]);
                }

                axios.post('{{ CRUDBooster::adminPath('preference/speaker-update') }}', formData)
                    .then(({ data: { item } }) => {
                        this.clearData();
                        this.speakers = item;

                        $('#modal-speaker-update').modal('hide');
                        swal('Successfully updated data!', '', 'success');
                    })
                    .catch((err) => {
                        console.log(err);
                        swal('Failed to update data!', '', 'error');
                    });
            },

            // helper
            validate(input) {
                if($(input).attr('type') == 'email' || $(input).attr('name') == 'email' && $(input).val().trim() != '') {
                    if($(input).val().trim() == ''){
                        return false;
                    } else {
                        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                            this.setValidate(input, 'Email format is incorrect');
                            return false;
                        }
                    }
                }else {
                    if($(input).val().trim() == ''){
                        return false;
                    }
                }
            },
        }
    });
</script>
@endpush
@endsection
