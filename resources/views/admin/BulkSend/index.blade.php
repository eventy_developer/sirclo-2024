@extends('crudbooster::admin_template')

@push('head')

@endpush

@section('content')
    <div class="content-center">

        <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?env=demo') }}" class="btn {{ g('env') === 'demo' ? 'btn-primary' : 'btn-default' }}">Demo</a>
        <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('?env=production') }}" class="btn {{ g('env') === 'production' ? 'btn-primary' : 'btn-default' }}">Production</a>

        @if(g('env') === 'production')
            <div class="alert alert-danger mt-3" role="alert">
                Be careful. You are in production mode!
            </div>
        @endif

        <div class="row mt-5">
            <div class="col-md-6">
                <h5 class="text-center color-dark" style="margin-bottom: 20px;">Email</h5>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ CRUDBooster::adminpath('bulk-send/email') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" name="env" value="{{ g('env') }}">
                            <div class="form-group">
                                <label for="email_template_id" class="form-control-label">
                                    Templates
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                <select name="email_template_id" id="email_template_id" required class="form-control input-custom">
                                    <option value="">* Please select a Templates *</option>
                                    @foreach($email_templates as $template)
                                        <option value="{{ $template->id }}">{{ $template->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div style="margin-top: 50px;">
                                <button type="submit" class="btn btn-success">Send Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('bottom')
@endpush
