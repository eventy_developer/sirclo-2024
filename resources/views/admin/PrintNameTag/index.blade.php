@extends('crudbooster::admin_template')

@include('admin.Layout.plugin.css')
@include('admin.Layout.plugin.js')

@section('content')
<div class="content-center">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ CRUDBooster::mainpath() }}" method="GET">
                        <div class="form-group">
                            <label for="qr_code" class="form-control-label">
                                QR Code
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="d-flex">
                                <form action="{{ CRUDBooster::mainpath() }}" method="GET">
                                    <input type="text" name="qr_code" id="qr_code" maxlength="100" minlength="5"
                                        class="form-control input-custom mr-2" required value="{{ $qrcode ?? old('qr_code') }}">
                                    
                                    @if (request()->has('qr_code'))
                                        <a href="{{ url()->current() }}">
                                            <button type="button" class="btn btn-outline-danger rounded-pill mr-2 px-4">Clear</button>
                                        </a>
                                        <a href="{{ CRUDBooster::mainpath('print') }}?qr_code={{ $qrcode }}" target="_blank">
                                            <button type="button" class="btn btn-primary rounded-pill px-4">Print</button>
                                        </a>
                                    @else
                                        <button type="submit" class="btn btn-success rounded-pill px-4">Search</button>
                                    @endif
                            </div>
                        </div>
                    </form>
                    @if ($data)
                        <div class="form-group">
                            <label for="name" class="form-control-label">
                                Name
                            </label>
                            <input type="text" name="name" id="name" class="form-control input-custom" value="{{ $data->name ?? '-' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="company" class="form-control-label">
                                Company
                            </label>
                            <input type="text" name="company" id="company" class="form-control input-custom" value="{{ $data->company ?? '-' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ticket" class="form-control-label">
                                Ticket
                            </label>
                            <input type="text" name="ticket" id="ticket" class="form-control input-custom" value="{{ $data->ticket->name ?? '-' }}" disabled>
                        </div>

                        <div class="form-group">
                            <label for="print_at" class="form-control-label">
                                Print At
                            </label>
                            <input type="text" name="print_at" id="print_at" class="form-control input-custom" value="{{ $data->nametag->printed_date ?? '-' }}" disabled>
                        </div>
                        @if ($data->participantGroup)
                            <label for="name" class="form-control-label">
                                <b>Related Participants Members</b>
                            </label>
                            @foreach ($data->participantGroup as $item)
                                <div class="form-group">
                                    <label for="name" class="form-control-label">
                                        Name
                                    </label>
                                    <div class="d-flex">
                                        <input type="text" name="name" id="name" class="form-control input-custom" value="{{ $item->name ?? '-' }}" disabled>
                                        <a href="{{ CRUDBooster::mainpath('print') }}?qr_code={{ $item->qr_code }}" target="_blank">
                                            <button type="button" class="btn btn-primary rounded-pill px-4 ml-2">Print</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="print_at" class="form-control-label">
                                        Print At
                                    </label>
                                    <input type="text" name="print_at" id="print_at" class="form-control input-custom" value="{{ $item->nametag->printed_date ?? '-' }}" disabled>
                                </div>
                            @endforeach
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
