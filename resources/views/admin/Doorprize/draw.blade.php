<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doorprize Items</title>
    <link rel="shortcut icon" href="{{ preferGet('icon_events') }}">
    <link rel="stylesheet" href="{{ asset('packages/bootstrap5/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/swiper/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/Doorprize/doorprize.css') . '?v=' . date('YmdH') }}">
    <style>
        .custom-doorprize-item__title {
            white-space: normal;
            width: 80%;
            font-size: 12px;
        }
        @media (min-width: 1400px) {
            .custom-doorprize-item__title {
                font-size: 14px;
            }
        }
        @media (min-width: 1900px) {
            .custom-doorprize-item__title {
                font-size: 20px;
            }
        }
        .row-scroll {
            max-height: 395px;
            overflow-x: auto;
        }
        @media (min-width: 1400px) {
            .row-scroll {
                max-height: 405px;
            }
        }
        @media (min-width: 1600px) {
            .row-scroll {
                max-height: 405px;
            }
        }
        @media (min-width: 1900px) {
            .row-scroll {
                max-height: 620px;
            }
        }
        .row-scroll::-webkit-scrollbar {
            width: 5px;
        }
        .row-scroll::-webkit-scrollbar-track {
            background: #DADADA;
            border-radius: 15px;
        }
        .row-scroll::-webkit-scrollbar-thumb {
            background: #B2B2B2;
            border-radius: 15px;
        }
        .row-scroll::-webkit-scrollbar-thumb:hover {
            background: #999999;
        }
    </style>
</head>
<body>

<div class="container container-custom">
    <div class="row">
        <div class="col-lg-12">
            <div class="img-group">
                <img src="{{ asset('images/doorprize/img_logo_event@2x.png') }}" alt="" class="img-center img-fluid" style="max-width: 15%; height: auto;">
            </div>

            <div class="text-center">
                <h1 class="doorprize-title border-white fs-3 mb-4">SELECT DOORPRIZE ITEMS</h1>
            </div>
        </div>
    </div>
    <div class="row position-relative justify-content-center">
        <div class="col-lg-8">
            <div class="row row-scroll">
                @foreach($doorprizes as $doorprize)
                    <div class="col-lg-4">
                        <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('button?id=' . $doorprize->id) }}" class="custom-doorprize-item">
                            @if(dateTime() < $doorprize->available_at)
                                <img src="{{ asset('images/doorprize/item_box_nextdraw.svg') }}" alt="" class="w-100">
                                <h3 class="custom-doorprize-item__title" style="color: #041423;">
                                    @if($doorprize->draw_type === 'General')
                                        <span class="d-inline-block mb-2">{{ $doorprize->present_name }}</span> <br> <span>(Next Draw)</span>
                                    @else
                                        <span class="d-inline-block mb-2">{{ $doorprize->present_name }}</span> <br> <span>(Grand Prize)</span>
                                    @endif
                                </h3>
                            @else
                                @if($doorprize->is_drawn)
                                    <img src="{{ asset('images/doorprize/item_box_generated.svg') }}" alt="" class="w-100">
                                    <h3 class="custom-doorprize-item__title text-white">
                                        <span class="d-inline-block mb-2">{{ $doorprize->present_name }}</span> <br> <span>(Done)</span>
                                    </h3>
                                @else
                                    <img src="{{ asset('images/doorprize/item_box_ready.svg') }}" alt="" class="w-100">
                                    <h3 class="custom-doorprize-item__title {{ $doorprize->is_drawn ? '' : 'text-white' }}">
                                        <span class="d-inline-block mb-2">{{ $doorprize->present_name }}</span> @if($doorprize->is_drawn) <br> (Done) @else <br> <span>(Ready)</span> @endif
                                    </h3>
                                @endif
                            @endif
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>



<script src="{{ asset('packages/jquery/jquery-3.6.1.min.js') }}"></script>
<script src="{{ asset('packages/bootstrap5/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('packages/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('packages/sweetalert@2/sweetalert2@9.js') }}"></script>
<script>
    @if(session()->has('message'))
    Swal.fire({
        icon: 'warning',
        title: '{{ session()->get('message') }}',
        text: '',
    });
    @endif

    $(document).ready(function () {
        new Swiper('.swiper-doorprizes', {
            slidesPerView: 2,
            spaceBetween: 20,

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
                draggable: true,
                dragSize: 40
            },

            breakpoints: {
                992: {
                    slidesPerView: 5,
                }
            }
        });
    });
</script>
</body>
</html>
