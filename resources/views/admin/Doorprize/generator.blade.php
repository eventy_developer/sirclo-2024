@extends('crudbooster::admin_template')

@push('head')
    <style>
        .card-doorprize__item {
            color: #34395F;
        }
        .card-doorprize__name {
            background: #F5F5FB;
            border-radius: 10px;
            padding: 7px 15px;
            margin-bottom: 20px;
            color: #34395F;
        }
        .wrapper-body {
            padding: 15px;
            border-radius: 10px;
            background: #F5F5FB;
        }
        .card-doorprize__list {
            padding-bottom: 20px;
            padding-left: 40px;
            color: #34395F;
        }
        .card-doorprize__list__wrapper {
            min-height: 300px;
            overflow-x: hidden;
            padding-right: 15px;
        }
        .row-scroll {
            max-height: 804px;
            overflow-x: hidden;
        }
        .card-doorprize__list__wrapper::-webkit-scrollbar,
        .row-scroll::-webkit-scrollbar{
            width: 5px;
        }
        .card-doorprize__list__wrapper::-webkit-scrollbar-track,
        .row-scroll::-webkit-scrollbar-track {
            background: #DDCDCD;
            border-radius: 2.5px;
        }
        .card-doorprize__list__wrapper::-webkit-scrollbar-thumb,
        .row-scroll::-webkit-scrollbar-thumb {
            background: #B3ADAD;
            border-radius: 2.5px;
        }
        .card-doorprize__list__wrapper::-webkit-scrollbar-thumb:hover,
        .row-scroll::-webkit-scrollbar-thumb:hover {
            background: #999999;
        }

        .btn-generate {
            background: #00B2F9;
            border-radius: 10px;
            margin-top: 50px;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="row row-scroll">
                    @foreach($doorprizes as $doorprize)
                        <div class="col-lg-6">
                            <div class="card-doorprize mb-5">
                                <p class="card-doorprize__item mb-2">Item Name {{ $loop->iteration }}</p>

                                <h6 class="card-doorprize__name">{{ $doorprize->name }}</h6>

                                <div class="wrapper-body">
                                    <div class="card-doorprize__list__wrapper" style="max-height: 346px;min-height: 346px;">
                                        <ol class="card-doorprize__list mb-5">
                                            @foreach($doorprize->participants as $participant)
                                                <li>{{ $participant->name }}, {{ $participant->full_registration_number }}</li>
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
{{--                    <div class="row">--}}
{{--                        <div class="col-lg-12 text-center">--}}
{{--                            <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('generate-doorprize') }}" class="btn btn-primary btn-generate">Generate Data</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <p class="card-doorprize__item mb-2">User Success Check-In</p>

                    <div class="wrapper-body">
                        <div class="card-doorprize__list__wrapper" style="max-height: 346px;min-height: 346px;">
                            <ol class="card-doorprize__list mb-5">
                                @foreach($participants as $participant)
                                    <li>{{ $participant->name }}, {{ $participant->full_registration_number }}</li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('bottom')
@endpush
