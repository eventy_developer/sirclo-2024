<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Draw Winner</title>
    <link rel="shortcut icon" href="{{ preferGet('icon_events') }}">
    <link rel="stylesheet" href="{{ asset('packages/bootstrap5/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/swiper/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/Doorprize/doorprize.css') . '?v=' . date('YmdH') }}">
    <style>
        #modalSpinner .modal-content {
            background: transparent;
            border: none;
        }
        @media (min-width: 992px) {
            #modalSpinner .modal-lg,
            #modalSpinner .modal-xl {
                --bs-modal-width: 700px;
            }
        }
        .winner-title {
            font-size: 13px;
            background: rgba(217, 105, 45, 0.80);
            border: 1px solid rgba(217, 105, 45, 0.80);
            border-radius: 50px;
            padding: 8px 20px;
            color: #fff;
            margin-bottom: 20px;
            text-align: center;
            min-height: 55px;
            display: flex;
            align-items: center;
            flex-direction: column;
        }
        .winner-title span {
            margin-bottom: 5px;
            font-family: 'Inter-Bold', sans-serif;
        }
        .winner-title span:nth-child(2) {
            font-size: 11px;
        }
        .winner-title span:last-child {
            margin-bottom: 0;
        }
        @media (min-width: 1400px) {
            .winner-title {
                font-size: 16px;
            }
            .winner-title span:nth-child(2) {
                font-size: 14px;
            }
        }
        @media (min-width: 1900px) {
            .winner-title {
                font-size: 22px;
            }
            .winner-title span:nth-child(2) {
                font-size: 16px;
            }
        }
        #audio {
            display: none;
        }
        .success-title {
            margin-bottom: 20px;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            color: #fff;
        }
        .countdown {
            border-radius: 12px;
            background: rgba(170, 54, 120, 0.80);
            padding: 10px;
            display: flex;
            margin-top: 12px;
        }
        @media (min-width: 1600px) {
            .countdown {
                margin-top: 15px;
            }
        }
        .countdown__time,
        .countdown span {
            color: #fff;
            font-size: 30px;
            font-family: 'Inter-Bold', sans-serif;
        }
        @media (min-width: 1600px) {
            .countdown__time,
            .countdown span {
                font-size: 40px;
            }
            .success-title {
                font-size: 18px;
                margin-bottom: 30px;
            }
        }
        @media (min-width: 1900px) {
            .countdown__time,
            .countdown span {
                font-size: 60px;
            }
            .success-title {
                font-size: 20px;
            }
        }
        .pickup-info {
            border-radius: 10px;
            background: rgba(29, 166, 217, 0.80);
            padding: 10px;
            display: inline-block;
            color: #fff;
            font-size: 14px;
            font-family: 'Inter-Bold', sans-serif;
            position: fixed;
            bottom: -10px; 
            left: 50%;
            transform: translateX(-50%);
            z-index: 999;
            text-align: center;
        }
        @media (min-width: 1600px) {
            .pickup-info {
                font-size: 16px;
            }
        }
        @media (min-width: 1900px) {
            .pickup-info {
                font-size: 22px;
            }
        }
        .btn-group-left {
            position: absolute;
            left: 0;
            bottom: 20px;
        }

        #modalDoorprize .modal-dialog {
            width: 400px;
        }
        #modalDoorprize .modal-content {
            border-radius: 12px;
            background: #041423;
            box-shadow: 0px 4px 10px 0px rgba(0, 0, 0, 0.25);
        }
        #modalDoorprize .modal-body {
            padding: 15px;
        }
        #modalDoorprize .modal-doorprize-title {
            font-size: 20px;
            color: #fff;
            text-align: center;
            margin-bottom: 15px;
        }
        #modalDoorprize .winner-title {
            margin-left: auto;
            margin-right: auto;
            width: 80%;
        }
        #modalDoorprize .scroll-doorprize {
            height: 305px;
            overflow-x: auto;
        }
        #modalDoorprize .scroll-doorprize::-webkit-scrollbar {
            width: 6px;
        }
        #modalDoorprize .scroll-doorprize::-webkit-scrollbar-track {
            background-color: #EFF2F6;
            border-radius: 4px;
        }
        #modalDoorprize .scroll-doorprize::-webkit-scrollbar-thumb {
            background-color: #CCD1D8;
            border-radius: 4px;
        }
        #modalDoorprize .scroll-doorprize::-webkit-scrollbar-thumb:hover {
            background-color: #A7ACB4;
        }
        #modalDoorprize .btn-close-custom {
            background: transparent;
            font-size: 24px;
            border: none;
        }
        #modalDoorprize .btn-reset {
            border-radius: 12px;
            background: rgba(29, 166, 217, 0.80);
            color: #fff;
            font-size: 16px;
            font-family: 'Inter-Bold', sans-serif;
            display: flex;
            align-items: center;
        }
        #modalDoorprize button {
            width: 50%;
        }

        .swiper-doorprizes .swiper-slide .row .col-lg-12:last-child .winner-title {
            margin-bottom: 0;
        }
        .fixed-buttons {
            position: fixed;
            bottom: 10px;
            left: 20px;
            display: flex;
            flex-direction: row;
            gap: 10px;
        }
        .fixed-buttons .btn-back {
            background-color: rgba(29, 166, 217, 0.80);
            color: white;
            border: none;
            border-radius: 5px;
            font-size: 16px;
            text-align: center;
            text-decoration: none;
            cursor: pointer;
            transition: background-color 0.3s;
        }
    </style>
</head>
<body>

<!-- Modal -->
<div class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" id="modalSpinner" tabindex="-1" aria-labelledby="modalSpinnerLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{ asset('images/doorprize/spin_motion_new.gif') }}" alt="" class="w-100">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDoorprize" tabindex="-1" aria-labelledby="modalDoorprizeLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h1 class="modal-doorprize-title">({{ $doorprize->is_left }}) Belum ambil doorprize</h1>

                <div class="scroll-doorprize">
                    @foreach($doorprize->participants_left as $participant)
                    <h3 class="winner-title">
                        <span>{{ $participant->name }}</span>
                        <span>{{ str_limit($participant->company, 27) }}</span>
                        <span>{{ $participant->full_registration_number }}</span>
                    </h3>
                    @endforeach
                </div>

                <div class="d-flex justify-content-between mt-1">
                    <button type="button" class="btn btn-secondary btn-close-custom" data-bs-dismiss="modal">Nanti</button>
                    <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('button?id=' . g('id') . '&reset=1') }}" class="btn btn-primary btn-reset">Reset Doorprize</a>
                </div>
            </div>
        </div>
    </div>
</div>

<audio id="audio">
    <source src="{{ asset('images/doorprize/spin_audio.mp3') }}" type="audio/mp3">
    Your browser does not support the audio element.
</audio>

<div class="container container-custom">
    <div class="row">
        <div class="col-lg-12">
            <div class="img-group">
                <img src="{{ asset('images/doorprize/img_logo_event@2x.png') }}" alt="" class="img-center">
            </div>

            <h1 class="doorprize-title mb-3" id="title-spinner" @if($doorprize->is_drawn) style="display: none;" @endif>DRAW AND FIND THE WINNERS!</h1>
            <h1 class="doorprize-title doorprize-title--green mt-3" @if($doorprize->is_drawn) style="display: none;" @endif>({{ $doorprize->name }})</h1>
            <div id="title-winner" @if(!$doorprize->is_drawn) style="display: none;" @endif>
                <h1 class="doorprize-title mb-0">DOORPRIZE WINNERS</h1>
                <h1 class="doorprize-title doorprize-title--green mt-2 mb-2">({{ $doorprize->name }})</h1>
            </div>
        </div>
    </div>
    <div class="row" id="row-spinner" @if($doorprize->is_drawn) style="display: none;" @endif>
        <div class="col-lg-12 text-center">
            <img src="{{ asset('images/doorprize/img_spin.svg') }}" alt="" class="d-block m-auto img-draw">

        </div>
    </div>
    @if ($doorprize->is_drawn)
        <p class="success-title mb-2">Selamat Kepada Para Pemenang!</p>
    @endif
    <div class="row position-relative" id="row-winner" @if(!$doorprize->is_drawn) style="display: none;" @endif>
        <div class="swiper swiper-doorprizes">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper {{ count($doorprize->participants) < 20 ? 'justify-content-center' : '' }}">
                @foreach($doorprize->participants->chunk($chunk_size) as $participant_chunks)

                    <!-- Slides -->
                    <div class="swiper-slide">
                        <div class="row">
                            @foreach($participant_chunks as $participant)
                                <div class="col-lg-12">
                                    <div class="px-2">
                                        <h3 class="winner-title">
                                            <span>{{ $participant->name }}</span>
                                            <span>{{ str_limit($participant->company, 27) }}</span>
                                            <span>{{ $participant->full_registration_number }}</span>
                                        </h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                @endforeach

            </div>

            <!-- If we need scrollbar -->
            <div class="swiper-scrollbar"></div>
        </div>

        <!-- If we need navigation buttons -->
        {{-- <div class="swiper-slider-navigation">
            <div class="swiper-button-prev">
                <svg width="53" height="38" viewBox="0 0 53 38" fill="none" xmlns="http://www.w3.org/2000/svg" class="swiper-slider-arrow">
                    <path d="M40.4291 34.1145C40.8744 33.6691 41.1245 33.0651 41.1245 32.4354C41.1245 31.8056 40.8744 31.2016 40.4291 30.7563L28.6729 19L40.4291 7.24378C40.8618 6.79585 41.1012 6.19592 41.0957 5.5732C41.0903 4.95049 40.8406 4.3548 40.4002 3.91446C39.9599 3.47412 39.3642 3.22434 38.7415 3.21893C38.1188 3.21352 37.5188 3.45291 37.0709 3.88553L23.6355 17.3209C23.1903 17.7663 22.9402 18.3703 22.9401 19C22.9401 19.6298 23.1903 20.2338 23.6355 20.6791L37.0709 34.1145C37.5163 34.5598 38.1203 34.8099 38.75 34.8099C39.3798 34.8099 39.9838 34.5598 40.4291 34.1145Z" fill="url(#paint0_linear_86_249)"/>
                    <path d="M25.4291 34.1145C25.8744 33.6691 26.1245 33.0651 26.1245 32.4354C26.1245 31.8056 25.8744 31.2016 25.4291 30.7563L13.6729 19L25.4291 7.24378C25.8618 6.79585 26.1012 6.19592 26.0957 5.5732C26.0903 4.95049 25.8406 4.3548 25.4002 3.91446C24.9599 3.47412 24.3642 3.22434 23.7415 3.21893C23.1188 3.21352 22.5188 3.45291 22.0709 3.88553L8.63552 17.3209C8.19028 17.7663 7.94015 18.3703 7.94015 19C7.94015 19.6298 8.19028 20.2338 8.63552 20.6791L22.0709 34.1145C22.5163 34.5598 23.1203 34.8099 23.75 34.8099C24.3798 34.8099 24.9838 34.5598 25.4291 34.1145Z" fill="url(#paint1_linear_86_249)"/>
                    <defs>
                        <linearGradient id="paint0_linear_86_249" x1="22.9401" y1="34.8099" x2="24.6388" y2="2.35597" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#0C4DA2"/>
                            <stop offset="1" stop-color="#0C4DA2"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_86_249" x1="7.94014" y1="34.8099" x2="9.63881" y2="2.35597" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#0C4DA2"/>
                            <stop offset="1" stop-color="#0C4DA2"/>
                        </linearGradient>
                    </defs>
                </svg>
            </div>
            <div class="swiper-button-next">
                <svg width="53" height="38" viewBox="0 0 53 38" fill="none" xmlns="http://www.w3.org/2000/svg" class="swiper-slider-arrow">
                    <path d="M40.4291 34.1145C40.8744 33.6691 41.1245 33.0651 41.1245 32.4354C41.1245 31.8056 40.8744 31.2016 40.4291 30.7563L28.6729 19L40.4291 7.24378C40.8618 6.79585 41.1012 6.19592 41.0957 5.5732C41.0903 4.95049 40.8406 4.3548 40.4002 3.91446C39.9599 3.47412 39.3642 3.22434 38.7415 3.21893C38.1188 3.21352 37.5188 3.45291 37.0709 3.88553L23.6355 17.3209C23.1903 17.7663 22.9402 18.3703 22.9401 19C22.9401 19.6298 23.1903 20.2338 23.6355 20.6791L37.0709 34.1145C37.5163 34.5598 38.1203 34.8099 38.75 34.8099C39.3798 34.8099 39.9838 34.5598 40.4291 34.1145Z" fill="url(#paint0_linear_86_249)"/>
                    <path d="M25.4291 34.1145C25.8744 33.6691 26.1245 33.0651 26.1245 32.4354C26.1245 31.8056 25.8744 31.2016 25.4291 30.7563L13.6729 19L25.4291 7.24378C25.8618 6.79585 26.1012 6.19592 26.0957 5.5732C26.0903 4.95049 25.8406 4.3548 25.4002 3.91446C24.9599 3.47412 24.3642 3.22434 23.7415 3.21893C23.1188 3.21352 22.5188 3.45291 22.0709 3.88553L8.63552 17.3209C8.19028 17.7663 7.94015 18.3703 7.94015 19C7.94015 19.6298 8.19028 20.2338 8.63552 20.6791L22.0709 34.1145C22.5163 34.5598 23.1203 34.8099 23.75 34.8099C24.3798 34.8099 24.9838 34.5598 25.4291 34.1145Z" fill="url(#paint1_linear_86_249)"/>
                    <defs>
                        <linearGradient id="paint0_linear_86_249" x1="22.9401" y1="34.8099" x2="24.6388" y2="2.35597" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#0C4DA2"/>
                            <stop offset="1" stop-color="#0C4DA2"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_86_249" x1="7.94014" y1="34.8099" x2="9.63881" y2="2.35597" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#0C4DA2"/>
                            <stop offset="1" stop-color="#0C4DA2"/>
                        </linearGradient>
                    </defs>
                </svg>
            </div>
        </div> --}}
    </div>
    <div class="row position-relative">
        @if(!$doorprize->is_drawn)
        <div class="col-lg-12 text-center">
            <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('') }}" class="btn btn-back" style="margin-top: 60px;">Back</a>
        </div>
        @else
            <div class="col-lg-12 text-center">
                <div class="d-inline-block">
                    @if($doorprize->draw_type === 'General')
                    <div class="countdown">
                        <div class="countdown__time" id="countdown-hours">00</div>
                        <span>:</span>
                        <div class="countdown__time" id="countdown-minutes">00</div>
                        <span>:</span>
                        <div class="countdown__time" id="countdown-seconds">00</div>
                    </div>
                    @endif
                </div>
            <br>
            @if($doorprize->draw_type === 'General')
                <p class="pickup-info">📌 {{ preferGet('pickup_words') }}</p>
            @else
                <p class="pickup-info">📌 {{ preferGet('pickup_words_grand') }}</p>
            @endif
            <br>
            <div class="fixed-buttons">
                <a href="{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('') }}" class="btn btn-back">
                    Back
                </a>
                <a href="javascript:location.reload();" class="btn btn-back">
                    Reload
                </a>
            </div>
        </div>
        @endif
    </div>
</div>


<script src="{{ asset('packages/jquery/jquery-3.6.1.min.js') }}"></script>
<script src="{{ asset('packages/bootstrap5/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('packages/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('packages/axios/axios.min.js') }}"></script>
<script>
    token = '{{ csrf_token() }}';
    $(document).ready(function () {
        const selectors = {
            hours: document.getElementById('countdown-hours'),
            minutes: document.getElementById('countdown-minutes'),
            seconds: document.getElementById('countdown-seconds'),
        }
        countDownClock(selectors, {{ $doorprize->present_countdown }}, 'seconds');

        new Swiper('.swiper-doorprizes', {
            slidesPerView: 2,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
                draggable: true,
                dragSize: 40
            },

            breakpoints: {
                992: {
                    slidesPerView: 5,
                }
            }
        });

        var modalSpinner = new bootstrap.Modal(document.getElementById('modalSpinner'));
        var modalDoorprize = new bootstrap.Modal(document.getElementById('modalDoorprize'));

        var audio = document.getElementById("audio");
        http = axios.create({
            baseURL: '{{ url('/') }}',
            headers: {
                'X-CSRF-TOKEN': token,
                'X-Requested-With': 'XMLHttpRequest'
            }
        });

        function countDownClock(selectors, number = 100, format = 'seconds') {
            if (number < 0) return false;

            const hoursElement = selectors.hours;
            const minutesElement = selectors.minutes;
            const secondsElement = selectors.seconds;
            let countdown;
            convertFormat(format);


            function convertFormat(format) {
                switch (format) {
                    case 'seconds':
                        return timer(number);
                    case 'minutes':
                        return timer(number * 60);
                    case 'hours':
                        return timer(number * 60 * 60);
                    case 'days':
                        return timer(number * 60 * 60 * 24);
                }
            }

            function timer(seconds) {
                const now = Date.now();
                const then = now + seconds * 1000;

                countdown = setInterval(() => {
                    const secondsLeft = Math.round((then - Date.now()) / 1000);

                    if (secondsLeft < 0) {
                        clearInterval(countdown);
                        window.location.reload();
                        return;
                    }


                    displayTimeLeft(secondsLeft);

                }, 1000);
            }

            function displayTimeLeft(seconds) {
                const hours = Math.floor((seconds % 86400) / 3600);
                const minutes = Math.floor((seconds % 86400) % 3600 / 60);

                hoursElement.textContent = hours < 10 ? `0${hours}` : hours;
                minutesElement.textContent = minutes < 10 ? `0${minutes}` : minutes;
                secondsElement.textContent = seconds % 60 < 10 ? `0${seconds % 60}` : seconds % 60;
            }
        }

        $('.img-draw').click(function () {
            $('#title-spinner').hide();
            $('#row-spinner').hide();
            audio.play();

            modalSpinner.toggle();

            // update is_drawn to 1
            http.post('{{ \crocodicstudio\crudbooster\helpers\CRUDBooster::mainpath('draw-winner') }}', {
                '_token': token,
                'doorprize_id': '{{ $doorprize->id }}',
            }).then(({ data : { data } }) => {

                $('#title-winner').show();
                $('#row-winner').show();
                setTimeout(() => {
                    audio.pause();

                    window.location.reload();
                }, 5000);
            }).catch((err) => {
                modalSpinner.toggle();

                if (typeof err.response.status != 'undefined' && err.response.status != null) {
                    let status = err.response.status.toString();
                    if (status === '419') {
                        window.location.reload();
                    }

                    if (err.response && !status.startsWith('5')) {
                        alert(err.response.data.message)
                    } else {
                        alert(err.message)
                    }
                }

                if (!err.response) {
                    console.log(err);
                    alert(err.message);
                }
            });
        });
    });
</script>
</body>
</html>
