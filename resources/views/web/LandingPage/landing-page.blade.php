<!DOCTYPE html>
<html lang="en" class="notranslate" translate="no">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google" content="notranslate">
    <link rel="shortcut icon" href="{{ preferGet('icon_events') }}">

    <title>{{ preferGet('name_events') }}</title>

    <style>
        :root {
            /* color */
            --eventy-color-primary: {{ preferGet('color_primary') }}!important;
            --eventy-color-primary-light: {{ adjustColorBrightness(preferGet('color_primary'), 0.2) }}!important;
            --eventy-color-primary-light-80: {{ adjustColorBrightness(preferGet('color_primary'), 0.8) }}!important;
            --eventy-color-secondary: {{ preferGet('color_secondary') }}!important;
            --eventy-color-secondary-light: {{ adjustColorBrightness(preferGet('color_secondary'), 0.2) }}!important;
            --eventy-color-secondary-light-50: {{ adjustColorBrightness(preferGet('color_secondary'), 0.5) }}!important;

            --eventy-font-family: 'Inter-Regular';
            --eventy-font-family-bold: 'Inter-Bold';
            --eventy-font-family-semibold: 'Inter-SemiBold';
            --eventy-font-family-medium: 'Inter-Medium';

            --eventy-header-height: 80px;
        }
        /*  */

        .select-item--two {
            min-width: 30%;
        }
        @media (min-width: 992px) {
            .select-item--two {
                min-width: 15%;
            }
        }
    </style>

    @include('layout.plugin.css')

    <link rel="stylesheet" href="{{ asset('css/web/LandingPage/landing-page.css') }}">

</head>
<body data-bs-spy="scroll" data-bs-target="#navbar-eventy" data-bs-offset="160">

    <x-preloader/>

    <!-- Navbar -->
    <nav id="navbar-eventy" class="navbar navbar-expand-lg navbar-light navbar-eventy fixed-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ preferGet('logo_landingpage') }}" alt="Logo Header" class="img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                <ul class="navbar-nav nav mx-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <div class="nav-gap">
                            <a class="nav-link" aria-current="page" href="#about">About</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-gap">
                            <a class="nav-link" href="#speakers">Speakers</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-gap">
                            <a class="nav-link" href="#schedule">Schedule</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-gap">
                            <a class="nav-link" href="#sponsor">Sponsor</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-gap">
                            <a class="nav-link" href="#contact">Contact</a>
                        </div>
                    </li>
                </ul>

                @if(auth()->check())
                    <div class="d-flex flex-column flex-lg-row justify-content-center align-items-center m-auto m-lg-0">
                        <a href="{{ url('logout') }}" class="btn btn-red btn-logout me-lg-4 mb-4 mb-lg-0">Log Out</a>
                    </div>
                @else
                    <a href="{{ url('login') }}" class="btn btn-main m-auto m-lg-0">Log In</a>
                @endif

            </div>
        </div>
    </nav>

    <main class="main" id="root">

        <!-- Hero -->
        <section class="hero-section">
            <div class="container">
                @if(count($banners) > 0)
                <div class="row mb-80">
                    <div class="col-lg-12">
                        <div class="swiper swiper-banners">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">

                                <!-- Slides -->
                                @foreach($banners as $banner)
                                    <div class="swiper-slide">
                                        <img src="{{ $banner->present_image }}" alt="Landing Page Poster" class="w-100 img-fluid">
                                    </div>
                                @endforeach

                            </div>

                            <!-- If we need scrollbar -->
                            <div class="swiper-scrollbar"></div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row mb-40">
                    <div class="col-lg-8 hero-left">
                        <h1 class="section-title--large mb-40 text-center text-md-start">{{ preferGet('name_events') }}</h1>

                        @if(!auth()->check())
                        <div class="hero-left__btn">
                            <a href="{{ url('login') }}" class="btn btn-main btn-main--md d-block mb-4 mb-md-0">Log In</a>
                            <a href="{{ url('register') }}" class="btn btn-outline-main btn-outline-main--md d-block">Registration</a>
                        </div>
                        @else
                            <div class="hero-left__btn">
                                <a href="{{ url('home') }}" class="btn btn-main btn-main--md d-inline-block">
                                    @if(gSession('status') === 'Active')
                                        Show QR Check-In
                                    @elseif(in_array(gSession('status'), ['Waiting Payment', 'Waiting Confirmation', 'Payment Changed', 'Pending']))
                                        Continue to Payment
                                    @else
                                        Please book a ticket
                                    @endif
                                </a>
                            </div>
                        @endif

                    </div>
                    <div class="col-lg-4 hero-right">
                        <h1 class="section-title--large">Event Date</h1>
                        <h1 class="section-title--medium mb-20" style="font-style: italic;">2 Days</h1> <!-- TODO : dynamic date -->

                        <p class="date-info-text"><span class="label">Start</span> : {{ carbon(preferGet('date_start_events'))->isoFormat('dddd, DD MMMM YYYY') }}</p>
                        <p class="date-info-text"><span class="label">End</span> : {{ carbon(preferGet('date_end_events'))->isoFormat('dddd, DD MMMM YYYY') }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="countdown">
                            <div class="countdown__card">
                                <div class="countdown__card__inner">
                                    <div class="countdown__card__number" id="countdown-days">00</div>
                                    <div class="countdown__card__text">Days</div>
                                </div>
                            </div>
                            <div class="countdown__card">
                                <div class="countdown__card__inner">
                                    <div class="countdown__card__number" id="countdown-hours">00</div>
                                    <div class="countdown__card__text">Hours</div>
                                </div>
                            </div>
                            <div class="countdown__card">
                                <div class="countdown__card__inner">
                                    <div class="countdown__card__number" id="countdown-minutes">00</div>
                                    <div class="countdown__card__text">Minutes</div>
                                </div>
                            </div>
                            <div class="countdown__card">
                                <div class="countdown__card__inner">
                                    <div class="countdown__card__number" id="countdown-seconds">00</div>
                                    <div class="countdown__card__text">Seconds</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- About -->
        <section id="about" class="about-section pt-80">
            <div class="container">
                <div class="row justify-content-center">
                    @if($youtube)
                        <div class="col-lg-6">
                            <div class="about__banner">
                                <img src="{{ $youtube->present_image }}" alt="Banner About" class="img-fluid mw-100 mb-5 mb-lg-0">
                                <img src="{{ asset('images/icon/landingpage/icon_play.svg') }}"
                                     alt="Icon PLay"
                                     class="about__icon-play"
                                     data-video-id="{{ $youtube->youtube_url }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h1 class="section-title mb-20 text-center text-lg-start">{{ preferGet('landingpage_about_title_text') ?? 'Launching Virtual Event v3 by Eventy' }}</h1>
                            <p class="section-desc">{{ preferGet('landingpage_about_description_text') ?? 'Sollicitudin in massa, mi eget. Ac nunc etiam amet, rhoncus cursus mus. At mattis ac auctor nulla. Tellus eros, eu, congue sit. Sollicitudin purus, lectus convallis eu suspendisse mauris mauris blandit. Velit erat nisl faucibus vel morbi ante. Ridiculus enim scelerisque in odio. Justo nibh proin nulla lobortis potenti faucibus feugiat. Sit sollicitudin adipiscing egestas quam quis sit arcu lacus risus.' }}</p>
                        </div>
                    @else
                        <div class="col-lg-12 text-center">
                            <h1 class="section-title mb-40">{{ preferGet('landingpage_about_title_text') ?? 'Launching Virtual Event v3 by Eventy' }}</h1>
                            <p class="section-desc">{{ preferGet('landingpage_about_description_text') ?? 'Sollicitudin in massa, mi eget. Ac nunc etiam amet, rhoncus cursus mus. At mattis ac auctor nulla. Tellus eros, eu, congue sit. Sollicitudin purus, lectus convallis eu suspendisse mauris mauris blandit. Velit erat nisl faucibus vel morbi ante. Ridiculus enim scelerisque in odio. Justo nibh proin nulla lobortis potenti faucibus feugiat. Sit sollicitudin adipiscing egestas quam quis sit arcu lacus risus.' }}</p>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        <!-- Speaker -->
        @if(count($speakers) > 0)
        <section id="speakers" class="speaker-section pt-80">
            {{--   TODO : preference title speaker         --}}
            <h1 class="section-title text-center mb-40">Speakers</h1>
            <div class="container position-relative">
                <div class="row">

                    <div class="col-lg-12">
                        <!-- Slider main container -->
                        <div class="swiper swiper-speakers">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->

                                @foreach($speakers as $speaker)
                                <div class="swiper-slide">
                                    <div class="card-default">
                                        <div class="card-default__img">
                                            <img src="{{ $speaker->present_image }}" alt="Image Speaker" class="img-fluid w-100 mw-100">
                                        </div>
                                        <div class="card-default__inner">
                                            <h3 class="card-default__inner__title">{{ $speaker->name }}</h3>
                                            <p class="card-default__inner__desc">{{ $speaker->description }}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>

                            <!-- If we need scrollbar -->
                            <div class="swiper-scrollbar"></div>
                        </div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-slider-navigation d-none d-lg-block">
                            <div class="swiper-button-prev">
                                <svg width="53" height="38" viewBox="0 0 53 38" fill="none" xmlns="http://www.w3.org/2000/svg" class="swiper-slider-arrow">
                                    <path d="M40.4291 34.1145C40.8744 33.6691 41.1245 33.0651 41.1245 32.4354C41.1245 31.8056 40.8744 31.2016 40.4291 30.7563L28.6729 19L40.4291 7.24378C40.8618 6.79585 41.1012 6.19592 41.0957 5.5732C41.0903 4.95049 40.8406 4.3548 40.4002 3.91446C39.9599 3.47412 39.3642 3.22434 38.7415 3.21893C38.1188 3.21352 37.5188 3.45291 37.0709 3.88553L23.6355 17.3209C23.1903 17.7663 22.9402 18.3703 22.9401 19C22.9401 19.6298 23.1903 20.2338 23.6355 20.6791L37.0709 34.1145C37.5163 34.5598 38.1203 34.8099 38.75 34.8099C39.3798 34.8099 39.9838 34.5598 40.4291 34.1145Z" fill="url(#paint0_linear_86_249)"/>
                                    <path d="M25.4291 34.1145C25.8744 33.6691 26.1245 33.0651 26.1245 32.4354C26.1245 31.8056 25.8744 31.2016 25.4291 30.7563L13.6729 19L25.4291 7.24378C25.8618 6.79585 26.1012 6.19592 26.0957 5.5732C26.0903 4.95049 25.8406 4.3548 25.4002 3.91446C24.9599 3.47412 24.3642 3.22434 23.7415 3.21893C23.1188 3.21352 22.5188 3.45291 22.0709 3.88553L8.63552 17.3209C8.19028 17.7663 7.94015 18.3703 7.94015 19C7.94015 19.6298 8.19028 20.2338 8.63552 20.6791L22.0709 34.1145C22.5163 34.5598 23.1203 34.8099 23.75 34.8099C24.3798 34.8099 24.9838 34.5598 25.4291 34.1145Z" fill="url(#paint1_linear_86_249)"/>
                                    <defs>
                                        <linearGradient id="paint0_linear_86_249" x1="22.9401" y1="34.8099" x2="24.6388" y2="2.35597" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#28B7FB"/>
                                            <stop offset="1" stop-color="#5BCAFF"/>
                                        </linearGradient>
                                        <linearGradient id="paint1_linear_86_249" x1="7.94014" y1="34.8099" x2="9.63881" y2="2.35597" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#28B7FB"/>
                                            <stop offset="1" stop-color="#5BCAFF"/>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg width="53" height="38" viewBox="0 0 53 38" fill="none" xmlns="http://www.w3.org/2000/svg" class="swiper-slider-arrow">
                                    <path d="M40.4291 34.1145C40.8744 33.6691 41.1245 33.0651 41.1245 32.4354C41.1245 31.8056 40.8744 31.2016 40.4291 30.7563L28.6729 19L40.4291 7.24378C40.8618 6.79585 41.1012 6.19592 41.0957 5.5732C41.0903 4.95049 40.8406 4.3548 40.4002 3.91446C39.9599 3.47412 39.3642 3.22434 38.7415 3.21893C38.1188 3.21352 37.5188 3.45291 37.0709 3.88553L23.6355 17.3209C23.1903 17.7663 22.9402 18.3703 22.9401 19C22.9401 19.6298 23.1903 20.2338 23.6355 20.6791L37.0709 34.1145C37.5163 34.5598 38.1203 34.8099 38.75 34.8099C39.3798 34.8099 39.9838 34.5598 40.4291 34.1145Z" fill="url(#paint0_linear_86_249)"/>
                                    <path d="M25.4291 34.1145C25.8744 33.6691 26.1245 33.0651 26.1245 32.4354C26.1245 31.8056 25.8744 31.2016 25.4291 30.7563L13.6729 19L25.4291 7.24378C25.8618 6.79585 26.1012 6.19592 26.0957 5.5732C26.0903 4.95049 25.8406 4.3548 25.4002 3.91446C24.9599 3.47412 24.3642 3.22434 23.7415 3.21893C23.1188 3.21352 22.5188 3.45291 22.0709 3.88553L8.63552 17.3209C8.19028 17.7663 7.94015 18.3703 7.94015 19C7.94015 19.6298 8.19028 20.2338 8.63552 20.6791L22.0709 34.1145C22.5163 34.5598 23.1203 34.8099 23.75 34.8099C24.3798 34.8099 24.9838 34.5598 25.4291 34.1145Z" fill="url(#paint1_linear_86_249)"/>
                                    <defs>
                                        <linearGradient id="paint0_linear_86_249" x1="22.9401" y1="34.8099" x2="24.6388" y2="2.35597" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#28B7FB"/>
                                            <stop offset="1" stop-color="#5BCAFF"/>
                                        </linearGradient>
                                        <linearGradient id="paint1_linear_86_249" x1="7.94014" y1="34.8099" x2="9.63881" y2="2.35597" gradientUnits="userSpaceOnUse">
                                            <stop stop-color="#28B7FB"/>
                                            <stop offset="1" stop-color="#5BCAFF"/>
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        @endif

        <!-- Schedule -->
        <section id="schedule" class="schedule-section pt-80">
            <h1 class="section-title text-center mb-40">Event Schedule</h1>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="schedule-section__inner">
                            <div class="select-group">

                                <div class="select-item me-3">
                                    <label for="selectDateHome">Dates</label>
                                    <select name="" id="selectDateHome" class="select-choice"
                                        @change="changeSelectDate">
                                        <option v-for="(option, index) in dates" :key="index" :value="option.value">
                                            @{{ option.option }}
                                        </option>
                                    </select>
                                </div>

                                <div class="select-item select-item--two">
                                    <label for="selectLocationHome">Location</label>
                                    <select name="" id="selectLocationHome" class="select-choice"
                                            @change="changeSelectLocation">
                                        <option v-for="(option, index) in locations" :key="index" :value="option.value">
                                            @{{ option.option }}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="table-list">
                                <div class="table-card" v-for="(item, index) in filterSchedules" :key="index">
                                    <div class="table-card__start">
                                        <div class="table-card__icon">
                                            <img :src="item.present_image" alt="Icon Schedule" class="w-100">
                                        </div>

                                        <div class="table-card__keynote"
                                             :class="{ 'table-card__keynote--center': !item.present_speaker }">
                                            <h3 class="table-card__title" v-cloak>@{{ item.title }}</h3>
                                            <p class="table-card__speaker" v-if="item.present_speaker">
                                                <svg width="13" height="15" viewBox="0 0 13 15" fill="#2C2C2C" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9735 14.5948C12.9735 14.8165 12.7931 15 12.5708 15H0.402695C0.183535 15 0 14.8187 0 14.5948C0 11.0124 2.90426 8.10805 6.4867 8.10805C10.0692 8.10805 12.9734 11.0123 12.9734 14.5948H12.9735ZM6.48678 7.29738C4.4716 7.29738 2.83823 5.66372 2.83823 3.64883C2.83823 1.63366 4.4716 0 6.48678 0C8.50195 0 10.1353 1.63366 10.1353 3.64883C10.1353 5.66372 8.50195 7.29738 6.48678 7.29738Z"/>
                                                </svg>
                                                <span class="badge ms-2" v-cloak>@{{ item.present_speaker }}</span>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="table-card__end">
                                        <p class="table-card__location table-card__location__place" v-cloak>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.0115 12.726C9.53662 11.3837 12.25 8.6415 12.25 6.125C12.25 4.73261 11.6969 3.39726 10.7123 2.41269C9.72774 1.42812 8.39239 0.875 7 0.875C5.60761 0.875 4.27226 1.42812 3.28769 2.41269C2.30312 3.39726 1.75 4.73261 1.75 6.125C1.75 8.6415 4.4625 11.3837 5.9885 12.726C6.26685 12.9744 6.6269 13.1118 7 13.1118C7.3731 13.1118 7.73315 12.9744 8.0115 12.726ZM5.25 6.125C5.25 5.66087 5.43437 5.21575 5.76256 4.88756C6.09075 4.55937 6.53587 4.375 7 4.375C7.46413 4.375 7.90925 4.55937 8.23744 4.88756C8.56563 5.21575 8.75 5.66087 8.75 6.125C8.75 6.58913 8.56563 7.03425 8.23744 7.36244C7.90925 7.69063 7.46413 7.875 7 7.875C6.53587 7.875 6.09075 7.69063 5.76256 7.36244C5.43437 7.03425 5.25 6.58913 5.25 6.125Z" fill="#054462"/>
                                            </svg> @{{ item.schedule.location.name }}
                                        </p>
                                        <p class="table-card__location mb-0" v-cloak>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.99984 1.16675C10.2257 1.16675 12.8332 3.78008 12.8332 7.00008C12.8332 10.2259 10.2257 12.8334 6.99984 12.8334C3.77984 12.8334 1.1665 10.2259 1.1665 7.00008C1.1665 3.78008 3.77984 1.16675 6.99984 1.16675ZM6.79567 4.04258C6.5565 4.04258 6.35817 4.23508 6.35817 4.48008V7.42591C6.35817 7.57758 6.43984 7.71758 6.574 7.79925L8.86067 9.16425C8.93067 9.20508 9.0065 9.22842 9.08817 9.22842C9.234 9.22842 9.37984 9.15258 9.4615 9.01258C9.584 8.80841 9.51984 8.54008 9.30984 8.41175L7.23317 7.17508V4.48008C7.23317 4.23508 7.03484 4.04258 6.79567 4.04258Z" fill="#054462"/>
                                            </svg> @{{ item.time_start }} - @{{ item.time_end }} @{{ item.abbreviation_timezone }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sponsor -->
        @if(count($sponsor_categories) > 0)
        <section id="sponsor" class="sponsor-section pt-80">
            <h1 class="section-title text-center mb-40">Sponsor</h1>
            <div class="container">
                <div class="row justify-content-center">

                    <div class="justify-content-center d-flex">
                        <ul class="nav nav-tabs" id="tabPartner" role="tablist">
                            @foreach($sponsor_categories as $category)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link {{ $category->name === 'Gold' ? 'active' : '' }}" id="sponsor-{{ $category->id }}-tab" data-bs-toggle="tab" data-bs-target="#sponsor-{{ $category->id }}-tab-pane" type="button" role="tab" aria-controls="sponsor-{{ $category->id }}-tab-pane" aria-selected="true">{{ $category->name }}</button>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="tab-content" id="tabPartnerContent">
                        @foreach($sponsor_categories as $category)
                        <div class="tab-pane fade {{ $category->name === 'Gold' ? 'show active' : '' }}" id="sponsor-{{ $category->id }}-tab-pane" role="tabpanel" aria-labelledby="sponsor-{{ $category->id }}-tab" tabindex="0">
                            <div class="partner-col-5">
                                <div class="partner-item">
                                    @foreach($category->sponsors as $sponsor)
                                        <a href="{{ $sponsor->link }}" target="_blank" class="partner-link">
                                            <img src="{{ $sponsor->present_image }}" alt="Sponsor">
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </section>
        @endif
    </main>

    @if(preferGet('landingpage_whatsapp') || preferGet('landingpage_email') || preferGet('landingpage_website'))
    <footer id="contact" class="footer mt-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__inner">
                        <h1 class="footer__title">Contact Us :</h1>

                        <div class="d-flex justify-content-center">
                            @if(preferGet('landingpage_whatsapp'))
                            <a class="footer__link" href="{{ preferGet('landingpage_whatsapp') }}" target="_blank">
                                <img src="{{ asset('images/icon/landingpage/icon_whatsapp_circle.svg') }}" alt="Icon Chat" class="footer__icon">
                                <p class="footer__link__text">Whatsapp</p>
                            </a>
                            @endif
                            @if(preferGet('landingpage_email'))
                            <a class="footer__link ms-lg-5" href="mailto:{{ preferGet('landingpage_email') }}" target="_blank">
                                <img src="{{ asset('images/icon/landingpage/icon_email_circle.svg') }}" alt="Icon Email" class="footer__icon">
                                <p class="footer__link__text">Email</p>
                            </a>
                            @endif

                            @if(preferGet('landingpage_website'))
                            <a class="footer__link ms-lg-5" href="{{ preferGet('landingpage_website') }}" target="_blank">
                                <img src="{{ asset('images/icon/landingpage/icon_web_circle.svg') }}" alt="Icon Web" class="footer__icon">
                                <p class="footer__link__text">Website</p>
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    @endif


    @include('layout.plugin.js')

    <script>
        let youtube_url = '{{ $youtube->youtube_url }}';
        let numberOfCountdown = {{ $numberOfCountdown }};
        let schedule_sessions = {!! json_encode($schedule_sessions) !!};
        let locations = {!! json_encode($locations) !!};
    </script>
    <script src="{{ asset('js/web/LandingPage/landing-page.js') . '?v=' . date('YmdH') }}"></script>
</body>
</html>
