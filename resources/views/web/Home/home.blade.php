@extends('layout.Web.index')

@section('title', 'Home')

@push('head')
    <style>
        @media (max-width: 640px) {
            .event-title {
                font-size: 12px;
            }
        }
        .card-qr {
            margin-top: 40px;
            background: #FFFFFF;
            box-shadow: 0px 8px 50px rgba(0, 0, 0, 0.15);
            border-radius: 6px;
            padding: 60px 40px;
        }

        @media (max-width: 991.98px) {
            .card-qr {
                margin-left: 10px;
                margin-right: 10px;
                /*margin-bottom: 40px;*/
                padding: 60px 15px;
            }
        }
        .info-text-qr {
            font-size: 12px;
            margin-bottom: 20px;
        }
        .img-logo {
            width: 80px;
            height: 80px;
            border-radius: 50%;
            margin-bottom: 20px;
        }
        .name-text {
            margin-bottom: 5px;
            font-size: 18px;
            font-family: var(--eventy-font-family-bold);
        }
        .company-text {
            color: #A7ACB4;
            font-size: 14px;
            margin-bottom: 5px;
        }
        .img-qr {
            width: 100%;
            margin-bottom: 40px;
        }
        @media (min-width: 992px) {
            .img-qr {
                width: 70%;
            }
        }
        .btn-logout {
            background: #fff;
            border: 2px solid #D20000;
            color: #D20000;
            border-radius: 6px;
        }
        .btn-logout:hover {
            color: #D20000;
        }

        .btn-change-password {
            background: #fff;
            border: 2px solid green;
            color: green;
            border-radius: 6px;
        }
        .btn-change-password:hover {
            color: green;
        }

        /* Modal styles */
        .modal {
            display: none;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.5);
            justify-content: center;
            align-items: center;
        }

        /* Make sure the modal uses flex display only once */
        .modal.show {
            display: flex; /* Use Flexbox to center */
        }

        .modal-content {
            font-family: var(--eventy-font-family-bold);
            font-size: 14px;
            background-color: #fff;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 70%;
            max-width: 400px;
            border-radius: 13px;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
            cursor: pointer;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
        }

        /* Form styles */
        form {
            display: flex;
            flex-direction: column;
        }

        label {
            margin: 5px 0 5px;
            font-weight: bold;
        }

        input[type="password"] {
            padding: 5px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        /* Responsive styles */
        @media (max-width: 600px) {
            .modal-content {
                width: 90%;
                padding: 15px;
            }
        }

        @media (max-width: 768px) {
            .profile-group {
                display: none;
            }
        }

        .nav.nav-tabs {
            background: #EFF2F6;
            border-radius: 18px;
            flex-wrap: nowrap;
        }
        .nav.nav-tabs .nav-item {
            flex: 1;
            width: 100%;
        }
        .nav-tabs .nav-link {
            background: #EFF2F6;
            border-radius: 18px;
            color: #A4A9B1;
            text-align: center;
            padding-left: 15px;
            padding-right: 15px;
            font-size: 10px;
            white-space: nowrap;
            width: 100%;
        }
        @media (min-width: 480px) {
            .nav-tabs .nav-link {
                font-size: 12px;
            }
        }
        @media (min-width: 992px) {
            .nav-tabs .nav-link {
                padding-left: 20px;
                padding-right: 20px;
                font-size: 14px;
            }
        }
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            background-color: var(--eventy-color-primary);
            color: #fff;
            border-radius: 18px;
        }
        .tab-pane {
            min-height: 300px;
        }

        .ticket-title {
            font-family: var(--eventy-font-family-bold);
            font-size: 14px;
            margin-bottom: 10px;
        }
        .img-sympo {
            position: relative;
        }
        .img-sympo__text {
            position: absolute;
            bottom: 6px;
            right: 10px;
            color: #fff;
            font-size: 12px;
            font-family: var(--eventy-font-family-bold);
        }
        .img-sympo__text-2 {
            position: absolute;
            top: 10px;
            left: 10px;
            color: #fff;
            font-size: 14px;
            font-family: var(--eventy-font-family-bold);
            margin-bottom: 0;
        }

        .click-here {
            color: var(--eventy-color-primary);
            font-size: 12px;
            text-decoration: none;
            text-align: center;
            font-family: var(--eventy-font-family-bold);
            display: block;
            margin-top: 30px;
        }
        @media (min-height: 992px) {
            .click-here {
                font-size: 14px;
            }
        }
        .click-here:hover {
            color: var(--eventy-color-primary);
        }

        .big-point {
            text-align: center;
        }
        .big-point h1 {
            font-size: 50px;
            font-family: 'Inter-Bold', sans-serif;
            color: var(--eventy-color-primary);
        }
        .big-point p {
            font-size: 14px;
            font-family: 'Inter-Bold', sans-serif;
            color: var(--eventy-color-primary);
        }
        .table-point {
            margin-top: 20px;
        }
        .table-point th {
            padding-bottom: 15px;
            padding-top: 15px;
            vertical-align: middle;
            font-family: 'Inter-Bold', sans-serif;
        }
        @media (max-width: 640px) {
            .event-title {
                display: none;
            }
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card-qr text-center">

                    <img src="{{ $participant->present_image }}" alt="Image" class="img-logo">

                    <h4 class="name-text">{{ $participant->name }}</h4>
                    <p class="company-text" style="margin-bottom: 15px;">{{ $participant->company }}</p>
                    <p class="company-text" style="color: #2C2C2C;margin-bottom: 40px;">No. Registration: SRL-{{ $participant->type_code }}-{{ $participant->registration_number }}</p>

                    <img src="{{ $participant->present_qr_code }}" alt="Qr Code" class="img-qr">


                    <p class="info-text-qr">*Show your QR Code when check-in at the venue</p>

                    <div class="d-flex">
                        <a class="btn-logout btn-change-password" id="changePasswordBtn">Change Password</a>
{{--                        <a href="{{ url('download-qr', ['qr_code' => $participant->qr_code]) }}">--}}
{{--                            <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg" class="ms-3">--}}
{{--                                <rect x="0.5" y="0.5" width="44" height="44" rx="5.5" fill="var(--eventy-color-primary)" stroke="var(--eventy-color-primary)"/>--}}
{{--                                <path d="M15.214 30.6667H29.786C30.0937 30.6668 30.3899 30.779 30.6147 30.9808C30.8395 31.1826 30.9762 31.4589 30.9972 31.7538C31.0181 32.0487 30.9218 32.3403 30.7277 32.5696C30.5335 32.7989 30.256 32.9488 29.9512 32.9891L29.786 33H15.214C14.9063 32.9999 14.6101 32.8876 14.3853 32.6858C14.1605 32.484 14.0238 32.2078 14.0028 31.9129C13.9819 31.6179 14.0782 31.3264 14.2723 31.0971C14.4665 30.8678 14.744 30.7178 15.0488 30.6776L15.214 30.6667H29.786H15.214ZM22.3348 12.0109L22.5 12C22.7934 12 23.077 12.1021 23.2981 12.2874C23.5193 12.4727 23.6631 12.7287 23.703 13.008L23.7143 13.1667V25.1258L27.3655 21.6196C27.5714 21.4218 27.8444 21.3017 28.1347 21.2811C28.4251 21.2606 28.7135 21.3409 28.9473 21.5076L29.0833 21.6196C29.2892 21.8174 29.4142 22.0797 29.4356 22.3586C29.457 22.6376 29.3733 22.9147 29.1999 23.1393L29.0833 23.27L23.3581 28.7689C23.1525 28.9665 22.8799 29.0866 22.5899 29.1074C22.2999 29.1283 22.0118 29.0484 21.7779 28.8824L21.6419 28.7689L15.9167 23.27C15.6994 23.0618 15.5722 22.7823 15.5607 22.4878C15.5491 22.1932 15.6541 21.9053 15.8545 21.682C16.0549 21.4588 16.3358 21.3167 16.6408 21.2843C16.9458 21.2519 17.2523 21.3317 17.4985 21.5076L17.6346 21.6196L21.2857 25.1289V13.1667C21.2857 12.8847 21.3919 12.6124 21.5848 12.3999C21.7777 12.1874 22.0441 12.0492 22.3348 12.0109L22.5 12L22.3348 12.0109Z" fill="white"/>--}}
{{--                            </svg>--}}
{{--                        </a>--}}
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="card-qr" style="padding-top: 40px;padding-bottom: 30px;">
                    <ul class="nav nav-tabs mb-5" id="Point" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="point-tab" data-bs-toggle="tab" data-bs-target="#point" type="button" role="tab" aria-controls="point" aria-selected="true">My Journey</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="information-tab" data-bs-toggle="tab" data-bs-target="#information" type="button" role="tab" aria-controls="information" aria-selected="false">Information</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="PointContent">
                        <div class="tab-pane fade show active" id="point" role="tabpanel" aria-labelledby="point-tab" style="min-height: 0px;">

                            <div class="big-point">
                                <h1>{{ $participant->total_point }}</h1>
                                <p>Point Activity</p>
                            </div>
                            @if(count($participant->histories) > 0)
                                <div class="table--invoice-wrapper simple-scrollbar">
                                    <table class="table table-point">
                                        @foreach($participant->histories as $history)
                                            <tr>
                                                <th>
                                                    {{ date('H:i', strtotime($history->created_at)) }} <br>
                                                    {{ date('d M Y', strtotime($history->created_at)) }}
                                                </th>
                                                <th>{{ $history->scanType->scanGroup->name }} - {{ $history->scanType->name }}</th>
                                                <th class="text-end">+{{ $history->point_added }}</th>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endif
                        </div>
                        <div class="tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab" style="min-height: 0px;">
                            <img src="{{ preferGet('sirclo_information') }}" alt="" class="w-100">
                        </div>
                    </div>
                </div>

                <div class="card-qr my-5" style="padding-top: 30px;padding-bottom: 30px;">
                    <ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Invoices</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">My Tickets</button>
                        </li>
                        {{--                        <li class="nav-item" role="presentation">--}}
                        {{--                            <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Certificates</button>--}}
                        {{--                        </li>--}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <div class="table--invoice-wrapper simple-scrollbar">
                                <table class="table table--invoice">
                                    <thead>
                                    <tr>
                                        <th>No Order</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($participant->transactions as $item)
                                        @if($item->is_activated_manually == 0)
                                            <tr>
                                                <td>{{ $item->invoice_code }}</td>
                                                <td>{{ date('Y-m-d', strtotime($item->created_at)) }}</td>
                                                <td>
                                                    <span class="badge badge-{{ $item->status === 'SUCCESS' ? 'success' : ($item->status === 'EXPIRED' ? 'expired' : ($item->status === 'PAYMENT_CHANGED' ? 'pending-2' : 'pending')) }}">
                                                        @if($item->status === 'EXPIRED')
                                                            EXPIRED
                                                        @elseif($item->status !== 'SUCCESS' && $item->status !== 'PAYMENT_CHANGED')
                                                            PENDING
                                                        @else
                                                            {{ $item->status }}
                                                        @endif
                                                    </span>
                                                </td>
                                                <td>
                                                    @if($item->status === 'SUCCESS')
                                                        <a href="{{ action([\App\Http\Controllers\FrontEnd\InvoiceController::class, 'index'], [
                                                        'key' => encrypt(['id' => $item->id])
                                                    ]) }}"
                                                           target="_blank">
                                                            <i class="fas fa-download ic-download"></i>
                                                        </a>
                                                    @endif

                                                    @if($item->status === 'PAYMENT_CHANGED' && $loop->iteration === 1)
                                                        <a href="{{ action([\App\Http\Controllers\Registration\TicketController::class, 'classroomAddition'], [
                                                        'p' => g('p'),
                                                        'method_changed' => encrypt([
                                                            'id' => gSession('id'),
                                                            'is_payment_changed' => true
                                                        ])
                                                    ]) }}">
                                                            <i class="fas fa-eye ic-download"></i>
                                                        </a>
                                                    @endif

                                                    @if($item->status === 'WAITING_CONFIRMATION' ||
                                                        $item->status === 'PENDING' ||
                                                        $item->status === 'WAITING_PAYMENT' ||
                                                        $item->status === 'EXPIRED')
                                                        <a href="{{ action([\App\Http\Controllers\Registration\TransactionController::class, 'show'], [
                                                        'key' => encrypt($item->id)
                                                    ]) }}">
                                                            @if($item->status === 'EXPIRED')
                                                                <i class="fas fa-eye ic-download"></i>
                                                            @else
                                                                <svg width="28" height="20" viewBox="0 0 28 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M0 3.23529C0 2.37724 0.33802 1.55433 0.939699 0.947596C1.54138 0.34086 2.35743 0 3.20833 0H24.7917C25.6426 0 26.4586 0.34086 27.0603 0.947596C27.662 1.55433 28 2.37724 28 3.23529V5.88235H0V3.23529ZM0 7.64706H28V16.7647C28 17.6228 27.662 18.4457 27.0603 19.0524C26.4586 19.6591 25.6426 20 24.7917 20H3.20833C2.35743 20 1.54138 19.6591 0.939699 19.0524C0.33802 18.4457 0 17.6228 0 16.7647V7.64706ZM19.25 13.4118C18.9715 13.4118 18.7045 13.5233 18.5075 13.7219C18.3106 13.9205 18.2 14.1898 18.2 14.4706C18.2 14.7514 18.3106 15.0207 18.5075 15.2193C18.7045 15.4179 18.9715 15.5294 19.25 15.5294H22.75C23.0285 15.5294 23.2955 15.4179 23.4925 15.2193C23.6894 15.0207 23.8 14.7514 23.8 14.4706C23.8 14.1898 23.6894 13.9205 23.4925 13.7219C23.2955 13.5233 23.0285 13.4118 22.75 13.4118H19.25Z" fill="url(#paint0_linear_5282_27636)"/>
                                                                    <defs>
                                                                        <linearGradient id="paint0_linear_5282_27636" x1="14" y1="0" x2="14" y2="20" gradientUnits="userSpaceOnUse">
                                                                            <stop offset="0.726872" stop-color="var(--eventy-color-primary-light)"/>
                                                                            <stop offset="1" stop-color="var(--eventy-color-primary)"/>
                                                                        </linearGradient>
                                                                    </defs>
                                                                </svg>
                                                            @endif
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <h4 class="ticket-title">Ticket</h4>
                            <div class="img-sympo">
                                <img src="{{ preferGet('bg_ticket') }}" alt="Symposium" class="w-100 img-fluid">
                                <p class="img-sympo__text-2">{{ $participant->ticket->name }}</p>
                                <p class="img-sympo__text">{{ $participant->ticket->venue }}</p>
                            </div>

                            @if(count($participant->schedules) > 0)
                                <h4 class="ticket-title mt-4">Type</h4>
                            @endif

                            @foreach($participant->schedules as $schedule)
                                <div class="img-sympo mt-3">
                                    <img src="{{ preferGet('bg_sessions') }}" alt="Symposium" class="w-100 img-fluid">
                                    <p class="img-sympo__text-2">{{ $schedule->schedule->name }}</p>
                                    {{--                                    <p class="img-sympo__text">{{ $schedule->schedule->location->name }}</p>--}}
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if($can_add_classroom)
                        {{--                        <a href="{{ action([\App\Http\Controllers\Registration\TicketController::class, 'classroomAddition']) }}" class="click-here">If you want to add more sessions, click here.</a>--}}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Popup Modal -->
    <div id="changePasswordModal" class="modal">
        <div class="modal-content">
            <span class="close" id="closeModal">&times;</span>
            <form id="changePasswordForm">
                <label for="currentPassword">Current Password</label>
                <input type="password" id="currentPassword" name="currentPassword" required>
                <label for="newPassword">New Password</label>
                <input type="password" id="newPassword" name="newPassword" required>
                <label for="newPassword">Retype New Password</label>
                <input type="password" id="RetypeNewPassword" name="RetypeNewPassword" required>
                <button type="submit" class="btn-logout btn-change-password" style="margin-top: 20px;">Submit</button>
            </form>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        let message_title = '{{ session('message_title') }}';
        let message_body = '{{ session('message_body') }}';

        $(document).ready(function() {
            if (message_title) {
                Swal.fire({
                    icon: 'warning',
                    confirmButtonColor: 'var(--eventy-color-primary)',
                    title: message_title,
                    text: message_body
                });
            }
        });
        $(document).ready(function() {
            const changePasswordModal = $('#changePasswordModal');
            const changePasswordForm = $('#changePasswordForm');

            const csrfToken = $('meta[name="csrf-token"]').attr('content');
            const participant_id = {{$participant->id}};

            // Open the modal when the button is clicked
            $('#changePasswordBtn').click(function() {
                changePasswordModal.addClass('show');
            });

            // Close the modal when the close button (x) is clicked
            $('#closeModal').click(function() {
                changePasswordModal.removeClass('show');
                // Reset the form fields
                $('#newPassword').val('');
                $('#RetypeNewPassword').val('');
                $('#currentPassword').val('');
            });

            // Close the modal when clicking outside of the modal content
            $(window).click(function(event) {
                if ($(event.target).is(changePasswordModal)) {
                    changePasswordModal.removeClass('show');
                    // Reset the form fields
                    $('#newPassword').val('');
                    $('#RetypeNewPassword').val('');
                    $('#currentPassword').val('');
                }
            });

            // Handle the form submission
            changePasswordForm.submit(function(event) {
                event.preventDefault();

                const new_password = $('#newPassword').val();
                const retype_password = $('#RetypeNewPassword').val();
                const current_password = $('#currentPassword').val();

                $.ajax({
                    url: '/profile',
                    type: 'POST',
                    contentType: 'application/json',
                    headers: {
                        'X-CSRF-TOKEN': csrfToken,
                    },
                    data: JSON.stringify({
                        new_password: new_password,
                        retype_password: retype_password,
                        current_password: current_password,
                        participant_id: participant_id,
                    }),
                    success: function(data) {
                        // Handle the response data
                        console.log(data);
                        Swal.fire({
                            icon: 'success',
                            confirmButtonColor: 'var(--eventy-color-primary)',
                            title: 'Success',
                            text: 'Password changed successfully'
                        });

                        // Reset the form fields
                        $('#newPassword').val('');
                        $('#RetypeNewPassword').val('');
                        $('#currentPassword').val('');

                        changePasswordModal.removeClass('show'); // Remove the class that sets display: flex
                    },
                    error: function(error) {
                        // Handle any errors
                        console.log('Error:', error.responseJSON);
                        Swal.fire({
                            icon: 'warning',
                            confirmButtonColor: 'var(--eventy-color-primary)',
                            title: 'Failed',
                            text: error.responseJSON.message
                        });
                    }
                });
            });
        });
    </script>
@endpush
