@extends('layout.Auth.index')

@section('page_title', __('eventy/register.title_page', ['EVENT_NAME' => preferGet('name_events')]))

@push('head')
    <link rel="stylesheet" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Auth/auth.css') . '?v=1.0.1' }}">
    <style>
        /* custom select */
        .selectize-control {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }

        .selectize-control.single .selectize-input {
            height: 100%;
            padding-left: 15px;
            display: flex;
            color: #6a7b89;
        }

        .selectize-control.single .selectize-input.input-active,
        .selectize-input {
            display: flex;
            background-color: #EFF2F6;
        }

        .selectize-input.focus {
            border: none;
            outline: 0;
            box-shadow: none;
            border-radius: 15px;
        }

        .selectize-input {
            border: none;
            border-radius: 15px;
            background: transparent;
        }

        .selectize-input.full {
            background-color: #EFF2F6;
        }

        .selectize-input > * {
            vertical-align: middle;
            margin-top: auto;
            margin-bottom: auto;
            color: #2C2C2C;
        }

        .selectize-dropdown,
        .selectize-dropdown.form-control {
            padding: 0 !important;
            border-radius: 15px;
            border: none;
        }

        .selectize-dropdown .create,
        .selectize-dropdown .no-results,
        .selectize-dropdown .optgroup-header,
        .selectize-dropdown .option {
            padding: 10px .75rem;
        }

        .selectize-dropdown .active {
            background-color: #fbfbfb;
        }

        .selectize-dropdown-content {
            max-height: 500px;
            background-color: #EFF2F6;
        }

        /*  selectize arrow  */
        .selectize-input::after,
        .selectize-input::before {
            content: " ";
            display: block;
            clear: left;
        }

        .selectize-control.single .selectize-input:after {
            top: 60%;
            border-color: #6a7b89 transparent transparent transparent;
        }

        .selectize-control.single .selectize-input:before {
            content: " ";
            display: block;
            position: absolute;
            top: 40%;
            right: calc(0.75rem + 5px);
            margin-top: -3px;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 5px 5px 5px;
            border-color: transparent transparent #6a7b89 transparent;
        }

        .selectize-control.single .selectize-input.dropdown-active:after {
            margin-top: -3px !important;
            border-width: 5px 5px 0 5px !important;
            border-color: #6a7b89 transparent transparent transparent !important;
        }

        .selectize-control.single .selectize-input.dropdown-active::before {
            left: auto;
        }

        /* Customize the label (the container) */
        .container-radio {
            display: block;
            position: relative;
            padding-left: 30px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 14px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default radio button */
        .container-radio input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom radio button */
        .container-radio .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: #D3D4D9;
            border-radius: 50%;
        }

        /* On mouse-over, add a grey background color */
        .container-radio:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the radio button is checked, add a blue background */
        .container-radio input:checked ~ .checkmark {
            background-color: #D3D4D9;
        }

        /* Create the indicator (the dot/circle - hidden when not checked) */
        .container-radio .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the indicator (dot/circle) when checked */
        .container-radio input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the indicator (dot/circle) */
        .container-radio .checkmark:after {
            top: 5px;
            left: 5px;
            width: 10px;
            height: 10px;
            border-radius: 50%;
            background: var(--eventy-color-primary);
        }

        /* Customize the label (the container) */
        .container-checkbox {
            display: block;
            position: relative;
            padding-left: 30px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 14px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container-checkbox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .container-checkbox .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            border-radius: 50%;
            background-color: #D3D4D9;
        }

        /* On mouse-over, add a grey background color */
        .container-checkbox:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-checkbox input:checked ~ .checkmark {
            background-color: white;
            border: solid;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .container-checkbox .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-checkbox input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-checkbox .checkmark:after {
            height: 1.1rem;
            width: 1.1rem;
            border-radius: 50%;
            background-color: black;
            left: 0.15rem!important;
            top: 0.15rem!important;
        }

        .container-checkbox strong {
            font-family: var(--eventy-font-family-bold);
            color: var(--eventy-color-primary);
        }

        .phone-prefix {
            flex-basis: 30%;
        }

        /*new interest views*/
        /* Container for the entire section */
        .custom-container {
            width: 100%;
            margin-bottom: 20px;
        }

        /* Blue background styling */
        .checked-background {
            background-color: #28B7FB;
            color: white; /* White text color for better readability */
            border-radius: 12px;
        }

        /* Label style */
        .custom-label {
            font-size: 14px;
            color: black; /* White text color */
            margin-bottom: 10px;
            display: block;
        }

        /* Container for checkboxes, using CSS Grid for alignment */
        .custom-checkbox-grid {
            display: grid;
            grid-template-columns: repeat(2, 1fr); /* Two columns */
            gap: 10px; /* Gap between checkboxes */
        }

        /* Style for each checkbox item */
        .custom-checkbox {
            display: flex;
            align-items: center;
            color: #333; /* Checkbox text color */
            font-size: 14px;
            cursor: pointer;
        }

        /* Hide default checkbox input */
        .custom-checkbox input {
            opacity: 0;
            position: absolute;
        }

        /* Custom checkbox design */
        .custom-checkbox label {
            padding: 8px;
            border: solid;
            border-radius: 12px;
            text-align: center;
            align-content: center;
            width: 100%;
            min-height: 6rem;
            position: relative;
            cursor: pointer;
        }

        /* Custom checkbox appearance */
        .custom-checkbox label:before {
            position: absolute;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
            width: 10%;
            height: 10%;
        }

        /* Checked state */
        .custom-checkbox input:checked + label:before {
            background-color: #3498db; /* Adjust color for checked state */
            border-color: #3498db;
        }

        /* Checked state checkmark */
        .custom-checkbox input:checked + label:after {
            position: absolute;
            top: 50%;
            left: 5px;
            transform: translateY(-50%);
        }

        /* Hover state */
        .custom-checkbox label:hover:before {
            background-color: #f0f0f0;
        }

        @media (min-width: 992px) {
            .phone-prefix {
                flex-basis: 20%;
            }
        }
    </style>
@endpush

@section('content')
    <div id="root" class="container-login100">
        <div class="wrap-login100 row">
            <div class="login100-form validate-form col-12 col-lg-6 login100-form-mobile">
                <!-- Register -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <div class="p-b-40">
                            <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                        </div>
                    </div>
                </div>
                <div class="p-b-30">
                    <h4 class="login100-form-title">{{ preferGet('register_title_text') ?? 'Register Your Account!' }}</h4>
                    <p class="txt2">
                        Already have an account? <span><a href="{{ url('login') }}"
                                                          class="blue-text">Login Now</a></span>
                    </p>
                </div>

                <form @submit.prevent="postRegister" class="row">
                    <div class="col-12">
                        <div class="wrap-input100 validate-input"
                             data-validate="First Name is required">
                            <label for="first_name" class="label-input100">Full Name<span
                                    class="required"></span></label>
                            <input class="input100 validated" id="name" type="text" name="name" minlength="3"
                                   maxlength="150"
                                   autocomplete="off" v-model="participant.name">
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="E-mail Address is required">
                            <label for="company" class="label-input100">Work E-mail<span
                                    class="required"></span></label>
                            <input class="input100 validated" id="email" type="email" name="email" minlength="3"
                                   maxlength="70"
                                   autocomplete="off" v-model="participant.email">
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="@lang('eventy/register.validate_phone')">
                            <label for="company"
                                   class="label-input100">@lang('eventy/register.placeholder_phone')</label>
                            <div class="parent-phone">
                                <div class="nation-phone">
                                    <select v-model="participant.phone_prefix" id="phone_prefix" name="phone_prefix" style="background: #EFF2F6; border: none;">
                                        @foreach($countries as $country)
                                            <option
                                                value="{{ $country->dial_code }}" {{ $country->dial_code == '+62' ? 'selected' : '' }}>
                                                {{ $country->flag }} {{ $country->dial_code }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <span class="phone-pre">|</span>
                                <input class="input100 only-0 validated" id="phone" type="text" name="phone"
                                       minlength="9" maxlength="15" pattern="\d*"
                                       autocomplete="off" v-model="participant.phone" @input.lazy="numberOnly">
                            </div>
                        </div>

                        <div class="wrap-input100 validate-input validated" id="wrap-nationality"
                             data-validate="Nationality is required">
                            <label for="nationality" class="label-input100">Nationality<span
                                    class="required"></span></label>
                            <select class="input100" id="nationality">
                                <option value="" selected></option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->name }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="Job Title is required">
                            <label for="job_title" class="label-input100">Job Title<span
                                    class="required"></span></label>
                            <input class="input100 validated" id="job_title" type="text" name="job_title" minlength="3"
                                   maxlength="100"
                                   autocomplete="off" v-model="participant.job_title">
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="Company Name is required">
                            <label for="company" class="label-input100">Company Name<span
                                    class="required"></span></label>
                            <input class="input100 validated" id="company" type="text" name="company" minlength="3"
                                   maxlength="100"
                                   autocomplete="off" v-model="participant.company">
                        </div>


                        <div class="wrap-input100 validate-input2" data-validate="Type of Business is required">
                            <label for="type_of_business" class="label-input100">Type of Business<span
                                    class="required"></span></label>
                            <label class="container-checkbox" v-for="item in type_of_business" :key="item">
                                @{{ item }}
                                <input type="checkbox" :checked="isSelectedBusiness(item)"
                                       @change="toggleSelectionBusiness(item)">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                        <div class="custom-container">
                            <label for="graduate_programme_area" class="custom-label">Areas of Interest (Max 3)<span
                                    class="required"></span>
                            </label>
                            <div class="custom-checkbox-grid">
                                <div class="custom-checkbox" v-for="(item, index) in area_of_interests" :key="index">
                                    <input type="checkbox" :id="'checkbox-' + index"
                                           v-model="participant.area_of_interests" :value="item">
                                    <label :for="'checkbox-' + index"
                                           :class="{'checked-background': participant.area_of_interests.includes(item)}">@{{
                                        item }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="@lang('eventy/register.validate_password')">
                            <label for="company" class="label-input100">@lang('eventy/register.placeholder_password')
                                <span class="required"></span></label>
                            <input class="input100 validated" id="password" type="password" name="password"
                                   autocomplete="off" v-model="participant.password" minlength="3">
                            <div class="show-password" style="right: 40px;">
                                <i class="fa fa-eye"></i>
                            </div>
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="@lang('eventy/register.validate_retype_password')">
                            <label for="company"
                                   class="label-input100">@lang('eventy/register.placeholder_retype_password')<span
                                    class="required"></span></label>
                            <input class="input100 validated" id="retype_password" type="password"
                                   name="retype_password"
                                   autocomplete="off" v-model="participant.retype_password" minlength="3">
                            <div class="show-password" style="right: 40px;">
                                <i class="fa fa-eye"></i>
                            </div>
                        </div>

                        <div class="checkbox-container">
                            <input type="checkbox" id="agree" v-model="agree">
                            <label for="agree">I agree to allow this website to process my registration information, including its use for marketing purposes.</label>
                        </div>

                        <div class="container-login100-form-btn p-t-30">
                            <div class="row">
                                <div class="col-12">
                                    <button class="login100-form-btn">
                                        @lang('eventy/register.text_btn_register')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- Register -->

            </div>
            <div class="col-12 col-lg-6 login-register-image">
                <img src="{{ preferGet('login_image') }}" alt="Welcome">
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        let valid_match_password = "{{ __('eventy/register.valid_match_password') }}";
    </script>
    <script src="{{ asset('js/web/Auth/autocomplete-input.js') }}"></script>
    <script src="{{ asset('js/web/Auth/register.js') . '?v=1.0.3' }}"></script>
@endpush
