@extends('layout.Auth.index')

@section('page_title', __('eventy/forgot.title_page_change', ['NAME_EVENT' => preferGet('name_events')]))

@push('head')
    <link rel="stylesheet" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Auth/auth.css') }}">
@endpush

@section('content')
    <main class="limiter">
        <div id="root" class="container-login100">
            <div class="wrap-login100 row">
                <div class="login100-form validate-form col-12 col-lg-6">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <div class="p-b-40">
                                <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                            </div>
                        </div>
                    </div>

                    <div class="p-b-30">
                        <h5 class="login100-form-title">{{ preferGet('change_password_title_text') ?? 'Update Password' }}</h5>
                        <p class="txt2">{{ preferGet('change_password_caption_text') ?? 'Please create your new password' }}</p>
                    </div>

                    <form v-on:submit.prevent="Change" class="row">
                        <div class="col-sm-12">
                            <div class="wrap-input100 validate-input"
                                 data-validate="@lang('eventy/forgot.valid_password')">
                                <label for="email" class="label-input100">@lang('eventy/forgot.placeholder_password')</label>
                                <input class="input100" type="password" v-model="password"
                                       name="password" autocomplete="false" ref="password">
                                <div class="show-password">
                                    <i class="fa fa-eye"></i>
                                </div>
                            </div>
                            <div class="wrap-input100 validate-input"
                                 data-validate="@lang('eventy/forgot.valid_repassword')">
                                <label for="email" class="label-input100">@lang('eventy/forgot.placeholder_repassword')</label>
                                <input class="input100" type="password" v-model="re_password"
                                       name="re_password" autocomplete="false" ref="re_password">
                                <div class="show-password">
                                    <i class="fa fa-eye"></i>
                                </div>
                            </div>

                            <div class="container-login100-form-btn p-t-30">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="login100-form-btn">
                                            @lang('eventy/forgot.text_btn_change')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-12 col-lg-6 login-register-image">
                    <img src="{{ preferGet('login_image') }}" alt="Welcome">
                </div>
            </div>
        </div>
    </main>
@endsection
@push('bottom')
    <script>
        key = "{{ request('key') }}";

        valid_email = "{{  __('eventy/forgot.valid_email') }}";
        valid_match = "{{  __('eventy/forgot.valid_match_password') }}";
    </script>
    <script src="{{ asset('js/web/Auth/change.js') }}"></script>
@endpush
