@extends('layout.Auth.index')

@section('page_title', __('eventy/login.title_page', ['NAME_EVENT' => preferGet('name_events')]))

@push('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/auth.css') }}">
@endpush

@section('content')

    <div id="root" class="container-login100">
        <div class="wrap-login100 row">
            <div id="formLogin" class="login100-form validate-form col-12 col-lg-6">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <div class="p-b-40">
                            <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                        </div>
                    </div>
                </div>
                <div class="p-b-30">
                    <h5 class="login100-form-title">{{ preferGet('login_title_text') ?? 'Welcome to the Login Page' }}</h5>
                    <p class="txt2">{!! preferGet('login_caption_text') ?? 'Please enter your registered email and password to join this event now' !!}</p>
                </div>
                <form v-on:submit.prevent="Login" class="row">
                    <div class="col-sm-12">

                        <div class="wrap-input100 validate-input" data-validate="@lang('eventy/login.valid_username')">
                            <label for="email" class="label-input100">@lang('eventy/login.placeholder_username')<span class="required">*</span></label>
                            <input class="input100" type="text" name="email"
                                v-model="email" autocomplete="false" ref="email">
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="@lang('eventy/login.valid_password')">
                            <label for="password" class="label-input100">@lang('eventy/login.placeholder_password')</label>
                            <a href="{{ url('forgot-password') }}" id="forgotPassword" class="red-text">
                                @lang('eventy/login.text_forgot')
                            </a>
                            <input class="input100" type="password" name="password"
                                   v-model="password" autocomplete="false" ref="password">
                            <div class="show-password">
                                <i class="fa fa-eye"></i>
                            </div>
                        </div>

                        <div class="checkbox-container">
                            <input type="checkbox" id="agree" v-model="agree">
                            <label for="agree">I Agree for Privacy Policies and <span class="blue-text">Terms and Conditions</span> </label>
                        </div>

                        <div class="container-login100-form-btn p-t-20">
                            <div class="row">
                                <div class="col-12 pb-2">
                                    <button class="login100-form-btn" :disabled="!agree">@lang('eventy/login.text_btn_login')</button>
                                </div>
                                <div class="col-12">
                                    <a href="{{ url('register') }}" class="login100-form-blue">Register</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="col-12 col-lg-6 login-register-image">
                <img src="{{ preferGet('login_image') }}" alt="Welcome">
            </div>
        </div>
    </div>

@endsection
@push('bottom')
    <script>
        let is_event_locked = '{{ $is_event_locked }}';
        let date_start = '{{ $date_start }}';
        let date_start_message = '{{ $date_start_message }}';
    </script>
    <script src="{{ asset('js/web/Auth/login.js') . '?v=' . date('Ymd') }}"></script>
@endpush
