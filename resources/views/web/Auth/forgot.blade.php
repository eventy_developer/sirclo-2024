@extends('layout.Auth.index')

@section('page_title', __('eventy/forgot.title_page', ['NAME_EVENT' => preferGet('name_events')]))

@push('head')
    <link rel="stylesheet" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Auth/auth.css') }}">
@endpush

@section('content')
    <main class="limiter">
        <div id="root" class="container-login100">
            <div class="wrap-login100 row">
                <div class="login100-form validate-form col-12 col-lg-6">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <div class="p-b-40">
                                <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                            </div>
                        </div>
                    </div>

                    <div class="p-b-30">
                        <h5 class="login100-form-title">{{ preferGet('reset_password_title_text') ?? 'Reset Password' }}</h5>
                        <p class="txt2">{{ preferGet('reset_password_caption_text') ?? 'Enter the email you previously registered for further verification' }}</p>
                    </div>

                    <form v-on:submit.prevent="Forgot" class="row">
                        <div class="col-sm-12">

                            <div class="wrap-input100 validate-input"
                                 data-validate="@lang('eventy/forgot.valid_username')">
                                <label for="email" class="label-input100">@lang('eventy/forgot.placeholder_username')</label>
                                <input class="input100" type="text" name="email"
                                       v-model="email" autocomplete="false" ref="email">
                            </div>

                            <div class="container-login100-form-btn p-t-30">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="login100-form-btn">
                                            @lang('eventy/forgot.text_btn_send')
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center pt-3">
                                        <a href="{{ url('login') }}" class="login100-form-blue">
                                            <strong>Cancel</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-12 col-lg-6 login-register-image">
                    <img src="{{ preferGet('login_image') }}" alt="Welcome">
                </div>
            </div>
        </div>
    </main>
@endsection
@push('bottom')
    <script>
        valid_email = "{{  __('eventy/forgot.valid_email') }}";
        valid_match = "{{  __('eventy/forgot.valid_match_password') }}";
    </script>
    <script src="{{ asset('js/web/Auth/forgot.js') }}"></script>
@endpush
