<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $file_name }}</title>
    <link rel="shortcut icon" href="{{ getS3EndpointFile(preferGet('icon_events')) }}">

    <style type="text/css">
        @font-face {
            font-family: 'Inter-Regular';
            src: url({{ storage_path('fonts/Inter/Inter-Regular.ttf') }});
        }
        @page {
            margin: 0;
            padding: 0;
        }
        body {
            font-family: 'Inter-Regular', "Helvetica Neue", Helvetica, sans-serif;
            margin: 0;
            padding: 0;
        }
        h1, h3, h4, h5, h6, p, span, b, strong {
            font-weight: normal;
            box-sizing: border-box;
        }
        h2 {
            font-weight: bold;
            box-sizing: border-box;
        }
        .row::after {
            content: "";
            clear: both;
            display: table;
        }
        [class*="col-"] {
            float: left;
            padding: 15px;
        }
        .col-1 { width: 8.33%; }
        .col-2 { width: 16.66%; }
        .col-3 { width: 25%; }
        .col-4 { width: 33.33%; }
        .col-5 { width: 41.66%; }
        .col-6 { width: 50%; }
        .col-7 { width: 58.33%; }
        .col-8 { width: 66.66%; }
        .col-9 { width: 75%; }
        .col-10 { width: 83.33%; }
        .col-11 { width: 91.66%; }
        .col-12 { width: 100%; }
        table {
            padding-top: 12px;
            border-collapse: collapse;
            width: 100%;
        }
        p {
            font-size: 13pt !important;
        }
    </style>
</head>
<body>

<div class="col-12">
    <table style="padding: 0 30px;">
        <tr>
            <td align="center">
                <br><br>
                <h2 style="color: #000; margin: 0; font-size: @if(strlen($res->name) > 35) 55px @else 65px @endif;">
                    {{ strtoupper($res->name) }}
                </h2>
                <br>
                <h2 style="color: #000; font-size: @if(strlen($res->company) > 35) 55px @else 65px @endif;">
                    {{ strtoupper($res->company) }}
                </h2>
            </td>
        </tr>
    </table>
</div>

<img src="{{ $qr_code }}" alt="" style="width: 90%; position: absolute; top: 43%; left: 50%; transform: translateX(-50%); z-index: 999;">

{{-- <img src="{{ $bg }}" style="width: 100%; position: absolute;"> --}}

</body>
</html>
