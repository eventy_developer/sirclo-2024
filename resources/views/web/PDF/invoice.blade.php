<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $file_name }}</title>
    <style>

        @font-face {
            font-family: "Inter-Regular";
            src: url('{{ storage_path('fonts/Inter/Inter-Regular.ttf') }}');
        }

        @font-face {
            font-family: "Inter-Medium";
            src: url('{{ storage_path('fonts/Inter/Inter-Medium.ttf') }}');
        }

        @font-face {
            font-family: "Inter-Bold";
            src: url('{{ storage_path('fonts/Inter/Inter-Bold.ttf') }}');
        }

        * {
            font-family: "Inter-Regular", Helvetica, sans-serif;
            font-size: 15px;
            color: #383838;
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .container {
            /* background-color: #eaeaea; */
        }

        .body {
            /* margin: 5% 25%; */
            background-color: white;
            width: 100%;
            /* max-width: 500px; */
            /* border: solid 1px #ddd; */
            /*padding: 40px 20px;*/
        }

        .header-invoice {
            display: block;
            text-align: center;
        }

        .header-title {
            font-size: 24px;
            margin: 0;
            margin-bottom: 4px;
            font-family: 'Inter-Bold', Helvetica, sans-serif;
        }

        .invoice-title {
            font-size: 18px;
            margin: 0 0 5px;
            font-family: 'Inter-Medium', Helvetica, sans-serif;
            color: #484848;
        }

        .order-id {
            margin: 0 0 5px;
            font-family: 'Inter-Medium', Helvetica, sans-serif;
            color: #484848;
        }
        .payment-method {
            margin: 0 0 5px;
            font-size: 13px;
            font-family: 'Inter-Medium', Helvetica, sans-serif;
            margin-bottom: 10px;
            color: #484848;
        }

        .header-invoice img {
            height: 50px;
            background: transparent;
            margin-bottom: 15px;
        }

        .body-email {
            margin-top: 20px;
            margin-bottom: 15px;
            /*padding: 0 20px;*/
        }

        .body-email .content-header .title {
            font-weight: bold;
            margin: 0;
            margin-bottom: 17px;
            line-height: 20px;
            font-size: 16px;
        }

        .body-email .content-header .caption {
            margin: 0;
            line-height: 20px;
        }

        .box-gray-top {
            background: #F6F6F6;
            padding: 15px;
            margin: 10px auto;
        }

        .box-gray-top h4 {
            margin: 0;
            line-height: 20px;
            padding-bottom: 10px;
            border-bottom: solid 1px #EBEBEB;
        }

        .box-gray-top .text {
            margin: 15px 0px;
            line-height: 20px;
        }

        .table-outer {
            background: #FBFBFB;
            border-radius: 15px;
            padding: 10px 15px;
        }

        .table {
            border-collapse: collapse;
            width: 100%;
            max-width: 100%;
            margin: 0 auto;
        }

        .table thead th {
            border-top: 0;
            border-bottom-width: 1px;
            vertical-align: bottom;
            font-weight: bold;
            text-align: left;
        }

        .table td, .table th {
            font-size: 16px;
            vertical-align: middle;
            line-height: 1;
            padding: 10px 8px;
            border-top: 2px solid #DADADA;
            text-align: left;
            color: #6F6F6F;
        }

        .table th,
        .table-title {
            color: #2E3538;
            font-family: 'Inter-Bold', Helvetica, sans-serif;
        }

        .table-title {
            width: 40%;
        }

        .table tr:first-child th,
        .table tr:first-child td {
            border-top: none;
        }

        .content-footer {
            margin-top: 15px;
        }

        .content-footer .caption {
            margin: 0;
            line-height: 20px;
            margin-bottom: 10px;
        }

        .footer-email {
            display: block;
            text-align: center;
            color: #939393;
        }

        .detail {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .detail-title {
            font-size: 22px;
        }

        .img-paid-wrapper {
            width: 100%;
        }
        .img-paid {
            height: 90px;
            float: right;
        }

        .caption {
            font-size: 12px;
            color: #6F6F6F;
            margin: 0;
        }

        /* @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
                padding: 40px 0;
            }

            .body-email {
                padding: 0;
            }

            .content-header,
            .content-footer {
                padding: 15px;
            }

            .body-email .content-header .title, .body-email .content-header .caption {
                line-height: 13px;
            }

            .box-gray-top h4, .box-gray-top .text {
                line-height: 13px;
            }

            .table td, .table th {
                padding: 17px 12px;
            }

            .content-footer .caption {
                line-height: 13px;
            }
        } */
    </style>
</head>
<body>
<div class="container">
    <div class="body">
        <div class="header-invoice">
            <img src="{{ preferGet('logo_events') }}" alt="Logo {{ preferGet('name_events') }}">
            <h1 class="header-title" style="color: {{ $color_primary }};">Registration {{ preferGet('name_events') }}</h1>
            <h3 class="invoice-title">Payment Receipt</h3>
            <h3 class="order-id">No : {{ $transaction->invoice_code }}</h3>
            <h3 class="payment-method">
                Method :
                @if($transaction->type === 'Virtual Accounts')
                    {{ $transaction->type }} ({{ $transaction->payment_method }})
                @else
                    {{ str_replace(['_'], ' ', $transaction->payment_method) }}
                @endif
            </h3>

            @if(preferGet('contact_email_events') || preferGet('contact_name_1') || preferGet('contact_name_2'))
                <p class="caption">For any registration enquiries please contact:</p>
            @endif

            @if(preferGet('contact_name_1') && preferGet('contact_phone_1'))
                <p class="caption">{{ preferGet('contact_name_1') }} : {{ preferGet('contact_phone_1') }}</p>
            @endif

            @if(preferGet('contact_name_2') && preferGet('contact_phone_2'))
                <p class="caption">{{ preferGet('contact_name_2') }} : {{ preferGet('contact_phone_1') }}</p>
            @endif

            @if(preferGet('contact_email_events'))
                <p class="caption">Email : {{ preferGet('contact_email_events') }}</p>
            @endif
        </div>
        <div class="body-email">

            <div class="table-outer" style="margin-bottom: 20px;">
                <table class="table">
                    <tr>
                        <td><span class="table-title">Name</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</td>
                        <td width="40%" style="padding-left: 0;">{{ $transaction->participant->name }}</td>
                        <td><span class="table-title">Phone</span> :</td>
                        <td style="padding-left: 0;">{{ $transaction->participant->phone }}</td>
                    </tr>
                    <tr>
                        <td><span class="table-title">Company</span> :</td>
                        <td style="padding-left: 0;">{{ $transaction->participant->company }}</td>
                        <td><span class="table-title">Email</span>&nbsp;&nbsp;&nbsp;:</td>
                        <td style="padding-left: 0;">{{ $transaction->participant->email }}</td>
                    </tr>
                </table>
            </div>


            <div class="detail" style="position: relative;width: 100%;margin-top: 0;">
                <img src="{{ asset('images/img_paid_full.svg') }}" alt=""
                     style="position: absolute;width: 80%;top: 30px;left: 50%;transform: translateX(-50%);">
                <h1 class="detail-title" style="color: {{ $color_primary }};margin-top: 0;">Registration Detail</h1>
                <div class="table-outer">
                    <table class="table">
                        @if($transaction->transactionTicket)
                            <tr>
                                <th width="60%" colspan="3">Ticket</th>
                                <th>Price</th>
                            </tr>
                            <tr>
                                <td width="60%" colspan="3">{{ $transaction->transactionTicket->ticket->name }}</td>
                                <td>
                                    @if($transaction->transactionTicket->price != 0)
                                        Rp {{ number_format($transaction->transactionTicket->price,0,'.','.') }}
                                    @else
                                        FREE
                                    @endif
                                </td>
                            </tr>
                        @endif

                            @if(count($transaction->transactionDetails) > 0)
                                <tr>
                                    <th width="60%" colspan="3">Participant List</th>
                                </tr>
                                @foreach($transaction->transactionDetails as $item)
                                    <tr>
                                        <td width="60%" colspan="3">{{ $item->participant->name }}</td>
                                    </tr>
                                @endforeach
                            @endif

{{--                        @if(count($transaction->transactionDetails) > 0)--}}
{{--                            <tr>--}}
{{--                                @if($transaction->transactionTicket)--}}
{{--                                    <th colspan="4" style="text-align: left;">Type</th>--}}
{{--                                @else--}}
{{--                                    <th width="60%" colspan="3" style="text-align: left;">Type</th>--}}
{{--                                    <th style="text-align: left;">Price</th>--}}
{{--                                @endif--}}
{{--                            </tr>--}}

{{--                            @foreach($transaction->transactionDetails as $item)--}}
{{--                                <tr>--}}
{{--                                    <td colspan="3">{{ $item->ticketSchedule->schedule->name }}</td>--}}
{{--                                    <td>Rp {{ number_format($item->price,0,'.','.') }}</td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}

{{--                        <tr>--}}
{{--                            <th colspan="3" style="text-align: right;font-size: 16px;">Transaction Fee</th>--}}
{{--                            <th style="font-size: 16px;">--}}
{{--                                Rp {{ number_format($transaction->total_fee,0,'.','.') }}--}}
{{--                            </th>--}}
{{--                        </tr>--}}
                        <tr>
                            <th colspan="3" style="text-align: right;font-size: 18px;">Total</th>
                            <th style="font-size: 18px;">Rp {{ number_format($transaction->amount,0,'.','.') }}</th>
                        </tr>
                    </table>
                </div>
                <p style="color: #6F6F6F;text-align: center;margin-top: 30px;font-size: 12px;font-style: italic;">Invoice Generated by System: {{ date('d M Y H:i', strtotime($transaction->updated_at)) }} GMT+7</p>
            </div>

            {{--            @if($transaction->getStatus() === "SUCCESS")--}}
            {{--                <div class="img-paid-wrapper">--}}
            {{--                    <img src="{{ asset('template/images/payment/img_paid_bottom.svg') }}" alt="" class="img-paid">--}}
            {{--                </div>--}}
            {{--            @else--}}
            {{--                <div class="img-paid-wrapper">--}}
            {{--                    <img src="{{ asset('example/image/img_unpaid.png') }}" alt="" class="img-paid">--}}
            {{--                </div>--}}
            {{--            @endif--}}

        </div>
    </div>
</div>
</body>
</html>
