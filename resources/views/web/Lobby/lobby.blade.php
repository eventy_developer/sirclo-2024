@extends('layout.Web.index')

@section('title', 'Lobby')

@push('head')
    <style>
        .partner-list {
            max-height: calc(100vh - 330px);
        }
        #virtual3d-iframe {
            width: 100%;
            height: 100vh;
        }
    </style>
@endpush

@section('content')
<iframe id="virtual3d-iframe" class="iframe-3d" src="3d/{{ config('eventy.3d_version') }}" frameborder="0"></iframe>

<x-drawer.button :drawer-buttons="$drawerButtons">
    <x-drawer.offcanvas>
        @include('web.Lobby.offcanvas-content')
    </x-drawer.offcanvas>
</x-drawer.button>


<a href="{{ preferGet('help_center_link') }}" target="_blank" class="btn-float-whatsapp">
    <img src="{{ asset('images/button/btn_whatsapp.svg') }}" alt="Btn Whatsapp">
    <p>Help Center</p>
</a>

@endsection

@push('bottom')
<script>
    let is_video = '{{ $is_video }}';
    let sponsors = {!! json_encode($sponsors) !!};
    {{--let exdays = {{ $exdays }};--}}
</script>
<script src="{{ asset('js/web/Booth/template.js') . '?v=1.0.0' }}"></script>
<script src="{{ asset('js/web/Home/home.js') . '?v=' . date('Ymd') }}"></script>
@endpush
