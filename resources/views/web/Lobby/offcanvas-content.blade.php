<ul class="nav nav-tabs nav-tabs-custom" id="tabDrawer" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="schedule-tab" data-bs-toggle="tab" data-bs-target="#schedule" type="button" role="tab" aria-controls="schedule" aria-selected="true">
            <svg width="14" height="13" viewBox="0 0 14 13" fill="#28B7FB" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.9642 0.927289H11.2182V0H10.3091V0.927289H3.69094V0H2.78183V0.927289H1.03584C0.464792 0.927289 0 1.39198 0 1.96313V11.637C0 12.208 0.464692 12.6728 1.03584 12.6728H12.9642C13.5352 12.6728 14 12.2081 14 11.637V1.96313C14 1.39195 13.5353 0.927289 12.9642 0.927289ZM11.3137 4.38164C11.4645 4.38164 11.5865 4.50359 11.5865 4.65437V6.06932C11.5865 6.2201 11.4645 6.34205 11.3137 6.34205H9.8987C9.74818 6.34205 9.62597 6.2201 9.62597 6.06932V4.65437C9.62597 4.50359 9.74818 4.38164 9.8987 4.38164H11.3137ZM9.6805 8.25424C9.6805 8.10346 9.80271 7.98151 9.95322 7.98151H11.3683C11.5191 7.98151 11.641 8.10346 11.641 8.25424V9.66919C11.641 9.81997 11.5191 9.94192 11.3683 9.94192H9.95322C9.80271 9.94192 9.6805 9.81997 9.6805 9.66919V8.25424ZM6.01968 8.25424C6.01968 8.10346 6.14163 7.98151 6.29241 7.98151H7.70736C7.85815 7.98151 7.98009 8.10346 7.98009 8.25424V9.66919C7.98009 9.81997 7.85814 9.94192 7.70736 9.94192H6.29241C6.14163 9.94192 6.01968 9.81997 6.01968 9.66919V8.25424ZM5.96514 6.06902V4.65407C5.96514 4.50328 6.08709 4.38134 6.23786 4.38134H7.65282C7.8036 4.38134 7.92555 4.50329 7.92555 4.65407V6.06902C7.92555 6.2198 7.8036 6.34175 7.65282 6.34175H6.23786C6.08708 6.34175 5.96514 6.2198 5.96514 6.06902V6.06902ZM3.992 4.38147C4.14252 4.38147 4.26473 4.50342 4.26473 4.6542V6.06916C4.26473 6.21994 4.14252 6.34188 3.992 6.34188H2.57695C2.42617 6.34188 2.30422 6.21993 2.30422 6.06916V4.6542C2.30422 4.50342 2.42617 4.38147 2.57695 4.38147H3.992ZM2.35875 8.25407C2.35875 8.10329 2.4807 7.98134 2.63147 7.98134H4.04653C4.19705 7.98134 4.31926 8.10329 4.31926 8.25407V9.66903C4.31926 9.81981 4.19705 9.94175 4.04653 9.94175H2.63147C2.48069 9.94175 2.35875 9.8198 2.35875 9.66903V8.25407Z"/>
            </svg>
            <span class="ms-2">Schedule</span>
        </button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="boothlist-tab" data-bs-toggle="tab" data-bs-target="#boothlist" type="button" role="tab" aria-controls="boothlist" aria-selected="false">
            <svg width="14" height="11" viewBox="0 0 14 11" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.54555 1.27277C2.54555 1.97568 1.97568 2.54555 1.27277 2.54555C0.569868 2.54555 0 1.97568 0 1.27277C0 0.569868 0.569868 0 1.27277 0C1.97568 0 2.54555 0.569868 2.54555 1.27277Z"/>
                <path d="M13.3636 0.636475H4.45466C4.22731 0.636475 4.01723 0.757711 3.90355 0.954586C3.78988 1.15146 3.78988 1.39414 3.90355 1.59103C4.01723 1.78791 4.22731 1.90914 4.45466 1.90914H13.3636C13.591 1.90914 13.8011 1.7879 13.9147 1.59103C14.0284 1.39415 14.0284 1.15147 13.9147 0.954586C13.8011 0.757708 13.591 0.636475 13.3636 0.636475Z"/>
                <path d="M2.54555 5.09101C2.54555 5.79392 1.97568 6.36379 1.27277 6.36379C0.569868 6.36379 0 5.79392 0 5.09101C0 4.38811 0.569868 3.81824 1.27277 3.81824C1.97568 3.81824 2.54555 4.38811 2.54555 5.09101Z"/>
                <path d="M13.3636 4.45471H4.45466C4.22731 4.45471 4.01723 4.57595 3.90355 4.77282C3.78988 4.9697 3.78988 5.21238 3.90355 5.40927C4.01723 5.60614 4.22731 5.72738 4.45466 5.72738H13.3636C13.591 5.72738 13.8011 5.60614 13.9147 5.40927C14.0284 5.21239 14.0284 4.96971 13.9147 4.77282C13.8011 4.57595 13.591 4.45471 13.3636 4.45471Z"/>
                <path d="M2.54555 8.90925C2.54555 9.61215 1.97568 10.182 1.27277 10.182C0.569868 10.182 0 9.61215 0 8.90925C0 8.20634 0.569868 7.63647 1.27277 7.63647C1.97568 7.63647 2.54555 8.20634 2.54555 8.90925Z"/>
                <path d="M13.3636 8.27295H4.45466C4.22731 8.27295 4.01723 8.39419 3.90355 8.59106C3.78988 8.78794 3.78988 9.03062 3.90355 9.2275C4.01723 9.42438 4.22731 9.54561 4.45466 9.54561H13.3636C13.591 9.54561 13.8011 9.42438 13.9147 9.2275C14.0284 9.03062 14.0284 8.78794 13.9147 8.59106C13.8011 8.39418 13.591 8.27295 13.3636 8.27295Z"/>
            </svg>
            <span class="ms-2">Booth List</span>
        </button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="sponsor-tab" data-bs-toggle="tab" data-bs-target="#sponsor" type="button" role="tab" aria-controls="sponsor" aria-selected="false">
            <svg width="14" height="13" viewBox="0 0 14 13" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.6231 3.29744C11.6231 4.65158 10.5254 5.74927 9.17132 5.74927C7.81718 5.74927 6.71927 4.65154 6.71927 3.29744C6.71927 1.94335 7.81709 0.845395 9.17132 0.845395C10.5255 0.845395 11.6231 1.94322 11.6231 3.29744Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.17132 5.74927C5.7893 5.74927 4.05621 9.59628 4.39433 12.5977H13.9483C14.3288 9.59628 12.5956 5.74927 9.17132 5.74927Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.70494 1.90229C5.70494 2.95292 4.85318 3.80468 3.80255 3.80468C2.75193 3.80468 1.90026 2.95292 1.90026 1.90229C1.90026 0.851671 2.75193 0 3.80255 0C4.85318 0 5.70494 0.851671 5.70494 1.90229Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M3.80246 3.88931C1.09688 3.88931 -0.255791 6.89071 0.0400296 9.25817H4.26745C4.77476 7.8208 5.66246 6.55259 6.88844 5.87614C6.25433 4.73475 5.23974 3.88931 3.80246 3.88931Z"/>
            </svg>
            <span class="ms-2">Sponsor</span>
        </button>
    </li>
</ul>
<div class="tab-content" id="tabDrawerContent">
    <div class="tab-pane fade show active" id="schedule" role="tabpanel" aria-labelledby="schedule-tab">
        <div class="tab-content__header">
            <h5 class="tab-content__title">All Schedule</h5>
            <p class="tab-content__desc">Non aenean a feugiat natoque</p>
        </div>
        <div class="tab-content__list">
            <div class="d-flex mb-5">
                <div class="form-filter-group me-3">
                    <small class="date-label">Schedule Date</small>
                    <select name="" class="form-control form-filter">
                        <option>Thursday, 21 July 2022</option>
                        <option>Friday, 22 July 2022</option>
                    </select>
                </div>
                <div class="form-filter-group">
                    <small class="date-label">Location</small>
                    <select name="" class="form-control form-filter">
                        <option value="All">All</option>
                        <option value="Main Stage">Main Stage</option>
                        <option value="Classroom">Classroom</option>
                    </select>
                </div>
            </div>

            <ul class="scroll-list">
                <li class="">
                    <a href="javascript:void(0)" class="schedule-item">
                        <div class="schedule-img">
                            <span class="schedule-item__status schedule-item__status--live">LIVE NOW</span>
                            <img src="{{ asset('images/placeholder/ph_schedule_frame.svg') }}" alt="Icon Schedule">
                        </div>
                        <div class="schedule-content">
                            <h6 class="schedule-title">Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur</h6>
                            <div class="schedule-time">
                                <img src="{{ asset('images/icon/icon_clock.svg') }}" alt="Icon Clock" class="schedule-content__icon">
                                <span>08:00 - 09:00 WIB</span>
                            </div>
                            <div class="schedule-marker">
                                <img src="{{ asset('images/icon/icon_loc.svg') }}" alt="Icon Location" class="schedule-content__icon">
                                <span>Main Stage</span>
                            </div>
                            <div class="schedule-speaker">
                                <img src="{{ asset('images/icon/icon_speaker.svg') }}" alt="Icon Speaker" class="schedule-content__icon">
                                <span>Richard Cholin</span>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>

        </div>
    </div>
    <div class="tab-pane fade" id="boothlist" role="tabpanel" aria-labelledby="boothlist-tab">
        <div class="tab-content__header">
            <h5 class="tab-content__title">Booth List</h5>
            <p class="tab-content__desc">Non aenean a feugiat natoque</p>
        </div>
        <div class="tab-content__list">
            <div class="d-flex mb-5">
                <div class="form-filter-group me-3">
                    <small class="date-label">Category</small>
                    <select name="" class="form-control form-filter" v-model="selectedCategoryId" v-cloak>
                        <option
                            :value="category.id"
                            v-for="(category, index) in categories">@{{ category.name }}
                        </option>
                    </select>
                </div>
                <div class="form-filter-group">
                    <small class="date-label">Search</small>
                    <input type="text" class="form-control form-filter" placeholder="Find a Booth" v-model="searchBooth">
                </div>
            </div>

            <ul class="booth-list scroll-list">
                <li class="booth-list__item"
                    v-for="(booth, i) in filterBooths">
                    <div class="booth-list__icon">
                        <img src="{{ asset('images/placeholder/ph_booth_icon.svg') }}" alt="Icon Booth">
                    </div>
                    <div class="booth-list__content">
                        <h5 class="booth-list__content__title" v-cloak>@{{ booth.name }}</h5>
                        <p class="booth-list__content__subtitle">
                            <svg width="11" height="11" viewBox="0 0 11 11" fill="#28B7FB" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.90499 8.62882V9.75713C7.90499 9.96514 7.7332 10.1331 7.52045 10.1331H0.853871C0.641033 10.1331 0.469238 9.96514 0.469238 9.75694V8.62864C0.469238 7.30224 1.34867 6.17515 2.56793 5.77657C3.03456 6.06865 3.58838 6.23669 4.18201 6.23669C4.77687 6.23669 5.33195 6.06749 5.79993 5.77417C7.02304 6.17158 7.90506 7.29981 7.90506 8.62876L7.90499 8.62882Z"/>
                                <path d="M6.55383 2.50506V3.18962C6.55383 4.49858 5.48974 5.56139 4.18207 5.56139C2.87437 5.56139 1.8103 4.49853 1.8103 3.18962L1.81039 2.50506C1.81039 1.19737 2.87448 0.133301 4.18215 0.133301C5.48976 0.13339 6.55392 1.19739 6.55392 2.50506H6.55383Z"/>
                                <path d="M7.83721 2.95377C7.55926 2.95377 7.29752 3.02155 7.06643 3.13969V3.18968C7.06643 4.00803 6.72294 4.74696 6.17358 5.27245C6.17779 5.30733 6.18333 5.34185 6.18977 5.37592C6.88257 5.65421 7.45312 6.129 7.84355 6.72217C8.76492 6.71868 9.51379 5.98244 9.51379 5.07581V4.60041C9.51379 3.69263 8.76162 2.95377 7.83721 2.95377L7.83721 2.95377Z"/>
                                <path d="M8.98095 7.03821C8.73888 7.19068 8.46379 7.29514 8.16895 7.34065C8.32974 7.74246 8.41773 8.17743 8.41773 8.62887V9.75717C8.41773 9.89167 8.38661 10.0188 8.33224 10.1333H10.1971C10.3476 10.1331 10.4689 10.0139 10.4689 9.8661V9.06492C10.4689 8.12155 9.84554 7.32037 8.98094 7.03823L8.98095 7.03821Z"/>
                            </svg>
                            <span>10 people</span> visited
                        </p>
                    </div>
                </li>
            </ul>

        </div>
    </div>
    <div class="tab-pane fade" id="sponsor" role="tabpanel" aria-labelledby="sponsor-tab">
        <div class="tab-content__header">
            <h5 class="tab-content__title">Sponsor</h5>
            <p class="tab-content__desc">Non aenean a feugiat natoque</p>
        </div>
        <div class="tab-content__list">
            <div class="partner-list scroll-list">

                <div :class="'partner-col-' + sponsor.column_size"
                    v-for="(sponsor, i) in sponsors">
                    <span class="partner-label" v-cloak>@{{ sponsor.name }}</span>
                    <div class="partner-item">
                        <a :href="item.link" target="_blank" class="partner-link"
                            v-for="(item, idx) in sponsor.sponsors">
                            <img :src="item.present_image" alt="Sponsor">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
