<ul class="nav nav-tabs nav-tabs-custom" id="tabDrawer" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="about-tab" data-bs-toggle="tab" data-bs-target="#about" type="button" role="tab" aria-controls="about" aria-selected="true">
            <svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                <path d="M7 14C8.85656 14 10.637 13.2625 11.9497 11.9497C13.2625 10.637 14 8.8564 14 7C14 5.1436 13.2625 3.36304 11.9497 2.05026C10.637 0.737531 8.8564 0 7 0C5.1436 0 3.36304 0.737531 2.05026 2.05026C0.737531 3.36299 0 5.1436 0 7C0 8.8564 0.737531 10.637 2.05026 11.9497C3.3631 13.2625 5.1436 14 7 14ZM7 2.37551C7.20063 2.37551 7.39311 2.45526 7.53495 2.59708C7.67677 2.7389 7.75652 2.93139 7.75652 3.13203C7.75652 3.33267 7.67677 3.52503 7.53495 3.66686C7.39313 3.80879 7.20064 3.88844 7 3.88844C6.79936 3.88844 6.607 3.80879 6.46516 3.66686C6.32324 3.52504 6.24359 3.33267 6.24359 3.13203C6.24359 2.93139 6.32324 2.73892 6.46516 2.59708C6.60698 2.45526 6.79936 2.37551 7 2.37551ZM5.1086 5.87219C5.37998 5.51694 5.71072 5.21116 6.08614 4.96855C6.80836 4.52182 7.75661 4.72582 7.75661 5.55405V9.70045C7.75661 10.3212 8.15537 10.4043 8.64345 10.1853C8.72754 10.1461 8.82748 10.1691 8.88607 10.241C8.94456 10.313 8.94673 10.4155 8.89128 10.4898C8.61991 10.8451 8.28917 11.1509 7.91375 11.3935C7.19153 11.8402 6.24327 11.6362 6.24327 10.808L6.24338 6.66158C6.24338 6.0408 5.84463 5.95768 5.35654 6.17677C5.27245 6.21594 5.17251 6.19294 5.11392 6.121C5.05543 6.04906 5.05326 5.94652 5.1086 5.87219Z"/>
            </svg>
            <span class="ms-2">About</span>
        </button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="product-tab" data-bs-toggle="tab" data-bs-target="#product" type="button" role="tab" aria-controls="product" aria-selected="true">
            <svg width="14" height="17" viewBox="0 0 14 17" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 5.32677L6.41574 8.9932V16.757L0 13.091V5.32677ZM14 5.32677L7.58426 8.9932V16.757L14 13.091V5.32677ZM7.00344 7.92185L0.0713148 3.9611L7.00344 0L13.9287 3.9611L7.00344 7.92185Z"/>
            </svg>
            <span class="ms-2">Product</span>
        </button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="attachment-tab" data-bs-toggle="tab" data-bs-target="#attachment" type="button" role="tab" aria-controls="attachment" aria-selected="false">
            <svg width="14" height="16" viewBox="0 0 14 16" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.84067 15.271C2.10681 15.2874 1.39664 15.0108 0.867318 14.5023C0.350202 13.9813 0.0662215 13.273 0.0799781 12.539C0.105781 11.4956 0.544673 10.5052 1.29995 9.78506L8.01797 3.14006C8.93091 2.23564 10.3037 2.12189 11.0723 2.88558H11.0722C11.4119 3.22944 11.5989 3.69532 11.5915 4.17859C11.5787 4.84484 11.3 5.47834 10.8177 5.93816L4.4715 12.2165C4.34213 12.3445 4.15428 12.3936 3.97874 12.3454C3.80333 12.2974 3.66673 12.1592 3.62071 11.9832C3.57466 11.8071 3.62603 11.6199 3.75552 11.492L10.1017 5.20518H10.1016C10.3865 4.93756 10.5552 4.5691 10.5717 4.17861C10.5789 3.96805 10.5005 3.76364 10.3545 3.61177C9.98115 3.24356 9.2363 3.35895 8.72542 3.8646L2.01619 10.5096C1.45232 11.0397 1.12207 11.7722 1.09816 12.5458C1.08419 13.0073 1.25878 13.4546 1.5818 13.7846C2.37768 14.5703 3.86752 14.3751 4.90429 13.3485L11.6138 6.69686C12.4843 5.83483 12.9815 4.73027 12.9815 3.66464H12.9814C13.0018 2.95221 12.7309 2.26229 12.2315 1.7539C11.0182 0.56613 8.76806 0.834202 7.21544 2.37328L0.867393 8.65345C0.737905 8.78128 0.550051 8.83052 0.374639 8.78235C0.199106 8.73417 0.0626304 8.59604 0.0166032 8.42004C-0.029439 8.24403 0.0218113 8.05679 0.1513 7.92883L6.49935 1.64896C8.44558 -0.278694 11.3387 -0.556977 12.9474 1.02958C13.6379 1.73089 14.0172 2.68074 13.9994 3.66482C13.9994 5.00018 13.392 6.36967 12.3297 7.42153L5.62044 14.0662C4.88646 14.815 3.88913 15.2473 2.84093 15.271L2.84067 15.271Z" />
            </svg>
            <span class="ms-2">Attachment</span>
        </button>
    </li>
</ul>
<div class="tab-content" id="tabDrawerContent">
    <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">
        <div class="tab-content__header">
            <div class="tab-about__logo">
                <img src="{{ asset('images/example/logo.png') }}" alt="Logo" class="w-100 img-fluid">
            </div>
            <h5 class="tab-about__name">Booth 01</h5>
            <ul class="tab-about__contact">
               <li class="tab-about__contact__item">
                   <a href="" target="_blank" class="tab-about__contact__link">
                       <img src="{{ asset('images/button/btn_call.svg') }}" alt="Icon Call">
                   </a>
               </li>
                <li class="tab-about__contact__item">
                    <a href="" target="_blank" class="tab-about__contact__link">
                        <img src="{{ asset('images/button/btn_web.svg') }}" alt="Icon Call">
                    </a>
                </li>
                <li class="tab-about__contact__item">
                    <a href="" target="_blank" class="tab-about__contact__link">
                        <img src="{{ asset('images/button/btn_email.svg') }}" alt="Icon Call">
                    </a>
                </li>
            </ul>
            <div class="tab-about__description">
                <p class="tab-about__description__text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu vitae sit et lectus luctus. Tempus neque laoreet volutpat risus eu. Cras quis id turpis egestas eget vitae dictum vel. Nulla varius a, porta ultricies pretium, nam. Id nulla sed eros, in aliquam eget. Amet, blandit scelerisque in orci est, pharetra. Interdum dolor commodo viverra mauris vitae dui volutpat et mi. Egestas lobortis ullamcorper tristique sed.
                    Dolor quis mi ante bibendum egestas. Nunc egestas vitae consequat tortor dignissim. Amet velit, gravida orci quis maecenas. Tortor mauris id porta maecenas fringilla ultrices tempus velit. Dolor diam sem adipiscing mi egestas.
                    Sed bibendum sit congue fusce a nisi, vestibulum. Lectus convallis interdum mattis platea laoreet. Turpis nisl venenatis hendrerit.
                </p>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="product" role="tabpanel" aria-labelledby="product-tab">
        <div class="tab-content__header">
            <ul class="nav nav-tabs nav-tabs-attachment" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="handphone-tab" data-bs-toggle="tab"
                            data-bs-target="#handphone" type="button" role="tab"
                            aria-controls="handphone" aria-selected="true">
                        <span>Handphone</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="laptop-tab" data-bs-toggle="tab"
                            data-bs-target="#laptop" type="button" role="tab"
                            aria-controls="laptop" aria-selected="true">
                        <span>Laptop</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="speaker-tab" data-bs-toggle="tab"
                            data-bs-target="#speaker" type="button" role="tab"
                            aria-controls="speaker" aria-selected="true">
                        <span>Speaker</span>
                    </button>
                </li>
            </ul>
            <div class="tab-content" id="tabDrawerContent">
                <div class="tab-pane fade show active" id="handphone" role="tabpanel" aria-labelledby="handphone-tab">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <a href="#" class="card card-product" data-bs-toggle="modal" data-bs-target="#productModal">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 mb-4">
                            <a href="#" class="card card-product" data-bs-toggle="modal" data-bs-target="#productModal">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 mb-4">
                            <a href="#" class="card card-product" data-bs-toggle="modal" data-bs-target="#productModal">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 mb-4">
                            <a href="#" class="card card-product" data-bs-toggle="modal" data-bs-target="#productModal">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="laptop" role="tabpanel" aria-labelledby="laptop-tab">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="speaker" role="tabpanel" aria-labelledby="speaker-tab">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card card-product">
                                <div class="card-product__img">
                                    <img src="{{ asset('images/placeholder/ph_product.png') }}" alt="Image Product"
                                         class="w-100 img-fluid">
                                </div>
                                <div class="card-product__content">
                                    <h5 class="card-product__name">Sit lorem tellus varius ..</h5>
                                    <p class="card-product__desc">Venenatis sed ultricies id duis ..</p>
                                    <p class="card-product__price">Rp 150.000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="attachment" role="tabpanel" aria-labelledby="attachment-tab">
        <div class="tab-content__header">
            <ul class="nav nav-tabs nav-tabs-attachment" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="image-tab" data-bs-toggle="tab"
                            data-bs-target="#image" type="button" role="tab"
                            aria-controls="image" aria-selected="true">
                        <span>Image</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="document-tab" data-bs-toggle="tab"
                            data-bs-target="#document" type="button" role="tab"
                            aria-controls="document" aria-selected="true">
                        <span>Document</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="video-tab" data-bs-toggle="tab"
                            data-bs-target="#video" type="button" role="tab"
                            aria-controls="video" aria-selected="true">
                        <span>Video</span>
                    </button>
                </li>
            </ul>
            <div class="tab-content" id="tabDrawerContent">
                <div class="tab-pane fade show active" id="image" role="tabpanel" aria-labelledby="image-tab">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                            <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Attachment"
                                 class="w-100 img-fluid">
                        </div>
                        <div class="col-md-6 mb-4">
                            <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Attachment"
                                 class="w-100 img-fluid">
                        </div>
                        <div class="col-md-6 mb-4">
                            <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Attachment"
                                 class="w-100 img-fluid">
                        </div>
                        <div class="col-md-6 mb-4">
                            <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Attachment"
                                 class="w-100 img-fluid">
                        </div>
                        <div class="col-md-6 mb-4">
                            <img src="{{ asset('images/placeholder/ph_image.png') }}" alt="Image Attachment"
                                 class="w-100 img-fluid">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                    <ul class="tab-document__list">
                        <li class="tab-document__item">
                            <div class="tab-document__info">
                                <h5 class="tab-document__title">Vel amet sit euismod odio arcu vestibulum nec ..</h5>
                                <span class="tab-document__size">14 MB</span>
                            </div>
                            <a href="" target="_blank" class="btn-download-document">
                                <img src="{{ asset('images/icon/icon_download.svg') }}" alt="Icon Download"> Download
                            </a>
                        </li>
                        <li class="tab-document__item">
                            <div class="tab-document__info">
                                <h5 class="tab-document__title">Vel amet sit euismod odio arcu vestibulum nec ..</h5>
                                <span class="tab-document__size">14 MB</span>
                            </div>
                            <a href="" target="_blank" class="btn-download-document">
                                <img src="{{ asset('images/icon/icon_download.svg') }}" alt="Icon Download"> Download
                            </a>
                        </li>
                        <li class="tab-document__item">
                            <div class="tab-document__info">
                                <h5 class="tab-document__title">Vel amet sit euismod odio arcu vestibulum nec ..</h5>
                                <span class="tab-document__size">14 MB</span>
                            </div>
                            <a href="" target="_blank" class="btn-download-document">
                                <img src="{{ asset('images/icon/icon_download.svg') }}" alt="Icon Download"> Download
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                    <ul class="tab-video__list">
                        <li class="tab-video__item">
                            <div class="tab-video__view">
                                <img src="{{ asset('images/placeholder/ph_video.png') }}" alt="Thumbnail Video"
                                     class="w-100 img-fluid">
                                <img src="{{ asset('images/button/btn_play.svg') }}" alt="Icon Button Play" class="tab-video__btn-play">
                            </div>
                            <p class="tab-video__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat non in orci at eleifend neque ornare maecenas. </p>
                        </li>
                        <li class="tab-video__item">
                            <div class="tab-video__view">
                                <img src="{{ asset('images/placeholder/ph_video.png') }}" alt="Thumbnail Video"
                                     class="w-100 img-fluid">
                                <img src="{{ asset('images/button/btn_play.svg') }}" alt="Icon Button Play" class="tab-video__btn-play">
                            </div>
                            <p class="tab-video__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat non in orci at eleifend neque ornare maecenas. </p>
                        </li>
                        <li class="tab-video__item">
                            <div class="tab-video__view">
                                <img src="{{ asset('images/placeholder/ph_video.png') }}" alt="Thumbnail Video"
                                     class="w-100 img-fluid">
                                <img src="{{ asset('images/button/btn_play.svg') }}" alt="Icon Button Play" class="tab-video__btn-play">
                            </div>
                            <p class="tab-video__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat non in orci at eleifend neque ornare maecenas. </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
