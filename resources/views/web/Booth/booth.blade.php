@extends('layout.Web.index')

@section('title', 'Booth')

@section('content')
    <img src="{{ asset('images/placeholder/ph_exhibitionhall.png') }}" class="background-content" alt="Background Content">


    <div class="container container-booth page-content-wrapper">
        <div class="row">
            <div class="row-header">
                <div class="form-filter-group me-4">
                    <small class="date-label">Category</small>
                    <select name="" class="form-control form-filter" v-model="selectedCategoryId" v-cloak>
                        <option
                            :value="category.id"
                            v-for="(category, index) in categories">@{{ category.name }}
                        </option>
                    </select>
                </div>
                <div class="form-filter-group">
                    <small class="date-label">Search</small>
                    <input type="text" class="form-control form-filter" placeholder="Find a Booth" v-model="searchBooth">
                </div>

                <!-- TODO: implement sorting by like -->
                <p class="sort-by-like">Sort by Likes <img src="{{ asset('images/icon/icon_arrow_down.svg') }}" alt="Icon Down"/></p>
            </div>
        </div>
        <div class="row floorplan-booth-list scroll-list">

            <div class="col-lg-3"
                v-for="(booth, i) in filterBooths">
                <a class="card-booth" :href="`/booth/detail?category_id=${selectedCategoryId}&booth_id=${booth.id}`">
                    <span class="card-booth__like">
                        <img src="{{ asset('images/icon/icon_likes.svg') }}" alt="Icon Like"> 308
                    </span>

                    <div class="card-booth__thumbnail">
                        <img :src="booth.present_image" alt="Booth">
                    </div>

                    <div class="card-booth__inner">
                        <h5 class="card-booth__name" v-cloak>@{{ booth.name }}</h5>
                        <p class="card-booth__visited">1201 Visited</p>
                    </div>
                </a>
            </div>

        </div>
    </div>
@endsection

@push('bottom')
    <script src="{{ asset('js/web/Booth/template.js') . '?v=1.0.0' }}"></script>
    <script src="{{ asset('js/web/Booth/booth.js') . '?v=1.0.0' }}"></script>
@endpush
