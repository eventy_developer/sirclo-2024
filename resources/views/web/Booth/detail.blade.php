@extends('layout.Web.index')

@section('title', 'Detail')

@section('content')
<img src="{{ asset('images/placeholder/ph_exhibitionhall.png') }}" class="background-content" alt="Background Content">

<x-drawer.button :drawer-buttons="$drawerButtons">
    <x-drawer.offcanvas>
        @include('web.Booth.offcanvas-content')
    </x-drawer.offcanvas>
    <!-- Modal Product -->
    <div class="modal modal-product fade" id="productModal" tabindex="-1" aria-labelledby="productModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <!-- Slider main container -->
                            <div class="swiper swiper-product">
                                <!-- Additional required wrapper -->
                                <div class="swiper-wrapper">
                                    <!-- Slides -->

                                    <div class="swiper-slide">
                                        <div class="swiper-card-product">
                                            <img src="{{ asset('images/placeholder/ph_product@2x.png') }}"
                                                 alt="Image Product"
                                                 class="swiper-card-product__img">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-card-product">
                                            <img src="{{ asset('images/placeholder/ph_product@2x.png') }}"
                                                 alt="Image Product"
                                                 class="swiper-card-product__img">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-card-product">
                                            <img src="{{ asset('images/placeholder/ph_product@2x.png') }}"
                                                 alt="Image Product"
                                                 class="swiper-card-product__img">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- If we need pagination -->
                            <div class="swiper-pagination swiper-pagination__product"></div>

                            <!-- If we need navigation -->
                            <div class="swiper-product-navigation" role="navigation">
                                <div class="swiper-button-prev swiper-button-prev__product">
                                    <svg width="13" height="23" viewBox="0 0 13 23" fill="#ffffff" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.2002 0.418219C11.4661 0.686086 11.6154 1.04934 11.6154 1.42811C11.6154 1.80687 11.4661 2.17013 11.2002 2.43799L4.18106 9.50862L11.2002 16.5793C11.4585 16.8487 11.6015 17.2095 11.5982 17.584C11.595 17.9585 11.4459 18.3168 11.183 18.5816C10.9201 18.8465 10.5644 18.9967 10.1926 18.9999C9.8208 19.0032 9.4626 18.8592 9.19516 18.599L1.17345 10.5185C0.907609 10.2506 0.75827 9.88739 0.75827 9.50862C0.75827 9.12986 0.907609 8.7666 1.17345 8.49874L9.19516 0.418219C9.46108 0.150434 9.82169 0 10.1977 0C10.5737 0 10.9343 0.150434 11.2002 0.418219Z"/>
                                    </svg>
                                </div>
                                <div class="swiper-button-next swiper-button-next__product">
                                    <svg width="13" height="23" viewBox="0 0 13 23" fill="#ffffff" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.2002 0.418219C11.4661 0.686086 11.6154 1.04934 11.6154 1.42811C11.6154 1.80687 11.4661 2.17013 11.2002 2.43799L4.18106 9.50862L11.2002 16.5793C11.4585 16.8487 11.6015 17.2095 11.5982 17.584C11.595 17.9585 11.4459 18.3168 11.183 18.5816C10.9201 18.8465 10.5644 18.9967 10.1926 18.9999C9.8208 19.0032 9.4626 18.8592 9.19516 18.599L1.17345 10.5185C0.907609 10.2506 0.75827 9.88739 0.75827 9.50862C0.75827 9.12986 0.907609 8.7666 1.17345 8.49874L9.19516 0.418219C9.46108 0.150434 9.82169 0 10.1977 0C10.5737 0 10.9343 0.150434 11.2002 0.418219Z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="modal-product__info">
                                <span class="modal-product__copy">Copy</span>
                                <h5 class="modal-product__name">Lorem ipsum dolor</h5>
                                <p class="modal-product_price">Rp 150.000</p>
                                <div class="modal-product__desc">
                                    <p class="modal-product__desc__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est blandit volutpat nam adipiscing orci. Morbi platea maecenas maecenas gravida tempor sit. Mauris auctor eu in facilisis felis facilisi in. Egestas eget etiam quis dolor. Consequat laoreet duis neque auctor adipiscing dui tincidunt. Arcu sit id aenean laoreet nisl ullamcorper integer praesent sociis. Lorem eget varius nibh eros dictum. Adipiscing aliquet nunc, bibendum aliquam aliquet morbi. In imperdiet mauris ut dui ornare proin eu ut. Commodo blandit proin est mi egestas.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut est blandit volutpat nam adipiscing orci. Morbi platea maecenas maecenas gravida tempor sit. Mauris auctor eu in facilisis felis facilisi in. Egestas eget etiam quis dolor. Consequat laoreet duis neque auctor adipiscing dui tincidunt. Arcu sit id aenean laoreet nisl ullamcorper integer praesent sociis. Lorem eget varius nibh eros dictum. Adipiscing aliquet nunc, bibendum aliquam aliquet morbi. In imperdiet mauris ut dui ornare proin eu ut. Commodo blandit proin est mi egestas.</p>
                                </div>
                                <div class="modal-product__contact">
                                    <a href="" target="_blank" class="modal-product__btn modal-product__btn--whatsapp">
                                        <img src="{{ asset('images/button/btn_whatsapp_mini.svg') }}" alt="Icon Whatsapp"> <span class="ms-3">Contact Admin</span>
                                    </a>
                                    <button class="modal-product__btn modal-product__btn--close" data-bs-dismiss="modal">
                                        <img src="{{ asset('images/button/btn_close.svg') }}" alt="Icon Close"> <span class="ms-3">Close</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-drawer.button>

<div class="container page-content-wrapper">
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <div class="dropdown dropdown--booth" id="dropdownBooth">
                <div class="btn-header-booth" data-bs-toggle="dropdown">
                    <div class="btn-header-booth__icon">
                        <img :src="booth.present_icon" alt="Icon">
                    </div>
                    <div class="btn-header-booth__inner">
                        <span class="btn-header-booth__label">You're on</span>
                        <h6 class="btn-header-booth__name" v-cloak>@{{ booth.name }}</h6>
                    </div>

                    <svg width="10" height="10" viewBox="0 0 10 10" fill="#2C2C2C" xmlns="http://www.w3.org/2000/svg" class="icon__arrow-down">
                        <path d="M0.220115 2.5262C0.361098 2.38629 0.552285 2.30769 0.751634 2.30769C0.950983 2.30769 1.14217 2.38629 1.28315 2.5262L5.00454 6.22051L8.72592 2.5262C8.86771 2.39025 9.05762 2.31503 9.25474 2.31673C9.45186 2.31843 9.64042 2.39692 9.7798 2.53529C9.91919 2.67367 9.99826 2.86085 9.99997 3.05654C10.0017 3.25222 9.92591 3.44074 9.78896 3.5815L5.53606 7.80346C5.39508 7.94337 5.20389 8.02197 5.00454 8.02197C4.80519 8.02197 4.614 7.94337 4.47302 7.80346L0.220115 3.5815C0.0791754 3.44155 -2.08513e-07 3.25175 -2.17163e-07 3.05385C-2.25814e-07 2.85595 0.0791754 2.66616 0.220115 2.5262V2.5262Z"/>
                    </svg>
                </div>
                <div class="dropdown-menu dropdown-menu--booth">
                    <div class="d-flex mb-5">
                        <div class="form-filter-group me-3">
                            <small class="date-label">Category</small>
                            <select name="" class="form-control form-filter" v-model="selectedCategoryId" v-cloak>
                                <option
                                    :value="category.id"
                                    v-for="(category, index) in categories">@{{ category.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-filter-group">
                            <small class="date-label">Search</small>
                            <input type="text" class="form-control form-filter" placeholder="Find a Booth" v-model="searchBooth">
                        </div>
                    </div>

                    <ul class="booth-list scroll-list">
                        <li class="booth-list__item"
                            :class="{'active' : isActiveId === item.id}"
                            @click="postFindDetailBooth(item.id, i)"
                            v-for="(item, i) in filterBooths">
                            <div class="booth-list__icon">
                                <img :src="item.present_icon" alt="Icon Booth">
                            </div>
                            <div class="booth-list__content">
                                <h5 class="booth-list__content__title" v-cloak>@{{ item.name }}</h5>
                                <p class="booth-list__content__subtitle">
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="#28B7FB" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M7.90499 8.62882V9.75713C7.90499 9.96514 7.7332 10.1331 7.52045 10.1331H0.853871C0.641033 10.1331 0.469238 9.96514 0.469238 9.75694V8.62864C0.469238 7.30224 1.34867 6.17515 2.56793 5.77657C3.03456 6.06865 3.58838 6.23669 4.18201 6.23669C4.77687 6.23669 5.33195 6.06749 5.79993 5.77417C7.02304 6.17158 7.90506 7.29981 7.90506 8.62876L7.90499 8.62882Z"/>
                                        <path d="M6.55383 2.50506V3.18962C6.55383 4.49858 5.48974 5.56139 4.18207 5.56139C2.87437 5.56139 1.8103 4.49853 1.8103 3.18962L1.81039 2.50506C1.81039 1.19737 2.87448 0.133301 4.18215 0.133301C5.48976 0.13339 6.55392 1.19739 6.55392 2.50506H6.55383Z"/>
                                        <path d="M7.83721 2.95377C7.55926 2.95377 7.29752 3.02155 7.06643 3.13969V3.18968C7.06643 4.00803 6.72294 4.74696 6.17358 5.27245C6.17779 5.30733 6.18333 5.34185 6.18977 5.37592C6.88257 5.65421 7.45312 6.129 7.84355 6.72217C8.76492 6.71868 9.51379 5.98244 9.51379 5.07581V4.60041C9.51379 3.69263 8.76162 2.95377 7.83721 2.95377L7.83721 2.95377Z"/>
                                        <path d="M8.98095 7.03821C8.73888 7.19068 8.46379 7.29514 8.16895 7.34065C8.32974 7.74246 8.41773 8.17743 8.41773 8.62887V9.75717C8.41773 9.89167 8.38661 10.0188 8.33224 10.1333H10.1971C10.3476 10.1331 10.4689 10.0139 10.4689 9.8661V9.06492C10.4689 8.12155 9.84554 7.32037 8.98094 7.03823L8.98095 7.03821Z"/>
                                    </svg>
                                    <!-- TODO: Implement relation model count -->
                                    <span>0 people</span> visited
                                </p>
                            </div>
                        </li>
                    </ul>

                    <h6 class="booth-not-found" v-if="filterBooths.length === 0">Booth not found!</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8 col-lg-10 col-xxl-12">

            <!-- Slider main container -->
            <div class="swiper swiper-booth">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->

                    <div class="swiper-slide"
                        v-for="(item, index) in filterBooths">
                        <img :src="item.present_image" alt="Stand Booth" class="w-100">
                    </div>

                </div>

                <div class="booth-slider-navigation" role="navigation">
                    <div class="swiper-button-prev swiper-button-prev__booth">
                        <img src="{{ asset('images/icon/icon_arrow_left.svg') }}" alt="Icon Arrow Left">
                    </div>
                    <div class="swiper-button-next swiper-button-next__booth">
                        <img src="{{ asset('images/icon/icon_arrow_left.svg') }}" alt="Icon Arrow Right">
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('js/web/Booth/template.js') . '?v=1.0.0' }}"></script>
<script src="{{ asset('js/web/Booth/detail.js') . '?v=1.0.0' }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        new Swiper('.swiper-product', {
            slidesPerView: 1,
            spaceBetween: 20,

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next__product',
                prevEl: '.swiper-button-prev__product',
            },

            // Pagination
            pagination: {
                el: '.swiper-pagination__product',
                type: 'bullets',
                clickable: true
            },
        });
    });
</script>
@endpush
