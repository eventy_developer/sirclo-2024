@extends('layout.Web.index')

@section('title', 'Main Stage')

@push('head')
@endpush

@section('content')
    <img src="{{ asset('images/placeholder/ph_stage.png') }}" class="background-content" alt="Background Content">

    <x-drawer.button :drawer-buttons="$drawerButtons">
        <x-drawer.offcanvas>
            @include('web.MainStage.offcanvas-content')
        </x-drawer.offcanvas>
    </x-drawer.button>

    <div class="container page-content-wrapper">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="stage-view">
                    <iframe class="stage-iframe" ref="embedZoom" frameborder="0" scrolling="no"
                            marginheight="0" marginwidth="0" type="text/html"
                            src="https://www.youtube.com/embed/TmvB3ZI8Nf8" allow="autoplay;fullscreen"></iframe>
                </div>

                <div class="stage-information">
                    <h5 class="stage-information__title" v-cloak>@{{ detail.title }}</h5>
                    <div class="stage-information__notes">
                        <span class="stage-information__clock" v-cloak><img src="{{ asset('images/icon/icon_clock.svg') }}" alt="Clock" class="me-2"> @{{ detail.time_start }} - @{{ detail.time_end }} @{{ detail.abbreviation_timezone }}</span> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <span class="stage-information__speaker" v-cloak><img src="{{ asset('images/icon/icon_speaker.svg') }}" alt="Speaker" class="me-2"> @{{ detail.present_speaker }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script src="{{ asset('js/web/MainStage/main-stage.js') }}"></script>
    <script>
        $(window).on('load', function() {
            frame16_9('.stage-iframe');
        });
    </script>
@endpush
