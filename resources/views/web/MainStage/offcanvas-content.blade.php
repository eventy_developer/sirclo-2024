<ul class="nav nav-tabs nav-tabs-custom" id="tabDrawer" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="schedule-tab" data-bs-toggle="tab" data-bs-target="#schedule" type="button" role="tab" aria-controls="schedule" aria-selected="true">
            <svg width="14" height="13" viewBox="0 0 14 13" fill="#28B7FB" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.9642 0.927289H11.2182V0H10.3091V0.927289H3.69094V0H2.78183V0.927289H1.03584C0.464792 0.927289 0 1.39198 0 1.96313V11.637C0 12.208 0.464692 12.6728 1.03584 12.6728H12.9642C13.5352 12.6728 14 12.2081 14 11.637V1.96313C14 1.39195 13.5353 0.927289 12.9642 0.927289ZM11.3137 4.38164C11.4645 4.38164 11.5865 4.50359 11.5865 4.65437V6.06932C11.5865 6.2201 11.4645 6.34205 11.3137 6.34205H9.8987C9.74818 6.34205 9.62597 6.2201 9.62597 6.06932V4.65437C9.62597 4.50359 9.74818 4.38164 9.8987 4.38164H11.3137ZM9.6805 8.25424C9.6805 8.10346 9.80271 7.98151 9.95322 7.98151H11.3683C11.5191 7.98151 11.641 8.10346 11.641 8.25424V9.66919C11.641 9.81997 11.5191 9.94192 11.3683 9.94192H9.95322C9.80271 9.94192 9.6805 9.81997 9.6805 9.66919V8.25424ZM6.01968 8.25424C6.01968 8.10346 6.14163 7.98151 6.29241 7.98151H7.70736C7.85815 7.98151 7.98009 8.10346 7.98009 8.25424V9.66919C7.98009 9.81997 7.85814 9.94192 7.70736 9.94192H6.29241C6.14163 9.94192 6.01968 9.81997 6.01968 9.66919V8.25424ZM5.96514 6.06902V4.65407C5.96514 4.50328 6.08709 4.38134 6.23786 4.38134H7.65282C7.8036 4.38134 7.92555 4.50329 7.92555 4.65407V6.06902C7.92555 6.2198 7.8036 6.34175 7.65282 6.34175H6.23786C6.08708 6.34175 5.96514 6.2198 5.96514 6.06902V6.06902ZM3.992 4.38147C4.14252 4.38147 4.26473 4.50342 4.26473 4.6542V6.06916C4.26473 6.21994 4.14252 6.34188 3.992 6.34188H2.57695C2.42617 6.34188 2.30422 6.21993 2.30422 6.06916V4.6542C2.30422 4.50342 2.42617 4.38147 2.57695 4.38147H3.992ZM2.35875 8.25407C2.35875 8.10329 2.4807 7.98134 2.63147 7.98134H4.04653C4.19705 7.98134 4.31926 8.10329 4.31926 8.25407V9.66903C4.31926 9.81981 4.19705 9.94175 4.04653 9.94175H2.63147C2.48069 9.94175 2.35875 9.8198 2.35875 9.66903V8.25407Z"/>
            </svg>
            <span class="ms-2">Schedule</span>
        </button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="attachment-tab" data-bs-toggle="tab" data-bs-target="#attachment" type="button" role="tab" aria-controls="attachment" aria-selected="false">
            <svg width="14" height="16" viewBox="0 0 14 16" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.84067 15.271C2.10681 15.2874 1.39664 15.0108 0.867318 14.5023C0.350202 13.9813 0.0662215 13.273 0.0799781 12.539C0.105781 11.4956 0.544673 10.5052 1.29995 9.78506L8.01797 3.14006C8.93091 2.23564 10.3037 2.12189 11.0723 2.88558H11.0722C11.4119 3.22944 11.5989 3.69532 11.5915 4.17859C11.5787 4.84484 11.3 5.47834 10.8177 5.93816L4.4715 12.2165C4.34213 12.3445 4.15428 12.3936 3.97874 12.3454C3.80333 12.2974 3.66673 12.1592 3.62071 11.9832C3.57466 11.8071 3.62603 11.6199 3.75552 11.492L10.1017 5.20518H10.1016C10.3865 4.93756 10.5552 4.5691 10.5717 4.17861C10.5789 3.96805 10.5005 3.76364 10.3545 3.61177C9.98115 3.24356 9.2363 3.35895 8.72542 3.8646L2.01619 10.5096C1.45232 11.0397 1.12207 11.7722 1.09816 12.5458C1.08419 13.0073 1.25878 13.4546 1.5818 13.7846C2.37768 14.5703 3.86752 14.3751 4.90429 13.3485L11.6138 6.69686C12.4843 5.83483 12.9815 4.73027 12.9815 3.66464H12.9814C13.0018 2.95221 12.7309 2.26229 12.2315 1.7539C11.0182 0.56613 8.76806 0.834202 7.21544 2.37328L0.867393 8.65345C0.737905 8.78128 0.550051 8.83052 0.374639 8.78235C0.199106 8.73417 0.0626304 8.59604 0.0166032 8.42004C-0.029439 8.24403 0.0218113 8.05679 0.1513 7.92883L6.49935 1.64896C8.44558 -0.278694 11.3387 -0.556977 12.9474 1.02958C13.6379 1.73089 14.0172 2.68074 13.9994 3.66482C13.9994 5.00018 13.392 6.36967 12.3297 7.42153L5.62044 14.0662C4.88646 14.815 3.88913 15.2473 2.84093 15.271L2.84067 15.271Z" />
            </svg>
            <span class="ms-2">Attachment</span>
        </button>
    </li>
</ul>
<div class="tab-content" id="tabDrawerContent">
    <div class="tab-pane fade show active" id="schedule" role="tabpanel" aria-labelledby="schedule-tab">
        <div class="tab-content__header">
            <h5 class="tab-content__title">All Schedule</h5>
            <p class="tab-content__desc">Non aenean a feugiat natoque</p>
        </div>
        <div class="tab-content__list">
            <div class="d-flex mb-5">
                <div class="form-filter-group me-3">
                    <small class="date-label">Schedule Date</small>
                    <select name="" class="form-control form-filter" v-model="selectedDate">
                        <option :value="date.date" v-for="(date, i) in dates" v-cloak>@{{ date.present_date_text }}</option>
                    </select>
                </div>
                <div class="form-filter-group">
                    <small class="date-label">Location</small>
                    <select name="" class="form-control form-filter" v-model="locationType" v-cloak>
                        <option value="All">All</option>
                        <option value="Main Stage">Main Stage</option>
                        <option value="Class Room">Class Room</option>
                    </select>
                </div>
            </div>

            <ul class="schedule-list scroll-list">
                <li v-for="(item, index) in scheduleList">
                    <a href="javascript:void(0)" class="schedule-item"
                       @click="postFindDetailSchedule(index, item.id)">
                        <div class="schedule-img">
                            <span class="schedule-item__status schedule-item__status--live">LIVE NOW</span>
                            <img src="{{ asset('images/placeholder/ph_schedule_frame.svg') }}" alt="Icon Schedule">
                        </div>
                        <div class="schedule-content">
                            <h6 class="schedule-title" v-cloak>@{{ item.title }}</h6>
                            <div class="schedule-time">
                                <img src="{{ asset('images/icon/icon_clock.svg') }}" alt="Icon Clock" class="schedule-content__icon">
                                <span v-cloak>@{{ item.time_start }} - @{{ item.time_end }} @{{ detail.abbreviation_timezone }}</span>
                            </div>
                            <div class="schedule-marker">
                                <img src="{{ asset('images/icon/icon_loc.svg') }}" alt="Icon Location" class="schedule-content__icon">
                                <span v-cloak>@{{ item.schedule.location.name }}</span>
                            </div>
                            <div class="schedule-speaker">
                                <img src="{{ asset('images/icon/icon_speaker.svg') }}" alt="Icon Speaker" class="schedule-content__icon">
                                <span v-cloak>@{{ item.present_speaker }}</span>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>

        </div>
    </div>
    <div class="tab-pane fade" id="attachment" role="tabpanel" aria-labelledby="attachment-tab">
        <div class="tab-content__header">
            <ul class="nav nav-tabs nav-tabs-attachment" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="image-tab" data-bs-toggle="tab"
                            data-bs-target="#image" type="button" role="tab"
                            aria-controls="image" aria-selected="true">
                        <span>Image</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="document-tab" data-bs-toggle="tab"
                            data-bs-target="#document" type="button" role="tab"
                            aria-controls="document" aria-selected="true">
                        <span>Document</span>
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="video-tab" data-bs-toggle="tab"
                            data-bs-target="#video" type="button" role="tab"
                            aria-controls="video" aria-selected="true">
                        <span>Video</span>
                    </button>
                </li>
            </ul>
            <div class="tab-content" id="tabDrawerContent">
                <div class="tab-pane fade show active" id="image" role="tabpanel" aria-labelledby="image-tab">
                    <div class="row">
                        <div class="col-md-6 mb-4"
                            v-for="(item, i) in detail.images">
                            <img :src="item.file" alt="Image Attachment"
                                class="w-100 img-fluid">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                    <ul class="tab-document__list">
                        <li class="tab-document__item"
                            v-for="(item, i) in detail.documents">
                            <div class="tab-document__info">
                                <h5 class="tab-document__title" v-cloak>@{{ item.file_name }}</h5>
                                <span class="tab-document__size" v-cloak>@{{ item.file_size }}</span>
                            </div>
                            <a :href="item.file" target="_blank" class="btn-download-document">
                                <img src="{{ asset('images/icon/icon_download.svg') }}" alt="Icon Download"> Download
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                    <ul class="tab-video__list">
                        <li class="tab-video__item"
                            v-for="(item, i) in detail.videos">
                            <div class="tab-video__view"
                                 :id="'play-modal-' + item.id"
                                 :data-video-id="item.video_url">
                                <img src="{{ asset('images/placeholder/ph_video.png') }}" alt="Thumbnail Video"
                                    class="w-100 img-fluid">
                                <img src="{{ asset('images/button/btn_play.svg') }}" alt="Icon Button Play" class="tab-video__btn-play">
                            </div>
{{--                            <p class="tab-video__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat non in orci at eleifend neque ornare maecenas. </p>--}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
