<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 25%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-bottom: 30px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            margin: 0;
            color: #000;
            font-size: 18px;
            margin-bottom: 25px;
            line-height: 30px;
        }

        .body-email .content-center .caption {
            margin: 0;
            color: #373737;
            font-size: 18px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 50px 0 40px 0px;
        }

        .body-email .contact-text span {
            display: block;
            color: #000;
            font-size: 18px;
            line-height: 30px;
        }

        .footer-email {
            display: block;
            font-size: 15px;
            color: #939393;
        }

        .table-ticket img {
            width: 25px;
        }
        .table-ticket th,
        .table-ticket td {
            vertical-align: top;
            padding: 5px;
            font-size: 18px;
            line-height: 30px;
        }
        .ullist {
            padding-left: 30px;
        }
        .ullist li {
            font-size: 18px;
            margin-bottom: 5px;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title,
            .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .ullist li {
                font-size: 14px;
            }

            .footer-email {
                font-size: 13px;
            }
        }
    </style>
</head>
<body>

<div class="body">
    <div class="body-email">
        <div class="img-center">
            <img src="https://d1bzsjp5fzkb7z.cloudfront.net/virtual-event/registersircloexpo2023/assets/2023_E_Commerce_Expo_All_Confirmed_Speakers_Story_non_TS_Press_Release.jpeg" alt="" style="width: 100%;">
        </div>
        <div class="content-center">
            <p class="title" style="margin-bottom: 30px;">Hi,</p>
            <p class="title">We are writing to remind you of the upcoming <strong>E-Commerce Expo 2023: <i>powered by SIRCLO</i></strong>, which will be held on <strong>Friday, 25 - Saturday, 26 August 2023</strong> from <strong>9:00 AM to 6:00 PM at QBIG Convention Hall, BSD City.</strong></p>

            <p class="title" style="margin-bottom: 0px;">As a <strong>General Pass</strong> holder, you will have access to the following:</p>
            <ul class="ullist" style="margin-bottom: 25px;margin-top: 5px;">
                <li>Seminars in the Main Hall</li>
                <li>Seminars in the Business Track</li>
                <li>Selected Workshop Sessions at the Technology & Innovation Track</li>
                <li>Entire Exhibition Area</li>
            </ul>

            <div class="contact-text" style="margin-bottom: 25px;margin-top: 0;">
                <span style="margin-bottom: 25px;"><strong>Entrepreneur Pass</strong> holders are entitled to all of the above, including the <strong>Growth Hack Workshop for Local SMEs</strong> feat. <strong>Social Bread on Saturday, 26 August 2023 at 1:00 - 3:00 PM.</strong></span>
                <span style="margin-bottom: 25px;">Please show your e-ticket at the venue, where the committee will scan the QR code generated from your prior booking.</span>
                <span style="margin-bottom: 25px;">If you have not received your QR code, please be sure to complete the registration steps upon logging in to your account on the following link: <a href="https://registersircloexpo.com/login" target="_blank" style="color: #1DA6D9;text-decoration: underline;">https://registersircloexpo.com/login</a></span>
                <span>For your reference, you can check our latest <strong>agenda, speaker lineup,</strong> and <strong>schedule</strong> here: <a href="https://www.expo.sirclo.com/agenda" target="_blank" style="color: #1DA6D9;text-decoration: underline;">expo.sirclo.com/agenda</a></span>
            </div>

            <div class="contact-text" style="margin-bottom: 0;margin-top: 0;">
                <span style="margin-bottom: 25px;">Thank you for your interest in E-Commerce Expo 2023: powered by SIRCLO. We look forward to seeing you at the event!</span>
                <span>Warm Regards,<br><br>SIRCLO</span>
            </div>
        </div>
    </div>
</div>

</body>
</html>
