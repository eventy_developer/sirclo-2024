<!DOCTYPE html>
<html>

<head>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px;
            color: #383838;
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .container {
            background-color: #eaeaea;
        }

        .body {
            margin: 5% 25%;
            background-color: white;
            width: 100%;
            max-width: 500px;
            box-shadow: 0 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 30px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email {
            margin-top: 70px;
            margin-bottom: 40px;
            padding: 0 20px;
        }

        .body-email .content-header .title {
            font-weight: bold;
            margin: 0;
            margin-bottom: 17px;
            line-height: 20px;
            font-size: 16px;
        }

        .body-email .content-header .caption {
            margin: 0;
            line-height: 20px;
        }

        .box-gray-top {
            background: #F6F6F6;
            padding: 15px;
            margin: 10px auto;
        }

        .box-gray-top h4 {
            margin: 0;
            line-height: 20px;
            padding-bottom: 10px;
            border-bottom: solid 1px #EBEBEB;
        }

        .box-gray-top .text {
            margin: 15px 0;
            line-height: 20px;
        }

        .table {
            margin: 10px auto;
            border-collapse: collapse;
            width: 100%;
            max-width: 100%;
            margin-bottom: 0;
            background: #F6F6F6;
        }

        .table thead th {
            border-top: 0;
            border-bottom-width: 1px;
            vertical-align: bottom;
            font-weight: bold;
            text-align: left;
        }

        .table td,
        .table th {
            font-size: 14px;
            vertical-align: middle;
            line-height: 1;
            padding: 10px 8px;
            border-top: 2px solid #EBEBEB;
        }

        .content-footer {
            margin-top: 15px;
        }

        .content-footer .caption {
            margin: 0;
            line-height: 20px;
            margin-bottom: 20px;
        }

        .footer-email {
            display: block;
            text-align: center;
            color: #939393;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
                padding: 40px 0;
            }

            .body-email {
                padding: 0;
            }

            .content-header,
            .content-footer {
                padding: 15px;
            }

            .body-email .content-header .title,
            .body-email .content-header .caption {
                line-height: 13px;
            }

            .box-gray-top h4,
            .box-gray-top .text {
                line-height: 13px;
            }

            .table td,
            .table th {
                padding: 17px 12px;
            }

            .content-footer .caption {
                line-height: 13px;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="body">
            <div class="header-email">
                <img src="{{ $event_logo }}" alt="Logo" style="height: 60px;">
            </div>
            <div class="body-email">
                <div class="content-header">
                    <p class="title">Halo {{ $participant->name }},</p>
                    <p class="caption">Thank you for registering to <b>{{ $event_name }}</b></p>
                </div>
                <div class="box-gray-top">
                    <h4>Order Details</h4>
                    <p class="text"><b>Order ID &nbsp;&nbsp;&nbsp;:</b> {{ $payment->invoice_code }}</p>
                    <p class="text">
                        @if ($payment->type === 'Virtual Accounts')
                        <b>Payment &nbsp;&nbsp;: </b> Virtual Account ({{ $payment->payment_method }})
                        @elseif ($payment->type === 'Manual')
                        <b>Payment &nbsp;&nbsp;: </b> Bank Transfer
                        @else
                        <b>Payment &nbsp;&nbsp;: </b> {{ implode(' ', explode('_', $payment->payment_method)) }}
                        @endif
                    </p>
                </div>
                <table class="table">
                    @if($payment->transactionTicket)
                        <thead>
                            <tr>
                                <th width="50%" colspan="2">Ticket</th>
                                <th width="25%">Price</th>
                            </tr>
                        </thead>
                    @endif
                    <tbody>
                        @if($payment->transactionTicket)
                        <tr>
                            <td colspan="2">{{ $payment->transactionTicket->ticket->name }}</td>
                            @if($payment->transactionTicket->price <= 0)
                                <td>FREE</td>
                            @else
                                <td>Rp {{ number_format($payment->transactionTicket->price,0,'.','.') }}</td>
                            @endif
                        </tr>
                        @endif

                        @if(count($payment->transactionDetails) > 0)
                        <tr>
                            @if($payment->transactionTicket)
                                <th width="25%" colspan="3" style="text-align: left;">Type</th>
                            @else
                                <th width="67%" colspan="2" style="text-align: left;">Type</th>
                                <th style="text-align: left;">Price</th>
                            @endif
                        </tr>

                        @foreach($payment->transactionDetails as $item)
                            <tr>
                                <td colspan="2">{{ $item->ticketSchedule->schedule->name }}</td>
                                <td>Rp {{ number_format($item->price,0,'.','.') }}</td>
                            </tr>
                        @endforeach
                        @endif

                        <tr>
                            <th style="text-align: right" colspan="2">Transaction Fee</th>
                            <td>Rp {{ number_format($payment->total_fee,0,'.','.') }}</td>
                        </tr>
                        <tr>
                            <th style="text-align: right" colspan="2">Total Paid</th>
                            <td>Rp {{ number_format($payment->amount,0,'.','.') }}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="content-footer">
                    <p class="caption" style="text-align: center;margin-bottom: 10px;">
                        Please tap the button below to continue the transaction. <br><br>
                        <a style="padding: 10px 15px;background-color: #00B2F9;color: white;border-radius: 5px;"
                           href="{{ $url }}">
                            Pay Now
                        </a>
                    </p>
                    <p class="caption" style="font-size: 14px;">
                        For further information, please contact us
                        <br>
                        @if($contact_name_1 && $contact_phone_1)
                        <b style="font-size: 14px;">{{ $contact_name_1 }}
                            <a style="font-size: 14px;" href="https://api.whatsapp.com/send?phone={{ $contact_phone_1 }}" target="_blank">
                                ({{ $contact_phone_1 }})
                            </a>
                        </b>.<br>
                        @endif
                        @if($contact_name_2 && $contact_phone_2)
                        <b style="font-size: 14px;">{{ $contact_name_2 }}
                            <a style="font-size: 14px;" href="https://api.whatsapp.com/send?phone={{ $contact_phone_2 }}" target="_blank">
                                ({{ $contact_phone_2 }})
                            </a>
                        </b>.<br>
                        @endif

                        @if($event_email)
                        <strong>Email</strong> <strong><a href="mailto:{{ $event_email }}">{{ $event_email }}</a></strong>.<br>
                        @endif
                        Thank you, have a nice day.
                    </p>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
