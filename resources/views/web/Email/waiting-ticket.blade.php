<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 29%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-bottom: 50px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            text-align: center;
            margin: 0;
            color: #000;
            font-size: 18px;
            margin-bottom: 25px;
        }

        .body-email .content-center .caption {
            margin: 0;
            color: #373737;
            font-size: 18px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 50px 0 40px 0px;
        }

        .body-email .contact-text span {
            display: block;
            color: #000;
            font-size: 18px;
            line-height: 30px;
        }

        .footer-email {
            display: block;
            font-size: 15px;
            color: #939393;
        }

        .table-ticket img {
            width: 25px;
        }
        .table-ticket th,
        .table-ticket td {
            vertical-align: top;
            padding: 5px;
            font-size: 18px;
            line-height: 30px;
        }
        .qrcode {
            width: 30%;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title, .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .table-ticket img {
                width: 20px;
            }
            .table-ticket th,
            .table-ticket td {
                font-size: 14px;
            }

            .footer-email {
                font-size: 13px;
            }

            .qrcode {
                width: 50%;
            }
        }
    </style>
</head>
<body>

<div class="body">
    <div class="body-email">
        <div class="img-center">
            <img src="{{ $event_logo }}" alt="" style="width: 40%;">
        </div>
        <div class="content-center">
            <div class="contact-text" style="margin-bottom: 30px;">
                <span style="font-weight: bolder;">Hi, {{ $name }}!</span>
                <span style="margin-bottom: 20px;font-weight: bolder;">You have not yet selected a pass to gain access to E-Commerce Expo 2023: powered by SIRCLO.</span>
                <span>Upon completing the Registration form, be sure to immediately select your chosen ticket, either the General Pass or the Entrepreneur Pass.</span>
            </div>

            <a href="{{ action([\App\Http\Controllers\Auth\LoginController::class, 'index'], [
                        'token' => encrypt($email),
                    ]) }}" style="color: #FFF; display: block; background-color: #00B2F9;" class="btn-verify">Continue Registration</a>

            <div class="contact-text" style="margin-top: 30px;margin-bottom: 20px;">
                <span style="text-align: center;margin-bottom: 20px;">For more information regarding the passes,<br>visit <a href="https://expo.sirclo.com/tickets" style="text-decoration: underline;">expo.sirclo.com/tickets</a>.</span>
                <span style="text-align: center;">If you have any further inquiries or are facing any problems, please contact us.</span>
            </div>
        </div>
    </div>
</div>

</body>
</html>
