<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 29%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-bottom: 30px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            margin: 0;
            color: #000;
            font-size: 18px;
            margin-bottom: 25px;
        }

        .body-email .content-center .caption {
            margin: 0;
            color: #373737;
            font-size: 18px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 50px 0 40px 0px;
        }

        .body-email .contact-text span {
            display: block;
            color: #000;
            font-size: 18px;
            line-height: 30px;
        }

        .footer-email {
            display: block;
            font-size: 15px;
            color: #939393;
        }

        .table-ticket img {
            width: 25px;
        }
        .table-ticket th,
        .table-ticket td {
            vertical-align: top;
            padding: 5px;
            font-size: 18px;
            line-height: 30px;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title, .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .footer-email {
                font-size: 13px;
            }
        }
    </style>
</head>
<body>

<div class="body">
    <div class="body-email">
        <div class="header-email img-center" style="margin-bottom: 40px; text-align: center; justify-content: center; align-content: center; align-items: center; justify-items: center">
            <img src="{{ $event_logo }}" alt="Event Logo" style="height: 60px;">
        </div>
        <div class="content-center">
            <p class="title" style="margin-bottom: 40px;">Hello!</p>
            <p class="title">Thank you for your registration to attend E-Commerce Expo 2023: powered by SIRCLO.</p>

            <table class="table-ticket">
                <tr>
                    <td style="padding-left: 0!important;"><img src="https://d1bzsjp5fzkb7z.cloudfront.net/virtual-event/registersircloexpo2023/assets/icon_clock.png" alt=""></td>
                    <td>Friday-Saturday, 25-26 August 2023 <br> 9 am - 6 pm WIB/GMT+7</td>
                </tr>
                <tr>
                    <td style="padding-left: 0!important;"><img src="https://d1bzsjp5fzkb7z.cloudfront.net/virtual-event/registersircloexpo2023/assets/icon_location.png" alt=""></td>
                    <td>QBIG Convention Hall, BSD City</td>
                </tr>
            </table>

            <div class="contact-text" style="margin-top: 30px;margin-bottom: 30px;">
                @if($type === 'General Pass')
                    <span style="text-align: center;">With the <b>{{ $type }}</b>, you will have access to Seminars in the Main Hall, Business Hall, entry to Limited Workshops & the Exhibition Area in E-Commerce Expo 2023: <i>powered by SIRCLO</i>.</span>
                @else
                    <span style="text-align: center;">With the <b>{{ $type }}</b>, you will have unlimited access to the <a href="https://www.expo.sirclo.com/agenda" style="color: #1DA6D9;text-decoration: underline;">Growth Hack Workshop for Local SMEs</a>, Seminars in the Main Hall, Business Hall & Exhibition Area in E-Commerce Expo 2023: <i>powered by SIRCLO</i>.</span>
                @endif
            </div>

            <div class="contact-text" style="margin-bottom: 30px;">
                <h1 class="title" style="text-align: center;font-weight: bold;">Ticket</h1>
            </div>

            <img src="{{ $qr_code }}" alt="QR" width="30%" style="display: block;margin: auto;margin-top: 0;margin-bottom: 50px;">

            <div class="contact-text" style="margin-bottom: 0;">
                <span style="margin-bottom: 25px;">Please show your e-ticket at the venue, where our committee will scan your QR code at the entrance to the Convention Hall.</span>
                <span style="margin-bottom: 25px;">For reference, you can check our latest Agenda, Speaker lineup, and schedule on the link below:</span>

                <span><a href="https://www.expo.sirclo.com" target="_blank" style="color: #1DA6D9;text-decoration: underline;text-align: center;display: block;">expo.sirclo.com</a></span>

                <span style="margin-bottom: 25px;margin-top: 25px;">Thank you for your interest in E-Commerce Expo 2023: powered by SIRCLO. We look forward to seeing you at the event.</span>
                <span>Warm Regards,<br>SIRCLO</span>

            </div>
        </div>
    </div>
</div>

</body>
</html>
