<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 29%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-bottom: 50px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            text-align: center;
            margin: 0;
            color: #000;
            font-size: 18px;
            margin-bottom: 25px;
        }

        .body-email .content-center .title-2 {
            text-align: center;
            margin: 0;
            color: #000;
            font-size: 24px;
            margin-bottom: 25px;
        }

        .body-email .content-center .caption {
            margin: 0;
            color: #373737;
            font-size: 18px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 50px 0 40px 0px;
        }

        .body-email .contact-text span {
            display: block;
            color: #000;
            font-size: 18px;
            line-height: 30px;
        }

        .footer-email {
            display: block;
            font-size: 15px;
            color: #939393;
        }

        .table-ticket img {
            width: 25px;
        }

        .table-ticket th,
        .table-ticket td {
            vertical-align: top;
            padding: 5px;
            font-size: 18px;
            line-height: 30px;
        }

        .table {
            margin: 10px auto;
            border-collapse: collapse;
            width: 100%;
            max-width: 100%;
            margin-bottom: 0;
        }

        .table thead th {
            border-top-width: 1px;
            border-bottom-width: 1px;
            vertical-align: bottom;
            font-weight: bold;
            text-align: left;
        }

        .table td,
        .table th {
            font-size: 14px;
            vertical-align: middle;
            line-height: 1;
            border-top: 1px solid black;
            border-bottom: 1px solid black;
        }

        .table td,
        .table th {
            padding: 17px 12px;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title, .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .table-ticket img {
                width: 20px;
            }

            .table-ticket th,
            .table-ticket td {
                font-size: 14px;
            }

            .table td,
            .table th {
                padding: 17px 12px;
            }
        }
    </style>
</head>
<body>

<div class="body">
    <div class="body-email">
        <div class="header-email img-center" style="margin-bottom: 40px; text-align: center; justify-content: center; align-content: center; align-items: center; justify-items: center">
            <img src="{{ $event_logo }}" alt="Event Logo" style="height: 50px;">
        </div>
        <div class="content-center">
            <p class="title" style="margin-bottom: 15px;">Dear {{ $participant->name }},</p>
            <p class="title-2" style="margin-bottom: 15px;"><b>Transaction Successful</b></p>

            <p class="title" style="margin-top: 15px;">This is your order confirmation for ticket(s) you just purchased
                for E-Commerce Expo 2024.</p>
            <table class="table">
                @if($payment->transactionTicket)
                    <thead>
                    <tr>
                        <th width="50%" colspan="2">Type</th>
                        <th width="25%">Price</th>
                    </tr>
                    </thead>
                @endif
                <tbody>
                @if($payment->transactionTicket)
                    <tr>
                        <td colspan="2">{{ $payment->transactionTicket->ticket->name }}</td>
                        @if($payment->transactionTicket->price <= 0)
                            <td>FREE</td>
                        @else
                            <td>Rp {{ number_format($payment->transactionTicket->price,0,'.','.') }}</td>
                        @endif
                    </tr>
                @endif
                @if(count($payment->transactionDetails) > 0)
                    <tr>
                        <th width="50%" colspan="2" style="text-align: left;">Participant List</th>
                    </tr>
                    @foreach($payment->transactionDetails as $item)
                        <tr>
                            <td colspan="2">{{ $item->participant->name }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                @endif

                <tr>
                    <th style="text-align: right" colspan="2">Total Paid</th>
                    <td>Rp {{ number_format($payment->amount,0,'.','.') }}</td>
                </tr>
                </tbody>
            </table>

            <div class="content" style="margin-top: 10px;">
                <p class="title" style="text-align: center; margin-bottom: 10px;">
                    View Ticket And Invoice here: <br><br>
                    <a style="padding: 10px 15px;background-color: #00B2F9;color: white;border-radius: 5px;"
                       href="{{ $url }}">
                        Download
                    </a>
                </p>
            </div>

            <div class="contact-text">
                <span style="text-align: center;">Please show your e-ticket at the venue, where the committee will scan the QR code</span>
                <span style="text-align: center;">Check our website <a href="https://www.expo.sirclo.com/agenda" style="color: #1DA6D9;text-decoration: underline;">www.expo.sirclo.com</a> for more information about E-Commerce Expo 2024</span>
            </div>

            <div class="contact-text" style="margin-bottom: 10px; text-align: center;">
                <span style="margin-bottom: 40px;">Thank you for your interest in E-Commerce Expo 2024: brought to you by SIRCLO and idEA. We look forward to seeing you at the event.</span>
            </div>
        </div>
    </div>
</div>

</body>
</html>
