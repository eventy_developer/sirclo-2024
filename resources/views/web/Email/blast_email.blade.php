<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            font-family: 'Roboto', sans-serif;
            margin: 0;
            padding: 0;
        }

        .body {
            margin: 5% 10%;
            background-color: #fff;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 30px;
        }

        .banner {
            text-align: center;
        }

        .banner img {
            width: 100%;
            height: auto;
        }

        .event-details {
            text-align: center;
            font-size: 16px;
            margin-bottom: 2rem;
        }

        .event-images {
            text-align: center;
            margin-bottom: 2rem;
        }

        .event-images img {
            width: 60%;
            max-width: 60%;
            height: auto;
            margin: 0 auto;
        }

        .button {
            text-align: center;
            margin-bottom: 3rem;
        }

        .button a {
            background-color: #ffcc00;
            color: #000;
            text-decoration: none;
            padding: 15px 35px;
            border: 1px solid black;
            font-weight: bold;
            font-size: 15px;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
                padding: 15px;
            }

            .event-images img {
                width: 100%;
            }
        }
        
        .header {
            font-weight: bold;
            text-align: center;
            margin-bottom: 20px;
        }

        .info-section ul {
            padding-left: 20px;
            line-height: 1.6;
        }

        .footer {
            text-align: center;
        }
        
        .footer strong {
            font-size: 25px;
        }
    </style>
</head>
<body>
<div class="body">
    <div class="banner">
        <img src="{{ asset('images/email/banner.jpg') }}" alt="Event Logo" height="50px">
        <br>
        <h3 style="color: black;">1 DAY TO GO</h3>
    </div>

    <div class="event-details">
        {!! $template !!}
    </div>

    <div class="event-images" style="color: black;">
        <p>With 27 Insightful Sessions & 70+ Leading Speakers & Participants, this is an event you don't want to miss!</p>
        <img src="{{ asset('images/email/venue.jpg ') }}" alt="Event Image 2">
        <br/><br/>
        <p>You’ll gain valuable insights from industry leaders, explore innovative solutions, and expand your network with top professionals.</p>
        <img src="{{ asset('images/email/participant.jpg') }}" alt="Event Image 1">
    </div>
    
    <div class="container" style="color: black;">
        <div class="header">
            <h3>Prepare for E-Commerce Expo: Key Info for Attendees</h3>
        </div>
    
        <div class="info-section">
            <ul>
                <li><strong>Ticket Redemption begins at 9 AM.</strong> Please ensure you have downloaded your e-ticket in advance to streamline the process at the venue.</li>
                <li>Entrance to <strong>Nusantara Hall is accessible via Gate C, ICE BSD City.</strong></li>
                <li><strong>For your Food & Beverage needs, there is a minimarket</strong> located in the basement of Nusantara Hall, along with Salt All Day Dining inside the hall.</li>
                <li><strong>Parking is available in the basement area</strong> for your convenience.</li>
                <li><strong>We’ve also prepared a dedicated working space</strong> to ensure you can stay productive throughout the event.</li>
            </ul>
        </div>
        <br>
        <div class="footer">
            <p>And if you make a last-minute decision to attend, <strong>Good News!</strong>—tickets will be available for purchase onsite! Whether you plan ahead or decide on the day, we’ve got you covered.</p>
        </div>
    </div>
    <br>
    <h3 style="font-weight: bolder; text-align: center; color: black;">Sign Up and Be There!</h3>
    <br>
    <div class="button">
        <a href="https://www.expo.sirclo.com/" target="_blank">EXPO.SIRCLO.COM</a>
    </div>
    <div class="footer-email">
        <p style="color: black; text-align: center;">Presented by : </p>
    </div>
</div>
</body>
</html>
