<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 29%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-top: 90px;
            margin-bottom: 60px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
            height: 220px;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            text-align: center;
            margin: 0;
            color: #333333;
            font-size: 17px;
            margin-bottom: 25px;
        }

        .body-email .content-center .caption {
            text-align: center;
            margin: 0;
            color: #373737;
            font-size: 17px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 50px 0 40px 0px;
        }

        .body-email .contact-text span {
            display: block;
            text-align: center;
            color: #373737;
            font-size: 16px;
            margin: 7px;
        }

        .footer-email {
            display: block;
            text-align: center;
            font-size: 15px;
            color: #939393;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title, .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .footer-email {
                font-size: 13px;
            }
        }
    </style>
</head>
<body>

<div class="body">
    <div class="header-email img-center" style="margin-bottom: 40px; text-align: center; justify-content: center; align-content: center; align-items: center; justify-items: center">
        <img src="{{ $event_logo }}" alt="Event Logo" style="height: 50px;">
    </div>
    <div class="body-email">
        <div class="img-center">
            <img src="{{ asset('example/image/img_email_verification_1@2x.png') }}" alt="">
        </div>
        <div class="content-center">
            <h4 class="title">Hi {{ $name }}! Thank you for registration.</h4>
            <p class="caption">To join this event, please select your ticket first.</p>
            <br>
            <p class="caption mb-0">Tap the button below to select your ticket.</p>
            <a href="{{ $url }}"
                style="color: #FFF; display: block; background-color: #00B2F9;" class="btn-verify">
                Select a Ticket
            </a>

            <div class="contact-text">
                <span>For further information, please contact us via Whatsapp</span>
                @if ($contact_name_1 && $contact_phone_1)
                <span><strong>{{ $contact_name_1 }}</strong> <strong><a href="https://api.whatsapp.com/send?phone={{ $contact_phone_1 }}">{{ $contact_phone_1 }}</a></strong>.</span>
                @endif
                @if ($contact_name_2 && $contact_phone_2)
                <span><strong>{{ $contact_name_2 }}</strong> <strong><a href="https://api.whatsapp.com/send?phone={{ $contact_phone_2 }}">{{ $contact_phone_2 }}</a></strong>.</span>
                @endif
                <span><strong>Email</strong> <strong><a href="mailto:{{ $event_email }}">{{ $event_email }}</a></strong>.</span>
                <span>Thank you, have a nice day.</span>
            </div>
        </div>
    </div>
    <div class="footer-email">
        @lang('eventy/website_config.copyright', ['INSTANCE' => $event_name])
    </div>
</div>

</body>
</html>
