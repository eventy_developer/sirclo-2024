<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&family=Roboto+Slab:wght@200;300;400;500;531;600;700;800;900&display=swap');

        body {
            /* font-family: 'Open Sans', sans-serif; */
            font-family: 'Roboto', sans-serif;
            /* font-family: 'Roboto Slab', serif; */
        }

        a {
            text-decoration: none;
            color: #12b2f9;
        }

        .body {
            margin: 10% 29%;
            box-shadow: 0px 1px 1px #ddd;
            border: solid 1px #ddd;
            border-radius: 6px;
            padding: 40px 40px;
        }

        .header-email {
            display: block;
        }

        .header-email img {
            height: 44px;
            display: block;
            background: transparent;
        }

        .body-email .img-center {
            display: block;
            margin: auto;
            margin-top: 90px;
            margin-bottom: 60px;
        }

        .body-email .img-center img {
            display: block;
            margin: auto;
            height: 220px;
        }

        .body-email .content-center {
            display: block;
        }

        .body-email .content-center .title {
            text-align: center;
            margin: 0;
            color: #333333;
            font-size: 17px;
            margin-bottom: 25px;
        }

        .body-email .content-center .caption {
            text-align: center;
            margin: 0;
            color: #373737;
            font-size: 17px;
            margin-bottom: 35px;
        }

        .btn-verify {
            display: block;
            width: 185px;
            margin: auto;
            background: #00B2F9;
            color: #FFF;
            font-size: 17px;
            text-decoration: none;
            text-align: center;
            padding: 19px 10px;
            border-radius: 6px;
            box-shadow: 0px 1px 1px #DDD;
        }

        .body-email .contact-text {
            display: block;
            margin: 30px 0 20px 0px;
        }

        .body-email .contact-text span {
            display: block;
            text-align: center;
            color: #373737;
            font-size: 16px;
            margin: 7px;
        }

        .footer-email {
            display: block;
            text-align: center;
            font-size: 15px;
            color: #939393;
        }

        @media only screen and (max-device-width: 601px) {
            .body {
                margin: 9% auto;
            }

            .body-email .content-center .title, .body-email .content-center .caption {
                font-size: 14px;
            }

            .body-email .contact-text span {
                font-size: 14px;
            }

            .footer-email {
                font-size: 13px;
            }
        }
    </style>
</head>
<body>
    <div class="body">
        <div class="header-email" style="margin-bottom: 40px; text-align: center; justify-content: center; align-content: center; align-items: center; justify-items: center">
            <img src="{{ $event_logo }}" alt="Event Logo" style="height: 60px;">
        </div>
        <div class="body-email">
            <div class="content-center">
                <h4 class="title">Dear {{ $name }}, <br></h4>
                <div class="contact-text">
                    <span>Your ticket purchase has expired due to a payment delay. Please restart the process:</span>
                </div>
                <div class="contact-text">
                    <span>1. Click the button below.</span>
                    <span>2. Select your ticket.</span>
                    <span>3. Complete the payment.</span>
                </div>

                <a href="{{ action([\App\Http\Controllers\Auth\LoginController::class, 'index'], [
                            'token' => encrypt($email),
                        ]) }}" style="color: #FFF; display: block; background-color: #00B2F9;" class="btn-verify">Reset Payment</a>
                <div class="contact-text">
                    <span>For assistance, contact us at <a href="mailto:expo@sirclo.com">expo@sirclo.com</a></span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
