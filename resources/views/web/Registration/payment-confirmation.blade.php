@extends('layout.Auth.index')

@section('page_title', preferGet('name_events') . ' - Payment Confirmation')

@push('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/auth.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Register/payment-confirmation.css') }}">
@endpush

@section('content')
    <div id="root" class="container-login100">
        <div class="wrap-login100 row">
            <div class="login100-form validate-form col-12 col-lg-6" id="scrollContainer">

                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <div class="p-b-40">
                            <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                        </div>
                        <div class="p-b-30">
                            <h5 class="login100-form-title">{{ preferGet('payment_confirmation_title_text') ?? 'Payment Confirmation' }}</h5>
                            <p class="txt2">{{ preferGet('payment_confirmation_caption_text') ?? 'Please fill the form below to confirm your payment. Thank you' }}</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrap-input100 validate-input pointer-events-none"
                             data-validate="@lang('eventy/payment_confirmation.validate_order_id')">
                            <label for="email" class="label-input100">@lang('eventy/payment_confirmation.placeholder_order_id')</label>
                            <input class="input100" type="text" name="invoice"
                                   v-model="invoice" maxlength="20" @keyup="postFindInvoiceId">
                        </div>

                        <div class="wrap-input100 validate-input pointer-events-none"
                             data-validate="@lang('eventy/payment_confirmation.validate_name')">
                            <label for="name" class="label-input100">@lang('eventy/payment_confirmation.placeholder_name')</label>
                            <input class="input100" type="text" name="name" v-model="name">
                        </div>

                        <div class="wrap-input100 validate-input pointer-events-none"
                             data-validate="@lang('eventy/payment_confirmation.validate_paid_amount')">
                            <label for="price" class="label-input100">@lang('eventy/payment_confirmation.placeholder_paid_amount')</label>
                            <input class="input100" type="text" name="price"
                                   v-model="price" @keyup="changePriceFormat" ref="price">
                        </div>

                        <div class="wrap-input100 validate-input"
                             data-validate="@lang('eventy/payment_confirmation.validate_paid_date')">
                            <label for="price" class="label-input100">@lang('eventy/payment_confirmation.placeholder_paid_date')</label>
                            <input class="input100" type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')"
                                   name="date" value="{{ date('Y-m-d', strtotime($now)) }}"
                                   min="{{ date('Y-m-d', strtotime($created_at)) }}"
                                   max="{{ date('Y-m-d', strtotime($now)) }}"
                                   ref="date">
                        </div>

                        <div class="wrap-input100 validate-input position-relative upload-file"
                             data-validate="@lang('eventy/payment_confirmation.validate_file')">
                            <label for="price" class="label-input100">@lang('eventy/payment_confirmation.placeholder_file_upload')</label>
                            <div class="input-group-file" data-validate="@lang('eventy/payment_confirmation.validate_file')">
                                <input type="file" name="file" id="file" class="inputfile inputfile-1 input100"
                                       accept="image/jpg,image/jpeg,image/png" ref="file" @change="changeFile">
                                <label for="file"><span>@lang('eventy/payment_confirmation.placeholder_file')</span></label>
                            </div>
                            <p id="fileLocation">@{{ fileLocation }}</p>
                            <span class="info">@lang('eventy/payment_confirmation.text_info_upload_size')</span>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="row">
                                <div class="col-12 p-t-40 p-b-10">
                                    <button class="login100-form-btn" type="button" @click="postSubmitPaymentConfirmation">
                                        @lang('eventy/payment_confirmation.text_btn_submit')
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 login-register-image">
                <img src="{{ preferGet('login_image') }}" alt="Welcome">
            </div>
        </div>
    </div>
@endsection


@push('bottom')
    <script>
        let invoice = '{{ $invoice }}',
            amount = '{{ $amount }}',
            name = '{{ $name }}';
    </script>
    <script src="{{ asset('js/web/Payment/payment-confirmation.js') . '?v=' . time() }}"></script>
@endpush
