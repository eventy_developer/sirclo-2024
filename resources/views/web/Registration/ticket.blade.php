@extends('layout.Auth.index')

@section('page_title', preferGet('name_events') . ' - Ticket')

@push('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/auth.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Register/ticket.css') . '?v=1.0.3' }}">
    <style>
        @media (min-width: 992px) {
            .swiper-ticket .swiper-slide {
                padding-right: 10px;
            }
        }

        .selectize-control {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }

        .selectize-control.single .selectize-input {
            height: 100%;
            padding-left: 15px;
            display: flex;
            color: #6a7b89;
        }

        .selectize-control.single .selectize-input.input-active,
        .selectize-input {
            display: flex;
            background-color: #EFF2F6;
        }

        .selectize-input.focus {
            border: none;
            outline: 0;
            box-shadow: none;
            border-radius: 15px;
        }

        .selectize-input {
            border: none;
            border-radius: 15px;
            background: transparent;
        }

        .selectize-input.full {
            background-color: #EFF2F6;
        }

        .selectize-input > * {
            vertical-align: middle;
            margin-top: auto;
            margin-bottom: auto;
            color: #2C2C2C;
        }

        .selectize-dropdown,
        .selectize-dropdown.form-control {
            padding: 0 !important;
            border-radius: 15px;
            border: none;
        }

        .selectize-dropdown .create,
        .selectize-dropdown .no-results,
        .selectize-dropdown .optgroup-header,
        .selectize-dropdown .option {
            padding: 10px .75rem;
        }

        .selectize-dropdown .active {
            background-color: #fbfbfb;
        }

        .selectize-dropdown-content {
            max-height: 500px;
            background-color: #EFF2F6;
        }

        /*  selectize arrow  */
        .selectize-input::after,
        .selectize-input::before {
            content: " ";
            display: block;
            clear: left;
        }

        .selectize-control.single .selectize-input:after {
            top: 60%;
            border-color: #6a7b89 transparent transparent transparent;
        }

        .selectize-control.single .selectize-input:before {
            content: " ";
            display: block;
            position: absolute;
            top: 40%;
            right: calc(0.75rem + 5px);
            margin-top: -3px;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 5px 5px 5px;
            border-color: transparent transparent #6a7b89 transparent;
        }

        .selectize-control.single .selectize-input.dropdown-active:after {
            margin-top: -3px !important;
            border-width: 5px 5px 0 5px !important;
            border-color: #6a7b89 transparent transparent transparent !important;
        }

        .selectize-control.single .selectize-input.dropdown-active::before {
            left: auto;
        }

    </style>
@endpush

@section('content')
    <div id="root" class="container-login100">
        <div class="wrap-login100 row">
            <div class="login100-form validate-form col-12 col-lg-6 login100-form-mobile" id="scrollContainer">
                <!-- Register Ticket -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <div class="p-b-40">
                            <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Swiper -->
                        <div class="swiper swiper-ticket">
                            <div class="swiper-wrapper">

                                <!-- Ticket -->
                                <div class="swiper-slide">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-b-30 text-left">
                                                <h5 class="login100-form-title">{{ preferGet('ticket_title_text') ?? 'Master Ticket' }}</h5>
                                                <p class="txt2">
                                                    {{ preferGet('ticket_caption_text') ?? 'To continue registration, please book a ticket to join the session at this event' }}
                                                </p>
                                            </div>
                                            <div class="form-coupon">
                                                <p class="coupon-label">Redeem Voucher Code</p>
                                                <div class="coupon-container">
                                                    <input type="text" v-model="couponCode" class="coupon-input"
                                                           placeholder=" ...">
                                                    <button type="button" class="coupon-button" @click="getTicketFree">
                                                        Check
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-payment ticket"
                                                 v-for="(ticket, i) in tickets" :key="i">
                                                <div class="d-flex justify-content-between mb-0">
                                                    <p class="classname align-content-center" v-cloak>@{{ ticket.name
                                                        }}</p>
                                                    <div>
                                                        <template v-if="ticket.quota <= ticket.quota_achieve">
                                                            <p class="classsoldout text-center" v-cloak>Sold Out</p>
                                                        </template>
                                                        <template v-else>
                                                            <button
                                                                :class="'btn btn-outline-primary btn-sm btn-add-to-cart' + (selectedIndexTicket === i ? ' active' : '')"
                                                                @click="setSelectedTicket(i)">
                                                                <template v-if="selectedIndexTicket === i">Selected
                                                                </template>
                                                                <template v-else>Add to cart</template>
                                                            </button>
                                                        </template>
                                                        <p class="classcategories text-center" v-cloak>@{{
                                                            ticket.label_categories }}</p>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <p class="classdate" v-cloak v-html="ticket.description"></p>
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="">
                                                        <p class="classnominal" v-cloak
                                                           v-if="ticket.dummy_price <= 0"></p>
                                                        <p class="classnominal_dummy" v-cloak v-else>@{{ formatRupiah(''
                                                            + ticket.dummy_price) }}</p>
                                                    </div>
                                                    <div class="">
                                                        <p class="classnominal" v-cloak v-if="ticket.price <= 0">
                                                            FREE</p>
                                                        <p class="classnominal" v-cloak v-else>@{{ formatRupiah('' +
                                                            ticket.price) }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-5">
                                            <div class="d-flex align-item-center justify-content-between px-4">
                                                <p class="total-title">Total Price</p>
                                                <p class="total-nominal mr-5" v-cloak v-if="paidAmount <= 0">FREE</p>
                                                <p class="total-nominal mr-5" v-cloak v-else>@{{ totalNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-12 p-b-10">
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="slideToNext">
                                                    @lang('eventy/register.text_btn_next')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ticket -->

                                <!-- if Ticket Group -->
                                <div class="swiper-slide">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-b-30 text-left">
                                                <h5 class="login100-form-title">
                                                    {{ 'Participant Data' }}
                                                </h5>
                                                <p class="txt2">
                                                    {{  'Add data on participants who will attend in the type of ticket you choose' }}
                                                </p>
                                            </div>
                                            <div class="row">
                                                <div class="ticket-section">
                                                    <p style=" font-size: 12px; font-style: normal; font-weight: 600; ine-height: normal;">
                                                        Your Ticket</p>
                                                    <div class="ticket-info">
                                                        <div class="ticket-name pb-3">@{{ currentTicket.name }}
                                                        </div>
                                                        <div class="ticket-price">@{{ totalNominal }}</div>
                                                    </div>
                                                </div>

                                                <div class="participant-section">
                                                    <p style=" font-size: 12px; font-style: normal;font-weight: 600; line-height: normal;">
                                                        More Participant Details</p>
                                                    <div class="participant-list pb-3">
                                                        <!-- Display participants -->
                                                        <div v-for="(newParticipant, index) in participants"
                                                             :key="index"
                                                             class="participant-info justify-content-between m-b-5">
                                                            <div class="">
                                                                <p class="participant-name"
                                                                   style="font-size: 10px;font-style: normal;font-weight: 400;">
                                                                    Participant @{{ index + 2
                                                                    }}</p>
                                                                <p class="participant-name">@{{ newParticipant.name
                                                                    }}</p>
                                                                <p class="participant-email"
                                                                   style="font-size: 12px;font-style: normal;font-weight: 400;">
                                                                    @{{ newParticipant.email
                                                                    }}</p>
                                                            </div>
                                                            <div class="participant-actions">
                                                                <button @click="editParticipant(index)">
                                                                    <img class="participant-icon-add"
                                                                         src="{{asset('sirclo/icon_edit.png')}}" alt="">
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <template v-if="participants.length < groupNumber">
                                                        <button class="participant-info justify-content-between"
                                                                @click="slideToAddParticipant">
                                                            <span class="align-content-center">Add Participant</span>
                                                            <img class="participant-icon-add"
                                                                 src="{{asset('sirclo/icon_add.png')}}" alt="">
                                                        </button>
                                                    </template>
                                                </div>

                                                <div class="col-sm-12 mt-5">
                                                    <div class="d-flex align-item-center justify-content-between px-4">
                                                        <p class="total-title">Total Price</p>
                                                        <p class="total-nominal mr-5" v-cloak v-if="paidAmount <= 0">
                                                            FREE</p>
                                                        <p class="total-nominal mr-5" v-cloak v-else>@{{ totalNominal
                                                            }}</p>
                                                    </div>
                                                </div>

                                                <div class="container-login100-form-btn p-t-20">
                                                    <div class="row">
                                                        <div class="col-md-6 p-b-10">
                                                            <button type="button" class="login100-form-blue btn-back"
                                                                    @click="slideToPrev">@lang('eventy/register.text_btn_back')</button>
                                                        </div>
                                                        <div class="col-md-6 p-b-10">
                                                            <button type="button" class="login100-form-btn btn-next"
                                                                    @click="slideToNext"
                                                                    v-if="ticketType === 'Paid' && paidAmount > 0">
                                                                @lang('eventy/register.text_btn_next')
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- form add participant -->
                                <div class="swiper-slide">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-b-30 text-left">
                                                <h5 class="login100-form-title">
                                                    {{ 'Add Data' }}
                                                </h5>
                                            </div>
                                            <form @submit.prevent="saveParticipant">
                                                <div class="col-12">
                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Name is required">
                                                        <label for="name" class="label-input100">Full Name<span
                                                                class="required"></span></label>
                                                        <input class="input100 validated" id="name" type="text"
                                                               name="name" minlength="3" maxlength="150"
                                                               autocomplete="off" v-model="currentParticipant.name">
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Email is required">
                                                        <label for="email" class="label-input100">Work E-mail<span
                                                                class="required"></span></label>
                                                        <input class="input100 validated" id="email" type="email"
                                                               name="email" minlength="3" maxlength="70"
                                                               autocomplete="off" v-model="currentParticipant.email">
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Phone is required">
                                                        <label for="phone" class="label-input100">Phone</label>
                                                        <div class="parent-phone">
                                                            <div class="nation-phone">
                                                                <select v-model="currentParticipant.phone_prefix"
                                                                        id="phone_prefix" name="phone_prefix"
                                                                        style="background: #EFF2F6; border: none;">
                                                                    @foreach($countries as $country)
                                                                        <option
                                                                            value="{{ $country->dial_code }}" {{ $country->dial_code == '+62' ? 'selected' : '' }}>
                                                                            {{ $country->flag }} {{ $country->dial_code }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <span class="phone-pre">|</span>
                                                            <input class="input100 only-0 validated" id="phone"
                                                                   type="text" name="phone" minlength="9"
                                                                   maxlength="15" pattern="\d*" autocomplete="off"
                                                                   v-model="currentParticipant.phone"
                                                                   @input.lazy="numberOnlyInTicket">
                                                        </div>
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Nationality is required">
                                                        <label for="nationality" class="label-input100">Nationality<span
                                                                class="required"></span></label>
                                                        <select class="input100" id="nationality"
                                                                v-model="currentParticipant.nationality">
                                                            <option value="" selected></option>
                                                            @foreach($countries as $country)
                                                                <option
                                                                    value="{{ $country->name }}">{{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Job Title is required">
                                                        <label for="job_title" class="label-input100">Job Title<span
                                                                class="required"></span></label>
                                                        <input class="input100 validated" id="job_title" type="text"
                                                               name="job_title" minlength="3" maxlength="100"
                                                               autocomplete="off"
                                                               v-model="currentParticipant.job_title">
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Company Name is required">
                                                        <label for="company" class="label-input100">Company Name<span
                                                                class="required"></span></label>
                                                        <input class="input100 validated" id="company" type="text"
                                                               name="company" minlength="3" maxlength="100"
                                                               autocomplete="off" v-model="currentParticipant.company">
                                                    </div>

                                                    <div class="wrap-input100 validate-input2"
                                                         data-validate="Type of Business is required">
                                                        <label for="type_of_business" class="label-input100">Type of
                                                            Business<span class="required"></span></label>
                                                        <label class="container-checkbox" v-for="item in businessTypes"
                                                               :key="item">
                                                            @{{ item }}
                                                            <input type="radio"
                                                                   :checked="currentParticipant.business_type === item"
                                                                   @change="selectBusinessType(item)">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>

                                                    <div class="custom-container">
                                                        <label for="area_of_interests" class="custom-label">Areas of
                                                            Interest (Max 3)<span class="required"></span></label>
                                                        <div class="custom-checkbox-grid">
                                                            <div class="custom-checkbox"
                                                                 v-for="(item, index) in areaOfInterests" :key="index">
                                                                <input @change="checkMaxAreaOfInterests" type="checkbox"
                                                                       :id="'checkbox-' + index"
                                                                       v-model="currentParticipant.area_of_interests"
                                                                       :value="item">
                                                                <label :for="'checkbox-' + index"
                                                                       :class="{'checked-background': currentParticipant.area_of_interests.includes(item)}">@{{
                                                                    item }}</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Password is required">
                                                        <label for="password" class="label-input100">Password<span
                                                                class="required"></span></label>
                                                        <input class="input100 validated" id="password" type="password"
                                                               name="password" autocomplete="off"
                                                               v-model="currentParticipant.password" minlength="3">
                                                        <div class="show-password" style="right: 40px;">
                                                            <i class="fa fa-eye"></i>
                                                        </div>
                                                    </div>

                                                    <div class="wrap-input100 validate-input"
                                                         data-validate="Retype Password is required">
                                                        <label for="retype_password" class="label-input100">Retype
                                                            Password<span class="required"></span></label>
                                                        <input class="input100 validated" id="retype_password"
                                                               type="password" name="retype_password" autocomplete="off"
                                                               v-model="currentParticipant.retype_password"
                                                               minlength="3">
                                                        <div class="show-password" style="right: 40px;">
                                                            <i class="fa fa-eye"></i>
                                                        </div>
                                                    </div>

                                                    <div class="checkbox-container">
                                                        <input type="checkbox" id="agree"
                                                               v-model="currentParticipant.agree">
                                                        <label for="agree">I agree to allow this website to process my
                                                            registration information, including its use for marketing
                                                            purposes.</label>
                                                    </div>

                                                    <div class="container-login100-form-btn p-t-10">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <button class="login100-form-btn" type="submit">
                                                                    Add Participant
                                                                </button>
                                                            </div>
                                                            <div class="col-12 p-t-5">
                                                                <button class="login100-form-blue btn-back"
                                                                        type="button" @click="slideToBack">
                                                                    Back
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- form add participant -->
                                <!-- if Ticket Group -->

                                <!-- Order Summary -->
                                <div class="swiper-slide">

                                    <div class="p-b-30 text-left">
                                        <h5 class="login100-form-title">
                                            {{ preferGet('order_summary_title_text') ?? 'Order Summary' }}
                                        </h5>
                                        <p class="txt2">
                                            {{ preferGet('order_summary_caption_text') ?? 'Please check your ticket order before making payment' }}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-summary">
                                                <div class="table-row table-header">
                                                    <h3 class="table-column first-column">Type</h3>
                                                    <h3 class="table-column second-column">Price</h3>
                                                </div>
                                                <div class="table-row table-ticket">
                                                    <div class="table-column first-column">
                                                        <h3 class="title">@{{ currentTicket.name }}</h3>
                                                        {{--                                                        <p class="desc" v-cloak v-html="currentTicket.description"></p>--}}
                                                        {{--                                                        <p class="desc">@{{ currentTicket.description }}</p>--}}
                                                        <div v-if="participants.length > 0">
                                                            <span class="m-t-9"
                                                                  style="font-size: 10px;font-weight: 600;line-height: normal;">Participants</span>
                                                            <div class="m-t-0">
                                                                <ul style="list-style-type: disc; margin: 0; padding-left: 2rem;">
                                                                    <li>
                                                                        <span
                                                                            style="font-size: 10px; font-style: normal;font-weight: 400;line-height: normal;">@{{ participant.name }}</span>
                                                                    </li>
                                                                </ul>
                                                                <template>
                                                                    <div>
                                                                        <ul style="list-style-type: disc; margin: 0; padding-left: 2rem;"
                                                                            v-if="participants.length > 0">
                                                                            <li v-for="(listParticipant, index) in participants"
                                                                                :key="index">
                                                                                <span
                                                                                    style="font-size: 10px; font-style: normal; font-weight: 400; line-height: normal;">@{{ listParticipant.name }}</span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table-column second-column">
                                                        <p class="total-nominal text-black"
                                                           v-if="currentTicket.price <= 0">FREE</p>
                                                        <p class="total-nominal text-black" v-else>@{{ formatRupiah('' +
                                                            currentTicket.price) }}</p>
                                                    </div>
                                                </div>
                                                <div class="table-row table-classroom"
                                                     v-for="(i) in selectedIndexClassroom">
                                                    <div class="table-column first-column">
                                                        <h3 class="title">@{{ ticketClassroom[i].schedule.name }}</h3>
                                                        <p class="desc">@{{ ticketClassroom[i].schedule.description
                                                            }}</p>
                                                    </div>
                                                    <div class="table-column second-column">1</div>
                                                    <div class="table-column">
                                                        <p class="total-nominal" v-if="ticketClassroom[i].price <= 0">
                                                            FREE</p>
                                                        <p class="total-nominal" v-else>@{{ formatRupiah('' +
                                                            ticketClassroom[i].price) }}</p>
                                                    </div>

                                                    {{--                                                    <button class="btn-remove"--}}
                                                    {{--                                                            @click="removeSelectedClassroom({--}}
                                                    {{--                                                        i,--}}
                                                    {{--                                                        id: ticketClassroom[i].schedule_id,--}}
                                                    {{--                                                        ticket_schedule_id: ticketClassroom[i].id--}}
                                                    {{--                                                    })">--}}
                                                    {{--                                                        <span>&times;</span>--}}
                                                    {{--                                                    </button>--}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-5">
                                            <div class="d-flex align-item-center justify-content-between px-4">
                                                <p class="total-title">Total Price</p>
                                                <p class="total-nominal mr-5" v-cloak v-if="paidAmount <= 0">FREE</p>
                                                <p class="total-nominal mr-5" v-else>@{{ totalNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-blue btn-back"
                                                        @click="slideToPrev">@lang('eventy/register.text_btn_back')</button>
                                            </div>
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="slideToNext"
                                                        v-if="ticketType === 'Paid' && paidAmount > 0">
                                                    @lang('eventy/register.text_btn_next')
                                                </button>
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="postSubmitNoClassroom" v-else>
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Order Summary -->

                                <!-- Payment -->
                                <div class="swiper-slide">

                                    <div class="p-b-30 text-center">
                                        <h5 class="login100-form-title">
                                            {{ preferGet('payment_title_text') ?? 'Payment' }}
                                        </h5>
                                        <p class="txt2">
                                            {{ preferGet('payment_caption_text') ?? 'Complete your event registration by purchasing your ticket.' }}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="channel-box">
                                                <template v-for="(method, i) in paymentMethods">
                                                    <div class="channel-panel" :data-index="i"
                                                         :class="{ active: selectedPaymentMethod === method.name }"
                                                         @click="setActiveMethod(method)"
                                                         v-if="method.name !== 'VIRTUAL_ACCOUNT'">
                                                        <template v-if="method.name === 'CREDIT_CARD'">
                                                            <p class="channel-name">Credit Card</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'OVO'">
                                                            <p class="channel-name">OVO</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'QRIS'">
                                                            <p class="channel-name">QRIS</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'BANK_TRANSFER'">
                                                            <p class="channel-name">Bank Transfer</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                    </div>
                                                    <template v-if="method.name === 'VIRTUAL_ACCOUNT'">
                                                        <div class="channel-group"
                                                             :class="{ active: selectedPaymentMethod === method.name }">
                                                            <div class="channel-group-title">
                                                                <p class="channel-name">Virtual Account</p>
                                                                <span
                                                                    class="fa fa-caret-down ic-caret-slide rotate-up"></span>
                                                            </div>
                                                            <div class="channel-list">
                                                                <div class="channel-list-group">
                                                                    <div v-for="(bank, index) in virtualAccountBanks"
                                                                         class="channel-list-panel"
                                                                         :class="{ active: selectedPaymentMethod === method.name && indexVirtualAccount === index }"
                                                                         @click="setActiveMethod(method, index)">
                                                                        <img :src="bank.image" class="w-75"
                                                                             :alt="bank.name">
                                                                    </div>
                                                                    <div
                                                                        style="background: transparent;width: 32%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </template>
                                            </div>

                                            <div
                                                class="col-sm-12 d-flex align-item-center justify-content-between mb-2 mt-4">
                                                <p class="total-title">Amount</p>
                                                <p class="total-nominal mr-5">@{{ totalNominal }}</p>
                                            </div>
                                            <hr>
                                            <div
                                                class="col-sm-12 d-flex align-item-center justify-content-between my-2">
                                                <p class="total-title">Total Paid</p>
                                                <p class="total-nominal mr-5">@{{ totalPaidNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-blue btn-back"
                                                        v-if="!method_changed"
                                                        @click="slideToPrev">@lang('eventy/register.text_btn_back')</button>
                                            </div>
                                            <div class="p-b-10"
                                                 :class="[{ 'col-12' : method_changed }, { 'col-md-6' : !method_changed }]">
                                                <button type="button" class="login100-form-btn"
                                                        @click="postSubmitPayment">@lang('eventy/register.text_btn_submit')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Payment -->

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6 login-register-image">
                <img src="{{ preferGet('login_image') }}" alt="Welcome">
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        let tickets = {!! json_encode($tickets) !!};
        let paymentMethods = {!! json_encode($payment_methods) !!};
        let method_changed = {{ $method_changed === "yes" ? 'true' : 'false' }};
        let schedule_ids = {!! json_encode($schedule_ids) !!};
        let ticket_schedule_ids = {!! json_encode($ticket_schedule_ids) !!};
        let participant_ids_group = {!! json_encode($participant_ids_group) !!};
        let payment = {
            id: '{{ $payment_id }}',
            amount: '{{ $amount }}',
            initial_amount: '{{ $initial_amount }}',
            ticket_id: '{{ $ticket_id }}',
        };
    </script>
    <script src="{{ asset('js/web/Register/ticket.js') . '?v=1.1.5' }}"></script>
@endpush
