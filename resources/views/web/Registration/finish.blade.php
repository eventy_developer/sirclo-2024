@extends('layout.Auth.index')

@section('page_title', preferGet('name_events') . ' - Finish')

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/util.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/auth.css') }}">
<link rel="stylesheet" href="{{ asset('css/web/Register/ticket.css') }}">
<link rel="stylesheet" href="{{ asset('css/web/Register/finish.css') }}">
@endpush

@section('content')
<div id="root" class="container-login100">
    <div class="wrap-login100 row">
        <div class="login100-form validate-form col-12 col-lg-6" id="scrollContainer">
            <div class="row">
                <div class="col-sm-12 ml-auto">

                    <div class="row justify-content-center text-center">
                        <div class="col-lg-7">
                            <div class="p-b-50 grey">
                                <h5 class="login100-form-title">
{{--                                    @lang('eventy/register_finish.text_thanks_order')--}}
                                    Your order has been received
                                </h5>
                                <p class="txt2">
{{--                                    @lang('eventy/register_finish.text_complete_payment', ['method' => $method_readable])--}}
                                    To secure your ticket, please complete your payment as outlined below.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="channel-box">
                                <div class="payment-summary-box">
                                    <div class="summary-header">
                                        <h3 class="summary-title">@lang('eventy/register_finish.text_detail_title')</h3>
                                        <hr>
                                    </div>
                                    <div class="summary-body">
                                        @if(!empty($payment->participant_ids_group))
                                        <div class="summary-list">
                                            <div class="w-100">
                                                <h4 class="summary-key">Participant List</h4>
                                                @foreach($payment->transactionDetails as $participantList)
                                                    <div class="m-t-0">
                                                        <ul style="list-style-type: disc; margin: 0; padding-left: 1rem;">
                                                            <li>
                                                                <div style="font-size: 12px; font-style: normal; font-weight: 400; line-height: normal;">
                                                                    <p>{{$participantList->participant->name}}</p>
                                                                    <p>{{$participantList->participant->email}}</p>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <hr>
                                        </div>
                                        @endif
                                        <div class="summary-list">
                                            <div class="w-100">
                                                <h4 class="summary-key">Payment Status</h4>
                                                <p class="summary-value">
                                                    @if($payment->isWaitingConfirmation())
                                                    <span class="btn btn--status btn--waiting">Waiting</span>
                                                    @elseif($payment->isWaitingPayment())
                                                    <span class="btn btn--status btn--notyet">Not yet</span>
                                                    @else
                                                    <span class="btn btn--status btn--{{ strtolower($payment->status) }}">{{ strtolower($payment->status) }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="summary-list">
                                            <div class="w-100">
                                                <h4 class="summary-key">Payment Method</h4>
                                                @if ($payment_type === 'eWallets')
                                                <p class="summary-value">{{ $payment_method }}</p>
                                                @elseif ($payment_type === 'Cards')
                                                <p class="summary-value">Credit Card</p>
                                                @elseif($payment_type === 'Virtual Accounts')
                                                <p class="summary-value">Virtual Account ({{ $payment_method }})</p>
                                                @elseif ($payment_type === 'QR Codes')
                                                <p class="summary-value">QRIS</p>
                                                @else
                                                <p class="summary-value">Bank Transfer</p>
                                                @endif
                                            </div>
                                            <hr>
                                        </div>
{{--                                        @if ($payment_type === 'Virtual Accounts')--}}
{{--                                        <li>--}}
{{--                                            <div class="w-100">--}}
{{--                                                <h4 class="summary-key">Virtual Account Number</h4>--}}
{{--                                                <p class="summary-value text-copy">8808999939380502 (sample)</p>--}}
{{--                                            </div>--}}

{{--                                            <div class="copy-element">--}}
{{--                                                <button class="text-button ml-auto copy-button"--}}
{{--                                                    data-clipboard-action="copy"--}}
{{--                                                    data-clipboard-text="8808999939380502">--}}
{{--                                                    Copy--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </li>--}}
{{--                                        @endif--}}
                                        @if ($payment_type === 'Manual')
                                        <div>
                                            <div class="w-100">
                                                <h4 class="summary-key">Bank</h4>
                                                <p class="summary-value text-copy">
                                                    {{ preferGet('payment_bank_name') }} - {{ preferGet('payment_bank_branch') }}
                                                </p>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="w-100">
                                                <h4 class="summary-key">Account Owner</h4>
                                                <p class="summary-value text-copy">{{ preferGet('payment_account_owner') }}</p>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="w-100">
                                                <h4 class="summary-key">Account Number</h4>
                                                <p class="summary-value text-copy">{{ preferGet('payment_account_number') }}</p>
                                            </div>

                                            <div class="copy-element">
                                                <button class="text-button ml-auto copy-button"
                                                    data-clipboard-action="copy"
                                                    data-clipboard-text="{{ preferGet('payment_account_number') }}">
                                                    Copy
                                                </button>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="summary-list">
                                            <div class="w-100">
                                                <h4 class="summary-key">Paid Amount</h4>
                                                <p class="summary-value">Rp {{ $amount }}</p>
                                            </div>
                                            <hr>
                                            @if ($payment_type === 'Manual')
                                                <div class="copy-element">
                                                    <button class="text-button ml-auto copy-button"
                                                        data-clipboard-action="copy"
                                                        data-clipboard-text="{{ $amount }}">
                                                        Copy
                                                    </button>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="summary-list">
                                            <div class="w-100">
                                                <h4 class="summary-key">Order ID</h4>
                                                <p class="summary-value">{{ $no_invoice }}</p>
                                            </div>
                                            <hr>
                                            @if ($payment_type === 'Manual')
                                                <div class="copy-element">
                                                    <button class="text-button ml-auto copy-button"
                                                        data-clipboard-action="copy"
                                                        data-clipboard-text="{{ $no_invoice }}">
                                                        Copy
                                                    </button>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    @if(empty($payment->participant_ids_group))
                                        @if(countActivePaymentMethod() > 1)
                                        <div class="summary-footer">
                                            @if ($payment_type === 'Manual')
                                                @if (!$is_manual_have_confirm)
                                                <button type="button" class="btn-change-method"
                                                    @click="changePaymentMethod">
                                                    @lang('eventy/register_finish.text_btn_change_method')
                                                </button>
                                                @endif
                                            @else
                                            <button type="button" class="btn-change-method"
                                                @click="changePaymentMethod">
                                                @if($payment->isExpired())
                                                    Reset Payment
                                                @else
                                                    @lang('eventy/register_finish.text_btn_change_method')
                                                @endif
                                            </button>
                                            @endif
                                        </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-login100-form-btn">
{{--                        <div class="row p-t-30">--}}
{{--                            <div class="col-12">--}}
{{--                                @if($payment_type === 'Manual')--}}
{{--                                    <p class="caption-text">@lang('eventy/register_finish.text_caption_manual').</p>--}}
{{--                                    <br>--}}
{{--                                @elseif($payment_type === 'eWallets')--}}
{{--                                    <p class="caption-text">@lang('eventy/register_finish.text_caption_ovo').</p>--}}
{{--                                    <br>--}}
{{--                                @else--}}
{{--                                    <p class="caption-text">@lang('eventy/register_finish.text_caption_default').</p><br>--}}
{{--                                @endif--}}
{{--                                <p class="caption-text">@lang('eventy/register_finish.text_contact_info').<br>--}}
{{--                                    @if ($contact_name_1 && $contact_phone_1)--}}
{{--                                    <b class="caption-text">{{ $contact_name_1 }}--}}
{{--                                        <a style="font-size: 14px; color: var(--eventy-color-primary);"--}}
{{--                                            href="https://api.whatsapp.com/send?phone={{ $contact_phone_1 }}" target="_blank">--}}
{{--                                            ({{ $contact_phone_1 }})--}}
{{--                                        </a>--}}
{{--                                    </b><br>--}}
{{--                                    @endif--}}
{{--                                    @if ($contact_name_2 && $contact_phone_2)--}}
{{--                                    <b class="caption-text">{{ $contact_name_2 }}--}}
{{--                                        <a style="font-size: 14px; color: var(--eventy-color-primary);"--}}
{{--                                            href="https://api.whatsapp.com/send?phone={{ $contact_phone_2 }}" target="_blank">--}}
{{--                                            ({{ $contact_phone_2 }})--}}
{{--                                        </a>--}}
{{--                                    </b><br>--}}
{{--                                    @endif--}}
{{--                                    <strong>Email</strong> <strong><a style="font-size: 14px; color: var(--eventy-color-primary);" href="mailto:{{ $event_email }}">{{ $event_email }}</a></strong>.--}}
{{--                                    <br>--}}
{{--                                    @lang('eventy/register_finish.text_thanks').--}}
{{--                                </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        @if (!$is_pending)
                            @if(!$payment->isExpired())
                                <div class="row p-t-40">
                                    <div class="col-12">
                                        <p class="txt2">
{{--                                            @lang('eventy/register_finish.text_have_transfer')--}}
                                            Would you like to proceed with payment?
                                        </p>
                                    </div>
                                    <div class="col-12 p-t-20">
                                        <a href="javascript:void(0)" class="login100-form-btn"
                                           @click="confirmPayment">
                                            @if ($payment_type === 'Manual')
                                                @lang('eventy/register_finish.text_btn_confirm')
                                            @else
                                                @lang('eventy/register_finish.text_btn_pay')
                                            @endif
                                        </a>
                                    </div>
{{--                                    <div class="col-12 mt-5 text-center">--}}
{{--                                        <a href="{{ preferGet('landingpage_whatsapp') }}" target="_blank">--}}
{{--                                            <img src="{{ asset('images/icon/landingpage/icon_whatsapp_circle.svg') }}" alt="Icon Chat" class="footer__icon">--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
                                </div>
                            @endif
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 login-register-image">
            <img src="{{ preferGet('login_image') }}" alt="Welcome">
        </div>
    </div>
</div>
@endsection

@push('bottom')
<script src="{{ asset('packages/clipboard/clipboard.min.js') }}"></script>
<script>
    let payment = {
        id: '{{ $payment->id }}',
        method: '{{ $payment_method }}',
        type: '{{ $payment_type }}',
    };
    let confirmUrl = '{{ $url }}';
</script>
<script src="{{ asset('js/web/Register/finish.js') . '?v=1.0.0' }}"></script>
<script>
(function($) {
	'use strict';

	var clipboard = new Clipboard('.copy-button');
	clipboard.on('success', function(e) {
		Swal.fire(
			'Copied',
			'Text is copied !',
			'success'
		);
	});

	clipboard.on('error', function(e) {
		console.log(e);
	});
})(jQuery);
</script>
@endpush
