@extends('layout.Auth.index')

@section('page_title', preferGet('name_events') . ' - Additional Session')

@push('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/web/Auth/auth.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web/Register/ticket.css') }}">
@endpush

@section('content')
    <div id="root" class="container-login100">
        <div class="wrap-login100 row">
            <div class="login100-form validate-form col-12 col-lg-6" id="scrollContainer">
                <!-- Register Ticket -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <div class="p-b-40">
                            <img src="{{ preferGet('logo_events') }}" class="logo-img" alt="Logo Events">
                        </div>
                        <div class="p-b-60">
                            <div class="auth-tabs">
                                <a href="javascript:;" class="auth-tabs__item disabled">Login</a>
                                <a href="javascript:;" class="auth-tabs__item active">Registration</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <!-- Swiper -->
                        <div class="swiper swiper-ticket">
                            <div class="swiper-wrapper">

                                <!-- Ticket -->
                                <div class="swiper-slide">

                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <div class="p-b-30 text-center">
                                                <h5 class="login100-form-title">{{ preferGet('ticket_title_text') ?? 'Master Ticket' }}</h5>
                                                <p class="txt2">
                                                    {{ preferGet('ticket_caption_text') ?? 'To continue registration, please book a ticket to join the session at this event' }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-payment ticket"
                                                 v-for="(ticket, i) in tickets" :key="i">
                                                <div class="d-flex justify-content-between">
                                                    <p class="classname" v-cloak>@{{ ticket.name }}</p>
                                                    <div>
                                                        <button :class="'btn btn-outline-primary btn-sm btn-add-to-cart' + (selectedIndexTicket === i ? ' active' : '')"
                                                                @click="setSelectedTicket(i)">
                                                            <template v-if="selectedIndexTicket === i">Cancel</template>
                                                            <template v-else>Add to cart</template>
                                                        </button>
                                                    </div>
                                                </div>
                                                <p class="classnominal" v-cloak>@{{ formatRupiah('' + ticket.price) }}</p>
                                                <p class="classdate" v-cloak>@{{ ticket.description }}</p>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-5">
                                            <div class="d-flex align-item-center justify-content-between px-4">
                                                <p class="total-title">Total Price</p>
                                                <p class="total-nominal mr-5">@{{ totalNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-12 p-b-10">
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="slideToNext">
                                                    @lang('eventy/register.text_btn_next')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Ticket -->

                                <!-- Classroom -->
                                <div class="swiper-slide">

                                    <div class="p-b-30 text-center">
                                        <h5 class="login100-form-title">
                                            {{ preferGet('classroom_title_text') ?? 'Workshop Ticket' }}
                                        </h5>
                                        <p class="txt2">
                                            {{ preferGet('classroom_caption_text') ?? 'To continue registration, please book a ticket to join the session at this event' }}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel-payment classroom"
                                                 :class="{ 'active' : selectedIndexClassroom.includes(i) }"
                                                 v-for="(row, i) in ticketClassroom">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <p class="classname">@{{ row.schedule.name }}</p>
                                                        <p class="classnominal">@{{ formatRupiah('' + row.price) }}</p>
                                                    </div>
                                                    <div class="d-flex flex-column text-center">
                                                        <button class="btn btn-outline-primary btn-sm btn-add-to-cart btn-cart-classroom"
                                                                :class="{ 'active' : selectedIndexClassroom.includes(i) }"
                                                                :data-index="i"
                                                                @click="setSelectedClassroom">
                                                            <template v-if="selectedIndexClassroom.includes(i)">Cancel</template>
                                                            <template v-else>Add to cart</template>
                                                        </button>
                                                        <a class="btn-detail-session collapsed"
                                                           data-bs-toggle="collapse"
                                                           :href="'#collapseDetail' + i"
                                                           role="button"
                                                           aria-expanded="false"
                                                           aria-controls="collapseDetail">
                                                            <span class="text-collapsed">Detail</span>
                                                            <span class="text-not-collapsed">Close</span>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="collapse mt-4" :id="'collapseDetail' + i">
                                                    <div class="panel-session"
                                                         v-for="(session, si) in row.schedule.sessions">
                                                        <p class="panel-session__name">@{{ session.title }}</p>
                                                        <div class="panel-session__info">
                                                            <p class="panel-session__date">@{{ session.date }}</p>
                                                            <p class="panel-session__time">@{{ session.time_start }} - @{{ session.time_end }} WIB</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-5">
                                            <div class="d-flex align-item-center justify-content-between px-4">
                                                <p class="total-title">Total Price</p>
                                                <p class="total-nominal mr-5" v-cloak>@{{ totalNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-6 p-b-10">
                                                <a href="{{ url('/home') }}" class="login100-form-outline-btn btn-back">@lang('eventy/register.text_btn_back_to_home')</a>
                                            </div>
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="slideToNext">
                                                    @lang('eventy/register.text_btn_next')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Classroom -->

                                <!-- Order Summary -->
                                <div class="swiper-slide">

                                    <div class="p-b-30 text-center">
                                        <h5 class="login100-form-title">
                                            {{ preferGet('order_summary_title_text') ?? 'Order Summary' }}
                                        </h5>
                                        <p class="txt2">
                                            {{ preferGet('order_summary_caption_text') ?? 'Please check your ticket order before making payment' }}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-summary">
                                                <div class="table-row table-header">
                                                    <h3 class="table-column first-column">Type</h3>
                                                    <h3 class="table-column second-column">Qty</h3>
                                                    <h3 class="table-column">Price</h3>
                                                </div>
{{--                                                <div class="table-row table-ticket">--}}
{{--                                                    <div class="table-column first-column">--}}
{{--                                                        <h3 class="title">@{{ currentTicket.name }}</h3>--}}
{{--                                                        <p class="desc">@{{ currentTicket.description }}</p>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="table-column second-column">1</div>--}}
{{--                                                    <div class="table-column">--}}
{{--                                                        <p class="total-nominal">@{{ formatRupiah('' + currentTicket.price) }}</p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class="table-row table-classroom"
                                                     v-for="(i, realIndex) in selectedIndexClassroom">
                                                    <div class="table-column first-column">
                                                        <h3 class="title">@{{ ticketClassroom[i].schedule.name }}</h3>
                                                    </div>
                                                    <div class="table-column second-column">1</div>
                                                    <div class="table-column">
                                                        <p class="total-nominal">@{{ formatRupiah('' + ticketClassroom[i].price) }}</p>
                                                    </div>

                                                    <button class="btn-remove"
                                                            v-if="selectedIndexClassroom.length > 1 && realIndex > 0"
                                                            @click="removeSelectedClassroom({
                                                        i,
                                                        id: ticketClassroom[i].schedule_id,
                                                        ticket_schedule_id: ticketClassroom[i].id
                                                    })">
                                                        <span>&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 mt-5">
                                            <div class="d-flex align-item-center justify-content-between px-4">
                                                <p class="total-title">Total Price</p>
                                                <p class="total-nominal mr-5">@{{ totalNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-outline-btn btn-back"
                                                        @click="slideToPrev">@lang('eventy/register.text_btn_back')</button>
                                            </div>
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="slideToNext" v-if="ticketType === 'Paid'">
                                                    @lang('eventy/register.text_btn_next')
                                                </button>
                                                <button type="button" class="login100-form-btn btn-next"
                                                        @click="postSubmitNoClassroom" v-else>
                                                    @lang('eventy/register.text_btn_register')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Order Summary -->

                                <!-- Payment -->
                                <div class="swiper-slide">

                                    <div class="p-b-30 text-center">
                                        <h5 class="login100-form-title">
                                            {{ preferGet('payment_title_text') ?? 'Payment' }}
                                        </h5>
                                        <p class="txt2">
                                            {{ preferGet('payment_caption_text') ?? 'Pay your ticket fee to finish event registration' }}
                                        </p>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="channel-box">
                                                <template v-for="(method, i) in paymentMethods">
                                                    <div class="channel-panel" :data-index="i"
                                                         :class="{ active: selectedPaymentMethod === method.name }"
                                                         @click="setActiveMethod(method)"
                                                         v-if="method.name !== 'VIRTUAL_ACCOUNT'">
                                                        <template v-if="method.name === 'CREDIT_CARD'">
                                                            <p class="channel-name">Credit Card</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'OVO'">
                                                            <p class="channel-name">OVO</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'QRIS'">
                                                            <p class="channel-name">QRIS</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                        <template v-if="method.name === 'BANK_TRANSFER'">
                                                            <p class="channel-name">Bank Transfer</p>
                                                            <div class="channel-circle"><span></span></div>
                                                        </template>
                                                    </div>
                                                    <template v-if="method.name === 'VIRTUAL_ACCOUNT'">
                                                        <div class="channel-group"
                                                             :class="{ active: selectedPaymentMethod === method.name }">
                                                            <div class="channel-group-title">
                                                                <p class="channel-name">Virtual Account</p>
                                                                <span class="fa fa-caret-down ic-caret-slide rotate-up"></span>
                                                            </div>
                                                            <div class="channel-list">
                                                                <div class="channel-list-group">
                                                                    <div v-for="(bank, index) in virtualAccountBanks"
                                                                         class="channel-list-panel"
                                                                         :class="{ active: selectedPaymentMethod === method.name && indexVirtualAccount === index }"
                                                                         @click="setActiveMethod(method, index)">
                                                                        <img :src="bank.image" class="w-75" :alt="bank.name">
                                                                    </div>
                                                                    <div style="background: transparent;width: 32%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </template>
                                            </div>

                                            <div class="col-sm-12 d-flex align-item-center justify-content-between mb-2 mt-4">
                                                <p class="total-title">Amount</p>
                                                <p class="total-nominal mr-5">@{{ totalNominal }}</p>
                                            </div>
                                            <div class="col-sm-12 d-flex align-item-center justify-content-between my-2">
                                                <p class="total-title">Fee</p>
                                                <p class="total-nominal mr-5">@{{ totalFee }}</p>
                                            </div>
                                            <hr>
                                            <div class="col-sm-12 d-flex align-item-center justify-content-between my-2">
                                                <p class="total-title">Total Paid</p>
                                                <p class="total-nominal mr-5">@{{ totalPaidNominal }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-login100-form-btn p-t-20">
                                        <div class="row">
                                            <div class="col-md-6 p-b-10">
                                                <button type="button" class="login100-form-outline-btn btn-back"
                                                        v-if="!method_changed"
                                                        @click="slideToPrev">@lang('eventy/register.text_btn_back')</button>
                                            </div>
                                            <div class="p-b-10"
                                                 :class="[{ 'col-12' : method_changed }, { 'col-md-6' : !method_changed }]">
                                                <button type="button" class="login100-form-btn"
                                                        @click="postSubmitPayment">@lang('eventy/register.text_btn_submit')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Payment -->

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6 login-register-image">
                <img src="{{ preferGet('login_image') }}" alt="Welcome">
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script>
        let tickets = {!! json_encode($tickets) !!};
        let paymentMethods = {!! json_encode($payment_methods) !!};
        let method_changed = {{ $method_changed === "yes" ? 'true' : 'false' }};
        let schedule_ids = {!! json_encode($schedule_ids) !!};
        let ticket_schedule_ids = {!! json_encode($ticket_schedule_ids) !!};
        let payment = {
            id: '{{ $payment_id }}',
            amount: '{{ $amount }}',
            initial_amount: '{{ $initial_amount }}',
            ticket_id: '{{ $ticket_id }}',
        };
    </script>
    <script src="{{ asset('js/web/Register/classroom-addition.js') . '?v=1.0.0' }}"></script>
@endpush
