<div>
    <div class="drawer-navigation">
        <button class="btn-toggle-drawer" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight">
            <img src="{{ asset('images/button/btn_nav_drawer.svg') }}" alt="Icon Arrow Left">
        </button>

        <ul class="drawer-navigation__menu">
            @foreach($drawerButtons as $button)
                <li class="drawer-navigation__item">
                    <img src="{{ $button['icon'] }}" alt="Icon Drawer" class="drawer-navigation__icon">
                    <span class="drawer-navigation__label">{{ $button['text'] }}</span>
                </li>
            @endforeach
        </ul>
    </div>

    {{ $slot }}
</div>
