<div class="offcanvas offcanvas-end offcanvas-custom" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-body offcanvas-body-custom">
        {{ $slot }}
    </div>
</div>
