<?php

return [
    'title_page' => ':NAME_EVENT - Landing Page',

    'text_days' => 'Days',
    'text_hours' => 'Hours',
    'text_minutes' => 'Minutes',
    'text_seconds' => 'Seconds',

    'text_have_fun' => 'Ready to join this event?',

    'text_btn_join' => 'Join Now',
    'text_btn_login' => 'Login',
    'text_btn_register' => 'Registration',
];
