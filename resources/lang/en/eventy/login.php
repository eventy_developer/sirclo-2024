<?php

return  [
    'title_page' => ':NAME_EVENT - Login',
    'text_forgot' => 'Forgot your password?',
    'placeholder_username' => 'Email',
    'placeholder_password' => 'Password',
    'valid_username' => 'Email is required',
    'valid_password' => 'Password is required',
    'text_btn_login' => 'Login',
    'text_btn_register' => 'Register',

    'res_login_not_register' => 'Sorry this account is not registered, please register first!',
    'res_login_not_found' => 'Sorry this account is not registered, please register first!',
    'res_ticket_not_found' => 'Ticket not found, please contact the relevant admin!',
    'res_login_not_active' => 'Sorry this account is still not active, please contact the relevant admin!',
    'res_login_not_paidoff' => 'Please complete the payment for this account first!',
    'res_login_not_confirm' => 'Your account is being processed!',
    'res_login_is_pending' => 'Your account is being processed!',
    'res_login_not_match' => 'Password do not match',
    'res_login_success' => 'Congratulations, your account has successfully logged in.',
];
