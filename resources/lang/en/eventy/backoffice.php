<?php

return  [
    'page_title' => ':EVENT_NAME - Backoffice',

    'booth_welcome_back' => 'Welcome Back in Booth :BOOTH_NAME :ADMIN_NAME',

    'text_btn_edit' => 'Edit',
    'text_btn_submit' => 'Submit',
    'text_btn_close' => 'Close',
    'text_btn_save_changes' => 'Save changes',

    'text_information_name' => 'Name',
    'text_description_title' => 'Description',
    'text_modal_title_description' => 'Edit Description',
    'text_modal_title_upload' => 'Upload Attachment',
    'text_modal_help_file_types' => 'File types support',

    'text_total_visitor' => 'Total Visitor',
    'text_attendance' => 'Participant',

    'conversation_title' => 'Conversation Chat',
    'conversation_search_placeholder' => 'Search attendance',
    'conversation_text_empty' => 'No Participants',

    'attachment_title' => 'Attachment',
    'attachment_text_manage' => 'Manage your attachment file below easily.',
    'attachment_text_list' => 'Attachment list',
    'attachment_text_upload' => 'Upload New Attachment',
    'attachment_text_image' => 'Image',
    'attachment_text_document' => 'Document',
    'attachment_text_video' => 'Video',

    'my_product_title' => 'My Product',
    'my_product_text_manage' => 'Manage your products to increase sales.',
    'my_product_text_list' => 'Product list',
    'my_product_text_button' => 'Manage Products Now',

    'res_updated_success' => 'Successfully update description',
    'res_upload_success' => 'Successfully upload attachment'
];
