<?php

return  [
    'title_page' => ':EVENT_NAME - Register Finish',

    'placeholder_bank_name' => 'Bank Name',
    'placeholder_bank_branch' => 'Bank Branch',
    'placeholder_account_number' => 'Account Number',
    'placeholder_account_owner' => 'Account Owner',
    'placeholder_paid_amount' => 'Paid Amount',
    'placeholder_order_id' => 'Order ID',
    'placeholder_copy' => 'COPY',

    'text_thanks_order' => 'Thank you for your order',
    'text_complete_payment' => 'You make payment via :method. Please complete payment within the following time periods.',
    'text_detail_title' => 'Payment details',
    'text_payment_info' => 'To complete your payment process, please prepare and upload your copy of bank transfer through click the Confirm Payment button.',
    'text_have_transfer' => 'Have you finished the transfer ?',
    'text_caption_manual' => 'Please transfer according to the nominal stated',
    'text_caption_ovo' => 'We will send a notification to your OVO Account to complete payment after you click the Pay Now button',
    'text_caption_default' => 'Please pay according to the nominal stated',
    'text_contact_info' => 'For more detailed information, please contact us via Whatsapp',
    'text_thanks' => 'Thank you',

    'text_btn_login' => 'Login',
    'text_btn_confirm' => 'Upload Payment Proof',
    'text_btn_pay' => 'Pay Now',
    'text_back_login' => 'Back to Login form',
    'text_btn_change_method' => 'Change Payment Method',
];
