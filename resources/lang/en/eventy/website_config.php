<?php

return [
    'copyright' => 'Copyright :INSTANCE. All Rights Reserved',

    'btn_next' => 'Next',
    'btn_previous' => 'Previous',

    'notification_title' => 'Notification',
    'notification_mark_read' => 'Mark as read',

    'title_profile' => 'Profile',
    'title_edit_pass' => 'Edit Password',

    'profile_text_username' => 'Username',
    'profile_text_phone' => 'Phone Number',

    'profile_text_account_sett' => 'Account Settings',
    'profile_text_edit_pass' => 'Edit Password',
    'profile_text_password' => 'Password',

    'edit_pass_label_current_pass' => 'Current Password',
    'edit_pass_label_new_pass' => 'New Password',
    'edit_pass_label_retype_pass' => 'Retype New Password',

    'edit_pass_placeholder_current_pass' => 'Type your current password',
    'edit_pass_placeholder_new_pass' => 'Type your new password',
    'edit_pass_placeholder_retype_pass' => 'Type your retype new password',

    'profile_btn_logout' => 'Logout',
    'profile_btn_save_pass' => 'Save',

    'text_header_user' => 'You\'re On',
    'text_page_visitor' => 'Visitor',

    'floating_menu_booth_label' => 'Floor Plan',
    'floating_menu_booth_list_label' => 'Booth List',
    'floating_menu_schedule_label' => 'Schedule',
    'floating_menu_sponsor_label' => 'Sponsor',

    'sponsor_text_info' => 'Please click on one of the logos to visit the sponsor\'s website',

    'sidebar_label_about' => 'About',
    'sidebar_label_product' => 'Product',
    'sidebar_label_document' => 'Document',
    'sidebar_label_video' => 'Video',
    'sidebar_label_attachment' => 'Attachment',
    'sidebar_label_polling' => 'Polling',
    'sidebar_label_quiz' => 'Quiz',
    'sidebar_label_image' => 'Image',

    'chat_compose_placeholder' => 'Compose Message',
    'chat_public_title' => 'Public Chat',
    'chat_conversation_title' => 'Conversation Chat',

    'question_title' => 'Question',

    'polling_no_start_title' => 'No polling started yet!',
    'polling_finish_title' => 'Thank you for filling this polling, have a nice day!',
    'polling_btn_vote' => 'Vote!',

    'quiz_ready_title' => 'Ready for Start Quiz?',
    'quiz_ready_caption' => 'Click button below and answer all Questions we have',
    'quiz_leaderboard_title' => 'Leaderboard',
    'quiz_score_title' => 'Score',
    'quiz_btn_start' => 'Start Quiz!',
    'quiz_btn_result' => 'See Result',

    'res_user_not_found' => 'Participant is not found',
    'res_profile_photo_success_updated' => 'Profile photo updated successfully',
    'res_password_success_updated' => 'Password updated successfully',
    'res_current_password_wrong' => 'Your current password is wrong',
];
