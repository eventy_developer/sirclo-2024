<?php

return [
    'select_category_label' => 'Booth Category',
    'placeholder_search_category' => 'Find a Hall Categories',
    'placeholder_search_booth' => 'Find a Booth',

    'btn_visit' => 'Visit Now',

    'product_text_interested' => 'Interested in the product?',
    'product_text_ask_admin' => 'Ask Admin',
    'product_text_contact_title' => 'Contact the booth admin via chat for more detailed product info',
];
