<?php

return  [
    'title_page' => ':EVENT_NAME - Register',

    'placeholder_name' => 'Full Name',
    'placeholder_email' => 'Email',
    'placeholder_password' => 'Password',
    'placeholder_retype_password' => 'Retype Password',
    'placeholder_company' => 'Company',
    'placeholder_job_title' => 'Job Position',
    'placeholder_phone' => 'Mobile Number',
    'placeholder_city' => 'City',

    'validate_name' => 'Full Name is required',
    'validate_email' => 'Email is required',
    'validate_password' => 'Password is required',
    'validate_retype_password' => 'Retype Password is required',
    'validate_company' => 'Company is required',
    'validate_job_title' => 'Job Position is required',
    'validate_phone' => 'Phone is required',
    'validate_city' => 'City is required',
    'valid_match_password' => 'Password do not match',

    'text_btn_login' => 'Login',
    'text_btn_register' => 'Create Account',
    'text_btn_next' => 'Next',
    'text_btn_back' => 'Back',
    'text_btn_back_to_home' => 'Back to Home',
    'text_btn_submit' => 'Pay Now',

    'text_have_account' => 'Have an Account ?',

    'res_ticket_not_found' => 'Ticket not found, please contact the relevant admin!',
    'res_email_is_required' => 'Email is required!',
    'res_email_not_valid' => 'Your email is invalid, please use another email address!',
    'res_email_not_available' => 'Email has been taken. Please log in!',
    'res_email_is_blocked' => 'Your account has been blocked, please contact the relevant admin!',
    'res_success_register_free' => 'Thank you for registering!',
    'res_success_register' => 'Thank you for registering. You will be directed to the ticket page',
    'res_email_error' => 'Oops! Something went wrong'
];
