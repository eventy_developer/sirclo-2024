<?php

return  [
    'title_page' => ':NAME_EVENT - Forgot Password',
    'title_page_change' => ':NAME_EVENT - Change Password',
    'placeholder_username' => 'Your registered email',
    'placeholder_password' => 'New Password',
    'placeholder_repassword' => 'Retype New Password',
    'valid_username' => 'Your registered email is required',
    'valid_password' => 'Password is required',
    'valid_repassword' => 'Retype Password is required',
    'text_btn_send' => 'Send Confirmation',
    'text_btn_change' => 'Submit New Password',
    'text_return_login' => 'Return to ',
    'text_login_page' => 'Login Page',

    'res_forgot_not_found' => 'Sorry this account is not registered, please register first!',
    'res_forgot_error' => 'Oops! Something went wrong',
    'res_forgot_success' => 'Successfully send your forgot password link, check your email to confirm!',

    'res_change_success' => 'Successfully changed password',
    'res_change_expired' => 'Link has been expired. Please get a new password reset link!',
    'res_change_error' => 'Oops! Something went wrong',

    'valid_email' => 'Email format is wrong',
    'valid_match_password' => 'Password do not match',
];
