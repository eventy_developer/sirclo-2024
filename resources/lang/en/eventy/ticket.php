<?php

return [
    'res_other_payment_exists_title' => 'Complete other payment first!',
    'res_other_payment_exists_body' => 'Click the invoices button on the home page',
];
