<?php

return  [
    'title_page' => ':EVENT_NAME - Payment Confirmation',

    'placeholder_order_id' => 'Order ID',
    'placeholder_name' => 'Account Name',
    'placeholder_paid_amount' => 'Paid Amount',
    'placeholder_paid_date' => 'Paid Date',
    'placeholder_file_upload' => 'Image Transfer',
    'placeholder_file' => 'Choose File',

    'validate_order_id' => 'Order ID is required',
    'validate_name' => 'Name is required',
    'validate_paid_amount' => 'Paid Amount is required',
    'validate_paid_date' => 'Paid Date is required',
    'validate_file' => 'File is required',

    'text_info_upload_size' => '* Only supports types : jpg, jpeg with maximum size is 3MB',
    'text_btn_submit' => 'Submit',
    'text_btn_confirmation' => 'Confirmation',

    'res_payment_is_submitted' => 'Your data has been successfully submitted, please complete your payment.',
    'res_invoice_not_found' => 'Invoice not found, please input a valid invoice code!',
    'res_invoice_expired' => 'Invoice has expired, please input a valid invoice code!',
    'res_payment_is_waiting' => 'You have sent a confirmation, please check your email!',
    'res_payment_is_approved' => 'Your payment has been approved, please check your email!',
    'res_confirmation_is_submitted' => 'Thanks for confirmation, we\'ll sent feedback email for you'
];
