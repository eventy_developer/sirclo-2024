<?php

return  [
    'title_page' => ':EVENT_NAME - Register Finish',

    'placeholder_bank_name' => 'Nama Bank',
    'placeholder_bank_branch' => 'Cabang Bank',
    'placeholder_account_number' => 'Nomor Rekening',
    'placeholder_account_owner' => 'Nama Rekening',
    'placeholder_paid_amount' => 'Total Pembayaran',
    'placeholder_order_id' => 'Order ID',
    'placeholder_copy' => 'SALIN',

    'text_thanks_order' => 'Terima kasih telah memesan',
    'text_complete_payment' => 'Anda memilih pembayaran melalui :method. Mohon selesaikan pembayaran sesuai batas waktu yang ditentukan',
    'text_detail_title' => 'Detail pembayaran',
    'text_payment_info' => 'Untuk memproses pembayaran, mohon upload bukti pembayaran melalui transfer bank dengan menekan tombol Konfirmasi dibawah',
    'text_have_transfer' => 'Sudah melakukan pembayaran ?',
    'text_caption_manual' => 'Silahkan transfer sesuai nominal yang tertera',
    'text_caption_ovo' => 'Kami akan mengirimkan pemberitahuan ke Akun OVO Anda untuk menyelesaikan pembayaran setelah Anda mengklik tombol Bayar Sekarang',
    'text_caption_default' => 'Silahkan bayar sesuai nominal yang tertera',
    'text_contact_info' => 'Untuk informasi lebih detail, silahkan hubungi kami melalui Whatsapp',
    'text_thanks' => 'Terima Kasih',

    'text_btn_login' => 'Masuk',
    'text_btn_confirm' => 'Upload Bukti Pembayaran',
    'text_btn_pay' => 'Bayar Sekarang',
    'text_back_login' => 'Kembali ke halaman Login',
    'text_btn_change_method' => 'Ganti Metode Pembayaran',
];
