<?php

return  [
    'title_page' => ':NAME_EVENT - Login',
    'text_forgot' => 'Lupa kata sandi Anda?',
    'placeholder_username' => 'Email',
    'placeholder_password' => 'Password',
    'valid_username' => 'Email wajib diisi',
    'valid_password' => 'Password wajib diisi',
    'text_btn_login' => 'Masuk',
    'text_btn_register' => 'Daftar',

    'res_login_not_register' => 'Maaf akun ini tidak terdaftar, silakan registrasi terlebih dahulu!',
    'res_login_not_active' => 'Maaf akun ini masih belum aktif, silakan hubungi pihak admin yang terkait!',
    'res_login_not_paidoff' => 'Mohon selesaikan pembayaran untuk akun ini terlebih dahulu!',
    'res_login_not_confirm' => 'Akun Anda sedang diproses!',
    'res_login_is_pending' => 'Akun Anda sedang diproses!',
    'res_login_not_match' => 'Password tidak cocok',
    'res_login_success' => 'Selamat akun anda berhasil login.',
];
