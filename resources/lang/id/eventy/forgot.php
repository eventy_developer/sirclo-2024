<?php

return  [
    'title_page' => ':NAME_EVENT - Lupa Password',
    'title_page_change' => ':NAME_EVENT - Ubah Password',
    'placeholder_username' => 'Email',
    'placeholder_password' => 'Password',
    'placeholder_repassword' => 'Ketik Ulang Password',
    'valid_username' => 'Email wajib diisi',
    'valid_password' => 'Password wajib diisi',
    'valid_repassword' => 'Ketik Ulang Password wajib diisi',
    'text_btn_send' => 'Kirim Konfirmasi',
    'text_btn_change' => 'Ubah Password',
    'text_return_login' => 'Kembali ke ',
    'text_login_page' => 'Halaman Login',

    'res_forgot_not_found' => 'Akun tidak ditemukan',
    'res_forgot_error' => 'Ups! Ada yang tidak beres',
    'res_forgot_success' => 'Berhasil mengirim link forgot password, periksa email anda untuk melakukan konfirmasi!',

    'res_change_success' => 'Berhasil mengubah kata sandi',
    'res_change_expired' => 'Tautan sudah tidak berlaku. Silakan dapatkan tautan pengaturan ulang kata sandi baru!',
    'res_change_error' => 'Ups! Ada yang tidak beres',

    'valid_email' => 'Format email salah',
    'valid_match_password' => 'Kata sandi tidak cocok',
];
