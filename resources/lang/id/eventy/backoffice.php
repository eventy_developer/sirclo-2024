<?php

return [
    'page_title' => ':EVENT_NAME - Backoffice',

    'booth_welcome_back' => 'Selamat datang kembali di Booth :BOOTH_NAME :ADMIN_NAME',

    'text_btn_edit' => 'Ubah',
    'text_btn_submit' => 'Kirim',
    'text_btn_close' => 'Tutup',
    'text_btn_save_changes' => 'Simpan perubahan',

    'text_information_name' => 'Nama',
    'text_description_title' => 'Deskripsi',
    'text_modal_title_description' => 'Ubah Deskripsi',
    'text_modal_title_upload' => 'Unggah Lampiran',
    'text_modal_help_file_types' => 'Mendukung file format',

    'text_total_visitor' => 'Jumlah pengunjung',
    'text_attendance' => 'Pengunjung',

    'conversation_title' => 'Obrolan',
    'conversation_search_placeholder' => 'Cari peserta',
    'conversation_text_empty' => 'Belum ada peserta',

    'attachment_title' => 'Lampiran',
    'attachment_text_manage' => 'Kelola lampiran Anda di bawah dengan mudah.',
    'attachment_text_list' => 'Daftar Lampiran',
    'attachment_text_upload' => 'Unggah Lampiran Baru',
    'attachment_text_image' => 'Gambar',
    'attachment_text_document' => 'Dokumen',
    'attachment_text_video' => 'Video',

    'my_product_title' => 'Produk Saya',
    'my_product_text_manage' => 'Kelola produk Anda untuk meningkatkan penjualan.',
    'my_product_text_list' => 'Daftar Produk',
    'my_product_text_button' => 'Kelola Produk Sekarang',

    'res_updated_success' => 'Berhasil memperbarui deskripsi',
    'res_upload_success' => 'Berhasil mengunggah lampiran'
];
