<?php

return  [
    'title_page' => ':EVENT_NAME - Register',

    'placeholder_name' => 'Nama Lengkap',
    'placeholder_email' => 'Email',
    'placeholder_password' => 'Password',
    'placeholder_retype_password' => 'Retype Password',
    'placeholder_company' => 'Perusahaan',
    'placeholder_job_title' => 'Pekerjaan',
    'placeholder_phone' => 'Nomor Telepon',
    'placeholder_city' => 'Kota',

    'validate_name' => 'Nama Lengkap wajib diisi',
    'validate_email' => 'Email wajib diisi',
    'validate_password' => 'Password wajib diisi',
    'validate_retype_password' => 'Retype Password wajib diisi',
    'validate_company' => 'Perusahaan wajib diisi',
    'validate_job_title' => 'Pekerjaan wajib diisi',
    'validate_phone' => 'Nomor Telepon wajib diisi',
    'validate_city' => 'Kota wajib diisi',
    'valid_match_password' => 'Kata sandi tidak cocok',

    'text_btn_login' => 'Masuk',
    'text_btn_register' => 'Daftar',
    'text_btn_next' => 'Selanjutnya',
    'text_btn_back' => 'Kembali',
    'text_btn_submit' => 'Kirim',

    'text_have_account' => 'Sudah memiliki akun ?',

    'res_email_not_valid' => 'Email Anda tidak valid, harap gunakan alamat email lain!',
    'res_email_not_available' => 'Email telah terdaftar!',
    'res_email_is_blocked' => 'Akun Anda di blokir, silahkan hubungi pihak admin yang terkait!',
    'res_success_register' => 'Terima kasih telah mendaftar. Anda akan diarahkan ke halaman ticket.',
    'res_email_error' => 'Ups! Ada yang tidak beres'
];
