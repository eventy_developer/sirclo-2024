<?php

return  [
    'title_page' => ':EVENT_NAME - Payment Confirmation',

    'placeholder_order_id' => 'Order ID',
    'placeholder_name' => 'Nama Akun',
    'placeholder_paid_amount' => 'Jumlah Dibayar',
    'placeholder_paid_date' => 'Tanggal Pembayaran',
    'placeholder_file_upload' => 'Bukti Pembayaran',
    'placeholder_file' => 'Pilih File',

    'validate_order_id' => 'Order ID wajib diisi',
    'validate_name' => 'Nama wajib diisi',
    'validate_paid_amount' => 'Jumlah Dibayar wajib diisi',
    'validate_paid_date' => 'Tanggal Pembayaran wajib diisi',
    'validate_file' => 'File wajib diupload',

    'text_info_upload_size' => '* Hanya mendukung : jpg, jpeg dengan maksimal ukuran 3MB',
    'text_btn_submit' => 'Kirim',
    'text_btn_confirmation' => 'Konfirmasi',

    'res_payment_is_submitted' => 'Data Anda berhasil dikirim, mohon selesaikan pembayaran.',
    'res_invoice_not_found' => 'Order ID tidak ditemukan, mohon masukkan dengan benar!',
    'res_invoice_expired' => 'Order ID telah kedaluwarsa, mohon masukkan dengan benar!',
    'res_payment_is_waiting' => 'Anda telah mengirim konfirmasi, mohon periksa email Anda!',
    'res_payment_is_approved' => 'Pembayaran telah berhasil, mohon cek email Anda!',
    'res_confirmation_is_submitted' => 'Terima kasih, kami akan mengirim Anda email konfirmasi'
];
