<?php

return [
    'copyright' => 'Copyright :INSTANCE. All Rights Reserved',

    'btn_next' => 'Selanjutnya',
    'btn_previous' => 'Sebelumnya',

    'notification_title' => 'Notifikasi',
    'notification_mark_read' => 'Tandai telah dibaca',

    'title_profile' => 'Profil',
    'title_edit_pass' => 'Ubah Kata Sandi',

    'profile_text_username' => 'Username',
    'profile_text_phone' => 'Nomor Handphone',

    'profile_text_account_sett' => 'Account Settings',
    'profile_text_edit_pass' => 'Ubah Kata Sandi',
    'profile_text_password' => 'Kata Sandi',

    'edit_pass_label_current_pass' => 'Kata sandi saat ini',
    'edit_pass_label_new_pass' => 'Kata sandi baru',
    'edit_pass_label_retype_pass' => 'Ulang kata sandi baru',

    'edit_pass_placeholder_current_pass' => 'Ketik kata sandi saat ini',
    'edit_pass_placeholder_new_pass' => 'Ketika kata sandi baru',
    'edit_pass_placeholder_retype_pass' => 'Ketik ulang kata sandi baru',

    'profile_btn_logout' => 'Keluar',
    'profile_btn_save_pass' => 'Simpan',

    'text_header_user' => 'You\'re On',
    'text_page_visitor' => 'Visitor',

    'floating_menu_booth_label' => 'Denah Stan',
    'floating_menu_booth_list_label' => 'Daftar Stan',
    'floating_menu_schedule_label' => 'Jadwal',
    'floating_menu_sponsor_label' => 'Sponsor',

    'sponsor_text_info' => 'Silakan klik pada salah satu logo untuk mengunjungi website sponsor',

    'sidebar_label_about' => 'About',
    'sidebar_label_product' => 'Product',
    'sidebar_label_document' => 'Document',
    'sidebar_label_video' => 'Video',
    'sidebar_label_attachment' => 'Attachment',
    'sidebar_label_polling' => 'Polling',
    'sidebar_label_quiz' => 'Quiz',
    'sidebar_label_image' => 'Image',

    'chat_compose_placeholder' => 'Ketik pesan',
    'chat_public_title' => 'Public Chat',
    'chat_conversation_title' => 'Conversation Chat',

    'question_title' => 'Pertanyaan',

    'polling_no_start_title' => 'Polling belum dimulai!',
    'polling_finish_title' => 'Terima kasih telah mengisi polling ini, semoga harimu menyenangkan',
    'polling_btn_vote' => 'Kirim!',

    'quiz_ready_title' => 'Siap untuk Mulai Kuis?',
    'quiz_ready_caption' => 'Klik tombol di bawah dan jawab semua pertanyaan yang kami miliki',
    'quiz_leaderboard_title' => 'Leaderboard',
    'quiz_btn_start' => 'Mulai Kuis!',
    'quiz_btn_result' => 'Hasil',

    'res_user_not_found' => 'Peserta tidak ditemukan',
    'res_profile_photo_success_updated' => 'Foto profil berhasil diubah',
    'res_password_success_updated' => 'Password berhasil diubah',
    'res_current_password_wrong' => 'Kata sandi tidak cocok',
];
