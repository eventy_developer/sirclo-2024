<?php

return [
    'select_category_label' => 'Kategori Booth',
    'placeholder_search_category' => 'Cari kategori booth',
    'placeholder_search_booth' => 'Cari booth',

    'btn_visit' => 'Kunjungi sekarang',

    'product_text_interested' => 'Tertarik dengan produk?',
    'product_text_ask_admin' => 'Tanyakan Admin',
    'product_text_contact_title' => 'Kontak admin booth via chat untuk info produk lebih detail',
];
