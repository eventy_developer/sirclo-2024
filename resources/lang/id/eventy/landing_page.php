<?php

return [
    'text_days' => 'Hari',
    'text_hours' => 'Jam',
    'text_minutes' => 'Menit',
    'text_seconds' => 'Detik',

    'text_have_fun' => 'Siap mengikuti acara ini?',

    'text_btn_join' => 'Bergabung sekarang',
    'text_btn_login' => 'Masuk',
    'text_btn_register' => 'Daftar',
];
