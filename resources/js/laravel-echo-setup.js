import Echo from 'laravel-echo'

// # Socket.io with laravel-echo-server
// window.io = require('socket.io-client');
// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.host + ':' + process.env.MIX_LARAVEL_ECHO_PORT,
// });

// # Pusher.js with self-hosting Laravel Websockets
window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    encrypted: true,
    wsHost: process.env.MIX_PUSHER_HOST,
    wsPort: process.env.MIX_PUSHER_PORT,
    wssPort: process.env.MIX_PUSHER_PORT,
    disableStats: true,
    forceTLS: true,
    enabledTransports: ['ws', 'wss'],
});
